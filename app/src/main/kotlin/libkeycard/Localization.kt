package libkeycard

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle


/** Converts an ISO 8601 timestamp to the format specified. */
fun convertISO8601(s: String, formatter: DateTimeFormatter? = null) = runCatching {
    LocalDateTime.ofInstant(Instant.parse(s), ZoneOffset.UTC)
        .format(formatter ?: DateTimeFormats.shortLocalDateTime)
}

object DateTimeFormats {
    /** "1/28/25, 12:24PM" */
    val shortLocalDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)!!

    /** "Jan 28, 2025, 12:24PM" */
    val medLocalDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)!!

    /** "Jan 28" */
    val monthDay = DateTimeFormatter.ofPattern("MMM dd")!!

    /** 1/10/23 */
    val monthDayYear = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)!!

    /** "2025-01-28" */
    val keycardDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC)!!
}
