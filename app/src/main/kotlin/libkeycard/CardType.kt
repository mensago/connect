package libkeycard

/**
 * Type class used to stand for the two different types of keycards
 */
enum class CardType {
    User,
    Organization;


    override fun toString(): String {
        return when (this) {
            User -> "User"
            Organization -> "Organization"
        }
    }

    companion object {
        fun fromString(s: String?): CardType? {
            return when (s?.lowercase()) {
                "user" -> User
                "organization" -> Organization
                else -> null
            }
        }
    }
}
