package libmensago

import keznacl.EmptyDataException
import keznacl.toFailure
import keznacl.toSuccess
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

var MAX_MSG_SIZE: Int = 65532

/**
 * DataFrame is a structure for the lowest layer of abstraction for sending network messages and is
 * designed to allow two endpoints to send messages of arbitrary size without having to deal with
 * buffer size details. It represents a single packaged segment of data. Above the DataFrame level
 * is the message, which is just an opaque array of bytes.
 *
 * The wire protocol expects a network buffer of 64K -- 3 bytes for the header followed by up to
 * 65532 bytes of payload. The header consists of a 1-byte frame type and 2 bytes in MSB format for
 * the payload size.
 *
 * Frame types are limited to single, multipart start, multipart data, and multipart final. The
 * single type indicates that the payload represents a completed, self-contained message.
 *
 * Multipart messages are those where the payload size is greater than 65532 bytes and are sent in
 * multiple frames. Sending a multipart message consists of a start frame, at least one data frame,
 * and a final frame.
 *
 * Multipart start frames contain an integer string representing the total size of the payload.
 * Although implementations will impose maximum message size limits, the wire protocol effectively
 * has none -- a multipart payload size can be an integer with up to 65532 digits in it! An endpoint
 * will see the start frame, read the total size, and convert it to an integer. One or more frames
 * will follow with the maximum payload size of 65532 bytes. The final multipart data frame has a
 * dedicated frame type so that the receiving endpoint can expect that the frame represents the
 * last segment of data in the message.
 */
class DataFrame {

    private var index = 0
    private val buffer = ByteArray(MAX_MSG_SIZE)

    /** The type of frame, which can be single, or one of the multipart types */
    var type: FrameType = FrameType.InvalidFrame

    /** The size of the payload carried by the frame object */
    val size: Int
        get() {
            return index
        }

    /** The data carried by the frame object */
    val payload: ByteArray
        get() {
            return buffer.sliceArray(IntRange(0, index - 1))
        }

    /**
     * Returns the total size of the payload transmitted by a multipart frame set.
     *
     * @exception TypeException Returned when called on any frame that doesn't have the
     * `FrameType.MultipartFrameStart` type
     */
    fun getMultipartSize(): Result<Int> {
        if (size < 1) return BadFrameException().toFailure()
        if (type != FrameType.MultipartFrameStart) return TypeException().toFailure()

        return runCatching { payload.decodeToString().toInt() }
    }

    /**
     * Reads a frame from the input stream.
     *
     * @exception ConnectionClosedException If the stream was closed
     * @exception BadFrameException If a bad frame header or invalid data is read
     */
    fun read(conn: InputStream): Throwable? {
        // Invalidate internal data in case we error out
        index = 0

        val header = ByteArray(3)
        var bytesRead = runCatching { conn.read(header) }.getOrElse { return it }
        if (bytesRead == -1) return ConnectionClosedException()

        if (bytesRead < 3) return BadFrameException("Header size of $bytesRead")

        type = FrameType.fromByte(header[0])
        if (type == FrameType.InvalidFrame) return BadFrameException("Bad frame type value ${header[0]}")

        // The size bytes are in network order (MSB), so this makes dealing with CPU architecture
        // much less of a headache regardless of what architecture this is compiled for.

        // Once again, however, Java's complete lack of unsigned types come back to cause trouble.
        // Again. Which leads to more hoops to jump through.
        val payloadMSB = header[1].toUByte().toInt().shl(8)
        val payloadLSB = header[2].toUByte().toInt()
        val payloadSize = payloadMSB.or(payloadLSB)

        // This loop is necessary because a condition that at least appears on Linux where the bytes sent by the
        // client don't always arrive immediately. This means that the call to read() doesn't always read the full
        // number of bytes sent, but a second call does.
        var totalRead = 0
        while (totalRead < payloadSize) {
            bytesRead = runCatching { conn.read(buffer, totalRead, payloadSize - totalRead) }.getOrElse { return it }

            // The bytesRead count will be less than zero if the stream has been closed.
            if (bytesRead < 0) return SizeException("Network stream closed during transmission")

            totalRead += bytesRead
        }

        if (bytesRead != payloadSize)
            return SizeException("bytes read = $bytesRead, expected payload size = $payloadSize")

        index = bytesRead

        return null
    }
}

/**
 * Writes a single frame to the output stream. The format for the frame is a 1-byte frame type, two
 * bytes in MSB order for the size of the frame's payload, and the byte payload itself. The maximum
 * size of a frame is 64K unless the max size constant is altered.
 *
 * @exception SizeException Returned if the payload is larger than the maximum allowed
 * @exception IOException Returned if there was an underlying I/O exception writing to the stream
 */
fun writeFrame(conn: OutputStream, frameType: FrameType, payload: ByteArray): Throwable? {
    if (payload.size > MAX_MSG_SIZE) return SizeException()

    val header = ByteArray(3)
    header[0] = frameType.toByte()
    header[1] = payload.size.shr(8).and(255).toByte()
    header[2] = payload.size.and(255).toByte()

    return runCatching {
        conn.write(header)
        conn.write(payload)
    }.exceptionOrNull()
}

/**
 * Reads a message from the input stream. A message consists of one or more data frames. This
 * implementation has a max size imposed by the JVM, but the messaging design effectively itself
 * has no limits - *the message payload size* can be up to 65535 digits long!
 *
 * @exception BadSessionException Returned if a multipart frame is received out of order
 * @exception BadFrameException Returned if an invalid frame type is read
 */
fun readMessage(conn: InputStream): Result<ByteArray> {
    val out = mutableListOf<Byte>()
    val frame = DataFrame()

    frame.read(conn)?.let { return it.toFailure() }

    when (frame.type) {
        FrameType.SingleFrame -> {
            out.addAll(frame.payload.toList())
            return out.toByteArray().toSuccess()
        }

        FrameType.MultipartFrameStart -> {}
        FrameType.MultipartFrameFinal -> return BadSessionException().toFailure()
        else -> return BadFrameException().toFailure()
    }

    // We got this far, so we have a multipart message which we need to reassemble.

    val totalSize = frame.getMultipartSize().getOrElse { return it.toFailure() }

    var sizeRead = 0
    while (sizeRead < totalSize) {
        frame.read(conn)?.let { return it.toFailure() }

        when (frame.type) {
            FrameType.SingleFrame, FrameType.MultipartFrameStart, FrameType.SessionSetupRequest,
            FrameType.SessionSetupResponse -> return BadSessionException().toFailure()

            FrameType.MultipartFrame, FrameType.MultipartFrameFinal -> {}
            FrameType.InvalidFrame -> return BadFrameException().toFailure()
        }
        out.addAll(frame.payload.toList())
        sizeRead += frame.size

        if (frame.type == FrameType.MultipartFrameFinal) break
    }

    return if (sizeRead == totalSize) out.toByteArray().toSuccess()
    else SizeException().toFailure()
}

/** Reads a message from the stream and returns it as a string instead of a byte array */
fun readStringMessage(conn: InputStream): Result<String> {
    return readMessage(conn).getOrElse { return it.toFailure() }
        .decodeToString().toSuccess()
}

/** Writes a message to the stream as a series of one or more data frames */
fun writeMessage(conn: OutputStream, msg: ByteArray): Throwable? {
    if (msg.isEmpty()) return EmptyDataException()

    // If the packet is small enough to fit into a single frame, just send it and be done.
    if (msg.size < MAX_MSG_SIZE) return writeFrame(conn, FrameType.SingleFrame, msg)

    // If the message is bigger than the max message length, then we will send it as a multipart
    // message. This takes more work internally, but the benefits at the application level are
    // worth it. By using a binary wire format, we don't have to deal with serialization, escaping
    // and all sorts of other complications.

    // The initial message indicates that it is the start of a multipart message and contains the
    // total size in the payload as a string. All messages that follow contain the actual message
    // data.

    writeFrame(conn, FrameType.MultipartFrameStart, msg.size.toString().toByteArray())
        ?.let { return it }
    var index = 0

    while (index + MAX_MSG_SIZE < msg.size) {
        writeFrame(
            conn, FrameType.MultipartFrame,
            msg.sliceArray(IntRange(index, index + MAX_MSG_SIZE - 1))
        )?.let { return it }
        runCatching { conn.flush() }.getOrElse { return it }
        index += MAX_MSG_SIZE
    }

    writeFrame(conn, FrameType.MultipartFrameFinal, msg.sliceArray(IntRange(index, msg.size - 1)))
    return null
}

/**
 * Debug method which returns a string that contains the first and last 16 bytes of the payload
 * in hex format and attempts to convert those bytes to a string, as well. If the payload is
 * less than 32 bytes in size, it returns the entire payload.
 */
@Suppress("unused")
fun getDataFrameDebugString(buffer: ByteArray): String {
    val sb = StringBuilder()
    when {
        buffer.isEmpty() -> return "<empty>"
        buffer.size == 1 -> sb.append("<${buffer.toHex()}>")
        buffer.size == 2 || buffer.size == 3 -> {
            with(sb) {
                append("<" + buffer.sliceArray(IntRange(0, 0)).toHex())
                append(" " + buffer.sliceArray(IntRange(1, buffer.size - 1)).toHex() + ">")
            }
        }

        buffer.size in 4..34 -> {
            with(sb) {
                append("<" + buffer.sliceArray(IntRange(0, 0)).toHex())
                append(" " + buffer.sliceArray(IntRange(1, 2)).toHex() + "> ")
                val payBuffer = buffer.sliceArray(IntRange(3, buffer.size - 1))
                append(payBuffer.toHex())
                append(" " + payBuffer.decodeToString())
            }
        }

        else -> {
            with(sb) {
                append("<" + buffer.sliceArray(IntRange(0, 0)).toHex())
                append(" " + buffer.sliceArray(IntRange(1, 2)).toHex() + "> ")
                val payBuffer = mutableListOf<Byte>()
                payBuffer.addAll(buffer.sliceArray(IntRange(3, 18)).toList())
                payBuffer.addAll(
                    buffer.sliceArray(IntRange(buffer.size - 16, buffer.size - 1)).toList()
                )
                append(payBuffer.toByteArray().toHex())
                append(" " + payBuffer.toByteArray().decodeToString())
            }
        }
    }

    return sb.toString()
}

/**
 * Debug method similar to getDataFrameDebugString() except that it doesn't try to interpret the
 * first three bytes as the header for a DataFrame
 */
@Suppress("unused")
fun getBufferDebugString(buffer: ByteArray): String {
    val sb = StringBuilder()
    when {
        buffer.isEmpty() -> return "<empty>"
        buffer.size < 32 -> sb.append(buffer.toHex() + " " + buffer.decodeToString())

        else -> {
            with(sb) {
                val payBuffer = mutableListOf<Byte>()
                payBuffer.addAll(buffer.sliceArray(IntRange(0, 15)).toList())
                payBuffer.addAll(
                    buffer.sliceArray(IntRange(buffer.size - 16, buffer.size - 1)).toList()
                )
                append(payBuffer.toByteArray().toHex())
                append(" " + payBuffer.toByteArray().decodeToString())
            }
        }
    }

    return sb.toString()
}

