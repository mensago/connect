package libmensago

import keznacl.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import libkeycard.MissingDataException
import libkeycard.Timestamp
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.security.GeneralSecurityException

/**
 * SealedEnvelopeHeader is an encrypted EnvelopeHeader. The receiver tag is decryptable only by the server
 * handling message delivery for the recipient. The sender tag is readable only by the server
 * carrying the message to the next hop on the behalf of the message author.
 *
 * This class is not intended to be instantiated directly, but rather either from readFromFile() or
 * EnvelopeHeader::seal().
 */
@Serializable
class SealedEnvelopeHeader(
    val receiver: CryptoString?, val sender: CryptoString?,
    val date: Timestamp, val payloadKey: CryptoString?,
    val keyHash: Hash, val version: APIVersion
) {
    constructor(
        keyHash: Hash, payloadKey: CryptoString?, date: Timestamp = Timestamp(),
        version: APIVersion = APIVersion(1, 0, 0)
    ) : this(null, null, date, payloadKey, keyHash, version)

    /**
     * Decrypts the encrypted recipient information in the message using the organization's
     * decryption key.
     *
     * @throws IllegalArgumentException Returned if there was a Base85 decoding error
     * @throws GeneralSecurityException Returned for decryption failures
     */
    fun decryptReceiver(keyPair: EncryptionPair): Result<RecipientLabel?> {
        return if (receiver != null) decryptAndDeserialize<RecipientLabel>(receiver, keyPair)
        else Result.success(null)
    }

    /**
     * Decrypts the encrypted recipient information in the message using the organization's
     * decryption key.
     *
     * @throws IllegalArgumentException Returned if there was a Base85 decoding error
     * @throws GeneralSecurityException Returned for decryption failures
     */
    fun decryptSender(keyPair: EncryptionPair): Result<SenderLabel?> {
        return if (sender != null) decryptAndDeserialize<SenderLabel>(sender, keyPair)
        else Result.success(null)
    }

    override fun toString(): String =
        "SealedEnvelopeHeader($receiver, $sender, $date, $payloadKey, $keyHash)"

    companion object {

        /**
         * Creates a new SealedEnvelopeHeader instance from a file.
         *
         * @throws IOException Returned if there was an I/O error
         * @throws BadValueException Returned if the file header could not be parsed
         * @throws kotlinx.serialization.SerializationException encoding-specific errors
         * @throws IllegalArgumentException if the encoded input does not comply format's specification
         */
        fun fromFile(file: File): Result<SealedEnvelopeHeader> {
            val reader = file.bufferedReader()
            val out = readEnvelopeHeader(file, reader).getOrElse { return it.toFailure() }
            reader.close()
            return out.toSuccess()
        }
    }
}

internal fun readEnvelopeHeader(file: File, reader: BufferedReader): Result<SealedEnvelopeHeader> {

    do {
        val line = runCatching {
            reader.readLine() ?: run {
                reader.close()
                return BadValueException("File $file missing start of header").toFailure()
            }
        }.getOrElse {
            reader.close()
            return it.toFailure()
        }.trim()

        if (line.trim() == "MENSAGO") {
            break
        }
    } while (true)

    val headerLines = mutableListOf<String>()
    do {
        val line = runCatching {
            reader.readLine() ?: run {
                reader.close()
                return MissingDataException("Premature end of header").toFailure()
            }
        }.getOrElse {
            reader.close()
            return it.toFailure()
        }.trim()

        if (line == "----------")
            break
        headerLines.add(line)
    } while (true)

    return runCatching {
        Json.decodeFromString<SealedEnvelopeHeader>(headerLines.joinToString())
    }
}
