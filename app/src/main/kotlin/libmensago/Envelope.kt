package libmensago

import keznacl.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.KeyMismatchException
import libkeycard.MissingDataException
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.resolver.KCResolver
import java.io.File
import java.io.IOException
import java.nio.file.Paths
import java.time.Instant

/**
 * Envelope is a representation of an encrypted message and its associated delivery tag. Its main
 * purpose is to provide a simple, high-level API to encrypt and decrypt messages and provide
 * easy serialization and deserialization. If you're just interested in sending a message, create
 * your message, grab the encryption key for the recipient, and call seal().
 */
class Envelope(var tag: SealedEnvelopeHeader, var payload: CryptoString) {

    /**
     * Returns the decrypted message contained in the envelope.
     *
     * @exception KeyMismatchException Returned if the hash of the public key does not match that of
     * the key hash in the delivery tag.
     */
    fun open(keyPair: EncryptionPair): Result<Payload> {

        val pubHash = hash(keyPair.pubKey.toByteArray(), tag.keyHash.getType()!!)
            .getOrElse { return it.toFailure() }
        if (pubHash != tag.keyHash) return KeyMismatchException().toFailure()

        val payloadKeyStr = keyPair.decrypt(tag.payloadKey!!)
            .getOrElse { return it.toFailure() }
            .decodeToString()
        val msgKey = SecretKey.fromString(payloadKeyStr).getOrElse { return it.toFailure() }
        return Payload.decrypt(msgKey, payload).getOrElse { return it.toFailure() }.toSuccess()
    }

    /**
     * Returns a string representing the Envelope's data in the official format. Unlike toString(),
     * this version will not throw exceptions, but otherwise functions like toString().
     */
    fun toStringResult(): Result<String> {
        return listOf(
            "MENSAGO",
            runCatching { Json.encodeToString(tag) }.getOrElse { return it.toFailure() },
            "----------",
            payload.prefix,
            payload.encodedData,
            "",
            ""
        ).joinToString("\r\n")
            .toSuccess()
    }

    override fun toString(): String {
        return toStringResult().getOrThrow()
    }

    /**
     * Similar to save() except that it only requires a path to a containing folder. The call
     * generates a name using the standard server-side format, saves the contents of the Envelope to
     * said file and returns the saved file's name.
     *
     * @exception NullPointerException Returned if given empty data
     * @exception SecurityException Returned if a security manager exists and gets in the way
     * @exception IOException Returned if an I/O error occurs
     * @return Name of the file saved
     */
    fun saveInDir(path: String): Result<String> {
        val data = toString()

        val destName = makeEnvelopeFilename(data.length)
        val file = Paths.get(path, destName).toFile()

        return runCatching {
            if (!file.exists()) file.createNewFile()
            file.bufferedWriter().apply {
                write(data)
                flush()
            }
            destName
        }
    }

    /**
     * Saves the Envelope's contents to a file using the official data format.
     *
     * @exception NullPointerException Returned if given empty data
     * @exception SecurityException Returned if a security manager exists and gets in the way
     * @exception IOException Returned if an I/O error occurs
     */
    fun saveWithName(path: String): Throwable? {
        return runCatching {
            val file = File(path)
            if (!file.exists()) file.createNewFile()
            val writer = file.bufferedWriter()
            writer.write(toString())
            writer.flush()
        }.exceptionOrNull()
    }

    companion object {

        /**
         * Creates a new Envelope object representing a ready-to-send encrypted message.
         *
         * @exception ResourceNotFoundException Returned if the management record doesn't exist
         * @exception BadValueException Returned for bad data in the management record
         * @exception MissingDataException Returned if a required key is missing from the management
         * record
         * @exception IOException Returned if there was a DNS lookup error
         * @exception UnsupportedAlgorithmException Returned if the library doesn't support the
         * encryption algorithm used by one of the servers
         */
        fun seal(keyEncryptionKey: Encryptor, payload: Payload): Result<Envelope> =
            seal(keyEncryptionKey, null, payload)

        /**
         * Creates a new Envelope object representing a ready-to-send encrypted message.
         *
         * @exception ResourceNotFoundException Returned if the management record doesn't exist
         * @exception BadValueException Returned for bad data in the management record
         * @exception MissingDataException Returned if a required key is missing from the management
         * record
         * @exception IOException Returned if there was a DNS lookup error
         * @exception UnsupportedAlgorithmException Returned if the library doesn't support the
         * encryption algorithm used by one of the servers
         */
        fun seal(
            keyEncryptionKey: Encryptor, from: WAddress, to: WAddress, payload: Payload
        ): Result<Envelope> = seal(keyEncryptionKey, AddressPair(from, to), payload)

        /**
         * Creates a new Envelope object representing a ready-to-send encrypted message.
         *
         * @exception ResourceNotFoundException Returned if the management record doesn't exist
         * @exception BadValueException Returned for bad data in the management record
         * @exception MissingDataException Returned if a required key is missing from the management
         * record
         * @exception IOException Returned if there was a DNS lookup error
         * @exception UnsupportedAlgorithmException Returned if the library doesn't support the
         * encryption algorithm used by one of the servers
         */
        fun seal(keyEncryptionKey: Encryptor, addPair: AddressPair?, payload: Payload):
                Result<Envelope> {
            // To do this, we need to create a EnvelopeHeader and then seal() it, and then encrypt
            // the message itself with the recipient's key. Pretty simple, actually.

            if (addPair == null) {
                val tag = EnvelopeHeader.new(null).getOrElse { return it.toFailure() }
                val sealedTag = tag.seal(keyEncryptionKey, null, null)
                    .getOrElse { return it.toFailure() }
                val encrypted = payload.encrypt(tag.payloadKey).getOrElse { return it.toFailure() }
                return Envelope(sealedTag, encrypted).toSuccess()
            }

            val senderKey = KCResolver.getMgmtRecord(addPair.from.domain)
                .getOrElse { return it.toFailure() }
                .ek.toEncryptionKey().getOrElse { return it.toFailure() }
            val receiverKey = KCResolver.getMgmtRecord(addPair.to.domain)
                .getOrElse { return it.toFailure() }
                .ek.toEncryptionKey().getOrElse { return it.toFailure() }

            val tag = EnvelopeHeader.new(addPair).getOrElse { return it.toFailure() }
            val sealedTag = tag.seal(keyEncryptionKey, senderKey, receiverKey)
                .getOrElse { return it.toFailure() }

            val encrypted = payload.encrypt(tag.payloadKey).getOrElse { return it.toFailure() }
            return Envelope(sealedTag, encrypted).toSuccess()
        }

        /**
         * Reads the specified file and returns an Envelope object
         */
        fun fromFile(file: File): Result<Envelope> {

            val reader = file.bufferedReader()
            val header = readEnvelopeHeader(file, reader).getOrElse { return it.toFailure() }

            val prefix = runCatching {
                reader.readLine() ?: run {
                    reader.close()
                    return BadValueException("File $file missing payload prefix").toFailure()
                }
            }.getOrElse {
                reader.close()
                return it.toFailure()
            }.trim()

            val payload = runCatching {
                reader.readLine() ?: run {
                    reader.close()
                    return BadValueException("File $file missing payload").toFailure()
                }
            }.getOrElse {
                reader.close()
                return it.toFailure()
            }.trim()

            reader.close()
            return Envelope(
                header,
                CryptoString.fromString("$prefix:$payload")
                    ?: return BadValueException("Bad payload format in $file").toFailure()
            ).toSuccess()
        }
    }
}

/** Returns a string suitable for the name of a sealed Envelope file */
fun makeEnvelopeFilename(
    fileSize: Int, unixtime: Long = Instant.now().epochSecond, id: RandomID = RandomID.generate()
): String = "$unixtime.$fileSize.$id"
