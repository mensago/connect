package libmensago

import keznacl.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.RandomID
import libkeycard.Timestamp
import libkeycard.WAddress

/**
 * The DeviceRequest class represents a device approval request message. These are sent by a Mensago
 * server to a user when a new device logs in after the first one for the workspace has been
 * registered. Instead of responding with a reply message, the client sends the appropriate command
 * to the server because the new device can't decrypt messages without the keys supplied by another
 * device.
 */
@Serializable
class DeviceRequest(
    var from: WAddress, var to: WAddress, var addr: String, val devInfo: CryptoString,
    val keyHash: Hash,
) : MessageInterface {

    var date: Timestamp = Timestamp()
    var subject = "New Device Login"
    var body = ""

    var version = 1.0f
    var type = PayloadType.DeviceRequest
    val subType = "devrequest"
    var id = RandomID.generate()
    var threadID = RandomID.generate()

    override fun toPayload(): Result<Payload> {
        val rawJSON = runCatching { Json.encodeToString(this) }
            .getOrElse { return it.toFailure() }
        return Payload(PayloadType.DeviceRequest, rawJSON).toSuccess()
    }

    init {
        if (addr.startsWith("/"))
            addr = addr.substring(1)
        body = "Timestamp=$date\r\nIP Address=$addr\r\n"
    }

    fun getBodyAttributes(): Map<String, String> =
        body.split("\n")
            .map { it.trim() }
            .associate {
                val parts = it.split("=", limit = 2)
                if (parts.size == 2)
                    Pair(parts[0], parts[1])
                else
                    Pair("", "")
            }
    

    override fun sender(): WAddress {
        return from
    }

    override fun recipient(): WAddress {
        return to
    }

    override fun updateBody(str: String) {
        body = str
    }

    override fun accessBody(): String {
        return body
    }

    override fun updateSubType(str: String?) {}

    @Suppress("RedundantNullableReturnType")
    override fun accessSubType(): String? {
        return subType
    }

    companion object {
        fun fromPayload(p: Payload): Result<DeviceRequest> {
            if (p.type != PayloadType.DeviceRequest)
                return BadValueException("Wrong payload type").toFailure()

            return kotlin.runCatching { Json.decodeFromString<DeviceRequest>(p.jsondata) }
        }
    }
}