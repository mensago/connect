package libmensago.db

class DatabaseException(message: String = "") : Exception(message)
class DatabaseCorruptionException(message: String = "") : Exception(message)
