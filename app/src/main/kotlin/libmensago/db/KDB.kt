package libmensago.db

import keznacl.*
import libmensago.NotConnectedException
import java.sql.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * The KDB class is a Kotlin-oriented quality-of-life class which provides a functional workflow
 * wrapping around the Java database API.
 */
open class KDB(var config: KDBConfig? = null) {
    private var conn: Connection? = null
    private var batch: Statement? = null

    var logConnections: Boolean = false

    /**
     * Connects to the server with the information specified in initialize()
     *
     * @exception EmptyDataException if called without the object being configured
     * @exception java.sql.SQLException if there are problems connecting to the database
     */
    fun connect(cfg: KDBConfig? = null): Result<KDB> {
        // No sense in creating a new connection if we're already connected
        if (conn != null) return this.toSuccess()

        if (config == null) {
            if (cfg == null) return EmptyDataException().toFailure()
            config = cfg
        }

        conn = kotlin.runCatching {
            val props = config!!.getProperties()
            if (props != null)
                DriverManager.getConnection(config!!.getURL(), props)
            else
                DriverManager.getConnection(config!!.getURL())
        }.getOrElse { return it.toFailure() }

        logConnections.onTrue { println("KDB connected: ${dbCount.addAndGet(1)}") }
        return this.toSuccess()
    }

    /**
     * Disconnects from the database
     *
     * @exception java.sql.SQLException if there are problems disconnecting from the database
     */
    fun disconnect(): Throwable? {
        return try {
            if (conn != null) {
                logConnections.onTrue { println("KDB disconnected: ${dbCount.addAndGet(-1)}") }
                conn!!.close()
            }
            conn = null
            null
        } catch (e: Exception) {
            e
        }
    }

    fun isConnected(): Boolean {
        return conn != null
    }

    fun getConnection(): Connection? {
        return conn
    }

    /**
     * Executes a query to the database
     *
     * @param q The query to execute
     * @param args Variable list of arguments to safely substitute into the command. This call only
     * supports primitive types, String, and ByteArray. Any types not explicitly supported, such
     * as objects, will be converted to strings
     *
     * @exception EmptyDataException if your query is empty
     * @exception NotConnectedException if not connected to the database
     * @exception BadValueException if the query placeholder count doesn't match the query argument
     * count
     * @exception java.sql.SQLException for database problems, most likely either with your query or
     * with the connection
     */
    fun query(q: String, vararg args: Any): Result<ResultSet> {
        if (q.isEmpty()) return EmptyDataException().toFailure()
        if (!isConnected()) return NotConnectedException().toFailure()

        val stmt = prepStatement(q, args).getOrElse { return it.toFailure() }
        return runCatching { stmt.executeQuery().toSuccess() }.getOrElse { it.toFailure() }
    }

    /**
     * Executes a single command on the database
     *
     * @param s The command to execute
     * @param args Variable list of arguments to safely substitute into the command. This call only
     * supports primitive types, String, and ByteArray. Any types not explicitly supported, such
     * as objects, will be converted to strings
     *
     * @exception EmptyDataException if your command is empty
     * @exception NotConnectedException if not connected to the database
     * @exception BadValueException if the query placeholder count doesn't match the query argument
     * count
     * @exception java.sql.SQLException for database problems, most likely either with your query or
     * with the connection
     */
    fun execute(s: String, vararg args: Any): Result<Int?> {
        if (s.isEmpty()) return EmptyDataException().toFailure()
        if (!isConnected()) return NotConnectedException().toFailure()

        val stmt = prepStatement(s, args).getOrElse { return it.toFailure() }
        return runCatching {
            stmt.execute()
            val count = stmt.updateCount
            return Result.success(if (count < 0) null else count)
        }.getOrElse {
            it.toFailure()
        }
    }

    /**
     * Adds a command to the internal list of handlers to be executed in batch
     *
     * @exception EmptyDataException if your query is empty
     * @exception NotConnectedException if not connected to the database
     * @exception BadValueException if the query placeholder count doesn't match the query argument
     * count
     * @exception java.sql.SQLException for database problems, most likely either with your query or
     * with the connection
     */
    fun add(s: String): Throwable? {
        if (s.isEmpty()) return EmptyDataException()
        if (!isConnected()) return NotConnectedException()

        return runCatching {
            if (batch == null)
                batch = conn!!.createStatement()
            batch!!.addBatch(s)
            null
        }.getOrElse {
            it
        }
    }

    /**
     * Adds a command to the internal list of statements to be executed in batch. Unlike the
     * regular add() command, this one throws any exceptions it encounters. It is intendend for
     * situations like setup code, where throwing exceptions is acceptable.
     *
     * @exception EmptyDataException if your query is empty
     * @exception NotConnectedException if not connected to the database
     * @exception BadValueException if the query placeholder count doesn't match the query argument
     * count
     * @exception java.sql.SQLException for database problems, most likely either with your query or
     * with the connection
     */
    fun addThrows(s: String): KDB {
        if (s.isEmpty()) throw EmptyDataException()
        if (!isConnected()) throw NotConnectedException()

        if (batch == null)
            batch = conn!!.createStatement()
        batch!!.addBatch(s)
        return this
    }

    /**
     * Runs all handlers in the internal list created with add()
     *
     * @exception EmptyDataException if your query is empty
     * @exception NotConnectedException if not connected to the database
     * @exception BadValueException if the query placeholder count doesn't match the query argument
     * count
     * @exception java.sql.SQLException for database problems, most likely either with your query or
     * with the connection
     */
    fun executeBatch(): Throwable? {
        if (!isConnected()) return NotConnectedException()
        if (batch == null) return EmptyDataException()
        return runCatching {
            batch!!.executeBatch()
            batch = null
            null
        }.getOrElse {
            it
        }
    }

    /**
     * Checks to see if at least one row is returned for a query. It expects usage of SELECT because
     * a database may not necessarily support COUNT().
     *
     * @exception EmptyDataException if your query is empty
     * @exception NotConnectedException if not connected to the database
     * @exception BadValueException if the query placeholder count doesn't match the query argument count
     * @exception java.sql.SQLException for database problems, most likely either with your query or
     * with the connection
     */
    fun exists(q: String, vararg args: Any): Result<Boolean> {
        if (q.isEmpty()) return EmptyDataException().toFailure()
        if (!isConnected()) return NotConnectedException().toFailure()

        return runCatching {
            val stmt = prepStatement(q, args).getOrThrow()
            val rs = stmt.executeQuery()
            rs.next().toSuccess()
        }.getOrElse {
            it.toFailure()
        }
    }

    /**
     * Convenience method which creates a prepared statement. It expects data of one of three
     * types: ByteArray, Boolean, or something convertible to a string.
     *
     * @exception BadValueException if the query placeholder count doesn't match the query argument
     * count
     * @exception java.sql.SQLException for database problems, most likely either with your query or
     * with the connection
     */
    private fun prepStatement(s: String, args: Array<out Any>): Result<PreparedStatement> {

        // Make sure the ? count in s matches the number of args
        val qCount = s.split("?").size - 1
        if (qCount != args.size)
            return BadValueException(
                "Parameter count $qCount does not match number of placeholders"
            ).toFailure()

        // This is an internal call for query() and execute(). The null check is done there.
        return try {
            val out = conn!!.prepareStatement(s)
            for (i in 0 until qCount) {
                when (args[i]::class.simpleName) {
                    "Boolean" -> out.setBoolean(i + 1, args[i] as Boolean)
                    "ByteArray" -> out.setBytes(i + 1, args[i] as ByteArray)
                    "Byte" -> out.setByte(i + 1, args[i] as Byte)
                    "Double" -> out.setDouble(i + 1, args[i] as Double)
                    "Float" -> out.setFloat(i + 1, args[i] as Float)
                    "Int" -> out.setInt(i + 1, args[i] as Int)
                    "Long" -> out.setLong(i + 1, args[i] as Long)
                    "Short" -> out.setShort(i + 1, args[i] as Short)

                    "NullBinary" -> out.setNull(i + 1, Types.BINARY)
                    "NullBoolean" -> out.setNull(i + 1, Types.BOOLEAN)
                    "NullByte" -> out.setNull(i + 1, Types.TINYINT)
                    "NullDouble" -> out.setNull(i + 1, Types.DOUBLE)
                    "NullFloat" -> out.setNull(i + 1, Types.FLOAT)
                    "NullInt" -> out.setNull(i + 1, Types.INTEGER)
                    "NullLong" -> out.setNull(i + 1, Types.BIGINT)
                    "NullVarBinary" -> out.setNull(i + 1, Types.VARBINARY)

                    else -> out.setString(i + 1, args[i].toString())
                }
            }
            out.toSuccess()
        } catch (e: Exception) {
            e.toFailure()
        }
    }

    class NullBinary
    class NullBoolean
    class NullByte
    class NullDouble
    class NullFloat
    class NullInt
    class NullLong
    class NullVarBinary

    companion object {
        private val dbCount = AtomicInteger(0)
    }
}