package libmensago.commands

import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.RandomID
import libmensago.*

fun getDeviceStatus(conn: MConn, wid: RandomID, devid: RandomID): Result<DeviceStatus> {
    if (!conn.isConnected()) return NotConnectedException().toFailure()

    val req = ClientRequest(
        "GETDEVICESTATUS", mutableMapOf(
            "Workspace-ID" to wid.toString(),
            "Device-ID" to devid.toString(),
        )
    )
    conn.send(req)?.let { return it.toFailure() }

    val resp = conn.receive().getOrElse { return it.toFailure() }
    if (resp.code != 200) return Result.failure(ProtocolException(resp.toStatus()))

    if (!resp.checkFields(listOf(Pair("Device-Status", true))))
        return SchemaFailureException().toFailure()
    val status = DeviceStatus.fromString(resp.data["Device-Status"]!!)
        ?: return ServerException(
            "Received bad device status ${resp.data["Device-Status"]} from server"
        ).toFailure()
    return status.toSuccess()
}
