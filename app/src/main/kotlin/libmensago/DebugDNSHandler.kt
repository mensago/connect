package libmensago

import keznacl.onFalse
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.Domain
import libmensago.db.KDB
import java.net.InetAddress

/**
 * This class is a DNS filter which handles lookups for example.com, but otherwise performs DNS
 * passthrough. It is intended to be used for debugging and should be placed behind some kind of
 * debug runtime flag in production releases.
 *
 * DebugDNSHandler requires a KDB object pointing to a database so that it can look up encryption
 * and signature verification keys.
 */
class DebugDNSHandler(val db: KDB) : DNSHandler() {

    override fun lookupA(d: String): Result<List<InetAddress>> {
        val low = d.lowercase()
        if (low == "example.com" || low.endsWith(".example.com"))
            return listOf(InetAddress.getByName("127.0.0.1")).toSuccess()
        return super.lookupA(d)
    }

    override fun lookupAAAA(d: String): Result<List<InetAddress>> {
        val low = d.lowercase()
        if (low == "example.com" || low.endsWith(".example.com"))
            return listOf(InetAddress.getByName("::1")).toSuccess()
        return super.lookupAAAA(d)
    }

    override fun lookupSRV(d: String): Result<List<ServiceConfig>> {
        // We only return service records for the base example.com domain because (a) it's simpler
        // and (b) it's still valid to have one set of Mensago server records for the entire domain.
        val low = d.lowercase()
        if (low == "example.com" || low.endsWith(".example.com")) {
            return listOf(
                ServiceConfig(Domain.fromString("mensago1.example.com")!!, 2001, 0),
                ServiceConfig(Domain.fromString("mensago2.example.com")!!, 2001, 1),
            ).toSuccess()
        }
        return super.lookupSRV(d)
    }

    override fun lookupTXT(d: String): Result<List<String>> {
        val low = d.lowercase()
        if (low != "example.com" && !low.endsWith(".example.com"))
            return super.lookupTXT(d)

        var rs = db.query("SELECT pubkey FROM orgkeys WHERE purpose = 'encrypt'")
            .getOrElse { return it.toFailure() }
        rs.next().onFalse {
            return ResourceNotFoundException("Couldn't find org encryption key in database")
                .toFailure()
        }
        val ek = rs.getString("pubkey")

        rs = db.query("SELECT pubkey FROM orgkeys WHERE purpose = 'sign'")
            .getOrElse { return it.toFailure() }
        rs.next().onFalse {
            return ResourceNotFoundException("Couldn't find org primary key in database")
                .toFailure()
        }
        val pvk = rs.getString("pubkey")

        rs = db.query("SELECT pubkey FROM orgkeys WHERE purpose = 'altsign'")
            .getOrElse { return it.toFailure() }
        val out = if (rs.next())
            listOf("ek=$ek", "pvk=$pvk", "avk=${rs.getString(1)}").toSuccess()
        else
            listOf("ek=$ek", "pvk=$pvk").toSuccess()

        return out
    }
}
