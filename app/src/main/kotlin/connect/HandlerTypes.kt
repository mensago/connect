package connect

typealias ChangeHandler<T> = (T, T) -> Unit