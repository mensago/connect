package connect.profiles

import keznacl.BadValueException
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.*
import libmensago.WorkspaceStatus
import utilities.AppSettings
import java.nio.file.Path
import java.nio.file.Paths

/**
 * The Profile class is a data class which represents the current profile and provides an
 * interface to workspaces in the profile.
 */
class Profile(var name: String = "") {
    var uid: UserID? = null
    var wid: RandomID = LOCAL_WID
    var domain: Domain = LOCAL_DOMAIN
    var devid: RandomID = RandomID.generate()
    var settings: AppSettings? = null
    var status: WorkspaceStatus = WorkspaceStatus.Local

    /** Returns the profile's identity address. This will be in the form of an alphanumeric user
     * address if the workspace has one or a workspace address if it doesn't.
     *
     * This call is deprecated. Please use getMAddress()
     */
    fun getIdentity(): Result<MAddress> {
        return if (uid != null)
            MAddress.fromParts(uid!!, domain).toSuccess()
        else
            MAddress.fromParts(UserID.fromWID(wid), domain).toSuccess()
    }

    fun getProfileName(): String {
        return name
    }

    /** Returns the profile's identity address as a Mensago address */
    fun getMAddress(): Result<MAddress> = getIdentity()

    /** Returns the profile's identity address as a workspace address */
    fun getWAddress(): Result<WAddress> {
        return WAddress.fromParts(wid, domain).toSuccess()
    }

    /** Returns true if the profile is local-only, i.e. not associated with a Mensago server. */
    fun isLocal(): Result<Boolean> {
        val localWID = wid.toString() == LOCAL_WID.toString()
        return if (
            localWID && status != WorkspaceStatus.Local ||
            !localWID && status == WorkspaceStatus.Local
        ) BadValueException("BUG: WID/ workspace status locality mismatch").toFailure()
        else localWID.toSuccess()
    }

    /** Returns the path of the temp folder and also ensures that the folder exists. */
    fun ensureTempFolder(): Result<Path> {
        val file = getTempFolder().toFile()
        if (!file.exists())
            runCatching { file.mkdirs() }.exceptionOrNull()?.let { return it.toFailure() }
        return file.toPath().toSuccess()
    }

    /** Returns the path of the temp folder for the profile. */
    fun getTempFolder(): Path {
        return Paths.get(ProfileManager.profilesPath, name, "temp")
    }
}
