package connect.profiles

import connect.LogDAO
import connect.contacts.ContactDAO
import connect.messages.MessageDAO
import connect.notes.NoteDAO
import connect.sync.SyncDAO
import keznacl.*
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.*
import libmensago.db.DatabaseException
import utilities.*
import java.io.File
import java.nio.file.Path

/** The ProfileDAO class provides Profile-related database access functions */
class ProfileDAO {

    /** Returns true if the application is starting up for the first time on the current profile. */
    fun isFirstStartup(): Result<Boolean> {
        val rs = DBProxy.get()
            .query("SELECT itemvalue FROM appconfig WHERE name='org.mensago.connect.firsttime'")
            .getOrElse { return it.toFailure() }
        if (!rs.next()) return true.toSuccess()
        val status = (rs.getString("itemvalue") ?: "yes") != "no"
        return status.toSuccess()
    }

    /** Clears the first-time startup flag in the database */
    fun clearFirstStartup(): Throwable? = DBProxy.get().execute(
        "MERGE INTO appconfig(name,itemvalue,sync) KEY(name) " +
                "VALUES('org.mensago.connect.firsttime','no',false)"
    )

    /** Loads state and identity information for the current profile */
    fun load(name: String): Result<Profile> {

        val rs = DBProxy.get().query(
            "SELECT * FROM workspaces WHERE type='identity' " +
                    "AND status!='archived'"
        ).getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }

        val profileSettings = loadSettings().getOrElse { return it.toFailure() }

        val out = Profile(name).apply {
            wid = rs.getRandomID("wid")
                ?: return DBDataException("Bad workspace ID loaded in Profile::load()").toFailure()
            uid = rs.getUserID("userid")
            domain = rs.getDomain("domain")
                ?: return DBDataException("Bad domain loaded in Profile::load()").toFailure()
            devid = rs.getRandomID("devid")
                ?: return DBDataException("Bad device ID loaded in Profile::load()").toFailure()
            settings = profileSettings
            status = WorkspaceStatus.fromString(rs.getString("status"))
                ?: return DatabaseException(
                    "Bad status '${rs.getString("status")}' loaded for workspace $wid"
                ).toFailure()
        }

        return out.toSuccess()
    }

    /**
     * Loads device information for a workspace. Note that system attributes and approval code are
     * new values when loaded.
     */
    fun loadDeviceInfo(waddr: WAddress): Result<DeviceInfo> {
        val rs = DBProxy.get().query(
            "SELECT devid,public_key,private_key FROM workspaces WHERE wid=? AND domain=?",
            waddr.id, waddr.domain
        ).getOrElse { return it.toFailure() }

        rs.next().onFalse { return ResourceNotFoundException().toFailure() }
        val devid = rs.getRandomID("devid")
            ?: return DBDataException("Bad device id in DeviceInfo::loadFromDB()")
                .toFailure()

        val pubKeyStr = rs.getString("public_key")
        val privKeyStr = rs.getString("private_key")
        val pubKeyCS = CryptoString.fromString(pubKeyStr)
            ?: return BadValueException("Bad public key: $pubKeyStr").toFailure()
        val privKeyCS = CryptoString.fromString(privKeyStr)
            ?: return BadValueException("Bad private key: $privKeyStr").toFailure()
        val keypair = EncryptionPair.from(pubKeyCS, privKeyCS).getOrElse { return it.toFailure() }

        val info = collectInfoForDevice().getOrElse { return it.toFailure() }

        return DeviceInfo(devid, keypair.pubKey, keypair.privKey, info).toSuccess()
    }

    /** Loads the instance state from the database, overwriting any changes */
    fun loadSettings(): Result<AppSettings> {
        val out = AppSettings("")

        val rs = DBProxy.get().query("SELECT name,itemvalue,sync FROM appconfig")
            .getOrElse { return it.toFailure() }

        while (rs.next()) {
            val name = rs.getString("name")
            if (name.isEmpty())
                return BadValueException(
                    "AppSettings::loadFromDB: empty field name in database"
                ).toFailure()

            out.setField(name, rs.getString("itemvalue"), rs.getInt("sync") == 1)
        }
        out.signature = out.getField("application_signature")?.first ?: ""
        return out.toSuccess()
    }

    /**
     * Saves long-term information for a DeviceInfo structure -- workspace address, device ID,
     * and the associated device keypair. System attributes and approval code are not saved.
     */
    fun saveDeviceInfo(waddr: WAddress, info: DeviceInfo): Throwable? {
        return DBProxy.get().execute(
            "MERGE INTO workspaces (wid,domain,devid,public_key,private_key) KEY(wid,domain) " +
                    "VALUES(?,?,?,?,?)",
            waddr.id, waddr.domain, info.id, info.pubkey, info.privkey!!
        )
    }

    /** Saves all changes in the AppSettings instance to the database */
    fun saveSettings(s: AppSettings): Throwable? {
        val db = DBProxy.get()
        for (key in s.modified) {
            val field = s.getField(key)!!
            db.execute(
                "MERGE INTO appconfig (name,itemvalue,sync) KEY(name) VALUES(?1,?2,?3);",
                key, field.first, if (field.second) 1 else 0
            )?.let { return it }
        }
        s.modified.clear()

        for (key in s.deleted) {
            db.execute("DELETE FROM keys WHERE name=?", key)?.let { return it }
        }
        s.deleted.clear()

        return null
    }

    /** Resets the specified database to empty and adds local-only workspace information */
    fun resetDB(dbPath: Path): Throwable? {
        val dbFile = File(dbPath.toString())
        dbFile.exists().onTrue {
            runCatching { dbFile.delete() }.onFailure { return it }
        }

        val newDB = DBProxy.get().connect("jdbc:h2:$dbPath").getOrElse { return it }
        initTables(newDB)?.let { return it }

        // Create the local-only workspace info
        val devpair = EncryptionPair.generate().getOrElse { return it }
        val w = Workspace(
            LOCAL_WID, LOCAL_DOMAIN, null, RandomID.generate(), devpair, WorkspaceStatus.Local
        )
        return WorkspaceDAO().save(w, null)
    }

    private fun initTables(db: DBProxy): Throwable? {
        val initCommands = listOf(
            // For the AppSettings API
            """CREATE TABLE IF NOT EXISTS appconfig(
                name VARCHAR(128) NOT NULL UNIQUE,
                itemvalue VARCHAR(256) NOT NULL,
                sync BOOL NOT NULL
            );""",
            // If these tables are being initialized, then this profile is brand-new and the
            // first-time startup window needs to be displayed.
            "INSERT INTO appconfig(name,itemvalue,sync) " +
                    "VALUES('org.mensago.connect.firsttime','yes',false)",

            // Attachments aren't really owned by anything right now. This will probably go into
            // an AttachmentDAO class at some future time.
            """CREATE TABLE IF NOT EXISTS attachments (
                rowid INT PRIMARY KEY AUTO_INCREMENT,
                id	    TEXT NOT NULL UNIQUE,
                ownid	    TEXT NOT NULL,
                name  	TEXT NOT NULL,
                mimetype	TEXT NOT NULL,
                data      BLOB
            );""",

            // Lookup for files, their types, and their server-side locations. Needed mostly for
            // efficiently processing Delete update records
            """CREATE TABLE IF NOT EXISTS iteminfo(
                rowid INT PRIMARY KEY AUTO_INCREMENT,
                waddress TEXT NOT NULL,
                itemtype TEXT NOT NULL,
                serverpath TEXT NOT NULL,
                dbid UUID NOT NULL
                );""",

            // This stores the keycards locally and enables caching. It's not actually the
            // responsibility of the ProfileDAO to handle this and eventually this call will be
            // moved elsewhere.
            """CREATE TABLE IF NOT EXISTS keycards(
                rowid INT PRIMARY KEY AUTO_INCREMENT,
                owner TEXT NOT NULL,
                entryindex TEXT NOT NULL,
                type TEXT NOT NULL,
                entry BLOB NOT NULL,
                textentry TEXT NOT NULL,
                hash TEXT NOT NULL,
                expires TEXT NOT NULL,
                timestamp TEXT NOT NULL,
                ttlexpires TEXT
            );""",
        )
        for (cmd in initCommands)
            db.add(cmd)?.let { return it }
        db.executeBatch()?.let { return it }

        LogDAO().initLogTable()?.let { return it }
        MessageDAO().initMessageTable()?.let { return it }
        ContactDAO().initContactTables()?.let { return it }
        NoteDAO().initNoteTable()?.let { return it }

        KeyDAO().initKeyTables()?.let { return it }
        SyncDAO().initUpdateTables()?.let { return it }
        WorkspaceDAO().initWorkspaceTables()?.let { return it }

        return null
    }

    /** Adds or updates a ServerItemInfo structure to the database */
    fun addItemInfo(item: ServerItemInfo): Throwable? {
        return DBProxy.get().execute(
            "MERGE INTO iteminfo(waddress,itemtype,serverpath,dbid) KEY(dbid) VALUES(?,?,?,?)",
            item.waddr, item.itemtype, item.serverPath, item.dbid,
        )
    }

    /** Deletes a ServerItemInfo instance from the database based on its path */
    fun removeItemInfo(path: MServerPath): Throwable? {
        return DBProxy.get().execute("DELETE FROM iteminfo WHERE serverpath=?", path)
    }

    fun getItemInfo(path: MServerPath): Result<ServerItemInfo> {
        val rs = DBProxy.get().query("SELECT FROM iteminfo WHERE serverpath=?", path)
            .getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }
        val waddress = WAddress.fromString(rs.getString("waddress"))
            ?: return DatabaseException(
                "Invalid workspace address ${rs.getString("waddress")} in getItemInfo"
            ).toFailure()
        val serverpath = MServerPath.fromString(rs.getString("serverpath"))
            ?: return DatabaseException(
                "Invalid server path ${rs.getString("serverpath")} in getItemInfo"
            ).toFailure()
        val dbid = RandomID.fromString(rs.getString("dbid"))
            ?: return DatabaseException(
                "Invalid id ${rs.getString("dbid")} in getItemInfo"
            ).toFailure()

        return ServerItemInfo(waddress, rs.getString("itemtype"), serverpath, dbid)
            .toSuccess()
    }
}

/**
 * The ServerItemInfo class is used for tracking the types and locations of files as they are stored
 * on the server. It's mainly needed to efficiently remove items from the appropriate location in
 * the database when the corresponding file is deleted from the server side.
 */
data class ServerItemInfo(
    val waddr: WAddress, val itemtype: String, val serverPath: MServerPath, val dbid: RandomID
)
