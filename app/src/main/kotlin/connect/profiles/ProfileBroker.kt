package connect.profiles

import connect.UserInfoDAO
import keznacl.*
import libkeycard.WAddress
import libmensago.DeviceInfo
import libmensago.ResourceNotFoundException
import libmensago.WorkspaceStatus

/**
 * A class that handles profile-level data management, such as workspace interaction,
 * key management, settings, and other similar tasks. It does not handle user-visible data like
 * messages and contacts.
 */

class ProfileBroker(private val model: ProfilesModel) {

    /**
     * This method performs the local side of making the active profile local, archiving data which is
     * tied to a cloud account (messages, etc.) and setting the internal identity to the Mensago
     * equivalent of localhost: 00000000-0000-0000-0000-000000000001/localhost.local. This call
     * should be made after all other cloud-related tasks are performed because all keys for the
     * identity account are archived and no longer active.
     */
    fun makeActiveProfileLocal(): Throwable? {
        val active = getActiveProfile().getOrElse { return it }

        active.isLocal().getOrElse { return it }.onTrue { return null }

        WorkspaceDAO().archive(active.getWAddress().getOrElse { return it })
        model.activeProfile!!.wid = LOCAL_WID
        model.activeProfile!!.domain = LOCAL_DOMAIN

        return null
    }

    /**
     * Assigns an identity workspace to the active profile. The profile can have multiple workspace
     * memberships, but only one can be used for the identity of the user. This call sets that
     * address.
     */
    fun setIdentity(workspace: Workspace, pwHash: Password): Throwable? {
        if (model.activeProfile == null) return EmptyDataException("No profile is active")
        model.activeProfile!!.isLocal().getOrElse { return it }
            .onFalse { return ResourceNotFoundException("Profile already has an existing identity") }

        WorkspaceDAO().save(workspace, pwHash)?.let { return it }

        with(model.activeProfile!!) {
            wid = workspace.wid
            domain = workspace.domain
            uid = workspace.uid
        }

        UserInfoDAO().saveIdentity(
            workspace.uid?.toString(), workspace.wid.toString(), workspace.domain.toString()
        )?.let { return it }

        return null
    }

    /**
     * Updates all key information for the active profile
     */
    fun setKeyData(waddr: WAddress, keyData: WorkspaceKeyData): Throwable? {
        if (model.activeProfile == null) return EmptyDataException("No profile is active")
        return WorkspaceDAO().saveKeyData(waddr, keyData)
    }

    /** Returns the cached DeviceInfo for the active profile */
    fun getDeviceInfo(): Result<DeviceInfo> {
        if (model.activeProfile == null)
            return EmptyDataException("No profile is active").toFailure()
        return ProfileDAO().loadDeviceInfo(
            model.activeProfile!!.getWAddress()
                .getOrElse { return it.toFailure() })
    }

    /**
     * Finishes setting up a workspace for a newly-approved device
     */
    fun finishApprovedDeviceSetup(waddr: WAddress, keyData: List<WorkspaceKey>): Throwable? {
        if (model.activeProfile == null) return EmptyDataException("No profile is active")
        runCatching { KeyDAO().saveWorkspaceKeys(keyData) }.getOrElse { return it }
        return WorkspaceDAO().setStatus(waddr, WorkspaceStatus.Active)
    }
}
