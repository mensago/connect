package connect.profiles

import keznacl.BadValueException
import keznacl.EmptyDataException
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.*
import libmensago.ResourceNotFoundException
import utilities.DBProxy

/** The KeycardDAO class interacts with keycard storage in a profile */
class KeycardDAO {

    /**
     * Obtains a keycard from the database. `owner` is expected to be either a workspace address for a
     * user or a domain for an organization. An error will be returned if something goes wrong in the
     * lookup. A lack of an entry in the database is not considered an error and if no matching
     * keycard exists in the local database, Ok(None) is returned. If managing the user's keycard,
     * check_ttl should be false. It should be set to true only if you are looking to look up a
     * a keycard from the database's cache and need to know if the owning server needs to be queried
     * directly. When check_ttl is true, Ok(None) will be returned if the keycard doesn't exist or if
     * the Time-To-Live has expired.
     */
    fun getCard(owner: String, eType: String, checkTTL: Boolean): Result<Keycard?> {
        val db = DBProxy.get()
        val card = Keycard.new(eType) ?: return BadValueException("Bad entry type").toFailure()

        var rs =
            db.query("""SELECT entry FROM keycards WHERE owner=? ORDER BY 'entryindex'""", owner)
                .getOrElse { return it.toFailure() }
        if (!rs.next()) return Result.success(null)
        do {
            val entry = when (eType) {
                "Organization" -> {
                    OrgEntry.fromString(rs.getString(1)).getOrElse { return it.toFailure() }
                }

                "User" -> {
                    UserEntry.fromString(rs.getString(1)).getOrElse { return it.toFailure() }
                }

                else -> {
                    // We should never be here
                    return BadValueException("Bad entry type").toFailure()
                }
            }
            card.entries.add(entry)
        } while (rs.next())

        if (card.entries.isEmpty()) return Result.success(null)

        if (checkTTL) {
            rs = db.query(
                """SELECT ttlexpires FROM keycards WHERE owner=? AND entryindex=?""", owner,
                card.current!!.getField("Index")!!.toString()
            ).getOrElse { return it.toFailure() }
            if (!rs.next()) return ResourceNotFoundException().toFailure()
            val ttl = rs.getString(1)

            // The only way that the TTL expiration string will be empty is if this belongs to the
            // user's keycard and this never expires.
            if (ttl.isEmpty()) return card.toSuccess()

            val isExpired = card.current!!.isExpired().getOrElse { return it.toFailure() }
            if (isExpired) return null.toSuccess()
        }

        return card.toSuccess()
    }

    /**
     * Adds a keycard to the database's cache or updates it if it already exists. This call is used
     * both for managing the local keycard cache and for managing the user's keycard, which is why
     * the `for_caching` flag exists. If you are managing the user's copy of the local keycard, make
     * sure that forCaching is set to false to ensure that the user's local copy isn't accidentally
     * deleted because its Time-To-Live value expired!
     **/
    fun updateCard(card: Keycard, forCaching: Boolean): Throwable? {
        val db = DBProxy.get()
        val owner = card.getOwner() ?: return EmptyDataException()

        db.execute("DELETE FROM keycards where owner=?", owner)

        // Calculate the expiration time of the current entries
        val ttlOffset = card.current!!.getFieldInteger("Time-To-Live")
            ?: return MissingFieldException("Time-To-Live")
        val ttlExpires = if (forCaching) Timestamp.plusDays(ttlOffset).toDateString() else ""

        card.entries.forEach { entry ->
            val fullText = entry.getFullText(null).getOrElse { return it }
            db.execute(
                """INSERT INTO keycards(
            owner, entryindex, type, entry, textentry, hash, expires, timestamp, ttlexpires)
            VALUES(?,?,?,?,?,?,?,?,?)""",
                owner,
                entry.getField("Index")!!.toString(),
                entry.getFieldString("Type")!!,
                fullText,
                fullText,
                entry.getAuthString("Hash")!!,
                entry.getFieldString("Expires")!!,
                entry.getFieldString("Timestamp")!!,
                ttlExpires,
            )
        }

        return null
    }
}
