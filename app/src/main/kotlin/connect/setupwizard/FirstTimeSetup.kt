package connect.setupwizard

import connect.main.MainController
import connect.notes.NoteItem
import keznacl.toSuccess
import libkeycard.RandomID
import org.controlsfx.dialog.CommandLinksDialog
import org.controlsfx.dialog.CommandLinksDialog.CommandLinksButtonType

enum class SetupType {
    Login,
    RegCode,
    NewAccount,
    LocalOnly,
    Quit
}

fun showFirstTimeWindow(): Result<SetupType> {
    val loginText = "I want to sign into my account."
    val regCodeText = "I have a sign-up code to use."
    val newAccountText = "I need an account."
    val localText = "I want to work just on this computer."
    val quitText = "Quit for now."

    // Start with the setup choice window
    val links = listOf(
        CommandLinksButtonType(loginText, false),
        CommandLinksButtonType(regCodeText, false),
        CommandLinksButtonType(newAccountText, false),
        CommandLinksButtonType(localText, false),
        CommandLinksButtonType(quitText, false),
    )
    val dialog = CommandLinksDialog(links).run {
        dialogPane.graphic = null
        title = "First Time Setup"
        dialogPane.headerText = "Welcome! How do you need Connect set up?"
        dialogPane.contentText = ""
        this
    }
    val result = dialog.showAndWait()
    val out = when (result.get().text) {
        loginText -> SetupType.Login
        regCodeText -> SetupType.RegCode
        newAccountText -> SetupType.NewAccount
        localText -> SetupType.LocalOnly
        else -> SetupType.Quit
    }

    return out.toSuccess()
}

fun addWelcomeContent(con: MainController) {
    val welcomeNote =
        NoteItem("Welcome to Mensago Connect!", welcomeText, RandomID.generate().toString()).apply {
            addLabel("Welcome")
        }
    con.processNewItem(welcomeNote)
}

private const val welcomeText = """
Welcome to Mensago Connect!

Connect is your key to the world of Mensago, a collection of digital tools designed only for helping you organize your life and stay in touch with the people that are important to you.

You can find more information about Mensago at https://www.mensago.org .
"""
