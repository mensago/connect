// Suppress duplicate code warnings because there's a lot of common code between the regcode
// dialog and the account signup dialog, but they're both different enough that you can't easily
// combine the code
@file:Suppress("DuplicatedCode")

package connect.setupwizard

import connect.Client
import connect.StartupOptions
import connect.UserInfoDAO
import connect.showFatalError
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox
import libkeycard.MAddress
import libmensago.ProtocolException
import utilities.PasswordFactory

@Suppress("DuplicatedCode")
fun showRegCodeDialog(): Boolean {

    val client = Client()
    var address: MAddress?

    var savedName: String
    var savedAddress: String
    while (true) {
        val regWin = RegCodeDialog()
        val result = regWin.showAndWait()
        if (result.isEmpty || result.get().buttonData == ButtonBar.ButtonData.CANCEL_CLOSE)
            return false

        // We got a Login button press. Attempt to log in with the credentials.
        address = MAddress.fromString(regWin.addressField.text)
        if (address == null) {
            Alert(Alert.AlertType.ERROR).apply {
                this.title = "Error"
                this.contentText = "Your Mensago address doesn't appear to be valid."
            }.showAndWait()
            continue
        }

        if (regWin.passwordField.text != regWin.confirmField.text) {
            Alert(Alert.AlertType.ERROR).apply {
                this.title = "Error"
                this.contentText = "The password boxes are not the same."
            }.showAndWait()
            continue
        }

        val pw = PasswordFactory.getHasher()
        pw.updateHash(regWin.passwordField.text)
        savedName = regWin.nameField.text.trim()
        savedAddress = regWin.addressField.text
        val regError = client.redeemRegcode(address, regWin.regCodeField.text, pw) ?: break
        if (regError is ProtocolException) {
            if (regError.code == 401) {
                Alert(Alert.AlertType.ERROR).apply {
                    this.title = "Error"
                    this.contentText = "Failed to sign up with the supplied address and " +
                            "sign-up code. Please check that the information you have entered " +
                            "is correct."
                }.showAndWait()
            } else showFatalError("Registration failure", regError)
        } else showFatalError("Registration failure", regError)
    }

    client.login(MAddress.fromString(savedAddress)!!).getOrElse {
        showFatalError("Failure logging in to update the keycard", it)
        return true
    }
    client.updateKeycard(savedName.ifBlank { null })?.let {
        showFatalError("Failure updating the keycard", it)
        return true
    }
    client.logout()?.let { /* errors logging out don't matter in this case */ }
    client.disconnect()?.let { /* errors disconnecting don't matter, either */ }

    val udao = UserInfoDAO()
    if (savedName.isNotEmpty()) {
        val parts = savedName.split(" ").map { it.trim() }
        udao.saveGivenName(parts[0])
        if (parts.size > 2)
            udao.saveFamilyName(parts.subList(1, parts.size).joinToString(" "))
        else
            udao.saveFamilyName(parts[1])
        udao.saveFormattedName(savedName)
    }

    Alert(Alert.AlertType.INFORMATION).apply {
        this.title = "Success"
        this.contentText = "Sign-up successful."
    }.showAndWait()
    return true
}

/**
 * A Dialog window which logs into a Mensago server and returns the address on success.
 */
class RegCodeDialog : Dialog<ButtonType>() {
    val nameField = TextField(if (StartupOptions.testMode) "Workspace Admin" else "")
    val addressField = TextField(if (StartupOptions.debugMode) "admin/example.com" else "")
    val regCodeField =
        TextField(if (StartupOptions.debugMode) "Undamaged Shining Amaretto Improve Scuttle Uptake" else "")
    val passwordField = PasswordField()
    val confirmField = PasswordField()

    init {
        val headerVBox = VBox().run {
            padding = Insets(10.0)
            alignment = Pos.TOP_CENTER
            this
        }
        val title = Label("Please enter your sign-up information")
        title.style = "-fx-font-weight: bold"
        headerVBox.children.add(title)
        dialogPane.header = headerVBox

        val bodyVBox = VBox()
        bodyVBox.alignment = Pos.CENTER
        dialogPane.children.add(bodyVBox)

        val nameLabel = Label("Name (optional):")
        val addressLabel = Label("Mensago Address:")
        val regCodeLabel = Label("Sign-Up Code:")
        val passwordLabel = Label("Password:")
        val confirmLabel = Label("Confirm:")

        val gridPane = GridPane().apply {
            hgap = 10.0
            vgap = 10.0

            add(nameLabel, 0, 0)
            add(nameField, 1, 0)

            add(addressLabel, 0, 1)
            add(addressField, 1, 1)

            add(regCodeLabel, 0, 2)
            add(regCodeField, 1, 2)

            add(passwordLabel, 0, 3)
            add(passwordField, 1, 3)

            add(confirmLabel, 0, 4)
            add(confirmField, 1, 4)
        }
        bodyVBox.children.add(gridPane)

        val cancelButton = ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE)
        dialogPane.buttonTypes.add(cancelButton)
        val loginButton = ButtonType("Sign Up", ButtonBar.ButtonData.OK_DONE)
        dialogPane.buttonTypes.add(loginButton)

        dialogPane.content = bodyVBox

    }
}