package connect.main

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ObservableList
import javafx.scene.input.KeyCodeCombination
import javafx.scene.layout.Region
import utilities.toObservable

class MainModel {

    private val modeListProperty = SimpleListProperty(mutableListOf<ModeInfo>().toObservable())
    var modeList: ObservableList<ModeInfo>
        get() = modeListProperty.get()
        set(value) = modeListProperty.set(value)

    val selectedModeProperty = SimpleObjectProperty<AppMode>()
    var selectedMode: AppMode
        get() = selectedModeProperty.get()
        set(value) = selectedModeProperty.set(value)

    val isProfileLocalProperty = SimpleBooleanProperty(true)
    var isProfileLocal: Boolean
        get() = isProfileLocalProperty.value
        set(value) = isProfileLocalProperty.set(value)
}

/** Type which corresponds to each of Connect's modes */
enum class AppMode {
    Messages,
    Contacts,
    Notes
}

/** Contains all the info needed to construct a mode menu for a specific view */
class ModeInfo(
    val mode: AppMode,
    val view: Region,
    val accelerator: KeyCodeCombination
)
