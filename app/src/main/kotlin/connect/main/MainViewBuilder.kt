package connect.main

import connect.profiles.ProfileManager
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.layout.*

class MainViewBuilder(private val model: MainModel) {

    fun build(): Region {
        val workspaceStatus = Label().apply {
            textProperty().bind(ProfileManager.model.activeIdentityStatusProperty.asString())
        }
        val statusContainer = BorderPane().apply {
            right = HBox().apply {
                padding = Insets(1.0, 5.0, 1.0, 5.0)
                spacing = 5.0
                children.addAll(Label("Account Status:"), workspaceStatus)
            }
        }
        val activities = StackPane().apply {
            model.modeList.forEach { children.add(it.view) }
        }
        return VBox().apply {
            children.addAll(activities, statusContainer)
        }
    }
}
