package connect.main

import connect.FilterableItem
import connect.contacts.ContactController
import connect.contacts.ContactItem
import connect.messages.MessageController
import connect.messages.MessageItem
import connect.messages.MsgBaseItem
import connect.notes.NoteController
import connect.notes.NoteItem
import javafx.scene.layout.Region
import keznacl.BadValueException
import libmensago.MServerPath

class MainController : FilterItemProcessor {
    private val model = MainModel()
    private val interactor = MainInteractor(model)
    private val handlers = MainHandlers(this::showNewItem)
    private val msgController = MessageController(model)
    private val contactController = ContactController(model)
    private val noteController = NoteController(model, handlers)
    private val mainViewBuilder = MainViewBuilder(model)

    init {
        model.selectedModeProperty.addListener { _ -> interactor.selectMode(model.selectedMode) }
    }

    fun setProfileLocality(isLocal: Boolean) {
        model.isProfileLocal = isLocal
    }

    fun messaging() = msgController

    /** Pass a new data item to the appropriate mode controller */
    override fun processNewItem(item: FilterableItem): Throwable? {
        return when (item) {
            is NoteItem -> noteController.loadNewItem(item)
            is ContactItem -> contactController.loadNewItem(item)
            is MsgBaseItem -> msgController.processNewItem(item)
            else -> BadValueException("Unhandled type ${item.javaClass.simpleName}")
        }
    }

    /** Changes an item's server-side path */
    override fun processMoveItem(oldPath: MServerPath, newPath: MServerPath) {
        // We don't know who actually has the item, so we'll notify all controllers. They won't
        // do anything if the item isn't found
        noteController.moveItem(oldPath, newPath)
        contactController.moveItem(oldPath, newPath)
        msgController.moveItem(oldPath, newPath)
    }

    /** Replaces the item at the specified path with the new one */
    override fun processReplaceItem(oldItem: MServerPath, newItem: FilterableItem) {
        when (newItem) {
            is NoteItem -> noteController.replaceItem(oldItem, newItem)
            is ContactItem -> contactController.replaceItem(oldItem, newItem)
            is MessageItem -> msgController.replaceItem(oldItem, newItem)
        }
    }

    override fun updateContactInfo(item: FilterableItem): Throwable? =
        contactController.updateInfo(item)

    /** Sets the current program mode, i.e. messages, notes, etc. */
    fun setMode(mode: AppMode) = interactor.selectMode(mode)

    /** Make all mode controllers register themselves. This call should be made only once. */
    fun registerModes() {
        msgController.registerView()
        contactController.registerView()
        noteController.registerView()
    }

    fun getView(): Region {
        return mainViewBuilder.build()
    }

    fun showNewItem() {

        when (model.selectedMode) {
            AppMode.Messages -> msgController.showNewMessageWindow()
            // TODO: Implement New Contact window
            AppMode.Contacts -> println("MainController::newItem: new contact")
            AppMode.Notes -> noteController.createNewNote()
        }
    }
}
