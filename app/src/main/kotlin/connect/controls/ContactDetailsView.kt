package connect.controls

import connect.contacts.ContactItem
import connect.locale.makeDisplayLanguageList
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.scene.control.Label
import javafx.scene.layout.VBox

/**
 * A control which dynamically displays the various properties of a ContactItem in the style of a
 * user profiles normally seen online.
 *
 * @param contactProperty A property containing the ContactItem to be displayed
 * @param safeMode Enable or disable Safe Mode, explained below.
 *
 * ContactDetailsView has a safety flag. This is because this control is used in both normal and
 * content-restricted contexts. safeMode disables interactivity with the various fields, so the user
 * is unable to click on hyperlinks to open a browser to a relevant website, email addresses won't
 * open the email client, etc. In the context of Contact Requests, this is critical functionality
 * because content from CRs is not only not-trusted, but also heavily restricted to prevent abuse
 * and exploits.
 */
class ContactDetailsView(
    val contactProperty: SimpleObjectProperty<ContactItem>, val useSafeMode: Boolean
) : VBox() {

    var contact: ContactItem?
        get() = contactProperty.get()
        set(value) = contactProperty.set(value)

    val safeModeProperty = SimpleBooleanProperty(useSafeMode)
    var safeMode: Boolean
        get() = safeModeProperty.value
        set(value) = safeModeProperty.set(value)

    private val bioLabel = Label().apply {
        style = "-fx-font-style: italic;"
        isWrapText = true
    }
    private val mensagoAddressLabel = LabeledValueView("Mensago:")
    private val birthdayLabel = LabeledValueView("Birthday:")
    private val anniversaryLabel = LabeledValueView("Anniversary:")
    private val languagesLabel = LabeledValueView("Languages:")

    private val phoneGroup = MultiInfoView("Phone:", contactProperty.value?.phoneListProperty)
    private val socialGroup =
        MultiInfoView("Social Networks:", contactProperty.value?.socialListProperty)
    private val emailGroup = MultiInfoView("Email:", contactProperty.value?.emailListProperty)
    private val websiteGroup =
        MultiInfoView("Websites:", contactProperty.value?.websiteListProperty)

    init {
        contactProperty.addListener { _, _, newValue -> updateControls(newValue) }
        updateLayout()
    }

    private fun updateControls(newValue: ContactItem?) {
        if (newValue == null) {
            bioLabel.text = ""
            mensagoAddressLabel.value = ""
            birthdayLabel.value = ""
            anniversaryLabel.value = ""
            languagesLabel.value = ""

            phoneGroup.itemListProperty.value = null
            socialGroup.itemListProperty.value = null
            emailGroup.itemListProperty.value = null
            websiteGroup.itemListProperty.value = null

            children.clear()
            return
        }

        bioLabel.text = newValue.bio
        mensagoAddressLabel.value = newValue.maddress
        birthdayLabel.value = newValue.birthday
        anniversaryLabel.value = newValue.anniversary

        if (newValue.languages.isNotEmpty())
            languagesLabel.value = makeDisplayLanguageList(newValue.languages)
        else
            languagesLabel.value = ""

        phoneGroup.itemListProperty.value = newValue.phoneListProperty.value
        socialGroup.itemListProperty.value = newValue.socialListProperty.value
        emailGroup.itemListProperty.value = newValue.emailListProperty.value
        websiteGroup.itemListProperty.value = newValue.websiteListProperty.value

        updateLayout()
    }

    private fun updateLayout() {
        children.clear()
        if (bioLabel.text.isNotEmpty())
            children.add(bioLabel)
        if (!mensagoAddressLabel.value.isNullOrEmpty())
            children.add(mensagoAddressLabel)
        if (!birthdayLabel.value.isNullOrEmpty())
            children.add(birthdayLabel)
        if (!anniversaryLabel.value.isNullOrEmpty())
            children.add(anniversaryLabel)
        if (!languagesLabel.value.isNullOrEmpty())
            children.add(languagesLabel)

        if (!phoneGroup.itemListProperty.value.isNullOrEmpty())
            children.add(phoneGroup)
        if (!socialGroup.itemListProperty.value.isNullOrEmpty())
            children.add(socialGroup)
        if (!emailGroup.itemListProperty.value.isNullOrEmpty())
            children.add(emailGroup)
        if (!websiteGroup.itemListProperty.value.isNullOrEmpty())
            children.add(websiteGroup)
    }
}