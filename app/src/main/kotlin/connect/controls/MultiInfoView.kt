package connect.controls

import connect.contacts.MultiInfo
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.layout.VBox

/**
 * The MultiInfoView control is used for dynamic display of MultiInfo structures owned by a contact.
 * Each MultiInfo item is displayed in its own labeled control.
 */
class MultiInfoView(labelStr: String, items: ObservableList<MultiInfo>?) : VBox() {
    val labelProperty = SimpleStringProperty(labelStr)

    val itemListProperty = SimpleListProperty(items).apply {
        addListener { _, _, _ -> updateLayout() }
    }
    private var itemList: ObservableList<MultiInfo>?
        get() = itemListProperty.value
        set(value) {
            itemListProperty.value = value
        }

    private val headingLabel = Label().apply {
        style = "-fx-font-size: 110%; -fx-font-weight: bold;"
        textProperty().bind(labelProperty)
    }

    private val valueBox = VBox()

    init {
        padding = Insets(5.0, 0.0, 0.0, 0.0)
        valueBox.padding = Insets(0.0, 0.0, 0.0, 20.0)
        children.addAll(headingLabel, valueBox)
        updateLayout()
    }

    private fun updateLayout() {
        valueBox.children.clear()
        if (itemList == null || itemList!!.isEmpty())
            return

        itemList!!.forEach { item ->
            valueBox.children.add(LabeledValueView(item.label, item.value))
        }
    }
}