package connect.controls

import connect.contacts.ContactItem
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox

/**
 * The ContactView class provides a compact way to view the details for a contact.
 */
class ContactView(
    private val contactProperty: SimpleObjectProperty<ContactItem>, orientation: Orientation,
    safeMode: Boolean
) : StackPane() {

    var contact: ContactItem?
        get() = contactProperty.get()
        set(value) = contactProperty.set(value)

    private val photoView = ImageView().apply {
        fitWidth = 100.0
        isPreserveRatio = true
    }
    private val formattedName = Label().apply {
        style = "-fx-font-size: 150%; -fx-font-weight: bold; -fx-font-alignment: center;"
        minWidth = 200.0
        alignment = Pos.CENTER
    }

    private val detailsView = ContactDetailsView(contactProperty, safeMode)
    private val genericPhoto = Image("blankprofile.png")
    private val scrollPane = ScrollPane().apply {
        style = "-fx-background-color:transparent;"
        isFitToWidth = true
        content = detailsView
    }

    private val box = if (orientation == Orientation.HORIZONTAL)
        HBox().apply {
            spacing = 5.0
            HBox.setHgrow(scrollPane, Priority.ALWAYS)
        }
    else
        VBox().apply {
            spacing = 5.0
            VBox.setVgrow(scrollPane, Priority.ALWAYS)
        }

    init {
        alignment = Pos.CENTER
        contactProperty.subscribe { newValue ->
            newValue?.let {
                formattedName.text = newValue.formattedName
                photoView.image = newValue.photo ?: genericPhoto
            }
        }
        children.add(box)

        val header = VBox().apply {
            alignment = Pos.TOP_CENTER
            children.addAll(photoView, formattedName)
        }

        with(box) {
            alignment = Pos.TOP_CENTER
            padding = Insets(5.0)
            children.addAll(header, scrollPane)
        }
    }
}
