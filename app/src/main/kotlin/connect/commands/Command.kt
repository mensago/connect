package connect.commands

/**
 * The Command class represents an action which can be invoked from some other part of the
 * application. This can be as simple as renaming an item to showing a new message window.
 */
open class Command(var name: String, var displayName: String, var description: String) {

    /**
     * Runs the command. The default implementation prints to standard out as a developer aid.
     */
    open fun execute(ctx: CommandContext) {
        println("Command::execute($name, $ctx)")
    }

    override fun toString(): String = "Command $name"
}

/**
 * A SimpleCommand instance represents a command that takes no parameters and returns no data.
 */
class SimpleCommand(
    name: String, displayName: String, description: String, var action: () -> Unit
) : Command(name, displayName, description) {

    override fun execute(ctx: CommandContext) {
        action()
    }
}

/**
 * A InDataCommand instance represents a command that takes one or more parameters and returns no
 * data.
 */
class InDataCommand(
    name: String, displayName: String, description: String, var args: List<CommandArg>,
    var action: (Map<String, Any>) -> Unit
) : Command(name, displayName, description) {

    override fun execute(ctx: CommandContext) {
        action(ctx.args)
    }
}
