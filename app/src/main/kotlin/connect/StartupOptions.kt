package connect

import connect.profiles.ProfileManager
import libmensago.db.KDBConfig
import libmensago.db.PGConfig
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Option
import org.apache.commons.cli.Options
import java.io.File

/**
 * Singleton which contains application-level options.
 */
object StartupOptions {
    /**
     * firstStartup is set if Connect is being started for the first time on a computer. This
     * prompts certain new user behaviors, such as showing the first-time setup wizard window.
     */
    var firstStartup = false

    private val parserOptions = Options().apply {
        listOf(
            Option("h", "help", false, "Show help information"),
            Option("d", "debug", false, "Enable debug output"),
            Option("t", "test", true, "Enable test mode"),
            Option(
                null, "weakhashes", false,
                "Enable weak password hash algorithms. Requires test mode."
            ),
            Option(
                "p", "profile", true, "Specify active profile by name"
            ),
            Option(
                null, "profdir", true, "Specify profile directory"
            )
        ).forEach { addOption(it) }
    }

    /**
     * debugMode is set to true if Connect is started in Debug Mode due to -debug being passed on
     * the command line. It just turns on extra diagnostic logging.
     */
    var debugMode: Boolean = false
        private set

    /**
     * testMode is for Connect developers only. Among other things, test mode enables debug DNS,
     * which returns hardcoded DNS lookups for the domain `example.com` and cryptographic keys
     * stored in a database somewhere. Putting Connect in test mode requires both the -debug and
     * -test switches to be passed to the command line.
     *
     * The test switch takes 1 argument, a string consisting of a database name, a database user,
     * and the password for the database user, each being separated by a colon, e.g.
     * "mensago:mensago:password123".
     */
    var testMode: Boolean = false
        private set
    var testDBConfig: KDBConfig? = null
        private set
    var startupProfile: String = ""
        private set
    var useWeakPasswordHashing: Boolean = false
        private set

    /**
     * Initializes the application state options based on environment variables and command-line
     * arguments.
     */
    fun init(args: Array<String>): Int {
        if (args.isEmpty()) return 0

        val cline = kotlin.runCatching {
            DefaultParser().parse(parserOptions, args, true)
        }.getOrElse {
            println(it)
            return 1
        }

        if (cline.hasOption("help")) {
            printHelp()
            return 1
        }

        debugMode = cline.hasOption("debug")
        if (cline.hasOption("test")) {
            testMode = true
            debugMode = true

            testDBConfig = getTestModeConfig(cline)
        } else testMode = false


        useWeakPasswordHashing = cline.hasOption("weakhashes")

        val envDebug = System.getenv("CONNECT_DEBUG")
        if (envDebug != null && envDebug == "1")
            debugMode = true

        if (useWeakPasswordHashing && !testMode) {
            println("Weak password hashing requires test mode")
            return -1
        }

        if (testMode) {
            if (useWeakPasswordHashing)
                println("connect is running in test mode with weak password hashing")
            else
                println("connect is running in test mode")
        } else if (debugMode) println("connect is running in debug mode")

        if (cline.hasOption("profile")) {
            startupProfile = cline.getOptionValue("profile").lowercase()
            println("Starting Connect with profile '$startupProfile'")
        }

        if (cline.hasOption("profdir")) {
            val profDir = cline.getOptionValue("profdir")
            if (!File(profDir).exists()) {
                println("Profile directory $profDir does not exist")
                return -1
            }
            println("Using non-default profile directory $profDir")
            ProfileManager.profilesPath = profDir
        }

        return 0
    }

    private fun getTestModeConfig(cline: CommandLine): KDBConfig? {
        val testModeHelp = "The -test switch requires 1 argument, a string consisting of a " +
                "database name, a database user, and the password for the database user, each " +
                "being separated by a colon, e.g. 'mensago:mensago:somepassword'"
        val testStr = cline.getOptionValue("test")
        if (testStr == null) {
            println(testModeHelp)
            return null
        }

        val parts = testStr.split(":", limit = 3)
        if (parts.size != 3) {
            println(testModeHelp)
            return null
        }
        return PGConfig(parts[1], parts[2], "localhost", 5432, parts[0])
    }

    private fun printHelp() {
        println("Usage: connect [option1 [option2...]]\n")
        println(
            "  --help, -h    Show this help information\n" +
                    "  --debug, -d   Enable debug mode, which enables additional diagnostic logging\n" +
                    "  --test, -t    Enable test mode, which also enables debug mode. Only for " +
                    "developers.\n" +
                    "  --weakhashes  Enable weak password hashing. Requires test mode. Use this only " +
                    "if you know what you are doing.\n"
        )
    }
}
