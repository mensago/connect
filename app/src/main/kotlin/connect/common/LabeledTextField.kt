package connect.common

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.Region

class LabeledTextField(labelText: String = "", contents: String = "") : Region() {
    val label = Label(labelText)
    val text = TextField(contents)

    init {
        children.add(
            HBox().apply {
                padding = Insets(5.0)
                spacing = 5.0
                alignment = Pos.CENTER_LEFT
                children.addAll(label, text)
            }
        )
    }
}
