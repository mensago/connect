package connect.common

import keznacl.BadValueException
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.IDType
import libkeycard.MAddress
import libkeycard.WAddress
import libmensago.resolver.KCResolver

fun MAddress.resolve(): Result<WAddress> = when (this.userid.type) {
    IDType.WorkspaceID -> WAddress.fromMAddress(this)?.toSuccess()
        ?: BadValueException("MAddresss is not a WAddress").toFailure()

    IDType.UserID -> {
        val wid =
            KCResolver.resolveMenagoAddress(this).getOrElse { return it.toFailure() }
        WAddress.fromParts(wid, this.domain).toSuccess()
    }
}
