package connect.common

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

/**
 * A Dialog window which displays detailed technical information for an exception.
 */
class ExceptionDialog(errorMsg: String, exception: Throwable? = null) : Dialog<ButtonType>() {

    init {
        title = "Error"
        isResizable = true

        val headerVBox = VBox().apply {
            padding = Insets(10.0)
        }
        val title = Label(
            "We're sorry. Connect ran into an unexpected error.\n" +
                    "Please report this to your technical support.\n\n$errorMsg"
        )
        headerVBox.children.add(title)
        dialogPane.header = headerVBox

        val detailsLabel = Label("Technical Details").apply {
            style = "-fx-font-weight: bold"
        }
        val straceBox = TextArea().apply {
            padding = Insets(5.0)
            exception?.let { text = exception.stackTraceToString() }
        }
        val copyButton = Button("Copy to Clipboard")
        copyButton.setOnAction {
            val content = ClipboardContent().apply {
                if (exception != null)
                    putString("$errorMsg\n\n${exception.stackTraceToString()}")
                else
                    putString(errorMsg)
            }
            Clipboard.getSystemClipboard().setContent(content)
        }

        val bodyVBox = VBox().apply {
            alignment = Pos.CENTER_LEFT
            padding = Insets(5.0)
            spacing = 5.0
            children.addAll(detailsLabel, straceBox, copyButton)
        }
        VBox.setVgrow(straceBox, Priority.ALWAYS)

        val closeButton = ButtonType("OK", ButtonBar.ButtonData.OK_DONE)
        dialogPane.buttonTypes.add(closeButton)
        dialogPane.content = bodyVBox
    }
}