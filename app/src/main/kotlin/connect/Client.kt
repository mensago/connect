package connect

import connect.common.resolve
import connect.contacts.ContactDAO
import connect.messages.MessageDAO
import connect.profiles.*
import connect.sync.DeliveryTarget
import connect.sync.OutgoingHandler
import keznacl.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.*
import libmensago.*
import libmensago.ContactMessage.Companion.approvalFromContact
import libmensago.ContactMessage.Companion.infoUpdate
import libmensago.commands.*
import libmensago.db.DatabaseException
import libmensago.resolver.KCResolver
import utilities.DBDataException
import utilities.DataMismatchException
import utilities.LoginRequiredException
import utilities.NotRegisteredException
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.Path

/** The Client class is the primary interface to the entire library */
class Client {
    private val conn = ServerConnection()
    private var lastIdle = 0L
    var isAdmin = false
        private set
    var currentAddress: MAddress? = null
        private set
    var currentDomain: Domain? = null
        private set
    var lastWorkspaceStatus: WorkspaceStatus = WorkspaceStatus.Local
    var expiration = 90
        set(value) {
            field = when {
                value < 1 -> 1
                value > 1095 -> 1095
                else -> value
            }
        }

    /**
     * Approves a contact request. This call handles several tasks: generating 2 unique keypairs for
     * the contact to communicate with us, saving these keypairs to the database in a way that they
     * are associated with the contact, saving the contact's information to the database, and
     * sending the approval message.
     */
    @Suppress("DuplicatedCode")
    fun approveContactRequest(
        address: WAddress, myInfo: Contact, otherInfo: Contact, saveToSent: Boolean
    ): Throwable? {

        val entry = KCResolver.getCurrentEntry(EntrySubject.fromWAddress(address))
            .getOrElse { return it }
        val recipientKey = entry.getEncryptionKey("Contact-Request-Encryption-Key")
            ?: return BadFieldValueException(
                "Bad contact request key '${
                    entry.getFieldString("Contact-Request-Encryption-Key")
                }' in recipient keycard"
            )

        if (otherInfo.domain == null || otherInfo.workspace == null || otherInfo.formattedName == "")
            return BadFieldValueException("Contact had missing required field(s)")

        ContactDAO().import(otherInfo)?.let { return it }

        val keySet = ContactKeySet.generate().getOrThrow()
        myInfo.keys = keySet.toPublicMap()

        KeyDAO().addKeySetForContact(otherInfo.id, keySet)?.let { return it }

        val approval = approvalFromContact(address, myInfo).getOrElse { return it }
        val env = Envelope.seal(
            recipientKey,
            AddressPair(approval.from, approval.to),
            Payload(PayloadType.ContactMessage, Json.encodeToString(approval))
        ).getOrElse { return it }
        val envData = env.toString()

        val profile = getActiveProfile().getOrElse { return it }
        val tempPath = profile.ensureTempFolder().getOrElse { return it }

        val tempFilePath = Paths.get(tempPath.toString(), makeEnvelopeFilename(envData.length))
        env.saveWithName(tempFilePath.toString())?.let { return it }

        val sender = profile.getWAddress().getOrElse { return it }
        OutgoingHandler.queueMessage(
            tempFilePath, DeliveryTarget(sender.domain, sender.id), DeliveryTarget(address.domain)
        ).getOrElse { return it }

        if (!saveToSent) return null

        val name = entry.getField("Name")
            ?: entry.getMAddress()
            ?: entry.getWAddress()
            ?: return InvalidKeycardException("Contact keycard has no workspace address")
        approval.subject = "Contact Request Approved for $name"
        approval.from = profile.getWAddress().getOrElse { return it }

        return queueMsgAsSent(approval, tempPath)
    }

    /**
     * Approves membership of a new device on a user's workspace. This call loads all of the keys
     * needed by the new device in order to function and interoperate.
     */
    fun handleNewDevice(
        devid: RandomID, action: MessageAction, devkey: Encryptor?
    ): Throwable? {
        getActiveProfile().getOrElse { return it }
        if (!isLoggedIn()) return LoginRequiredException()
        if (action == MessageAction.Reject)
            return newdevice(conn, devid, MessageAction.Reject, null)

        val wkeys = KeyDAO().loadKeyList().getOrElse { return it }
        val encData = serializeAndEncrypt(wkeys, devkey!!).getOrElse { return it }
        return newdevice(conn, devid, MessageAction.Approve, encData)
    }

    /**
     * Changes the user's password. NOTE: this password does NOT locally store the password hash
     * itself, only updating the hash on the server side. This is because the user's password hash
     * is *very* sensitive information and how (and if) it is stored is best handled based on the
     * specific application context.
     */
    fun changePassword(oldpw: Password, newpw: Password): Throwable? {
        if (oldpw.getHash().isEmpty() || newpw.getHash().isEmpty()) return EmptyDataException()
        if (!isLoggedIn()) return LoginRequiredException()
        ensureActiveProfile()?.let { return it }

        return setPassword(conn, oldpw, newpw)
    }

    /**
     * A method to check if the identity workspace on the active profile is still pending, and if
     * approved, finish setting up the workspace for usage. This call will do nothing and report
     * success if called on an active workspace. false.toSuccess() means the device is still
     * pending. true.toSuccess() means the device has been approved and this call has finished
     * setting up the workspace. A DeviceBlockedException is returned when the device is blocked
     * from the workspace.
     */
    fun checkDeviceApproved(wid: RandomID, devid: RandomID): Result<Boolean> {
        if (!isConnected()) return NotConnectedException().toFailure()
        val status = getDeviceStatus(conn, wid, devid).getOrElse { return it.toFailure() }
        return (status == DeviceStatus.Approved || status == DeviceStatus.Registered).toSuccess()
    }

    /** Establishes a network connection to a Mensago server. Logging in is not performed. */
    fun connect(domain: Domain): Throwable? {
        disconnect()?.let { return it }
        connectToServer(conn, domain)?.let { return it }
        currentDomain = domain
        return null
    }

    /** Gracefully closes a connection with a Mensago server */
    fun disconnect(): Throwable? {
        currentDomain = null
        return if (isConnected()) conn.disconnect() else null
    }

    /**
     * Downloads a file from the server.
     * @param serverPath a Mensago file path
     * @param localPath the full path for the file, including filename
     */
    fun download(serverPath: MServerPath, localPath: String): Throwable? {
        if (!isLoggedIn()) return LoginRequiredException()
        return download(conn, serverPath, localPath).exceptionOrNull()
    }

    /** Returns true if the client is connected to a Mensago server */
    fun isConnected(): Boolean {
        return conn.isConnected()
    }

    fun exists(path: MServerPath): Result<Boolean> {
        return if (!isLoggedIn()) LoginRequiredException().toFailure()
        else exists(conn, path.toString())
    }

    /** Convenience method to easily obtain keycards */
    private fun getKeycard(owner: String): Result<Keycard> {
        val subject = EntrySubject.fromString(owner) ?: return BadValueException().toFailure()
        return KCResolver.getKeycard(subject)
    }

    /** Returns a reference to the client's ServerConnection instance */
    fun getConnection(): ServerConnection {
        return conn
    }

    fun idle(): Result<Int> {
        if (!isConnected()) return NotConnectedException().toFailure()

        val now = System.currentTimeMillis() / 1000
        return if (isLoggedIn()) {
            val out = if (lastIdle > 0)
                idle(conn, lastIdle)
            else
                idle(conn, null)
            lastIdle = now
            out
        } else {
            lastIdle = now
            idle(conn, null)
        }
    }

    /**
     * Logs into a server. Note that while logging in and connecting are not the same, if this call
     * is made while not connected to a server, an attempt to connect will be made.
     *
     * login() can optionally be called with a cleartext user password. This is only done for first
     * time login on a new device.
     *
     * This call returns a pair containing the status of the workspace and a string. If the state
     * returned is anything but active, the client remains logged out. The string is populated only
     * when a new device tries to log into the workspace for the first time. The string returned
     * at that point is the authorization code.
     */
    fun login(address: MAddress, userpass: String = ""): Result<Pair<WorkspaceStatus, String>> {
        val profile = getActiveProfile().getOrElse { return it.toFailure() }
        if (!isConnected()) connect(address.domain)?.let { return it.toFailure() }
        if (currentAddress == address) return Pair(lastWorkspaceStatus, "").toSuccess()

        currentDomain = address.domain

        val isLocal = profile.isLocal().getOrElse { return it.toFailure() }
        if (userpass.isNotEmpty()) {
            return if (isLocal) newDeviceLogin(address, userpass)
            else ResourceExistsException().toFailure()
        }

        if (isLocal)
            return NotRegisteredException().toFailure()

        val record = KCResolver.getMgmtRecord(address.domain).getOrElse { return it.toFailure() }
        val waddr = address.resolve().getOrElse { return it.toFailure() }

        val serverKey = EncryptionKey.from(record.ek).getOrElse { return it.toFailure() }

        // An error returned by this call should indicate a failure in the communication process,
        // not a non-active status. For example, trying to log in and get a Pending response is not
        // an error, it's a reflection of the protocol working as expected.
        val pwInfo = login(conn, waddr.id, serverKey).getOrElse {
            if (it !is ProtocolException) return it.toFailure()

            currentAddress = null
            WorkspaceStatus.fromLoginCode(it.code)?.let { status ->
                lastWorkspaceStatus = status
                return Pair(lastWorkspaceStatus, "").toSuccess()
            }
            return it.toFailure()
        }

        val wdao = WorkspaceDAO()
        val pwHash = wdao.getCredentials(waddr).getOrElse { return it.toFailure() }
        password(conn, pwHash, pwInfo)?.let {
            currentAddress = null
            return it.toFailure()
        }

        val userEKey = KeyDAO().loadKeyByPurpose(KeyPurpose.Encryption).let {
            if (it.isFailure && it.exceptionOrNull()!! !is ResourceNotFoundException) {
                currentAddress = null
                return it.exceptionOrNull()!!.toFailure()
            }

            if (it.getOrNull() != null) {
                it.getOrNull()!!.first.toEncryptionKey()
                    .getOrElse { e -> currentAddress = null; return e.toFailure() }
            } else {
                val usercard = KCResolver.getKeycard(EntrySubject.fromMAddress(address))
                    .getOrElse { e -> currentAddress = null; return e.toFailure() }

                if (usercard.entries.isEmpty()) {
                    currentAddress = null
                    return InvalidKeycardException("Keycard for $address is missing")
                        .toFailure()
                }

                val k = usercard.current!!.getEncryptionKey("Encryption-Key")
                if (k == null) {
                    currentAddress = null
                    return InvalidKeycardException("Keycard is missing its Encryption-Key field")
                        .toFailure()
                }
                k
            }
        }

        val profileBroker = ProfileBroker(ProfileManager.model)
        val devInfo = profileBroker.getDeviceInfo()
            .getOrElse { currentAddress = null; return it.toFailure() }
        devInfo.encryptAttributes(userEKey)
            .getOrElse { currentAddress = null; return it.toFailure() }
        val keyHash = userEKey.getPublicHash()
            .getOrElse { currentAddress = null; return it.toFailure() }
        val pair = device(conn, devInfo, keyHash)
            .getOrElse { currentAddress = null; return it.toFailure() }

        if (pair.second != null) {
            val devpair = wdao.getDeviceKeypair(waddr)
                .getOrElse { currentAddress = null; return it.toFailure() }
            val keyList = decryptAndDeserialize<List<WorkspaceKey>>(pair.second!!, devpair)
                .getOrElse { currentAddress = null; return it.toFailure() }
            profileBroker.finishApprovedDeviceSetup(waddr, keyList)
                ?.let { currentAddress = null; return it.toFailure() }
        }
        isAdmin = pair.first

        currentAddress = address
        lastWorkspaceStatus = WorkspaceStatus.Active

        return Pair(lastWorkspaceStatus, "").toSuccess()
    }

    fun isLoggedIn(): Boolean {
        if (isConnected()) return currentAddress != null
        currentDomain = null
        currentAddress = null
        return false
    }

    /**
     * Logs out of any active login sessions. This does not disconnect from the server itself;
     * instead it reverts the session to an unauthenticated state.
     */
    fun logout(): Throwable? {
        isAdmin = false
        currentAddress = null
        return if (isConnected()) logout(conn) else null
    }

    /**
     * Creates a directory on the server. If parent directories don't exist, they are also created.
     */
    fun mkdir(path: MServerPath): Throwable? {
        if (!isLoggedIn()) return LoginRequiredException()

        val parts = path.toString().split('/').drop(1)
        val tempPath = MServerPath()

        parts.forEach { part ->
            tempPath.push(part)
            mkdir(conn, tempPath)?.let { return it }
        }

        return null
    }

    /**
     * Account setup function which logs into an existing account on a server as a new device. This
     * prompts device-checking and key exchange. If this call is made and the profile is already
     * connected to a server identity account -- even if pending -- it will return a
     * ResourceExistsException.
     */
    private fun newDeviceLogin(
        address: MAddress, password: String
    ): Result<Pair<WorkspaceStatus, String>> {
        // We don't need to do all the pre-login checks

        val record = KCResolver.getMgmtRecord(address.domain).getOrElse { return it.toFailure() }
        val waddr = address.resolve().getOrElse { return it.toFailure() }

        WorkspaceDAO().checkWorkspaceExists(waddr)
            .getOrElse { return it.toFailure() }
            .onTrue { return ResourceExistsException().toFailure() }

        val serverKey = EncryptionKey.from(record.ek).getOrElse { return it.toFailure() }
        val pwInfo = login(conn, waddr.id, serverKey).getOrElse { return it.toFailure() }
        val hasher = passhasherForInfo(pwInfo)
            .getOrElse { return ServerException(it.toString()).toFailure() }
        hasher.updateHash(password).getOrElse { return it.toFailure() }
        password(conn, hasher, pwInfo)?.let { return it.toFailure() }

        // We need to request the user's card because (a) we'll need it anyway, but (b) because the
        // Encryption-Key field is needed for login.
        val usercard = KCResolver.getKeycard(EntrySubject.fromMAddress(address))
            .getOrElse { return it.toFailure() }
        if (usercard.entries.isEmpty())
            return InvalidKeycardException("User card is empty").toFailure()
        val userEKey = usercard.current!!.getEncryptionKey("Encryption-Key")
            ?: return InvalidKeycardException("Bad/missing encryption key in user card").toFailure()

        val devInfo = DeviceInfo.generate().getOrElse { return it.toFailure() }
        devInfo.encryptAttributes(userEKey).getOrElse { return it.toFailure() }

        val keyHash = userEKey.getPublicHash().getOrElse { return it.toFailure() }
        val rawResult = device(conn, devInfo, keyHash)

        // This should technically never happen, but prepare for success anyway.
        if (rawResult.isSuccess) {
            isAdmin = rawResult.getOrThrow().first
            currentAddress = address
            return Pair(WorkspaceStatus.Active, "").toSuccess()
        }

        val result = rawResult.exceptionOrNull()!!

        // We got this far, so we know that the login credentials are correct. We can safely save
        // all the information to the database for the moment.
        if (result !is ProtocolException || result.code != 105) return result.toFailure()

        val uid = UserID.fromString(usercard.current!!.getFieldString("User-ID"))
        val w = Workspace.newPending(uid, waddr.domain, waddr.id, hasher.getHash(), devInfo)
            .getOrElse { return it.toFailure() }

        // Calling setIdentity() assigns the new workspace to the profile as its identity workspace
        // and saves all the necessary information into the database in the process
        ProfileBroker(ProfileManager.model)
            .setIdentity(w, hasher)?.let { return it.toFailure() }

        ProfileDAO().saveDeviceInfo(waddr, devInfo)?.let { return it.toFailure() }

        // Workspace now is in a pending state, awaiting approval. We're done here.

        return Pair(WorkspaceStatus.Pending, devInfo.approvalCode).toSuccess()
    }

    /**
     * Administrator command which preprovisions a new account on the server.
     *
     * This is a simple command because it is not meant to create a local profile. It is only meant
     * to provision the account on the server side. The administrator receives the information in
     * the PreRegInfo structure and gives it to the user to finish account setup.
     *
     * @exception BadValueException Returned if both the UserID and workspace ID are supplied and
     * the UserID is also a workspace ID.
     */
    fun preregister(uid: UserID?, domain: Domain?, wid: RandomID? = null): Result<PreregInfo> {
        if (!isLoggedIn()) return LoginRequiredException().toFailure()
        if (!isAdmin) return AdminRequiredException().toFailure()
        if (uid == null)
            return preregister(conn, null, null, domain)

        return when (uid.type) {
            IDType.WorkspaceID -> {
                if (wid != null) return BadValueException().toFailure()
                preregister(conn, RandomID.fromUserID(uid)!!, null, domain)
            }

            IDType.UserID -> preregister(conn, wid, uid, domain)
        }
    }

    /**
     * Completes setup of a preregistered account. The call requires information that the account
     * administrator would have given to the user: the account address, the registration code from
     * `preregister()`, and the user's cleartext password.
     *
     * This command initializes the user's local profile and does almost everything needed for the
     * user to get to work. It does _not_ set the user's personal information, such as their name.
     * If the user wants their name in their keycard, this will need to be set before calling
     * `update_keycard()`, which is the final step in setting up a user profile.
     *
     * Note that if the current profile is already associated with a cloud identity workspace, this
     * call will return a ResourceExistsException.
     */
    fun redeemRegcode(address: MAddress, userRegcode: String, pwhash: Password): Throwable? {
        val profile = getActiveProfile().getOrElse { return it }
        if (!profile.isLocal().getOrElse { return it }) return ResourceExistsException()

        if (!isConnected()) connect(address.domain)?.let { return it }

        val encPair = EncryptionPair.generate().getOrElse { return it }
        val devPair = EncryptionPair.generate().getOrElse { return it }
        val devInfo = DeviceInfo(profile.devid, devPair.pubKey, devPair.privKey)
            .collectAttributes().getOrElse { return it }
        devInfo.encryptAttributes(encPair).getOrElse { return it }
        val regInfo = regCode(conn, address, userRegcode, pwhash, devInfo).getOrElse { return it }
        return finalizeRegistration(regInfo, encPair)
    }

    /**
     * Create a new user account on the specified server.
     *
     * There are a lot of ways this method can fail. It will return ErrNoProfile if a user profile
     * has not yet been created. ErrExists will be returned if an individual workspace has already
     * been created in this profile.
     *
     * Note that if the current profile is already associated with a cloud identity workspace, this
     * call will return a ResourceExistsException.
     */
    fun register(dom: Domain, pwhash: Password, uid: UserID?): Throwable? {
        /*
            Process for registration of a new account:

            Check to see if we already have a workspace allocated on this profile. Because we don't
            yet support shared workspaces, it means that there are only individual ones right now.
            Each profile can have only one individual workspace.

            Check active profile for an existing workspace entry
            Get the password from the user
            Check active workspace for device entries. Because we are registering, existing device
                    entries should be removed.
            Add a device entry to the workspace. This includes both an encryption keypair and
                a UUID for the device
            Connect to requested server
            Send registration request to server, which requires a hash of the user's supplied
                    password
            Close the connection to the server
            If the server returns an error, such as 304 REGISTRATION CLOSED, then return an error.
            If the server has returned anything else, including a 101 PENDING, begin the
                    client-side workspace information to generate.
            Generate new workspace data, which includes the associated crypto keys
            Add the device ID and session to the profile and the server
            Create, upload, and cross-sign the first keycard entry
            Create the necessary client-side folders
            Generate the folder mappings

            If the server returned 201 REGISTERED, we can proceed with the server-side setup

            Create the server-side folders based on the mappings on the client side
            Save all encryption keys into an encrypted 7-zip archive which uses the hash of the
            user's password has the archive encryption password and upload the archive to the server.
        */
        val profile = getActiveProfile().getOrElse { return it }
        if (!profile.isLocal().getOrElse { return it }) return ResourceExistsException()

        if (!isConnected()) connect(dom)?.let { return it }
        val encPair = EncryptionPair.generate().getOrElse { return it }
        val devInfo = DeviceInfo.generate().getOrElse { return it }
        devInfo.encryptAttributes(encPair).getOrElse { return it }
        val regData = register(
            conn, uid, pwhash, profile.devid, devInfo.getKeyPair()!!, devInfo.encryptedInfo!!
        ).getOrElse { return it }
        return finalizeRegistration(regData, encPair)
    }

    /**
     * This is administrator command resets the password for a user account. It returns a
     * one-time-use reset code and expiration time. Mensago password resets are very different from
     * other platforms in that the process is designed such that at no time does an administrator
     * know the user's password.
     *
     * The `resetCode` and `expires` parameters are completely optional and exist only to give an
     * administrator the option of choosing the reset code and expiration time. If omitted, the
     * server will generate a secure reset code that will expire in the default period of time
     * configured.
     *
     * Although a server can be configured differently, the platform's expiration time is 60
     * minutes. This value can be as short as 10 minutes and as long as 48 hours. Any value outside
     * this range will result in an error. The resetCode must be at least 8 and at most 128 Unicode
     * code points in length.
     */
    fun resetPassword(
        wid: RandomID, resetCode: String? = null, expires: Timestamp? = null
    ): Result<Pair<String, String>> {

        if (!isLoggedIn()) return LoginRequiredException().toFailure()
        if (!isAdmin) return AdminRequiredException().toFailure()

        return resetPassword(conn, wid, resetCode, expires)
    }

    /**
     * Seals and saves the message to disk and adds to the delivery queue.
     *
     * @exception ResourceNotFoundException If the recipient address is not in the user's address book
     * @exception DataMismatchException If the sender address doesn't match that of the active
     * profile's identity workspace
     * @exception DatabaseException For a corrupted contact key
     */
    fun send(msg: Message, saveToSent: Boolean): Throwable? {
        val profile = getActiveProfile().getOrElse { return it }
        val userAddr = profile.getWAddress().getOrElse { return it }

        if (msg.from != userAddr) return DataMismatchException(
            "Message sender address must match profile identity workspace"
        )

        val recipientKey =
            if (msg.from == msg.to) {
                KeyDAO()
                    .loadKeyByPurpose(KeyPurpose.Encryption).getOrElse { return it }
                    .first.toEncryptionKey().getOrElse { return it }
            } else
                KeyDAO().loadContactKey(msg.to).getOrElse { return it }

        val env = Envelope.seal(
            recipientKey, msg.getAddressPair(), msg.toPayload().getOrElse { return it }
        ).getOrElse { return it }

        val tempPath = profile.ensureTempFolder().getOrElse { return it }
        val envPath = Path(
            tempPath.toString(), env.saveInDir(tempPath.toString()).getOrElse { return it }
        )
        OutgoingHandler.queueMessage(
            envPath, DeliveryTarget(msg.from.domain, msg.from.id), DeliveryTarget(msg.to.domain)
        ).getOrElse { return it }

        return if (saveToSent) queueMsgAsSent(msg, tempPath) else null
    }

    /** Create a contact information update message and send it */
    @Suppress("DuplicatedCode")
    fun sendContactInfoUpdate(address: WAddress, info: Contact): Throwable? {
        val recipientKey = KeyDAO().loadContactKey(address).getOrElse { return it }
        val profile = getActiveProfile().getOrElse { return it }

        val approval = infoUpdate(address, info).getOrElse { return it }
        val env = Envelope.seal(
            recipientKey, approval.getAddressPair(),
            approval.toPayload().getOrElse { return it }
        ).getOrThrow()
        val envData = env.toString()

        val tempPath = profile.ensureTempFolder().getOrElse { return it }

        val tempFilePath = Paths.get(tempPath.toString(), makeEnvelopeFilename(envData.length))
        env.saveWithName(tempFilePath.toString())?.let { return it }

        val sender = profile.getWAddress().getOrElse { return it }
        return OutgoingHandler.queueMessage(
            tempFilePath, DeliveryTarget(sender.domain, sender.id), DeliveryTarget(address.domain)
        ).exceptionOrNull()
    }

    /**
     * Creates a sealed, ready-to-send contact request. NOTE: this call expects the Contact
     * object passed to it to already have its keys populated. This is most easily done by
     * generating a ContactKeySet instance and calling its toPublicMap() method.
     *
     * @param address The address of the recipient.
     * @param info A Contact object containing all of the information the sender wishes to include
     * in the contact request
     * @param msg An optional context message to be included in the contact request. No validation
     * is performed on this text; the caller is expected to validate the text itself and ensure it
     * is safe.
     * @return A sealed Envelope object ready to be queued for delivery
     */
    fun makeContactRequest(address: MAddress, info: Contact, msg: String): Result<Envelope> {

        // Just a sanity check
        getActiveProfile().exceptionOrNull()?.let { return it.toFailure() }

        val recipientCard = getKeycard(address.toString()).getOrElse { return it.toFailure() }
        val entry = recipientCard.current!!
        val recipientKey = entry.getEncryptionKey("Contact-Request-Encryption-Key")
            ?: return BadFieldValueException(
                "Bad contact request key '${
                    entry.getFieldString("Contact-Request-Encryption-Key")
                }' in recipient keycard"
            ).toFailure()
        val waddr = entry.getWAddress()
            ?: return BadFieldValueException("Bad workspace address in recipient keycard")
                .toFailure()
        val creq =
            ContactMessage.requestContact(waddr, info, msg).getOrElse { return it.toFailure() }

        return Envelope.seal(
            recipientKey,
            creq.getAddressPair(),
            creq.toPayload().getOrElse { return it.toFailure() }
        )
    }

    /**
     * Create a contact request and send it. This call internally calls makeContactRequest and, as
     * such, expects the relationship keys to be attached to the Contact object's keys field.
     */
    fun sendContactRequest(address: MAddress, info: Contact, msg: String, saveToSent: Boolean)
            : Result<Thread> {

        val profile = getActiveProfile().getOrElse { return it.toFailure() }
        val wid = KCResolver.resolveMenagoAddress(address).getOrElse { return it.toFailure() }

        val keySet = ContactKeySet.generate().getOrThrow()
        info.keys = keySet.toPublicMap()

        // Just a sanity check

        // We can't just call makeContactRequest() because we still need access to the original
        // CR in order to save a Sent copy.
        val entry = getKeycard(address.toString()).getOrElse { return it.toFailure() }.current!!
        val recipientKey = entry.getEncryptionKey("Contact-Request-Encryption-Key")
            ?: return BadFieldValueException(
                "Bad contact request key '${
                    entry.getFieldString("Contact-Request-Encryption-Key")
                }' in recipient keycard"
            ).toFailure()
        val waddr = entry.getWAddress()
            ?: return BadFieldValueException("Bad workspace address in recipient keycard")
                .toFailure()
        val creq =
            ContactMessage.requestContact(waddr, info, msg).getOrElse { return it.toFailure() }

        val env = Envelope.seal(
            recipientKey,
            creq.getAddressPair(),
            creq.toPayload().getOrElse { return it.toFailure() }
        ).getOrElse { return it.toFailure() }

        val envData = env.toString()

        val tempPath = profile.ensureTempFolder().getOrElse { return it.toFailure() }

        val tempFilePath = Paths.get(tempPath.toString(), makeEnvelopeFilename(envData.length))
        env.saveWithName(tempFilePath.toString())?.let { return it.toFailure() }

        val sender = profile.getWAddress().getOrElse { return it.toFailure() }

        ContactDAO().savePendingCR(WAddress.fromParts(wid, address.domain), keySet)
            ?.let { return it.toFailure() }
        val thread = OutgoingHandler.queueMessage(
            tempFilePath, DeliveryTarget(sender.domain, sender.id), DeliveryTarget(address.domain)
        ).getOrElse { return it.toFailure() }

        if (!saveToSent) return thread.toSuccess()

        queueMsgAsSent(creq, tempPath)?.let { return it.toFailure() }
        return thread.toSuccess()
    }

    /** Convenience method to easily turn Mensago addresses into workspace addresses */
    fun resolveAddress(addr: MAddress): Result<WAddress> {
        WAddress.fromMAddress(addr)?.let { return it.toSuccess() }

        val wid = KCResolver.resolveMenagoAddress(addr).getOrElse { return it.toFailure() }
        return WAddress.fromParts(wid, addr.domain).toSuccess()
    }

    /** Updates the device key for the user's identity workspace */
    fun updateIdentityDeviceKey(): Throwable? {
        if (!isLoggedIn()) return LoginRequiredException()

        val wdao = WorkspaceDAO()
        val profile = getActiveProfile().getOrElse { return it }
        val addr = profile.getWAddress().getOrElse { return it }
        val oldKeypair = wdao.getDeviceKeypair(addr).getOrElse { return it }
        val newKeypair = EncryptionPair.generate().getOrElse { return it }

        val devID = wdao.getDeviceID(addr).getOrElse { return it }
        devKey(conn, devID, oldKeypair, newKeypair)?.let { return it }

        return wdao.updateDeviceKeypair(oldKeypair, newKeypair)
    }

    /**
     * Creates a new entry in the user's keycard. New keys are created and added to the database.
     *
     * If the name parameter is supplied when this call is made, the user's name is updated in the
     * active profile and the keycard is updated using the new value. Passing an empty string to
     * this call will result in the new keycard entry being updated without a name field.
     */
    fun updateKeycard(formattedName: String? = null): Throwable? {

        if (!isLoggedIn()) return LoginRequiredException()

        val profile = getActiveProfile().getOrElse { return it }
        if (formattedName != null) {
            val name = ContactName.fromString(formattedName)
            UserInfoDAO().saveName(name)?.let { return it }
        }

        val waddr = profile.getWAddress().getOrElse { return it }

        // We don't use this until much later, but if we can't get the organization's card, all the
        // other stuff is pointless.
        val orgCard = getCard(conn, null, 0).getOrElse { return it }

        val ovkey = KCResolver.getMgmtRecord(profile.domain).getOrElse { return it }
            .pvk.toVerificationKey().getOrElse {
                return KeyErrorException("Bad key from organization DNS")
            }
        val kdao = KeyDAO()
        val userCard = KeycardDAO().getCard(waddr.toString(), "User", false)
            .getOrElse { return it }

        val keys = kdao.loadKeySet(waddr).getOrElse {
            return DBDataException("Couldn't load user key set in updateKeycard(): $it")
        }

        if (userCard != null) {
            userCard.verify().getOrElse { return it }.onFalse { return VerifyError() }
            val prevHash = userCard.current!!.getAuthString("Hash")!!
            val newKeys = userCard.chain(keys.getCRSPair(), expiration).getOrElse { return it }
            newKeys as UserKeySet
            addEntry(conn, userCard.current!!, ovkey, newKeys.getCRSPair(), prevHash)
                ?.let { return it }
            kdao.saveUserKeySet(waddr, newKeys)?.let { return it }

            return KeycardDAO().updateCard(userCard, false)
        }

        // `cardOpt` is none, so it means that we need to create a new root keycard entry for the
        // user. We also don't need to generate any new keys because that was done when the
        // workspace was provisioned -- just pull them from the database and go. :)

        val entry = UserEntry().apply {
            setFieldInteger("Index", 1)

            val name = UserInfoDAO().loadFormattedName().getOrElse { return it }
            if (name.isNotEmpty())
                setField("Name", name)?.let { return it }
            setField("Workspace-ID", waddr.id.toString())?.let { return it }
            setField("Domain", profile.domain.toString())?.let { return it }
            setField(
                "Contact-Request-Verification-Key",
                keys.getCRSPair().pubKey.toString()
            )?.let { return it }
            setField("Contact-Request-Encryption-Key", keys.getCREPair().pubKey.toString())
                ?.let { return it }
            setField("Encryption-Key", keys.getEPair().pubKey.toString())
                ?.let { return it }
            setField("Verification-Key", keys.getSPair().pubKey.toString())
                ?.let { return it }

            if (profile.uid != null)
                setField("User-ID", profile.uid!!.toString())?.let { return it }
        }

        val prevHash = orgCard.current!!.getAuthString("Hash")!!
        // We don't worry about checking entry compliance because addEntry handles it
        addEntry(conn, entry, ovkey, keys.getCRSPair(), prevHash)?.let { return it }

        val card = Keycard.new("User")!!.apply { entries.add(entry) }
        return KeycardDAO().updateCard(card, false)
    }

    /**
     * Uploads a file to the server, optionally replacing an existing file with the sent one.
     * Upload-with-replace is intended to provide an atomic way of updating files, and there must
     * be enough free space in the workspace's quota to accommodate both files.
     */
    fun upload(
        localPath: String, serverPath: MServerPath, replacePath: MServerPath? = null
    ): Result<Pair<String, Long>> {
        if (!isLoggedIn()) return LoginRequiredException().toFailure()
        return upload(conn, localPath, serverPath, replacePath)
    }

    /**
     * Regains login access to an account using a one-time-use recovery code received from the
     * administrator. Like setPassword(), this method does not store the supplied password hash
     * anywhere, as a general hash storage solution will not be as secure as one customized to the
     * calling application's usage.
     */
    fun useRecoveryCode(wid: RandomID, recoveryCode: String, pwhash: Password): Throwable? {
        if (!isConnected()) return NotConnectedException()

        return passCode(conn, wid, recoveryCode, pwhash)
    }

    /**
     * Internal method which finishes all the profile and workspace setup common to standard
     * registration and registration via a code.
     */
    private fun finalizeRegistration(regInfo: RegInfo, encPair: EncryptionPair): Throwable? {
        val profile = getActiveProfile().getOrElse { return it }

        val infoPair = with(regInfo) {
            Workspace.generate(uid, domain, wid, pwhash.getHash()).getOrElse { return it }
        }
        infoPair.first.devid = profile.devid

        // Calling setIdentity() assigns the new workspace to the profile as its identity workspace
        // and saves all the necessary information into the database in the process
        with(ProfileBroker(ProfileManager.model)) {
            setIdentity(infoPair.first, regInfo.pwhash)?.let { return it }
            setKeyData(infoPair.first.waddress, infoPair.second)?.let { return it }
        }
        profile.status = WorkspaceStatus.Active

        login(regInfo.getAddress()).getOrElse { return it }

        val kdao = KeyDAO()
        kdao.updateKeypairByPurpose(infoPair.first.waddress, encPair, KeyPurpose.Encryption)
            ?.let { return it }

        // NOTE: we do not update the keycard here because the caller needs to do this. The user
        // may need/want to set their name information for their keycard and this must be done
        // before dealing with their keycard

        return null
    }

    private fun queueMsgAsSent(msg: MessageInterface, filePath: Path): Throwable? {

        val storageKey = KeyDAO().loadKeyByPurpose(KeyPurpose.Storage)
            .getOrElse { return it }
            .first.toSecretKey().getOrElse { return it }

        val sentEnv = Envelope.seal(
            storageKey, null, msg.toPayload().getOrElse { return it }
        ).getOrElse { return it }

        val sentEnvPath = Path(
            filePath.toString(), sentEnv.saveInDir(filePath.toString()).getOrElse { return it }
        )

        val wid = getActiveProfile().getOrElse { return it }.wid
        val sentPath = MServerPath().push(wid.toString()).getOrElse { return it }
            .push("out").getOrElse { return it }

        val msgItem = PayloadConverter().toMsgItem(msg).getOrElse { return it }

        MessageDAO().importMsgItem(msgItem, sentPath, "Sent")?.let { return it }
        return OutgoingHandler.queueUpload(sentEnvPath, sentPath).exceptionOrNull()
    }
}
