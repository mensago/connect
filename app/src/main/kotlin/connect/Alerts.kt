package connect

import connect.common.ExceptionDialog

fun showFatalError(msg: String, e: Throwable? = null) {
    println(msg)
    e?.printStackTrace()
    ExceptionDialog(msg, e).showAndWait()
}
