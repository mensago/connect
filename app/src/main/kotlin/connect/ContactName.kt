package connect

data class ContactName(
    var formattedName: String = "",
    var givenName: String? = null,
    var familyName: String? = null,
    var middleName: String? = null,
    var prefix: String? = null,
    var suffix: String? = null,
) {
    companion object {

        /**
         * Parses a string containing a formatted name and turns it into a ContactName object.
         * Eventually this call will support different parsing algorithms to accommodate differences
         * in locale.
         *
         * The current implementation is just a simple placeholder that flags the first item as a
         * prefix if it ends in a period and anything following a comma as suffix.
         */
        fun fromString(name: String): ContactName {
            return parseGivenFamilyNameOrder(name)
        }
    }
}

/**
 * Parses a formatted name using the format "prefix givenName familyName, suffix1, suffix2". Because
 * in Mensago Connect's usage context it is unlikely that a middle name will be used, we expect
 * that having more than the expected number of words will indicate that the user's last name has
 * multiple words, such as "van der Lek".
 *
 * The user can work around these assumptions by joining together words with underscores.
 */
fun parseGivenFamilyNameOrder(s: String): ContactName {
    if (s.isEmpty()) return ContactName()
    val out = ContactName(s.replace("_", " "))

    val suffixParts = s.split(",").map { it.trim() }
    if (suffixParts.size > 1)
        out.suffix = suffixParts.subList(1, suffixParts.size).joinToString(", ")

    val parts = suffixParts[0].split(" ").map { it.trim() }
    when (parts.size) {
        1 -> out.givenName = parts[0].replace("_", " ")
        2 -> {
            if (parts[0].endsWith(".")) {
                out.prefix = parts[0].replace("_", " ")
                out.givenName = parts[1].replace("_", " ")
            } else {
                out.givenName = parts[0].replace("_", " ")
                out.familyName = parts[1].replace("_", " ")
            }
        }

        else -> {
            if (parts[0].endsWith(".")) {
                out.prefix = parts[0].replace("_", " ")
                out.givenName = parts[1].replace("_", " ")
                out.familyName = parts[2].replace("_", " ")
            } else {
                out.givenName = parts[0].replace("_", " ")
                out.familyName = parts.subList(1, parts.size)
                    .joinToString(" ")
                    .replace("_", " ")
            }
        }
    }

    return out
}
