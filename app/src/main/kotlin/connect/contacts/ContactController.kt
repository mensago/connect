package connect.contacts

import connect.Filter
import connect.FilterableItem
import connect.contacts.aboutme.AboutMeController
import connect.contacts.newconreq.NewCRController
import connect.main.MainModel
import javafx.scene.Scene
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.stage.Stage
import javafx.stage.StageStyle
import libmensago.MServerPath
import utilities.Trace
import utilities.traceEvent

class ContactController(mainModel: MainModel) {

    private val model = ContactModel()
    private val interactor = ContactInteractor(model)
    private val viewBuilder = ContactViewBuilder(model, mainModel, getHandlers())
    private val aboutMeController = AboutMeController()
    private val newCRController = NewCRController()

    /** Instructs the controller to register its view with the main model's mode info structure */
    fun registerView() {
        viewBuilder.build()
    }

    /** Load a new data item */
    fun loadNewItem(item: ContactItem): Throwable? = interactor.addContact(item)

    /** Changes an item's server-side path */
    fun moveItem(oldPath: MServerPath, newPath: MServerPath) =
        interactor.moveContact(oldPath, newPath)

    /** Replaces the item at the specified path with the new one */
    fun replaceItem(oldItem: MServerPath, newItem: FilterableItem) =
        interactor.replaceContact(oldItem, newItem)

    /** Updates contact information in the database for the contact passed to the call. */
    fun updateInfo(info: FilterableItem): Throwable? = interactor.updateInfo(info)

    private fun handleFilterSelection(oldItem: Filter?, newItem: Filter?) {
        traceEvent(
            Trace.ContactController,
            "ContactController: handleFilterSelection($oldItem, $newItem)"
        )
        interactor.selectFilter(newItem)
    }

    private fun handleContactSelection(oldItem: ContactHeaderItem?, newItem: ContactHeaderItem?) {
        traceEvent(
            Trace.ContactController,
            "ContactController: handleContactSelection($oldItem, $newItem)"
        )
        interactor.selectContact(newItem)
    }

    private fun editUserInfo() {
        traceEvent(Trace.ContactController, "ContactController: editUserInfo()")

        aboutMeController.reloadState()
        val view = aboutMeController.getView()
        val sc = Scene(view)
        val stage = Stage(StageStyle.DECORATED).apply {
            VBox.setVgrow(view, Priority.ALWAYS)
            scene = sc
            title = "About Me"
            minWidth = 400.0
            minHeight = 400.0
            isResizable = false
        }
        sc.addEventHandler(KeyEvent.KEY_PRESSED) { event ->
            if (event.code == KeyCode.ESCAPE)
                stage.close()
        }
        stage.show()
    }

    private fun showNewContactRequest() {
        traceEvent(Trace.ContactController, "ContactController: showNewContactRequest()")

        val view = newCRController.getView()
        val sc = Scene(view)
        val stage = Stage(StageStyle.DECORATED).apply {
            VBox.setVgrow(view, Priority.ALWAYS)
            scene = sc
            title = "Send Contact Request"
            minWidth = 400.0
            minHeight = 400.0
            isResizable = false
        }
        sc.addEventHandler(KeyEvent.KEY_PRESSED) { event ->
            if (event.code == KeyCode.ESCAPE)
                stage.close()

            if (event.code == KeyCode.ENTER && event.isControlDown) {
                newCRController.sendCR()
                stage.close()
            }
        }
        stage.show()
    }

    private fun getHandlers(): ContactViewHandlers {
        return ContactViewHandlers(
            this::handleFilterSelection, this::handleContactSelection,
            this::editUserInfo, this::showNewContactRequest
        )
    }
}
