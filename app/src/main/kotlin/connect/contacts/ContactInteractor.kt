package connect.contacts

import connect.Filter
import connect.FilterableItem
import connect.messages.ConMsgItem
import keznacl.BadValueException
import libkeycard.WAddress
import libmensago.MServerPath
import libmensago.ResourceNotFoundException
import utilities.Trace
import utilities.toObservable
import utilities.traceEvent

class ContactInteractor(private val model: ContactModel) {
    private val broker = ContactBroker()

    init {
        loadLabels()
        addDemoData()
    }

    /** Adds a new contact from existing content */
    fun addContact(contact: ContactItem): Throwable? {
        traceEvent(
            Trace.ContactInteractor,
            "ContactInteractor: add contact ${contact.toDisplayString()}"
        )
        broker.save(contact)?.let { return it }
        model.contactList.add(contact.toHeader())
        return null
    }

    /** Changes an item's server-side path */
    fun moveContact(oldPath: MServerPath, newPath: MServerPath) {
        traceEvent(
            Trace.ContactInteractor,
            "ContactInteractor: move contact from $oldPath to $newPath"
        )
        broker.moveContact(oldPath, newPath)?.let { throw it }
    }

    /** Replaces the item at the specified path with the new one */
    fun replaceContact(oldItem: MServerPath, newItem: FilterableItem) {
        broker.deleteFromPath(oldItem)?.let { throw it }
        if (newItem is ContactItem)
            addContact(newItem)
    }

    fun loadLabels() {
        traceEvent(Trace.ContactInteractor, "ContactInteractor: load labels")
        model.filterListProperty.set(broker.loadLabels().getOrThrow().toObservable())
    }

    fun addDemoData() {
        traceEvent(Trace.ContactInteractor, "ContactInteractor: add demo data")
        broker.addDemoDataToDB()
        model.contactListProperty.set(broker.loadHeaders(null).getOrThrow().toObservable())
    }

    fun selectFilter(newFilter: Filter?) {
        if (newFilter == model.selectedFilterProperty.get()) return

        traceEvent(Trace.ContactInteractor, "ContactInteractor: select filter $newFilter")
        model.contactListProperty.set(broker.loadHeaders(newFilter).getOrThrow().toObservable())
        model.selectedFilterProperty.set(newFilter)
    }

    fun selectContact(newItem: ContactHeaderItem?) {
        traceEvent(Trace.ContactInteractor, "ContactInteractor: select contact $newItem")

        if (newItem == null) {
            model.selectedContactProperty.set(ContactItem())
            return
        }

        val contact = broker.load(newItem.id).getOrThrow() ?: run {
            model.selectedContactProperty.set(ContactItem())
            return
        }
        model.selectedContactProperty.set(contact)
    }

    fun updateInfo(filterable: FilterableItem): Throwable? {
        if (filterable !is ContactItem && filterable !is ConMsgItem)
            return BadValueException("Attempt to update contact info with non-contact item $filterable")

        val item = if (filterable is ContactItem) filterable
        else {
            filterable as ConMsgItem
            ContactItem.fromContact(filterable.contact, true).getOrElse { return it }
        }

        val waddr = WAddress.fromString(item.waddress)
            ?: return BadValueException("Incoming contact information has bad workspace address")
        val condao = ContactDAO()
        val loaded = condao.conItemForWAddress(waddr).getOrElse { return it }
            ?: return ResourceNotFoundException("Contact $filterable not found for info update")
        loaded.updateFrom(item)
        condao.saveConItem(loaded)
        return null
    }
}
