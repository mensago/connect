package connect.contacts

import connect.*
import connect.sync.DeliveryTarget
import javafx.scene.image.Image
import keznacl.*
import libkeycard.*
import libmensago.Contact
import libmensago.MServerPath
import libmensago.MimeType
import libmensago.ResourceNotFoundException
import libmensago.db.DatabaseException
import libmensago.db.KDB
import utilities.*
import java.io.ByteArrayInputStream
import java.sql.ResultSet

class ContactDAO {

    /** A Filter instance for excluding the user's contact information */
    private val filterUserInfo = Filter(
        "ExcludeUserInfo",
        mutableListOf(FilterTerm("id", FilterComparator.NotContains, gUserConID.toString()))
    )

    /**
     * Sets up all the necessary tables for storing contact information in the database and also
     * adds a mostly-empty record for the user's information.
     */
    fun initContactTables(): Throwable? {
        with(DBProxy.get()) {
            contactInitCmds.forEach {
                add(it)?.let { e -> return e }
            }
            executeBatch()?.let { return it }
            execute(
                "MERGE INTO contacts(id,entitytype,ismanaged) KEY(id) VALUES(?,?,?)", gUserConID,
                Contact.EntityType.Individual, false
            )?.let { return it }
        }
        return null
    }

    /* -------------------------------------------------------------------------- */

    /** Returns true if the user has a pending contact request with the recipient address. */
    fun hasPendingCR(address: WAddress): Result<Boolean> {
        val db = DBProxy.get()
        val rs = db.query(
            "SELECT public FROM contact_requests WHERE address=? AND purpose='relmyencryption'",
            address
        ).getOrElse { return it.toFailure() }
        return rs.next().toSuccess()
    }

    /**
     * Returns the relationship keys associated with a pending contact request
     *
     * @exception ResourceNotFoundException If the relationship is not found in the database
     */
    fun loadPendingCRKeys(address: WAddress): Result<ContactKeySet> {
        val db = DBProxy.get()
        var rs = db.query(
            "SELECT * FROM contact_requests WHERE address=? AND purpose='relmyencryption'",
            address
        ).getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }

        val encPair = kotlin.runCatching {
            EncryptionPair.fromStrings(
                rs.getString("public"), rs.getString("private")
            ).getOrElse { return it.toFailure() }
        }.getOrElse { return it.toFailure() }

        rs = db.query(
            "SELECT * FROM contact_requests WHERE address=? AND purpose='relmysigning'", address
        ).getOrElse { return it.toFailure() }
        rs.next().onFalse { return ResourceNotFoundException().toFailure() }

        val signPair = kotlin.runCatching {
            SigningPair.fromStrings(
                rs.getString("public"), rs.getString("private")
            ).getOrElse { return it.toFailure() }
        }.getOrElse { return it.toFailure() }
        return ContactKeySet(encPair, signPair).toSuccess()
    }

    /**
     * Saves all the information about a pending contact request. This is necessary because when
     * a CR is sent, there is no contact ID to associate the data with -- the other party supplies
     * the contact ID. It also gives Connect the capability of notifying the user if there is an
     * outstanding CR for an address.
     */
    fun savePendingCR(address: WAddress, keys: ContactKeySet): Throwable? {
        val db = DBProxy.get()
        db.execute(
            "MERGE INTO contact_requests(address,purpose,private,public,timestamp) " +
                    "KEY(address,purpose) VALUES(?,?,?,?,?)",
            address, KeyPurpose.RelMyEncryption, keys.encryptionPair.privKey,
            keys.encryptionPair.pubKey, Timestamp()
        )?.let { return it }

        db.execute(
            "MERGE INTO contact_requests(address,purpose,private,public,timestamp) " +
                    "KEY(address,purpose) VALUES(?,?,?,?,?)",
            address, KeyPurpose.RelMySigning, keys.signingPair.privKey, keys.signingPair.pubKey,
            Timestamp()
        )?.let { return it }

        return null
    }

    /* -------------------------------------------------------------------------- */

    /** Adds a workspace to the user's blocklist */
    fun blockContact(waddr: WAddress): Throwable? {
        DBProxy.get().execute(
            "MERGE INTO blocklist(id,wid,domain) KEY(wid,domain) VALUES(?,?,?)",
            RandomID.generate(), waddr.id, waddr.domain
        )?.let { return it }
        return null
    }

    /** Adds a domain to the user's blocklist */
    fun blockDomain(domain: Domain): Throwable? {
        DBProxy.get().execute(
            "MERGE INTO blocklist(id,domain) KEY(domain) VALUES(?,?)",
            RandomID.generate(), domain
        )?.let { return it }
        return null
    }

    /** Looks up a ContactItem given a specific WAddress. */
    fun conItemForWAddress(waddr: WAddress): Result<ContactItem?> {
        val db = DBProxy.get()
        val rs = db.query(
            "SELECT id FROM contacts WHERE wid=? AND domain=?", waddr.id,
            waddr.domain
        ).getOrElse { return Result.failure(it) }
        rs.next().onFalse { return Result.success(null) }

        val id = rs.getString("id")
        return loadConItem(id)
    }

    /** Returns a list of blocked workspaces and domains */
    fun loadBlockList(): Result<List<DeliveryTarget>> {
        val db = DBProxy.get()
        val rs = db.query("SELECT wid,domain FROM blocklist ORDER BY domain")
            .getOrElse { return it.toFailure() }
        val out = mutableListOf<DeliveryTarget>()
        while (rs.next()) {
            val domain = Domain.fromString(rs.getString("domain"))
                ?: return DatabaseException("Bad domain in blocklist").toFailure()
            val rawWID = rs.getString("wid")
            if (rawWID != null) {
                val wid = RandomID.fromString(rawWID)
                    ?: return DatabaseException("Bad domain in blocklist").toFailure()
                out.add(DeliveryTarget(domain, wid))
            } else out.add(DeliveryTarget(domain))
        }
        return out.toSuccess()
    }

    /**
     * Loads a list of label-based filters. This includes several predefined filters:
     * - 'All Contacts' - all contacts in the database, including the user
     * - 'Auto-Updated' - all contact entries which are not managed by the user
     * - 'Self-Managed' - contact entries made and managed by the user
     * - 'Trash' - contact entries which are one step from being permanently deleted
     */
    @Suppress("DuplicatedCode")
    fun loadLabelFilters(): Result<List<Filter>> {
        traceEvent(Trace.ContactDAO, "ContactDAO: loading label filters")
        val out = mutableListOf(
            Filter("All Contacts").apply {
                readOnly = true
                sorter = FilterSort("formatted_name", SortOrder.Ascending)
                add("-labels:Trash")
            },
            Filter("Auto-Updated").apply {
                readOnly = true
                sorter = FilterSort("formatted_name", SortOrder.Ascending)
                add("ismanaged:true")
                add("-labels:Trash")
            },
            Filter("Self-Managed").apply {
                readOnly = true
                sorter = FilterSort("formatted_name", SortOrder.Ascending)
                add("ismanaged:false")
                add("-labels:Trash")
            },
            Filter("Trash").apply {
                readOnly = true
                sorter = FilterSort("formatted_name", SortOrder.Ascending)
                add("labels:Trash")
            }
        )

        val db = DBProxy.get()

        val rs = db.query("SELECT labels FROM contacts").getOrElse { return Result.failure(it) }

        val labelSet = mutableSetOf<String>()
        while (rs.next()) {
            val labels = rs.getString("labels")?.split(",")?.toSet() ?: continue
            labelSet.addAll(labels)
        }
        labelSet.remove("")
        labelSet.remove("Trash")

        val filterList = labelSet.sorted().map { l ->
            Filter(l).apply {
                add("labels:$l")
                add("-labels:Trash")
            }
        }
        out.addAll(filterList)
        return out.toSuccess()
    }

    /** Loads a list of ContactHeaderItems based on the filter passed */
    fun loadHeaders(filter: Filter? = filterUserInfo): Result<List<ContactHeaderItem>> {
        traceEvent(Trace.ContactDAO, "ContactDAO: loadHeaders(${filter?.name})")

        return if (filter != null) {
            if (filter === filterUserInfo)
                queryHeaders(filterUserInfo)
            else {
                val clone = filter.clone()
                clone.terms.add(
                    FilterTerm("id", FilterComparator.NotContains, gUserConID.toString())
                )
                queryHeaders(clone)
            }
        } else queryHeaders(null)
    }

    /** Loads a ContactItem matching the specified id */
    fun loadConItem(id: String): Result<ContactItem?> {
        traceEvent(Trace.ContactDAO, "ContactDAO: loading contents for $id")
        val db = DBProxy.get()
        val rs = db.query("SELECT * FROM contacts WHERE id=?", id)
            .getOrElse { return it.toFailure() }

        if (!rs.next())
            return Result.success(null)

        return conItemFromQuery(id, rs)
    }

    fun loadConItemByName(formattedName: String): Result<ContactItem?> {
        traceEvent(Trace.ContactDAO, "ContactDAO: loading contents for $formattedName")
        val db = DBProxy.get()
        val rs = db.query("SELECT * FROM contacts WHERE formatted_name=?", formattedName)
            .getOrElse { return it.toFailure() }

        if (!rs.next())
            return Result.success(null)

        val id = runCatching { rs.getString("id") }.getOrElse { return it.toFailure() }
        return conItemFromQuery(id, rs)
    }

    /** Loads a Contact instance matching the specified id */
    fun loadContact(id: String): Result<Contact?> {
        traceEvent(Trace.ContactDAO, "ContactDAO: loading contact for $id")
        val db = DBProxy.get()
        val rs = db.query("SELECT * FROM contacts WHERE id=?", id)
            .getOrElse { return it.toFailure() }

        if (!rs.next())
            return Result.success(null)

        return runCatching {

            var tempStr = rs.getString("entitytype") ?: "person"
            val entityType = Contact.EntityType.fromString(tempStr)
                ?: return DatabaseException("bad entity type $tempStr").toFailure()

            val managed = rs.getBoolean("ismanaged")
            tempStr = rs.getString("uid") ?: ""

            val uid = if (managed) {
                if (tempStr.isNotEmpty()) {
                    UserID.fromString(tempStr)
                        ?: return DatabaseException("bad user ID $tempStr").toFailure()
                } else UserID.fromString(tempStr)
            } else UserID.fromString(tempStr)

            val wid = if (managed) {
                RandomID.fromString(rs.getString("wid"))
                    ?: return DatabaseException("bad workspace ID $tempStr").toFailure()
            } else RandomID.fromString(tempStr)

            val domain = if (managed) {
                Domain.fromString(rs.getString("domain"))
                    ?: return DatabaseException("bad domain $tempStr").toFailure()
            } else Domain.fromString(tempStr)

            tempStr = rs.getString("orgunits") ?: ""
            val orgunits = if (tempStr.isNotEmpty())
                tempStr.split(",").map { it.trim() }.toMutableList()
            else null

            tempStr = rs.getString("languages") ?: ""
            val languages = if (tempStr.isNotEmpty())
                tempStr.split(",").map { it.trim() }.toMutableList()
            else null

            Contact(
                entityType = entityType,
                formattedName = rs.getString("formatted_name")
                    ?: return DatabaseException("empty formatted name for $id").toFailure(),
                update = managed,
                givenName = rs.getString("given_name") ?: "",
                familyName = rs.getString("family_name") ?: "",
                middleName = rs.getString("middle_name") ?: "",
                prefix = rs.getString("prefix") ?: "",
                suffix = rs.getString("suffix") ?: "",
                gender = rs.getString("gender") ?: "",
                bio = rs.getString("bio") ?: "",

                social = loadLabeledField(id, "social")
                    .getOrElse { return it.toFailure() },
                phone = loadLabeledField(id, "phone")
                    .getOrElse { return it.toFailure() },
                websites = loadLabeledField(id, "website")
                    .getOrElse { return it.toFailure() },

                userID = uid,
                workspace = wid,
                domain = domain,

                mail = loadMailingAddresses(id).getOrElse { return it.toFailure() },
                email = loadLabeledField(id, "email")
                    .getOrElse { return it.toFailure() },

                anniversary = rs.getString("anniversary") ?: "",
                birthday = rs.getString("birthday") ?: "",
                organization = rs.getString("organization") ?: "",
                orgUnits = orgunits,
                title = rs.getString("title") ?: "",
                languages = languages,
                photo = loadContactPhoto(rs).getOrElse { return it.toFailure() },
            )
        }
    }

    /** Saves the information in a ContactItem to the database */
    fun saveConItem(contact: ContactItem): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: Saving $contact")

        var photoType: MimeType? = null
        var photoBytes: ByteArray? = null
        if (contact.photo != null) {
            imageToBytes(contact.photo).getOrElse { return it }?.let {
                photoType = it.first
                photoBytes = it.second
            }
        }
        with(DBProxy.get()) {
            execute(
                "MERGE INTO contacts(id,entitytype,ismanaged,labels,formatted_name,given_name," +
                        "family_name,middle_name,prefix,suffix,gender,uid,wid,domain,bio," +
                        "anniversary,birthday,organization,orgunits,title,languages," +
                        "notes,server_path,format,phototype,photo) KEY(id) " +
                        "VALUES(?,?,?,?,?,?," +
                        "?,?,?,?,?,?,?,?,?," +
                        "?,?,?,?,?,?," +
                        "?,?,?,?,?)",
                contact.id, contact.entityType, contact.isManaged, contact.getLabelString(),
                contact.formattedName, contact.givenName,

                contact.familyName, contact.middleName, contact.prefix, contact.suffix,
                contact.gender, contact.uid, contact.wid, contact.domain, contact.bio,

                contact.anniversary, contact.birthday, contact.organization, contact.orgUnits,
                contact.title, contact.languages,

                contact.contents, contact.serverPath, contact.format, photoType?.toString() ?: "",
                photoBytes ?: KDB.NullVarBinary()
            )?.let { return it }
        }
        saveAddresses(contact.id, contact.addressListProperty.get())?.let { return it }
        saveKeys(contact.id, contact.keyListProperty.get())?.let { return it }
        saveMultiInfo(contact.id, "phone", contact.phoneListProperty)
        saveMultiInfo(contact.id, "email", contact.emailListProperty)
        saveMultiInfo(contact.id, "social", contact.socialListProperty)
        saveMultiInfo(contact.id, "website", contact.websiteListProperty)
        return null
    }

    /** Deletes a contact based on its specified server path */
    fun deleteFromPath(path: MServerPath): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: delete from path $path")
        with(DBProxy.get()) {
            execute("DELETE FROM contacts WHERE server_path=?", path)?.let { return it }
        }
        return null
    }

    /** Deletes a contact based on its unique RandomID */
    fun delete(conid: RandomID): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: delete contact from id $conid")
        with(DBProxy.get()) {
            execute("DELETE FROM contact_multi WHERE conid=?", conid)?.let { return it }
            execute("DELETE FROM contacts WHERE id=?", conid)?.let { return it }
        }
        return null
    }

    /**
     * Bookkeeping method to 'move' a contact's server-side path. It exists just to ensure device
     * sync works correctly.
     */
    fun moveContact(oldPath: MServerPath, newPath: MServerPath): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: moving contact from $oldPath to $newPath")
        with(DBProxy.get()) {
            execute(
                "UPDATE contacts SET server_path=? WHERE server_path=?", newPath, oldPath
            )?.let { return it }
        }
        return null
    }

    /** Adds a label to a contact */
    fun addLabel(note: ContactHeaderItem, label: String): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: adding label $label to $note")
        with(DBProxy.get()) {
            note.addLabel(label)
            execute("UPDATE contacts SET labels=? WHERE id=?", note.getLabelString(), note.id)
                ?.let { return it }
        }
        return null
    }

    /** Removes a label from a contact */
    fun removeLabel(note: ContactHeaderItem, label: String): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: removing label $label from $note")
        with(DBProxy.get()) {
            note.removeLabel(label)
            execute("UPDATE contacts SET labels=? WHERE id=?", note.getLabelString(), note.id)
                ?.let { return it }
        }
        return null
    }

    /** Empties the trash */
    fun emptyTrash(): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: emptying trash")
        with(DBProxy.get()) {
            execute("DELETE FROM contacts WHERE labels ilike '%trash%'")?.let { return it }
        }
        return null
    }

    /** Adds a Contact instance to the database */
    fun import(contact: Contact): Throwable? {
        traceEvent(Trace.ContactDAO, "ContactDAO: updating contact $contact")

        var photoType: MimeType? = null
        var photoBytes: ByteArray? = null
        if (contact.photo != null) {
            getImageData(contact.photo!!.mime, contact.photo!!.data).getOrElse { return it }.let {
                photoType = it.first
                photoBytes = it.second
            }
        }

        with(DBProxy.get()) {
            execute(
                "MERGE INTO contacts(id,entitytype,ismanaged,labels,formatted_name,given_name," +
                        "family_name,middle_name,prefix,suffix,gender,uid,wid,domain,bio," +
                        "anniversary,birthday,organization,orgunits,title,languages," +
                        "notes,server_path,format,phototype,photo) KEY(id) " +
                        "VALUES(?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?)",
                contact.id, contact.entityType,
                contact.custom?.get("Managed")?.let { it == "true" } ?: false,
                "", contact.formattedName, contact.givenName ?: "",

                contact.familyName ?: "", contact.middleName ?: "", contact.prefix ?: "",
                contact.suffix ?: "", contact.gender ?: "", contact.userID?.toString() ?: "",
                contact.workspace?.toString() ?: "", contact.domain?.toString() ?: "",
                contact.bio ?: "",

                contact.anniversary ?: "", contact.birthday ?: "", contact.organization ?: "",
                contact.orgUnits?.joinToString(",") ?: "", contact.title ?: "",
                contact.languages?.joinToString(",") ?: "",

                "", "", "plain", photoType ?: "", photoBytes ?: KDB.NullVarBinary()
            )?.let { return it }
        }
        val idstr = contact.id.toString()
        contact.mail?.map { AddressInfo.fromMailingAddress(idstr, it) }
            ?.let { saveAddresses(idstr, it)?.let { e -> return e } }

        contact.phone?.map {
            MultiInfo(
                RandomID.generate().toString(), idstr, it.field, "phone", it.value,
                it.preferred ?: false
            )
        }?.let { saveMultiInfo(idstr, "phone", it)?.let { e -> return e } }

        contact.email?.map {
            MultiInfo(
                RandomID.generate().toString(), idstr, it.field, "email", it.value,
                it.preferred ?: false
            )
        }?.let { saveMultiInfo(idstr, "email", it)?.let { e -> return e } }

        contact.social?.map {
            MultiInfo(
                RandomID.generate().toString(), idstr, it.field, "social", it.value,
                it.preferred ?: false
            )
        }?.let { saveMultiInfo(idstr, "social", it)?.let { e -> return e } }

        contact.websites?.map {
            MultiInfo(
                RandomID.generate().toString(), idstr, it.field, "website", it.value,
                it.preferred ?: false
            )
        }?.let { saveMultiInfo(idstr, "website", it)?.let { e -> return e } }

        if (contact.keys != null)
            importContactKeys(idstr, contact.keys!!)?.let { return it }

        return null
    }

    private fun importContactKeys(conid: String, keys: Map<String, String>): Throwable? {
        val info = mutableListOf<ContactKeyInfo>()
        if (keys.containsKey("Encryption")) {
            info.add(
                ContactKeyInfo(
                    RandomID.generate().toString(), conid, true,
                    KeyPurpose.RelEncryption, keys["Encryption"]!!, Timestamp().toString(),
                )
            )
        }
        if (keys.containsKey("Verification")) {
            info.add(
                ContactKeyInfo(
                    RandomID.generate().toString(), conid, true,
                    KeyPurpose.RelSigning, keys["Verification"]!!, Timestamp().toString(),
                )
            )
        }
        return saveKeys(conid, info)
    }

    private fun loadAddresses(conid: String): Result<MutableList<AddressInfo>> {
        val out = mutableListOf<AddressInfo>()
        with(DBProxy.get()) {
            val rs = query("SELECT * FROM contact_addresses WHERE conid=? ORDER BY label", conid)
                .getOrElse { return it.toFailure() }

            while (rs.next()) {
                out.add(
                    AddressInfo(
                        rs.getString("id") ?: "",
                        conid,
                        rs.getString("label") ?: "",
                        rs.getString("street") ?: "",
                        rs.getString("extended") ?: "",
                        rs.getString("locality") ?: "",
                        rs.getString("region") ?: "",
                        rs.getString("postalcode") ?: "",
                        rs.getString("country") ?: "",
                        rs.getBoolean("preferred")
                    )
                )
            }
        }
        return out.toSuccess()
    }

    private fun saveAddresses(conid: String, addresses: List<AddressInfo>?): Throwable? {
        if (addresses == null) return null

        with(DBProxy.get()) {
            execute("DELETE FROM contact_addresses WHERE conid=?", conid)
                ?.let { return it }

            addresses.forEach {
                execute(
                    "INSERT INTO contact_addresses(id,conid,label,street,extended,locality," +
                            "region,postalcode,country,preferred) VALUES(?,?,?,?,?,?,?,?,?,?)",
                    it.id, conid, it.label, it.street, it.extended, it.locality,
                    it.region, it.postalCode, it.country, it.isPreferred
                )?.let { e -> return e }
            }
        }
        return null
    }

    private fun loadKeys(id: String): Result<MutableList<ContactKeyInfo>> {
        val out = mutableListOf<ContactKeyInfo>()
        with(DBProxy.get()) {
            val rs = query("SELECT * FROM keys WHERE conid=? ORDER BY purpose", id)
                .getOrElse { return it.toFailure() }

            while (rs.next()) {
                val conid = rs.getString("conid") ?: ""

                val purposeStr = rs.getString("purpose")
                val purpose = KeyPurpose.fromString(purposeStr)
                    ?: return BadValueException("Bad key purpose $purposeStr").toFailure()
                val timestamp = rs.getString("timestamp") ?: ""

                rs.getString("public")?.let {
                    out.add(
                        ContactKeyInfo(id, conid, true, purpose, it, timestamp)
                    )
                }
                rs.getString("private")?.let {
                    out.add(
                        ContactKeyInfo(id, conid, false, purpose, it, timestamp)
                    )
                }
            }
        }
        return out.toSuccess()
    }

    private fun saveKeys(conid: String, keyList: List<ContactKeyInfo>): Throwable? {
        with(DBProxy.get()) {
            execute(
                "DELETE FROM keys WHERE conid=? AND NOT purpose IN (?,?)",
                conid, KeyPurpose.RelMySigning, KeyPurpose.RelMyEncryption
            )?.let { return it }

            keyList.forEach {
                val keyCS = CryptoString.fromString(it.key)
                    ?: return KeyErrorException("Bad key ${it.key}")

                val keyType = CryptoType.fromString(keyCS.prefix)
                    ?: return UnsupportedAlgorithmException("Unsupported key algorithm ${keyCS.prefix}")

                val typeStr = when {
                    keyType.isSigning() -> "signing"
                    keyType.isAsymmetric() -> "asymmetric"
                    keyType.isSymmetric() -> "symmetric"
                    else -> return UnsupportedAlgorithmException("Unsupported key type $keyType")
                }

                val keyHash = hash(it.key.encodeToByteArray()).getOrElse { e -> return e }
                if (it.isPublic) {
                    execute(
                        "MERGE INTO keys(id,keyid,conid,type,purpose,public,timestamp,status) " +
                                "KEY(id) VALUES(?,?,?,?,?,?,?,'active')",
                        it.id, keyHash, conid, typeStr, it.purpose, it.key, it.timestamp
                    )?.let { e -> return e }
                } else {
                    // We can't just blindly merge in the private key here because when there is
                    // both a public and private key in the same record, the hash stored in the row
                    // is the hash of the public key. The only time the hash column would contain
                    // the hash of the private key is for symmetric keys, where the value is stored
                    // in both the private and public columns.
                    val rs = query("SELECT COUNT(*) FROM keys WHERE id=?", it.id)
                        .getOrElse { e -> return e }
                    if (rs.next()) {
                        if (rs.getInt(1) > 0) {
                            execute(
                                "MERGE INTO keys(id,conid,type,purpose,private,timestamp,status) " +
                                        "KEY(id) VALUES(?,?,?,?,?,?,'active')",
                                it.id, conid, typeStr, it.purpose, it.key, it.timestamp
                            )?.let { e -> return e }
                        } else {
                            execute(
                                "INSERT INTO keys(id,keyid,conid,type,purpose,private," +
                                        "timestamp,status) VALUES(?,?,?,?,?,?,?,'active')",
                                it.id, keyHash, conid, typeStr, it.purpose, it.key, it.timestamp
                            )?.let { e -> return e }
                        }
                    }
                }

            }
        }
        return null
    }

    private fun loadMultiInfo(conid: String, type: String): Result<MutableList<MultiInfo>> {
        val out = mutableListOf<MultiInfo>()
        with(DBProxy.get()) {
            val rs = query(
                "SELECT * FROM contact_multi WHERE conid=? AND itemtype=? ORDER BY label",
                conid,
                type
            ).getOrElse { return it.toFailure() }

            while (rs.next()) {
                out.add(
                    MultiInfo(
                        rs.getString("id") ?: "",
                        conid,
                        rs.getString("label") ?: "",
                        type,
                        rs.getString("itemvalue") ?: "",
                        rs.getBoolean("preferred")
                    )
                )
            }
        }
        return out.toSuccess()
    }

    private fun saveMultiInfo(conid: String, type: String, info: List<MultiInfo>): Throwable? {
        with(DBProxy.get()) {
            execute("DELETE FROM contact_multi WHERE conid=? AND itemtype=?", conid, type)
                ?.let { return it }

            info.forEach {
                execute(
                    "INSERT INTO contact_multi(id,conid,label,itemtype,itemvalue,preferred) " +
                            "VALUES(?,?,?,?,?,?)",
                    it.id, it.conid, it.label, it.itemtype, it.value, it.isPreferred
                )?.let { e -> return e }
            }
        }
        return null
    }

    private fun loadLabeledField(conid: String, multiInfoType: String)
            : Result<MutableList<Contact.LabeledField>?> {
        val out = mutableListOf<Contact.LabeledField>()
        val db = DBProxy.get()
        val rs = db.query(
            "SELECT * FROM contact_multi WHERE conid=? AND itemtype=? ORDER BY label",
            conid, multiInfoType
        ).getOrElse { return it.toFailure() }

        return runCatching {
            while (rs.next()) {
                out.add(
                    Contact.LabeledField(
                        rs.getString("label") ?: "",
                        rs.getString("itemvalue") ?: "",
                        rs.getBoolean("preferred")
                    )
                )
            }
            out
        }
    }

    private fun loadMailingAddresses(conid: String): Result<MutableList<Contact.MailingAddress>?> {
        val out = mutableListOf<Contact.MailingAddress>()
        val db = DBProxy.get()
        val rs = db.query("SELECT * FROM contact_addresses WHERE conid=? ORDER BY label", conid)
            .getOrElse { return it.toFailure() }

        runCatching {
            while (rs.next()) {
                out.add(
                    Contact.MailingAddress(
                        label = rs.getString("label") ?: "",
                        street = rs.getString("street") ?: "",
                        extended = rs.getString("extended") ?: "",
                        locality = rs.getString("locality") ?: "",
                        region = rs.getString("region") ?: "",
                        postalCode = rs.getString("postalcode") ?: "",
                        country = rs.getString("country") ?: "",
                        preferred = rs.getBoolean("preferred")
                    )
                )
            }
        }.exceptionOrNull()?.let { return it.toFailure() }
        return out.toSuccess()
    }

    private fun loadContactPhoto(rs: ResultSet): Result<Contact.Photo?> {
        return runCatching {
            val rawType = rs.getString("phototype")
            val rawPhoto = rs.getBytes("photo")
            if (rawType != null && rawPhoto != null) {
                val mime = MimeType.fromString(rawType)
                if (mime != null) {
                    val photo = runCatching { Image(ByteArrayInputStream(rawPhoto)) }
                        .getOrElse { return it.toFailure() }
                    val pair = imageToBytes(photo).getOrElse { return it.toFailure() }!!
                    Contact.Photo(pair.first, Base85.encode(pair.second))
                } else null
            } else null
        }
    }

    /** Generates and runs the necessary query to load contact headers given a specified filter */
    private fun queryHeaders(filter: Filter?): Result<List<ContactHeaderItem>> {
        val out = mutableListOf<ContactHeaderItem>()
        val db = DBProxy.get()

        val rs = if (filter != null) {
            db.query(buildQueryString(filter, "contacts"))
                .getOrElse { return Result.failure(it) }
        } else {
            db.query("SELECT formatted_name,labels,id FROM contacts ORDER BY formatted_name")
                .getOrElse { return Result.failure(it) }
        }

        while (rs.next()) {
            val item = ContactHeaderItem(
                rs.getString("formatted_name"),
                rs.getString("id"),
            )
            rs.getString("labels")?.let { item.setLabels(it.split(",").toSet()) }
            out.add(item)
        }
        return out.toSuccess()
    }

    internal fun conItemFromQuery(id: String, rs: ResultSet): Result<ContactItem> {

        val out = ContactItem(rs.getString("formatted_name"), id).apply {
            entityType = rs.getString("entitytype") ?: "person"
            isManaged = rs.getBoolean("ismanaged")
            givenName = rs.getString("given_name") ?: ""
            familyName = rs.getString("family_name") ?: ""
            middleName = rs.getString("middle_name") ?: ""
            prefix = rs.getString("prefix") ?: ""
            suffix = rs.getString("suffix") ?: ""
            gender = rs.getString("gender") ?: ""
            uid = rs.getString("uid") ?: ""
            wid = rs.getString("wid") ?: ""
            domain = rs.getString("domain") ?: ""
            bio = rs.getString("bio") ?: ""
            anniversary = rs.getString("anniversary") ?: ""
            birthday = rs.getString("birthday") ?: ""
            organization = rs.getString("organization") ?: ""
            orgUnits = rs.getString("orgunits") ?: ""
            title = rs.getString("title") ?: ""
            languages = rs.getString("languages") ?: ""
            contents = rs.getString("notes") ?: ""
            serverPath = rs.getString("server_path") ?: ""
            format = rs.getString("format") ?: "plain"

            val rawType = rs.getString("phototype")
            val rawPhoto = rs.getBytes("photo")
            if (rawType != null && rawPhoto != null) {
                val mime = MimeType.fromString(rawType)
                if (mime != null) {
                    photo = runCatching { Image(ByteArrayInputStream(rawPhoto)) }
                        .getOrElse { return it.toFailure() }
                    if (photo != null)
                        photoType = mime
                }
            }
        }
        rs.getString("labels")?.let { out.setLabels(it.split(", ").toSet()) }
        out.addressListProperty.set(
            loadAddresses(id)
                .getOrElse { return it.toFailure() }
                .toObservable()
        )
        out.keyListProperty.set(
            loadKeys(id)
                .getOrElse { return it.toFailure() }
                .toObservable()
        )
        out.phoneListProperty.set(
            loadMultiInfo(id, "phone")
                .getOrElse { return it.toFailure() }
                .toObservable()
        )
        out.emailListProperty.set(
            loadMultiInfo(id, "email")
                .getOrElse { return it.toFailure() }
                .toObservable()
        )
        out.socialListProperty.set(
            loadMultiInfo(id, "social")
                .getOrElse { return it.toFailure() }
                .toObservable()
        )
        out.websiteListProperty.set(
            loadMultiInfo(id, "website")
                .getOrElse { return it.toFailure() }
                .toObservable()
        )
        return out.toSuccess()
    }
}

private val contactInitCmds = listOf(
    // Lots of tables to store contact information

    """CREATE TABLE IF NOT EXISTS contacts (
		id UUID PRIMARY KEY NOT NULL UNIQUE,
		entitytype TEXT NOT NULL,
        ismanaged BOOL NOT NULL,
        labels TEXT,
        formatted_name TEXT,
        given_name TEXT,
        family_name TEXT,
        middle_name TEXT,
        prefix TEXT,
        suffix TEXT,
        gender TEXT,
        uid TEXT,
        wid TEXT,
        domain TEXT,
        bio TEXT,
        anniversary TEXT,
        birthday TEXT,
        organization TEXT,
        orgunits TEXT,
        title TEXT,
        languages TEXT,
        notes TEXT,
        server_path TEXT,
        format TEXT,
        phototype TEXT,
        photo BINARY VARYING
	);""",
    """CREATE TABLE IF NOT EXISTS contact_addresses (
		id UUID PRIMARY KEY NOT NULL UNIQUE,
        conid UUID NOT NULL,
        label TEXT NOT NULL,
        street TEXT,
        extended TEXT,
        locality TEXT,
        region TEXT,
        postalcode TEXT,
        country TEXT,
        preferred BOOL,
        FOREIGN KEY (conid) REFERENCES contacts(id) ON DELETE CASCADE
    );""",
    """CREATE TABLE IF NOT EXISTS contact_files (
		id UUID PRIMARY KEY NOT NULL UNIQUE,
        conid UUID NOT NULL,
        name TEXT NOT NULL UNIQUE,
        mime TEXT NOT NULL,
        itemdata BLOB,
        FOREIGN KEY (conid) REFERENCES contacts(id) ON DELETE CASCADE
    );""",
    """CREATE TABLE IF NOT EXISTS contact_multi (
		id UUID PRIMARY KEY NOT NULL UNIQUE,
        conid UUID NOT NULL,
        label TEXT NOT NULL,
        itemtype TEXT NOT NULL,
        itemvalue TEXT,
        preferred BOOL,
        FOREIGN KEY (conid) REFERENCES contacts(id) ON DELETE CASCADE
    );""",
    """CREATE TABLE IF NOT EXISTS contact_phones (
		id UUID PRIMARY KEY NOT NULL UNIQUE,
        conid UUID NOT NULL,
        label TEXT NOT NULL UNIQUE,
        phnumber TEXT NOT NULL,
        preferred BOOL,
        FOREIGN KEY (conid) REFERENCES contacts(id) ON DELETE CASCADE
    );""",
    """CREATE TABLE IF NOT EXISTS blocklist (
		id UUID PRIMARY KEY NOT NULL UNIQUE,
        wid TEXT,
        domain TEXT
    );""",
    """CREATE TABLE IF NOT EXISTS contact_requests (
            rowid INT PRIMARY KEY AUTO_INCREMENT,
            address TEXT NOT NULL,
            purpose TEXT NOT NULL,
            private TEXT NOT NULL,
            public TEXT NOT NULL,
            timestamp TEXT NOT NULL
        );""",
)
