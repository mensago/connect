package connect.contacts.aboutme

import connect.UserInfoDAO
import connect.contacts.ContactDAO
import connect.gUserConID
import javafx.scene.layout.Region

class AboutMeController {
    private val model = AboutMeModel()
    private val interactor = AboutMeInteractor(model)
    private val viewBuilder = AboutMeViewBuilder(model, this::handlePhotoSave)

    init {
        setupState()
    }

    /** Instructs the controller to register its view with the main model's mode info structure */
    fun getView(): Region {
        return viewBuilder.build()
    }

    fun reloadState() {
        val info = ContactDAO().loadConItem(gUserConID.toString()).getOrThrow()!!
        with(model) {
            givenName = info.givenName
            familyName = info.familyName
            maddress = info.maddress
            waddress = info.waddress
            title = info.title
            organization = info.organization
            bio = info.bio
            photo = info.photo
        }

        if (info.emailListProperty.isNotEmpty()) {
            info.emailListProperty.find { it.label.lowercase() == "personal" }
                ?.let { model.personalEmail = it.value }
            info.emailListProperty.find { it.label.lowercase() == "work" }
                ?.let { model.workEmail = it.value }
        }

        info.websiteListProperty.get()
            ?.firstOrNull()
            ?.let { model.website = it.value }
    }

    private fun handlePhotoSave() = interactor.saveUserPhoto()

    private fun setupState() {
        val dao = UserInfoDAO()
        model.givenName = dao.loadGivenName().getOrThrow()

        // This needs to be an addListener call instead of subscribe because the user's given name
        // is not allowed to be empty and subscribe sends an initial change with an empty name.
        // Oopsie. ;)
        model.givenNameProperty.addListener { _, _, newValue ->
            dao.saveGivenName(newValue.trim())?.let { throw it }
        }

        model.familyName = dao.loadFamilyName().getOrThrow()
        model.familyNameProperty.subscribe { newValue ->
            dao.saveFamilyName(newValue.trim())?.let { throw it }
        }

        // These are read-only in the view, so no binding or listeners are necessary
        model.maddress = dao.loadMAddress().getOrThrow()
        model.waddress = dao.loadWAddress().getOrThrow()

        model.title = dao.loadMisc("title").getOrThrow()
        model.titleProperty.subscribe { newValue ->
            dao.saveMisc("title", newValue.trim())?.let { throw it }
        }

        model.organization = dao.loadMisc("organization").getOrThrow()
        model.organizationProperty.subscribe { newValue ->
            dao.saveMisc("organization", newValue.trim())?.let { throw it }
        }

        model.bio = dao.loadMisc("bio").getOrThrow()
        model.bioProperty.subscribe { newValue ->
            dao.saveMisc("bio", newValue.trim())?.let { throw it }
        }

        model.mobileNumber = dao.loadMultiInfo("phone", "Mobile").getOrNull() ?: ""
        model.mobileNumberProperty.subscribe { newValue ->
            dao.saveMultiInfo("phone", "Mobile", newValue.trim(), false)
        }

        model.homeNumber = dao.loadMultiInfo("phone", "Home").getOrNull() ?: ""
        model.homeNumberProperty.subscribe { newValue ->
            dao.saveMultiInfo("phone", "Home", newValue.trim(), false)
        }

        model.workNumber = dao.loadMultiInfo("phone", "Work").getOrNull() ?: ""
        model.workNumberProperty.subscribe { newValue ->
            dao.saveMultiInfo("phone", "Work", newValue.trim(), false)
        }

        model.personalEmail = dao.loadMultiInfo("email", "Personal").getOrNull() ?: ""
        model.personalEmailProperty.subscribe { newValue ->
            dao.saveMultiInfo("email", "Personal", newValue.trim(), false)
        }

        model.workEmail = dao.loadMultiInfo("email", "Work").getOrNull() ?: ""
        model.workEmailProperty.subscribe { newValue ->
            dao.saveMultiInfo("email", "Work", newValue.trim(), false)
        }

        model.website = dao.loadMultiInfo("website", "Personal").getOrNull() ?: ""
        model.websiteProperty.subscribe { newValue ->
            dao.saveMultiInfo("website", "Personal", newValue.trim(), false)
        }

        model.photo = dao.loadPhoto().getOrThrow()
    }
}
