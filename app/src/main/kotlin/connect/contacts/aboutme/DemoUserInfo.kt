package connect.contacts.aboutme

import connect.contacts.ContactDAO
import connect.gUserConID
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.UserID
import libmensago.Contact

fun generateDemoUserInfo() {
    ContactDAO().import(
        Contact(
            Contact.EntityType.Individual,
            formattedName = "Workspace Administrator",
            givenName = "Workspace",
            familyName = "Administrator",
            userID = UserID.fromString("admin")!!,
            workspace = RandomID.fromString("ae406c5e-2673-4d3e-af20-91325d9623ca")!!,
            domain = Domain.fromString("example.com")!!,
            title = "Mensago Administrator",
            organization = "Acme Widgets, Inc.",
            bio = """I wish I had something clever to put here. ¯\_(ツ)_/¯""",
            id = gUserConID,
        )
    )?.let { throw it }
}
