package connect.contacts

import connect.*
import javafx.beans.property.*
import javafx.collections.ObservableList
import javafx.collections.ObservableMap
import javafx.scene.image.Image
import keznacl.Base85
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.MAddress
import libkeycard.RandomID
import libkeycard.Timestamp
import libkeycard.WAddress
import libmensago.Contact
import libmensago.MimeType
import utilities.clone
import utilities.toObservable
import java.io.ByteArrayInputStream

class ContactHeaderItem(formattedName: String = "", id: String = "") :
    FilterableHeaderItem(id, "plain") {

    override fun toString(): String = name

    override fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "name" -> term.compare(name)
            "address" -> term.compare(address)
            "phone" -> term.compare(phone)

            else -> super.match(term)
        }
    }

    private val nameProperty = SimpleStringProperty(formattedName)
    var name: String
        get() = nameProperty.value
        set(value) = nameProperty.set(value)

    private val addressProperty = SimpleStringProperty(formattedName)
    var address: String
        get() = addressProperty.value
        set(value) = addressProperty.set(value)

    private val phoneProperty = SimpleStringProperty(formattedName)
    var phone: String
        get() = phoneProperty.value
        set(value) = phoneProperty.set(value)
}

/**
 *  The ContactItem class is a lightweight data model class used for representing an entry in the
 *  user's address book. Unlike Contact, this class only uses strings for data storage and its
 *  primary purpose is for interaction with the database and for use as model state.
 */
@Suppress("MemberVisibilityCanBePrivate")
class ContactItem(formattedName: String = "", id: String = "") :
    FilterableItem("", id, "plain") {

    override fun toString(): String = formattedName

    override fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "name" -> term.compare(formattedName)
            "givenname" -> term.compare(givenName)
            "familyname" -> term.compare(familyName)
            "middlename" -> term.compare(middleName)
            "prefix" -> term.compare(prefix)
            "suffix" -> term.compare(suffix)
            "gender" -> term.compare(gender)
            "mensago" -> term.compare(waddress) || term.compare(maddress)
            "userid" -> term.compare(uid)
            "workspace" -> term.compare(wid)
            "domain" -> term.compare(domain)
            "bio" -> term.compare(bio)
            "anniversary" -> term.compare(anniversary)
            "birthday" -> term.compare(birthday)
            "organization" -> term.compare(organization)
            "orgunit" -> term.compare(orgUnits)
            "title" -> term.compare(title)
            "languages" -> term.compare(languages)
            "phone" -> term.compareSet(phoneListProperty.value.map { it.value }.toSet())
            "email" -> term.compareSet(emailListProperty.value.map { it.value }.toSet())
            "social" -> term.compareSet(socialListProperty.value.map { it.value }.toSet())
            "website" -> term.compareSet(websiteListProperty.value.map { it.value }.toSet())
            "address" -> term.compare(domain)

            else -> super.match(term)
        }
    }

    fun toHeader(): ContactHeaderItem {
        return ContactHeaderItem(formattedName, id).apply {
            this.name = name
            this.address = address
            this.phone = phone
        }
    }

    /**
     * Creates a friendly display string from contact information or "Unindentified Contact" if
     * the contact does not have a formatted name, or Mensago information, which is a bug.
     */
    fun toDisplayString(): String {
        val sb = StringBuilder()
        if (formattedName.isNotEmpty())
            sb.append(formattedName)

        if (domain.isNotEmpty()) {
            if (uid.isNotEmpty()) {
                val addr = MAddress.fromString("$uid/$domain")
                if (addr != null) {
                    if (sb.isNotEmpty())
                        sb.append(" <$addr>")
                    else
                        sb.append(addr.toString())
                }
            } else if (wid.isNotEmpty()) {
                val addr = WAddress.fromString("$wid/$domain")
                if (addr != null) {
                    if (sb.isNotEmpty())
                        sb.append(" <$addr>")
                    else
                        sb.append(addr.toString())
                }
            }
        }

        return if (sb.isNotEmpty())
            sb.toString()
        else
            "Unidentified Contact"
    }

    /**
     * Updates contact information in the item from information in another item. Note that this will
     * overwrite only the contact information fields and nothing else. This update is also an
     * assignment operation, not a Boolean OR, so fields that are not present in the item passed to
     * this method will be removed when the call is complete.
     */
    fun updateFrom(other: ContactItem) {
        formattedName = other.formattedName
        givenName = other.givenName
        familyName = other.familyName
        middleName = other.middleName
        prefix = other.prefix
        suffix = other.suffix
        gender = other.gender
        bio = other.bio
        anniversary = other.anniversary
        birthday = other.birthday
        organization = other.organization
        orgUnits = other.orgUnits
        title = other.title
        languages = other.languages

        if (other.addressListProperty.isEmpty())
            addressListProperty.clear()
        else
            addressListProperty.set(
                other.addressListProperty.get().map { it.clone() }.toObservable()
            )

        if (other.keyListProperty.isEmpty())
            keyListProperty.clear()
        else
            keyListProperty.set(
                other.keyListProperty.get().map { it.clone() }.toObservable()
            )

        if (other.phoneListProperty.isEmpty())
            phoneListProperty.clear()
        else
            phoneListProperty.set(
                other.phoneListProperty.get().map { it.clone() }.toObservable()
            )

        if (other.emailListProperty.isEmpty())
            emailListProperty.clear()
        else
            emailListProperty.set(
                other.emailListProperty.get().map { it.clone() }.toObservable()
            )

        if (other.emailListProperty.isEmpty())
            emailListProperty.clear()
        else
            emailListProperty.set(
                other.emailListProperty.get().map { it.clone() }.toObservable()
            )

        if (other.socialListProperty.isEmpty())
            socialListProperty.clear()
        else
            socialListProperty.set(
                other.socialListProperty.get().map { it.clone() }.toObservable()
            )

        if (other.websiteListProperty.isEmpty())
            websiteListProperty.clear()
        else
            websiteListProperty.set(
                other.websiteListProperty.get().map { it.clone() }.toObservable()
            )

        photo = other.photo?.clone()
        photoType = other.photoType?.clone()
        rawPhoto = if (other.rawPhoto != null)
            Pair(other.rawPhoto!!.first.clone(), other.rawPhoto!!.second.copyOf())
        else null

        custom = if (other.custom != null) other.custom!!.toMutableMap().toObservable() else null
    }

    // id property inherited from FilteredItem

    var entityType: String = "individual"
    var isManaged: Boolean = false

    // labels property inherited from FilteredItem

    val formattedNameProperty = SimpleStringProperty(formattedName)
    var formattedName: String
        get() = formattedNameProperty.value
        set(value) = formattedNameProperty.set(value)

    val givenNameProperty = SimpleStringProperty("")
    var givenName: String
        get() = givenNameProperty.value
        set(value) = givenNameProperty.set(value)

    val familyNameProperty = SimpleStringProperty("")
    var familyName: String
        get() = familyNameProperty.value
        set(value) = familyNameProperty.set(value)

    val middleNameProperty = SimpleStringProperty("")
    var middleName: String
        get() = middleNameProperty.value
        set(value) = middleNameProperty.set(value)

    val prefixProperty = SimpleStringProperty("")
    var prefix: String
        get() = prefixProperty.value
        set(value) = prefixProperty.set(value)

    val suffixProperty = SimpleStringProperty("")
    var suffix: String
        get() = suffixProperty.value
        set(value) = suffixProperty.set(value)

    val genderProperty = SimpleStringProperty("")
    var gender: String
        get() = genderProperty.value
        set(value) = genderProperty.set(value)

    val waddress: String
        get() {
            if (wid.isEmpty() || domain.isEmpty()) return ""
            return "$wid/$domain"
        }

    val maddress: String
        get() {
            if (uid.isEmpty()) return waddress
            return "$uid/$domain"
        }

    val uidProperty = SimpleStringProperty("")
    var uid: String
        get() = uidProperty.value
        set(value) = uidProperty.set(value)

    val widProperty = SimpleStringProperty("")
    var wid: String
        get() = widProperty.value
        set(value) = widProperty.set(value)

    val domainProperty = SimpleStringProperty("")
    var domain: String
        get() = domainProperty.value
        set(value) = domainProperty.set(value)

    val bioProperty = SimpleStringProperty("")
    var bio: String
        get() = bioProperty.value
        set(value) = bioProperty.set(value)

    val anniversaryProperty = SimpleStringProperty("")
    var anniversary: String
        get() = anniversaryProperty.value
        set(value) = anniversaryProperty.set(value)

    val birthdayProperty = SimpleStringProperty("")
    var birthday: String
        get() = birthdayProperty.value
        set(value) = birthdayProperty.set(value)

    val organizationProperty = SimpleStringProperty("")
    var organization: String
        get() = organizationProperty.value
        set(value) = organizationProperty.set(value)

    val orgunitsProperty = SimpleStringProperty("")
    var orgUnits: String
        get() = orgunitsProperty.value
        set(value) = orgunitsProperty.set(value)

    val titleProperty = SimpleStringProperty("")
    var title: String
        get() = titleProperty.value
        set(value) = titleProperty.set(value)

    val languagesProperty = SimpleStringProperty("")
    var languages: String
        get() = languagesProperty.value
        set(value) = languagesProperty.set(value)

    val serverPathProperty = SimpleStringProperty("")
    var serverPath: String
        get() = serverPathProperty.value
        set(value) = serverPathProperty.set(value)

    // notes property in db is the contents property from FilteredItem
    // format property inherited from FilteredItem

    val addressListProperty = SimpleListProperty(listOf<AddressInfo>().toObservable())
    val keyListProperty = SimpleListProperty(listOf<ContactKeyInfo>().toObservable())
    val phoneListProperty = SimpleListProperty(listOf<MultiInfo>().toObservable())
    val emailListProperty = SimpleListProperty(listOf<MultiInfo>().toObservable())
    val socialListProperty = SimpleListProperty(listOf<MultiInfo>().toObservable())
    val websiteListProperty = SimpleListProperty(listOf<MultiInfo>().toObservable())

    val photoProperty = SimpleObjectProperty<Image>()
    var photo: Image?
        get() = photoProperty.value
        set(value) {
            photoProperty.set(value)
        }
    var photoType: MimeType? = null

    // This property exists to be able to import raw data into the database without conversion,
    // preserving image quality for JPEG and other lossless formats.
    var rawPhoto: Pair<MimeType, ByteArray>? = null

    var customProperty = SimpleMapProperty<String, String>()
    var custom: ObservableMap<String, String>?
        get() = customProperty.value
        set(value) = customProperty.set(value)

    companion object {

        fun fromContact(con: Contact, managed: Boolean): Result<ContactItem> {
            val conPhoto = if (con.photo == null) null
            else {
                kotlin.runCatching {
                    val decoded = Base85.decode(con.photo!!.data).getOrThrow()
                    Image(ByteArrayInputStream(decoded))
                }.getOrElse { return it.toFailure() }
            }

            return ContactItem(con.formattedName, con.id.toString()).apply {
                isManaged = managed
                entityType = con.entityType.toString()
                id = con.id.toString()
                givenName = con.givenName ?: ""
                familyName = con.familyName ?: ""
                middleName = con.middleName ?: ""
                prefix = con.prefix ?: ""
                suffix = con.suffix ?: ""
                gender = con.gender ?: ""
                bio = con.bio ?: ""

                socialListProperty.set(convertLabeledFieldList(con.social, "social", id))
                phoneListProperty.set(convertLabeledFieldList(con.phone, "phone", id))
                keyListProperty.set(convertKeyInfo(con.keys, id))
                websiteListProperty.set(convertLabeledFieldList(con.websites, "website", id))
                uid = con.userID?.toString() ?: ""
                wid = con.workspace?.toString() ?: ""
                domain = con.domain?.toString() ?: ""

                addressListProperty.set(convertMailInfo(con.mail, id))
                emailListProperty.set(convertLabeledFieldList(con.email, "email", id))

                anniversary = con.anniversary ?: ""
                birthday = con.birthday ?: ""
                organization = con.organization ?: ""
                orgUnits = con.orgUnits?.joinToString(",") ?: ""
                title = con.title ?: ""
                languages = con.languages?.joinToString(",") ?: ""
                contents = con.notes ?: ""
                format = con.noteFormat?.toString() ?: ""

                photo = conPhoto

                attachments = con.attachments?.associateBy({ it.name }, { it })?.toObservable()
                customProperty.set(con.custom?.toMutableMap()?.toObservable())
            }.toSuccess()
        }

        private fun convertLabeledFieldList(
            list: List<Contact.LabeledField>?, type: String, conid: String
        ): ObservableList<MultiInfo>? {
            return list?.map {
                MultiInfo(
                    RandomID.generate().toString(), conid, it.field, type, it.value,
                    it.preferred ?: false
                )
            }?.toObservable()
        }

        private fun convertKeyInfo(keys: Map<String, String>?, conid: String):
                ObservableList<ContactKeyInfo>? {

            if (keys.isNullOrEmpty()) return null
            return mutableListOf<ContactKeyInfo>().toObservable().apply {
                keys.keys.forEach { key ->
                    when (key) {
                        "Encryption" -> ContactKeyInfo(
                            RandomID.generate().toString(), conid, true,
                            KeyPurpose.RelEncryption, keys[key]!!, Timestamp().toString()
                        ).also { add(it) }

                        "Verification" -> ContactKeyInfo(
                            RandomID.generate().toString(), conid, true,
                            KeyPurpose.RelSigning, keys[key]!!, Timestamp().toString()
                        ).also { add(it) }
                    }
                }
            }
        }

        private fun convertMailInfo(
            list: List<Contact.MailingAddress>?,
            conid: String
        ): ObservableList<AddressInfo>? {
            return list?.map {
                AddressInfo(
                    RandomID.generate().toString(), conid, it.label, it.street ?: "",
                    it.extended ?: "", it.locality ?: "", it.region ?: "",
                    it.postalCode ?: "", it.country ?: "", it.preferred ?: false
                )
            }?.toObservable()
        }
    }
}

/**
 * Lightweight data model class for representing mailing address information as part of a
 * ContactItem.
 */
class AddressInfo(
    var id: String,
    var conid: String,
    addLabel: String,
    addStreet: String = "",
    addExtended: String = "",
    addLocality: String = "",
    addRegion: String = "",
    addPostalCode: String = "",
    addCountry: String = "",
    preferred: Boolean = false,
) {
    private val labelProperty = SimpleStringProperty(addLabel)
    var label: String
        get() = labelProperty.value
        set(value) = labelProperty.set(value)

    private val streetProperty = SimpleStringProperty(addStreet)
    var street: String
        get() = streetProperty.value
        set(value) = streetProperty.set(value)

    private val extendedProperty = SimpleStringProperty(addExtended)
    var extended: String
        get() = extendedProperty.value
        set(value) = extendedProperty.set(value)

    private val localityProperty = SimpleStringProperty(addLocality)
    var locality: String
        get() = localityProperty.value
        set(value) = localityProperty.set(value)

    private val regionProperty = SimpleStringProperty(addRegion)
    var region: String
        get() = regionProperty.value
        set(value) = regionProperty.set(value)

    private val postalcodeProperty = SimpleStringProperty(addPostalCode)
    var postalCode: String
        get() = postalcodeProperty.value
        set(value) = postalcodeProperty.set(value)

    private val countryProperty = SimpleStringProperty(addCountry)
    var country: String
        get() = countryProperty.value
        set(value) = countryProperty.set(value)

    private val isPreferredProperty = SimpleBooleanProperty(preferred)
    var isPreferred: Boolean
        get() = isPreferredProperty.value
        set(value) = isPreferredProperty.set(value)

    /** Provides a deep copy of the object */
    fun clone(): AddressInfo = AddressInfo(
        id = id,
        conid = conid,
        addLabel = label,
        addStreet = street,
        addExtended = extended,
        addLocality = locality,
        addRegion = region,
        addPostalCode = postalCode,
        addCountry = country,
        preferred = isPreferred,
    )

    companion object {
        fun fromMailingAddress(conid: String, address: Contact.MailingAddress): AddressInfo {
            return AddressInfo(
                RandomID.generate().toString(), conid,
                addLabel = address.label,
                addStreet = address.street ?: "",
                addExtended = address.extended ?: "",
                addLocality = address.locality ?: "",
                addRegion = address.region ?: "",
                addPostalCode = address.postalCode ?: "",
                addCountry = address.country ?: "",
                preferred = address.preferred ?: false,
            )
        }
    }
}

/**
 * MultiInfo is a class which can store multiple types of personal data to which there is a
 * one-to-many relationship with the owning contact. This includes email addresses, websites,
 * phone numbers, and social media accounts.
 */
class MultiInfo(
    var id: String,
    var conid: String,
    itemLabel: String,
    var itemtype: String,
    itemValue: String,
    preferred: Boolean
) {
    private val labelProperty = SimpleStringProperty(itemLabel)
    var label: String
        get() = labelProperty.value
        set(value) = labelProperty.set(value)

    private val valueProperty = SimpleStringProperty(itemValue)
    var value: String
        get() = valueProperty.value
        set(value) = valueProperty.set(value)

    private val isPreferredProperty = SimpleBooleanProperty(preferred)
    var isPreferred: Boolean
        get() = isPreferredProperty.value
        set(value) = isPreferredProperty.set(value)

    /** Provides a deep copy of the object */
    fun clone(): MultiInfo = MultiInfo(
        id = id,
        conid = conid,
        itemLabel = label,
        itemtype = itemtype,
        itemValue = value,
        preferred = isPreferred,
    )
}

/**
 * ContactKeyInfo is a data model class which represents a key packaged as part of a Contact
 * instance. The key purpose is going to be either KeyPurpose.Encryption or KeyPurpose.Signing,
 * depending on the type of key. The label, on the other hand, denotes the usage, which can be:
 *
 * - *Encryption*, *Verification* - For encrypting messages to the contact and verifying signatures
 * in messages sent by the contact
 * - *MyEncryption*, *MyVerification* - Given to the contact so they can encrypt messages to the
 * user and verify signatures attached to messages sent by the user
 * - *MyDecryption*, *MySigning* - Private keys for decrypting messages from the contact and signing
 * messages sent to the contact.
 */
data class ContactKeyInfo(
    var id: String,
    var conid: String,
    var isPublic: Boolean,
    var purpose: KeyPurpose,
    var key: String,
    var timestamp: String,
) {
    /** Provides a deep copy of the object */
    fun clone(): ContactKeyInfo = ContactKeyInfo(
        id = id,
        conid = conid,
        isPublic = isPublic,
        purpose = purpose,
        key = key,
        timestamp = timestamp,
    )
}
