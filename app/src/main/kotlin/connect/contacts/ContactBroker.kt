package connect.contacts

import connect.Filter
import libmensago.MServerPath

class ContactBroker {
    private val dao = ContactDAO()

    init {
        dao.initContactTables()?.let { throw it }
    }

    fun addLabel(item: ContactHeaderItem, label: String) = dao.addLabel(item, label)
    fun removeLabel(item: ContactHeaderItem, label: String) = dao.removeLabel(item, label)
    fun loadLabels(): Result<List<Filter?>> = dao.loadLabelFilters()

    fun loadHeaders(filter: Filter?): Result<List<ContactHeaderItem>> {
        return if (filter != null)
            dao.loadHeaders(filter)
        else
            dao.loadHeaders()
    }

    fun load(id: String): Result<ContactItem?> = dao.loadConItem(id)

    fun save(note: ContactItem): Throwable? = dao.saveConItem(note)

    fun deleteFromPath(path: MServerPath): Throwable? = dao.deleteFromPath(path)

    fun moveContact(oldPath: MServerPath, newPath: MServerPath) = dao.moveContact(oldPath, newPath)

    fun addDemoDataToDB() {
        generateDemoContacts().forEach { dao.saveConItem(it)?.let { e -> throw e } }
    }
}