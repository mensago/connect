package connect.contacts.newconreq

import connect.showFatalError
import javafx.scene.layout.Region

class NewCRController {
    private val model = NewCRModel()
    private val interactor = NewCRInteractor(model)
    private val viewBuilder = NewCRViewBuilder(model, this::sendCR)

    fun getView(): Region {
        return viewBuilder.build()
    }

    fun sendCR() {
        interactor.queueCR().onFailure {
            showFatalError("Error sending contact request.", it)
        }
    }
}
