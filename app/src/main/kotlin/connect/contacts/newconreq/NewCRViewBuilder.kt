package connect.contacts.newconreq

import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.stage.Stage

class NewCRViewBuilder(private val model: NewCRModel, private val sendHandler: () -> Unit) {

    fun build(): Region {
        val toBox = TextField().apply {
            promptText = "Recipient's address"
            textProperty().bindBidirectional(model.recipientProperty)
        }
        val toGroup = HBox().apply {
            spacing = 5.0
            alignment = Pos.CENTER_LEFT
            children.addAll(Label("To:"), toBox)
            HBox.setHgrow(toBox, Priority.ALWAYS)
        }

        // Feature: SharingProfiles
        // FeatureTODO: Add sharing profile handling code

        val msgField = TextArea().apply {
            promptText = "Optional message to recipient"
            isWrapText = true
            textProperty().bindBidirectional(model.msgProperty)
        }

        val cancelButton = Button("Cancel").apply {
            onAction = EventHandler { (scene.window as Stage).close() }
        }
        val okButton = Button("OK").apply {
            onAction = EventHandler {
                sendHandler()
                (scene.window as Stage).close()
            }
        }
        val buttonGroup = HBox().apply {
            spacing = 5.0
            alignment = Pos.CENTER_RIGHT
            children.addAll(cancelButton, okButton)
        }

        return VBox().apply {
            spacing = 5.0
            padding = Insets(5.0)
            children.addAll(toGroup, msgField, buttonGroup)
        }
    }
}
