package connect.contacts.newconreq

import connect.Client
import connect.UserInfoDAO
import keznacl.BadValueException
import keznacl.toFailure
import libkeycard.MAddress

class NewCRInteractor(private val model: NewCRModel) {

    fun queueCR(): Result<Thread> {
        val addr = MAddress.fromString(model.recipient)
            ?: return BadValueException("Bad Mensago address").toFailure()

        val info = UserInfoDAO().loadPublicContact().getOrElse { return it.toFailure() }
        return Client().sendContactRequest(addr, info, model.msg, true)
    }
}
