package connect.contacts.newconreq

import javafx.beans.property.SimpleStringProperty

class NewCRModel {
    val recipientProperty = SimpleStringProperty()
    var recipient: String
        get() = recipientProperty.value
        set(value) {
            recipientProperty.value = value
        }

    // The information sharing profile used for the contact request
    val shProfileProperty = SimpleStringProperty()
    var shProfile: String
        get() = shProfileProperty.value
        set(value) {
            shProfileProperty.value = value
        }

    val msgProperty = SimpleStringProperty("")
    var msg: String
        get() = msgProperty.value
        set(value) {
            msgProperty.value = value
        }
}
