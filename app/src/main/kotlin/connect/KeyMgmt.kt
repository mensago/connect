package connect

import keznacl.CryptoString
import keznacl.EncryptionPair
import keznacl.SecretKey
import keznacl.SigningPair
import libkeycard.WAddress

/**
 * The ContactKeyInfo class is a descriptive class for getting all the information about a specific key or
 * keypair stored in a user's profile. Although it is possible to pass this structure around for
 * application use, its primary purpose is as a return value for calls like getKeysByStatus().
 */
data class KeyInfo(
    val type: String,
    val address: WAddress,
    val pubKey: CryptoString,
    val pubHash: CryptoString,
    val privKey: CryptoString,
    val category: KeyPurpose,
    val status: String,
    val timestamp: String,
)

fun Pair<CryptoString, CryptoString>.toEncryptionPair(): EncryptionPair? {
    return EncryptionPair.from(this.first, this.second).getOrNull()
}

fun Pair<CryptoString, CryptoString>.toSigningPair(): SigningPair? {
    return SigningPair.from(this.first, this.second).getOrNull()
}

fun Pair<CryptoString, CryptoString>.toSecretKey(): SecretKey? {
    return SecretKey.from(this.first).getOrNull()
}

/**
 * KeyPurpose defines the general usage of a key.
 * - ConReqEncryption: Contact request encryption/decryption
 * - ConReqSigning: Contact request signing/verification
 * - Device: Device identification/encryption keypair
 * - Encryption: General-purpose encryption/decryption
 * - Signing: General-purpose encryption/decryption
 * - Folder: server-side path name storage encryption
 * - PrimarySigning: organization primary signing/verification
 * - SecondarySigning: organization secondary signing/verification
 * - Storage: server-side file storage
 * - RelMyEncryption: Encryption keypair for receiving messages from another entity
 * - RelMySigning: Signing keypair for signing interactions intended for another entity
 * - RelEncryption: Encryption key for sending messages to another entity
 * - RelSigning: Verification key for verifying another entity's signatures
 */
enum class KeyPurpose {

    // Encryption and signing keys
    ConReqEncryption,
    ConReqSigning,

    // Encryption keypair used by devices for authentication
    Device,

    // General-purpose categories used when others don't apply
    Encryption,
    Signing,

    // For encrypting mappings of client-side folders to server-side folders.
    Folder,

    // Categories for keys used in organization keycards
    PrimarySigning,
    SecondarySigning,

    // Internal key used for encrypting
    Storage,

    // Relationship keys are created and used for interactions with one entity. The public half of
    // the My* keys are given to the other entity and the private keys are used for decrypting
    // messages from the entity. RelEncryption and RelSigning are used for storing the public keys
    // given by the entity to the user for encrypting messages sent to the entity.
    RelMyEncryption,
    RelMySigning,
    RelEncryption,
    RelSigning;

    override fun toString(): String {
        return when (this) {
            ConReqEncryption -> "crencryption"
            ConReqSigning -> "crsigning"
            Device -> "device"
            Encryption -> "encryption"
            Signing -> "signing"
            Folder -> "folder"
            PrimarySigning -> "orgsigning"
            SecondarySigning -> "altorgsigning"
            Storage -> "storage"
            RelMyEncryption -> "relmyencryption"
            RelMySigning -> "relmysigning"
            RelEncryption -> "relencryption"
            RelSigning -> "relsigning"
        }
    }

    companion object {
        fun fromString(s: String): KeyPurpose? {
            return when (s.lowercase()) {
                "crencryption" -> ConReqEncryption
                "crsigning" -> ConReqSigning
                "device" -> Device
                "encryption" -> Encryption
                "signing" -> Signing
                "folder" -> Folder
                "orgsigning" -> PrimarySigning
                "altorgsigning" -> SecondarySigning
                "storage" -> Storage
                "relmyencryption" -> RelMyEncryption
                "relmysigning" -> RelMySigning
                "relencryption" -> RelEncryption
                "relsigning" -> RelSigning
                else -> null
            }
        }
    }
}
