package connect.messages

import connect.Filter
import connect.FilterableItem
import connect.contacts.ContactDAO
import connect.main.MainModel
import connect.messages.newMsg.NewMsgController
import connect.profiles.KeyDAO
import connect.profiles.ProfileManager
import connect.showFatalError
import javafx.collections.ObservableList
import javafx.scene.Scene
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.stage.Stage
import javafx.stage.StageStyle
import keznacl.BadValueException
import keznacl.Encryptor
import libkeycard.RandomID
import libmensago.MServerPath
import libmensago.MessageAction
import utilities.Trace
import utilities.traceEvent

class MessageController(mainModel: MainModel) {

    private val model = MessageModel()
    private val interactor = MessageInteractor(model)
    private val viewBuilder = MessageViewBuilder(model, mainModel, getHandlers())

    init {
        interactor.loadFilters()

        // The Inbox label is always first. We throw here because when init() is running, there
        // isn't a GUI to show an error.
        interactor.selectFilter(model.filterListProperty[0])?.let { throw it }
    }


    /** Instructs the controller to register its view with the main model's mode info structure */
    fun registerView() {
        viewBuilder.build()
    }

    /**
     * Handles processing of new items. For regular messages, this means handling protocol messages
     * and loading up new user messages into the model.
     */
    fun processNewItem(item: MsgBaseItem): Throwable? {

        // A message with type "conreq.2" is a Contact Request approval message. Beyond just loading
        // the message up so the user can see the CR was approved, we also need to import the
        // attached information into the user's address book
        if (item.subType == "conreq.2") {
            item as ConMsgItem
            ContactDAO().import(item.contact)?.let { return it }
            KeyDAO().migrateCRKeys(item.contact.getWAddress()!!)?.let { return it }
        }
        loadNewItem(item)
        return null
    }

    /** Load a new data item */
    fun loadNewItem(item: MsgBaseItem) = interactor.addMessage(item)?.let {
        showFatalError("An unexpected error has occurred.", it)
    }

    /** Changes an item's server-side path */
    fun moveItem(oldPath: MServerPath, newPath: MServerPath) =
        interactor.moveMessage(oldPath, newPath)?.let {
            showFatalError("An unexpected error has occurred.", it)
        }

    /** Replaces the item at the specified path with the new one */
    fun replaceItem(oldItem: MServerPath, newItem: FilterableItem) =
        interactor.replaceMessage(oldItem, newItem)?.let {
            showFatalError("An unexpected error has occurred.", it)
        }

    /** Spawns a new window to create a new, blank message */
    fun showNewMessageWindow() {
        val profile = ProfileManager.model.activeProfile!!
        val isLocal = profile.isLocal().getOrElse {
            showFatalError("Couldn't determine if the user's profile is local")
            return
        }
        if (isLocal) return

        val newMsgController = NewMsgController()
        val view = newMsgController.getView()
        val sc = Scene(view)
        val stage = Stage(StageStyle.DECORATED).apply {
            scene = sc
            title = "New Message"
            minWidth = 400.0
            minHeight = 600.0
            isResizable = true
        }

        newMsgController.postSendHandler = Runnable { stage.close() }

        sc.addEventHandler(KeyEvent.KEY_PRESSED) { event ->
            if (event.code == KeyCode.ESCAPE)
                stage.close()

            if (event.code == KeyCode.ENTER && event.isControlDown)
                newMsgController.sendMessage()
        }
        stage.show()
    }

    fun getMessages(): ObservableList<MsgBaseHeaderItem> = model.msgList

    /** Pulls selected message(s) out of the trash */
    fun restoreMessages() {
        traceEvent(
            Trace.MessageController, "MessageController: restore selected messages"
        )
        interactor.restoreSelectedMessages()?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    /** Sends the selected message(s) to the trash */
    fun trashMessages() {
        traceEvent(
            Trace.MessageController, "MessageController: trash selected messages"
        )
        interactor.trashSelectedMessages()?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    /** Empties the trash for messages */
    fun emptyTrash() {
        traceEvent(Trace.MessageController, "MessageController: empty trash")
        interactor.emptyTrash()?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    private fun handleLabelSelection(oldItem: Filter?, newItem: Filter?) {
        traceEvent(
            Trace.MessageController,
            "MessageController: handleLabelSelection($oldItem, $newItem)"
        )
        interactor.selectFilter(newItem)?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    fun handleMsgSelection(oldItem: MsgBaseHeaderItem?, newItem: MsgBaseHeaderItem?) {
        traceEvent(
            Trace.MessageController,
            "MessageController: handleMessageSelection($oldItem, $newItem)"
        )
        if (oldItem == newItem)
            return

        interactor.selectMessage(newItem)?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    private fun handleStandardFilterSelection(oldItem: Filter?, newItem: Filter?) {
        traceEvent(
            Trace.MessageController,
            "MessageController: handleStandardFilterSelection($oldItem, $newItem)"
        )
        interactor.selectFilter(newItem)?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    fun handleConReqAction(action: MessageAction) {
        when (action) {
            MessageAction.Approve -> interactor.approveCR()
            MessageAction.Reject -> interactor.rejectCR(false)
            MessageAction.Block -> interactor.rejectCR(true)
            else -> BadValueException("Bad Contact Request action $action")
        }?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    fun handleDevReqAction(devid: RandomID, action: MessageAction, devkey: Encryptor?) {
        when (action) {
            MessageAction.Approve -> interactor.approveDevice(devid, devkey!!)
            MessageAction.Reject -> interactor.rejectDevice(devid)
            else -> BadValueException("Bad Device Request action $action")
        }?.let {
            showFatalError("An unexpected error has occurred.", it)
        }
    }

    private fun getHandlers(): MessageViewHandlers {
        return MessageViewHandlers(
            standardFilterSelection = this::handleStandardFilterSelection,
            labelSelection = this::handleLabelSelection,
            msgSelection = this::handleMsgSelection,
            showNewMsg = this::showNewMessageWindow,
            restoreMsg = this::restoreMessages,
            trashMsg = this::trashMessages,
            emptyTrash = this::emptyTrash,
            handleConReqAction = this::handleConReqAction,
            handleDevReqAction = this::handleDevReqAction,
        )
    }
}
