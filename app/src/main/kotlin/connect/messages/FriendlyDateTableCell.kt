package connect.messages

import javafx.scene.control.TableCell
import utilities.friendlyDateFormat
import java.time.LocalDateTime

class FriendlyDateTableCell : TableCell<MsgBaseHeaderItem, LocalDateTime?>() {
    override fun updateItem(item: LocalDateTime?, empty: Boolean) {
        super.updateItem(item, empty)
        text = if (empty) null else friendlyDateFormat(item).getOrDefault("error")
    }
}
