package connect.messages

import connect.Filter
import connect.FilterTerm
import connect.FilterableHeaderItem
import connect.FilterableItem
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import keznacl.toSuccess
import java.time.LocalDateTime

open class MsgBaseHeaderItem(subject: String = "", id: String = "", format: String = "plain") :
    FilterableHeaderItem(id, format) {

    override fun toString(): String = subject

    override fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "subject" -> term.compare(subject)
            "date" -> term.compare(date.toString())
            "from" -> term.compare(fromName) || term.compare(fromAddr)

            else -> super.match(term)
        }
    }

    private val subjectProperty = SimpleStringProperty(subject)
    var subject: String
        get() = subjectProperty.value
        set(value) = subjectProperty.set(value)

    /** This is the formatted display-string version of the message's date. */
    private val dateProperty = SimpleObjectProperty<LocalDateTime>()
    var date: LocalDateTime?
        get() = dateProperty.value
        set(value) = dateProperty.set(value)

    /**
     * A string representing the sender. Note that this is the actual name of the sender if they
     * supplied one -- it corresponds to Name field in the sender's keycard or their formatted
     * name if the sender is already in the user's address book.
     */
    private val fromNameProperty = SimpleStringProperty("")
    var fromName: String
        get() = fromNameProperty.value
        set(value) = fromNameProperty.set(value)

    /**
     * A display version of the address of the sender. The preferred address form is the sender's
     * regular Mensago address, but if the sender does not have a user ID, the workspace address is
     * used instead.
     */
    private val fromAddrProperty = SimpleStringProperty("")
    var fromAddr: String
        get() = fromAddrProperty.value
        set(value) = fromAddrProperty.set(value)

    /**
     * A string representing the recipient. Note that this is the actual name of the recipient if
     * they supplied one -- it corresponds to Name field in the recipient's keycard or their
     * formatted name if the recipient is already in the user's address book.
     */
    private val toNameProperty = SimpleStringProperty("")
    var toName: String
        get() = toNameProperty.value
        set(value) = toNameProperty.set(value)

    /**
     * A display version of the address of the recipient. The preferred address form is the
     * recipient's regular Mensago address, but if the recipient does not have a user ID, the
     * workspace address is used instead.
     */
    private val toAddrProperty = SimpleStringProperty("")
    var toAddr: String
        get() = toAddrProperty.value
        set(value) = toAddrProperty.set(value)

    /** The last action taken on the item, if any */
    private val lastActionProperty = SimpleStringProperty("")
    var lastAction: String
        get() = lastActionProperty.value
        set(value) = lastActionProperty.set(value)

    /** When the last action was taken */
    private val lastActionTimeProperty = SimpleStringProperty("")
    var lastActionTime: String
        get() = lastActionTimeProperty.value
        set(value) = lastActionTimeProperty.set(value)
}

open class MsgBaseItem(
    subject: String = "", contents: String = "", id: String = "", format: String = "plain",
) : FilterableItem(contents, id, format) {

    override fun toString(): String = subject

    override fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "type" -> term.compare(subType)
            "date" -> term.compare(date.toString())
            "subject" -> term.compare(subject)
            "from" -> term.compare(fromName) || term.compare(fromAddr)
            "to" -> term.compare(toAddr)
            "threadid" -> term.compare(threadID)

            else -> super.match(term)
        }
    }

    open fun toHeader(): MsgBaseHeaderItem {
        return MsgBaseHeaderItem(subject, id, format).apply {
            this.date = date
            this.fromName = fromName
            this.fromAddr = fromAddr
            this.lastAction = lastAction
        }
    }

    /**
     * Returns the message's extra associated data. Child classes which have this kind of data will
     * return a JSON string.
     */
    open fun getInfo(): Result<String> = "".toSuccess()

    /**
     * Sets internal extra assoociated data. Child classes will receive a JSON string for this which
     * corresponds to the data returned by [getInfo].
     */
    open fun setInfo(s: String): Throwable? = null

    private val subTypeProperty = SimpleStringProperty("")
    var subType: String
        get() = subTypeProperty.value
        set(value) = subTypeProperty.set(value)

    private val dateProperty = SimpleObjectProperty<LocalDateTime>()
    var date: LocalDateTime?
        get() = dateProperty.value
        set(value) = dateProperty.set(value)

    private val subjectProperty = SimpleStringProperty(subject)
    var subject: String
        get() = subjectProperty.value
        set(value) = subjectProperty.set(value)

    private val fromNameProperty = SimpleStringProperty("")
    var fromName: String
        get() = fromNameProperty.value
        set(value) = fromNameProperty.set(value)

    private val fromAddrProperty = SimpleStringProperty("")
    var fromAddr: String
        get() = fromAddrProperty.value
        set(value) = fromAddrProperty.set(value)

    private val toNameProperty = SimpleStringProperty("")
    var toName: String
        get() = toNameProperty.value
        set(value) = toNameProperty.set(value)

    private val toAddrProperty = SimpleStringProperty("")
    var toAddr: String
        get() = toAddrProperty.value
        set(value) = toAddrProperty.set(value)

    private val threadIDProperty = SimpleStringProperty("")
    var threadID: String
        get() = threadIDProperty.value
        set(value) = threadIDProperty.set(value)

    private val serverPathProperty = SimpleStringProperty("")
    var serverPath: String
        get() = serverPathProperty.value
        set(value) = serverPathProperty.set(value)

    /** The last action taken on the item, if any */
    private val lastActionProperty = SimpleStringProperty("")
    var lastAction: String
        get() = lastActionProperty.value
        set(value) = lastActionProperty.set(value)

    /** When the last action was taken */
    private val lastActionTimeProperty = SimpleStringProperty("")
    var lastActionTime: String
        get() = lastActionTimeProperty.value
        set(value) = lastActionTimeProperty.set(value)
}
