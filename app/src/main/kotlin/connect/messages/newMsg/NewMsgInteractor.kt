package connect.messages.newMsg

import connect.Client
import connect.UserInfoDAO
import connect.messages.MessageDAO
import connect.messages.MessageItem
import connect.profiles.getActiveProfile
import keznacl.BadValueException
import libkeycard.MAddress
import libkeycard.WAddress
import libmensago.ContentFormat
import libmensago.Message
import libmensago.resolver.KCResolver
import java.time.LocalDateTime

class NewMsgInteractor(private val model: NewMsgModel) {

    /** Saves the current message information as a draft */
    fun saveDraft(): Throwable? {
        val profile = getActiveProfile().getOrElse { return it }

        val draft = MessageItem(
            subject = model.subject,
            contents = model.contents,
            id = model.id,
        ).apply {
            toAddr = model.to
            fromName = UserInfoDAO().loadFormattedName().getOrElse { return it }
            fromAddr = profile.getWAddress().getOrElse { return it }.toString()
            date = LocalDateTime.now()
            threadID = model.threadID
            setLabels(setOf("Drafts"))
        }
        return MessageDAO().save(draft)
    }

    /** Creates a Message from the model state and queues it for delivery */
    fun sendMessage(): Throwable? {
        val addr = MAddress.fromString(model.to)
            ?: return BadValueException("Bad recipient address")

        val profile = getActiveProfile().getOrElse { return it }
        val from = profile.getWAddress().getOrElse { return it }
        val wid = KCResolver.resolveMenagoAddress(addr).getOrElse { return it }

        val msg = Message(from, WAddress.fromParts(wid, addr.domain), ContentFormat.Text).apply {
            subject = model.subject
            body = model.contents
        }

        return Client().send(msg, true)
    }
}
