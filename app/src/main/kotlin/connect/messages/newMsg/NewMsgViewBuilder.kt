package connect.messages.newMsg

import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.*
import javafx.scene.layout.HBox.setHgrow
import javafx.stage.Stage

class NewMsgViewBuilder(
    private val model: NewMsgModel, private val sendHandler: () -> Unit,
    private val saveHandler: () -> Unit
) {
    fun build(): Region {
        return VBox().apply {
            padding = Insets(5.0)
            spacing = 5.0
            val editor = buildEditor()
            children.addAll(buildHeader(), editor, buildButtons())
            VBox.setVgrow(editor, Priority.ALWAYS)
        }
    }

    private fun buildHeader(): Region {
        val toLayout = HBox().apply {
            alignment = Pos.CENTER
            spacing = 5.0
            val toField = TextField().apply {
                textProperty().bindBidirectional(model.toProperty)
            }
            children.addAll(Label("To:"), toField)
            setHgrow(toField, Priority.ALWAYS)
        }

        val subjectLayout = HBox().apply {
            alignment = Pos.CENTER
            spacing = 5.0
            val subField = TextField().apply {
                textProperty().bindBidirectional(model.subjectProperty)
            }
            children.addAll(Label("Subject:"), subField)
            setHgrow(subField, Priority.ALWAYS)
        }

        return VBox().apply {
            spacing = 5.0
            children.addAll(toLayout, subjectLayout)
        }
    }

    private fun buildEditor(): Region {
        return TextArea().apply {
            textProperty().bindBidirectional(model.contentsProperty)
        }
    }

    private fun buildButtons(): Region {
        return BorderPane().apply {
            left = Button("Save as Draft").apply {
                onAction = EventHandler {
                    saveHandler()
                    (scene.window as Stage).close()
                }
            }
            right = HBox().apply {
                alignment = Pos.BOTTOM_RIGHT
                spacing = 10.0

                val cancelButton = Button("Cancel").apply {
                    onAction = EventHandler { (scene.window as Stage).close() }
                }
                val sendButton = Button("Send").apply {
                    onAction = EventHandler {
                        sendHandler()
                        (scene.window as Stage).close()
                    }
                }
                children.addAll(cancelButton, sendButton)
            }
        }
    }
}