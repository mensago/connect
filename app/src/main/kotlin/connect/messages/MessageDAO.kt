package connect.messages

import connect.Filter
import connect.FilterSort
import connect.SortOrder
import connect.buildQueryString
import keznacl.BadValueException
import keznacl.EmptyDataException
import keznacl.toFailure
import keznacl.toSuccess
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.MissingFieldException
import libkeycard.RandomID
import libkeycard.Timestamp
import libmensago.*
import libmensago.db.DatabaseException
import utilities.*
import java.sql.ResultSet

class MessageDAO {

    fun initMessageTable(): Throwable? {
        return DBProxy.get().execute(
            """CREATE TABLE IF NOT EXISTS messages (
                id	             UUID PRIMARY KEY NOT NULL UNIQUE,
                format           TEXT NOT NULL,
                labels           TEXT,
                flags            TEXT,
                msgtype          TEXT,
                date             TEXT NOT NULL,
                subject          TEXT,
                contents         TEXT,
                from_name        TEXT,
                from_addr        TEXT,
                to_name          TEXT,
                to_addr          TEXT,
                cc               TEXT,
                bcc              TEXT,
                threadid         UUID,
                info             TEXT,
                server_path      TEXT,
                last_action      TEXT,
                last_action_time TEXT
            );"""
        )
    }

    fun import(msg: Message, serverPath: MServerPath, labels: String): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: Importing $msg")
        with(DBProxy.get()) {
            execute(
                "MERGE INTO messages(id,format,labels,msgtype,date,subject,contents," +
                        "from_addr,to_addr,cc,bcc,threadid,server_path) KEY(id) " +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
                msg.id, msg.format, labels, msg.subType ?: "", msg.date, msg.subject,
                msg.body, msg.from, msg.to, msg.cc, msg.bcc, msg.threadID, serverPath,
            )?.let { return it }
        }
        return null
    }

    /**
     * Imports a ContactMessage into the database, handling the different data type structure
     */
    fun importContactMessage(cmsg: ContactMessage, serverPath: MServerPath, labels: String)
            : Throwable? {

        traceEvent(Trace.MessageDAO, "MessageDAO: Importing ContactMessage $cmsg")
        val coninfo = runCatching { Json.encodeToString(cmsg.contactInfo) }.getOrElse { return it }
        return DBProxy.get().execute(
            "MERGE INTO messages(id,format,labels,msgtype,date,subject,contents," +
                    "from_addr,to_addr,server_path,info) KEY(id) " +
                    "VALUES(?,?,?,?,?,?,?,?,?,?,?)",
            cmsg.id, "plain", labels, cmsg.subType, cmsg.date, cmsg.subject, cmsg.body,
            cmsg.from, cmsg.to, serverPath, coninfo
        )
    }

    /**
     * Imports a DeviceRequest into the database, handling the different data type structure
     */
    fun importDeviceRequest(devmsg: DevReqMsgItem, serverPath: MServerPath, labels: String)
            : Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: Importing DeviceRequest $devmsg")
        val devinfo = runCatching { Json.encodeToString(devmsg.info) }.getOrElse { return it }
        DBProxy.get().execute(
            "MERGE INTO messages(id,format,labels,msgtype,date,contents," +
                    "from_name, from_addr,to_addr,server_path,info,flags) KEY(id) " +
                    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
            devmsg.id, "plain", labels, devmsg.subType, devmsg.date ?: "", devmsg.contents,
            devmsg.fromName, devmsg.fromAddr, devmsg.toAddr, serverPath, devinfo,
            devmsg.getFlagString()
        )?.let { return it }
        return null
    }

    /** Imports a MsgBaseItem into the database */
    fun importMsgItem(msg: MsgBaseItem, serverPath: MServerPath, labels: String): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: Importing message item $msg")
        val infoStr = msg.getInfo().getOrElse { return it }
        return DBProxy.get().execute(
            "MERGE INTO messages(id,format,labels,msgtype,date,subject,contents," +
                    "from_name,from_addr,to_name,to_addr,server_path,info,flags) KEY(id) " +
                    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            msg.id, "plain", labels, msg.subType, msg.date ?: "", msg.subject, msg.contents,
            msg.fromName, msg.fromAddr, msg.toName, msg.toAddr, serverPath, infoStr,
            msg.getFlagString()
        )
    }

    /**
     * Retrieves from the database a list of all messages with information suitable for display.
     */
    fun loadHeaders(filter: Filter? = null): Result<List<MsgBaseHeaderItem>> {
        traceEvent(Trace.MessageDAO, "MessageDAO: loadHeaders(${filter?.name})")
        return queryHeaders(filter)
    }

    fun loadFilters(): Result<List<Filter>> {
        traceEvent(Trace.MessageDAO, "MessageDAO: loading filters")
        val out = mutableListOf(
            Filter("Inbox").apply {
                readOnly = true
                sorter = FilterSort("date", SortOrder.Ascending)
                add("labels:Inbox")
                add("-labels:Trash")
            },
            Filter("Sent").apply {
                sortBy("date")
                readOnly = true
                sorter = FilterSort("date", SortOrder.Ascending)
                add("labels:Sent")
                add("-labels:Trash")
            },
            Filter("Drafts").apply {
                sortBy("date")
                readOnly = true
                sorter = FilterSort("date", SortOrder.Ascending)
                add("labels:Drafts")
                add("-labels:Trash")
            },
            Filter("All Mail").apply {
                sortBy("date")
                readOnly = true
                sorter = FilterSort("date", SortOrder.Ascending)
                add("-labels:Sent")
                add("-labels:Trash")
            },
            Filter("Trash").apply {
                sortBy("date")
                readOnly = true
                sorter = FilterSort("date", SortOrder.Ascending)
                add("labels:Trash")
            }
        )

        val db = DBProxy.get()
        val rs = db.query("SELECT labels FROM messages").getOrElse { return Result.failure(it) }

        val labelSet = mutableSetOf<String>()
        while (rs.next()) {
            val labels = rs.getString("labels")?.split(",")?.toSet() ?: continue
            labelSet.addAll(labels)
        }
        labelSet.removeAll(listOf("", "Inbox", "Sent", "Drafts", "Trash"))
        out.addAll(
            labelSet.sorted().map { l ->
                Filter(l).apply {
                    sortBy("date")
                    add("labels:$l")
                    add("-labels:Trash")
                }
            }
        )
        return out.toSuccess()
    }

    fun load(id: String): Result<MsgBaseItem?> {
        traceEvent(Trace.MessageDAO, "MessageDAO: loading contents for $id")
        val db = DBProxy.get()

        val rs = db.query("SELECT * FROM messages WHERE id=?", id)
            .getOrElse { return it.toFailure() }

        if (!rs.next()) return Result.success(null)

        val msgSubType = rs.getString("msgtype") ?: ""

        return when (msgSubType) {
            "conreq.1", "conreq.2", "conup" -> resultSetToConMsgItem(id, rs)
            "devrequest" -> resultSetToDevReqItem(id, rs)
            else -> resultSetToMsgItem(id, rs)
        }
    }

    fun loadConMsg(id: String): Result<ConMsgItem?> {
        traceEvent(Trace.MessageDAO, "MessageDAO: loading contents for CR $id")

        val msg = load(id).getOrElse { return it.toFailure() }
            ?: return Result.success(null)

        return if (msg.subType.startsWith("conreq."))
            (msg as ConMsgItem).toSuccess()
        else
            MessageTypeMismatchException("Message is not a contact request").toFailure()
    }

    /**
     * Method for loading the results of a message query into a regular MsgItem. It exists primarily
     * to make the logic of load() clearer and to enable multiple message type handling.
     */
    private fun resultSetToMsgItem(id: String, rs: ResultSet): Result<MessageItem> {
        val out = MessageItem(
            rs.getString("subject") ?: "",
            rs.getString("contents") ?: "",
            id,
            rs.getString("format") ?: "plain"
        ).apply {
            cc = rs.getString("cc") ?: ""
            bcc = rs.getString("bcc") ?: ""
        }
        resultSetAssignCommonFields(out, rs)
        return out.toSuccess()
    }

    /**
     * Method for loading the results of a message query into a ConMsgItem. Like resultSetToMsgItem,
     * it exists primarily to make the logic of load() clearer and to enable multiple message
     * type handling.
     */
    private fun resultSetToConMsgItem(id: String, rs: ResultSet): Result<ConMsgItem> {
        val rawInfo = rs.getString("info")
            ?: return EmptyDataException("Contact Request with no contact info").toFailure()

        val loaded = runCatching { Json.decodeFromString<Contact>(rawInfo) }
            .getOrElse { return it.toFailure() }

        val out = ConMsgItem(
            subject = rs.getString("subject") ?: "",
            contents = rs.getString("contents") ?: "",
            id = id,
            format = rs.getString("format") ?: "plain"
        )
        resultSetAssignCommonFields(out, rs)
        out.contact = loaded

        rs.getString("labels")?.let { out.setLabels(it.split(", ").toSet()) }
        return out.toSuccess()
    }

    /**
     * Method for loading the results of a message query into a ConMsgItem. Like the corresponding
     * methods for ConmsgItem and MessageItem, it exists primarily to make the logic of load()
     * clearer and to enable multiple message type handling.
     */
    private fun resultSetToDevReqItem(id: String, rs: ResultSet): Result<DevReqMsgItem> {
        val rawInfo = rs.getString("info")
            ?: return EmptyDataException("Device Request with no device info").toFailure()

        val loaded = runCatching { Json.decodeFromString<DeviceInfo>(rawInfo) }
            .getOrElse { return it.toFailure() }

        val out = DevReqMsgItem(
            loaded,
            subject = rs.getString("subject") ?: "",
            contents = rs.getString("contents") ?: "",
            id = id,
        )
        resultSetAssignCommonFields(out, rs)
        return out.toSuccess()
    }

    /** Internal function to apply member values to fields common to all message types */
    @Suppress("DuplicatedCode")
    private fun resultSetAssignCommonFields(item: MsgBaseItem, rs: ResultSet) {
        item.apply {
            subType = rs.getString("msgtype") ?: ""
            date = stringToLocalDateTime(rs.getString("date"))
            threadID = rs.getString("threadid") ?: ""
            serverPath = rs.getString("server_path") ?: ""
            fromName = rs.getString("from_name") ?: ""
            fromAddr = rs.getString("from_addr") ?: ""
            toName = rs.getString("to_name") ?: ""
            toAddr = rs.getString("to_addr") ?: ""
            lastAction = rs.getString("last_action") ?: ""
            lastActionTime = rs.getString("last_action_time") ?: ""
        }
        rs.getString("labels")?.let { labelStr ->
            item.setLabels(labelStr.split(",").map { it.trim() }.toSet())
        }
    }

    fun saveSubject(id: String, subject: String): Throwable? {
        id.ifEmpty { return null }

        traceEvent(Trace.MessageDAO, "MessageDAO: Saving title of $id")
        with(DBProxy.get()) {
            execute(
                "UPDATE messages SET subject=? WHERE id=?", subject, id
            )?.let { return it }
        }
        return null
    }

    fun saveContents(id: String, contents: String): Throwable? {
        id.ifEmpty { return null }

        traceEvent(Trace.MessageDAO, "MessageDAO: Saving contents of $id")
        with(DBProxy.get()) {
            execute(
                "UPDATE messages SET contents=? WHERE id=?", contents, id
            )?.let { return it }
        }
        return null
    }

    fun save(msg: MsgBaseItem): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: Saving $msg")
        if (msg.date == null) return MissingFieldException("Required field `date` is empty")
        with(DBProxy.get()) {
            when (msg.javaClass.simpleName) {
                "MessageItem" -> {
                    msg as MessageItem
                    execute(
                        "MERGE INTO messages(id,format,labels,msgtype,date,subject,contents," +
                                "from_name,from_addr,to_addr,cc,bcc,threadid,server_path) KEY(id) " +
                                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        msg.id, msg.format, msg.getLabelString(), msg.subType, msg.date ?: "",
                        msg.subject, msg.contents, msg.fromName, msg.fromAddr, msg.toAddr, msg.cc,
                        msg.bcc, msg.threadID, msg.serverPath
                    )?.let { return it }
                }

                "ConMsgItem" -> {
                    msg as ConMsgItem
                    execute(
                        "MERGE INTO messages(id,format,labels,msgtype,date,subject,contents," +
                                "from_name,from_addr,to_addr,threadid,info,server_path) " +
                                "KEY(id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        msg.id, msg.format, msg.getLabelString(), msg.subType, msg.date ?: "",
                        msg.subject, msg.contents, msg.fromName, msg.fromAddr, msg.toAddr,
                        msg.threadID, Json.encodeToString(msg.contact), msg.serverPath
                    )?.let { return it }
                }

                "DevReqMsgItem" -> {
                    msg as DevReqMsgItem
                    execute(
                        "MERGE INTO messages(id,format,labels,msgtype,date,subject,contents," +
                                "from_name,from_addr,to_addr,threadid,info,server_path) " +
                                "KEY(id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        msg.id, "plain", msg.getLabelString(), msg.subType, msg.date ?: "",
                        msg.subject, msg.contents, msg.fromName, msg.fromAddr, msg.toAddr,
                        msg.threadID, Json.encodeToString(msg.info), msg.serverPath
                    )?.let { return it }
                }

                else -> return BadValueException("Bad message type '${msg.javaClass.simpleName}'")
            }
        }
        return null
    }

    fun delete(msg: MsgBaseHeaderItem): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: deleting $msg")
        val id = RandomID.fromString(msg.id)
            ?: return DatabaseException("Bad msg id '${msg.id}in MessageDAO.delete()")
        return delete(id)
    }

    fun delete(id: RandomID): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: deleting msg with id $id")
        with(DBProxy.get()) {
            execute("DELETE FROM messages WHERE id=?", id)?.let { return it }
        }
        return null
    }

    fun deleteFromPath(path: MServerPath): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: delete from path $path")
        with(DBProxy.get()) {
            execute("DELETE FROM messages WHERE server_path=?", path)?.let { return it }
        }
        return null
    }

    fun moveMessage(oldPath: MServerPath, newPath: MServerPath): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: moving message from $oldPath to $newPath")
        with(DBProxy.get()) {
            execute(
                "UPDATE messages SET server_path=? WHERE server_path=?",
                newPath,
                oldPath
            )?.let { return it }
        }
        return null
    }

    /** Applies a MessageAction value to a message specified by ID */
    fun applyAction(id: RandomID, action: MessageAction): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: applying action $action to message $id")
        return DBProxy.get().execute(
            "UPDATE messages SET last_action=?,last_action_time=? WHERE id=?",
            action, Timestamp(), id
        )
    }

    fun addLabel(msg: MsgBaseHeaderItem, label: String): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: adding label $label to $msg")
        with(DBProxy.get()) {
            msg.addLabel(label)
            execute("UPDATE messages SET labels=? WHERE id=?", msg.getLabelString(), msg.id)
                ?.let { return it }
        }
        return null
    }

    fun removeLabel(msg: MsgBaseHeaderItem, label: String): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: removing label $label from $msg")
        with(DBProxy.get()) {
            msg.removeLabel(label)
            execute("UPDATE messages SET labels=? WHERE id=?", msg.getLabelString(), msg.id)
                ?.let { return it }
        }
        return null
    }

    fun emptyTrash(): Throwable? {
        traceEvent(Trace.MessageDAO, "MessageDAO: emptying trash")
        with(DBProxy.get()) {
            execute("DELETE FROM messages WHERE labels ilike '%trash%'")?.let { return it }
        }
        return null
    }

    /** Generates and runs the necessary query to load note headers given a specified filter */
    @Suppress("DuplicatedCode")
    private fun queryHeaders(filter: Filter?): Result<List<MsgBaseHeaderItem>> {
        val out = mutableListOf<MsgBaseHeaderItem>()
        val db = DBProxy.get()

        val rs = if (filter != null)
            db.query(buildQueryString(filter, "messages")).getOrElse { return it.toFailure() }
        else
            db.query("SELECT * FROM messages ORDER BY date").getOrElse { return it.toFailure() }

        while (rs.next()) {
            val item = MessageHeaderItem(
                rs.getString("subject") ?: "",
                rs.getString("id") ?: "",
                rs.getString("format") ?: "",
            ).apply {
                date = stringToLocalDateTime(rs.getString("date"))
                fromName = rs.getString("from_name") ?: ""
                fromAddr = rs.getString("from_addr") ?: ""
                toName = rs.getString("to_name") ?: ""
                toAddr = rs.getString("to_addr") ?: ""
                lastAction = rs.getString("last_action") ?: ""
                lastActionTime = rs.getString("last_action_time") ?: ""
                setFlags(rs.getString("flags") ?: "")
            }

            rs.getString("labels")?.let { item.setLabels(it.split(",").toSet()) }
            out.add(item)
        }
        return out.toSuccess()
    }
}
