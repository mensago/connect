package connect.messages

import javafx.beans.property.SimpleObjectProperty
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libmensago.Contact

class ConMsgHeaderItem(subject: String = "", id: String = "", format: String = "plain") :
    MsgBaseHeaderItem(subject, id, format)

class ConMsgItem(
    subject: String = "", contents: String = "", id: String = "", format: String = "plain",
) : MsgBaseItem(subject, contents, id, format) {

    private val contactProperty =
        SimpleObjectProperty(Contact(Contact.EntityType.Individual))
    var contact: Contact
        get() = contactProperty.value
        set(value) = contactProperty.set(value)

    override fun toHeader(): ConMsgHeaderItem {
        return ConMsgHeaderItem(subject, id, format).let {
            it.date = date
            it.fromName = fromName
            it.fromAddr = fromAddr
            it.setLabels(getLabelString())
            it
        }
    }

    override fun getInfo(): Result<String> = runCatching { Json.encodeToString(contact) }

    override fun setInfo(s: String): Throwable? {
        contact = runCatching { Json.decodeFromString<Contact>(s) }.getOrElse { return it }
        return null
    }
}
