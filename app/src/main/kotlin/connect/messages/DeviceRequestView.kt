package connect.messages

import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import keznacl.Encryptor
import keznacl.toEncryptionKey
import libkeycard.RandomID
import libmensago.MessageAction

class DeviceRequestView(
    private val model: MessageModel,
    private val actionHandler: (RandomID, MessageAction, Encryptor?) -> Unit
) : VBox() {

    private val devReqProperty: SimpleObjectProperty<DevReqMsgItem> =
        SimpleObjectProperty()
    private var devReq: DevReqMsgItem?
        get() = devReqProperty.value
        set(value) {
            devReqProperty.set(value)
        }
    private val approveButton = Button("Approve").apply {
        setOnAction {
            if (devReq != null)
                devReq!!.info.pubkey.toEncryptionKey().getOrNull()?.let {
                    actionHandler(devReq!!.info.id, MessageAction.Approve, it)
                }
        }
        tooltip = Tooltip("Allow the device to log into your account.")
    }
    private val rejectButton = Button("Reject").apply {
        setOnAction {
            if (devReq != null)
                actionHandler(devReq!!.info.id, MessageAction.Reject, null)
        }
        tooltip =
            Tooltip("Reject the new device, preventing it from trying to log in.")
    }
    private val infoView = TextArea()

    init {
        alignment = Pos.TOP_CENTER
        spacing = 5.0
        padding = Insets(5.0)

        model.selectedItemClassProperty.subscribe(Runnable {
            devReq = model.selectedMsg as? DevReqMsgItem
        })

        devReqProperty.subscribe { item ->
            infoView.text = item?.contents ?: ""
            if (item != null) {
                if (item.lastAction.isNotEmpty()) {
                    approveButton.isDisable = true
                    rejectButton.isDisable = true

                    val action = MessageAction.fromString(item.lastAction)
                    when (action) {
                        MessageAction.Approve -> approveButton.style = "-fx-border-color: blue"
                        MessageAction.Reject -> rejectButton.style = "-fx-border-color: blue"
                        else -> {}
                    }
                } else {
                    approveButton.isDisable = false
                    approveButton.style = ""
                    rejectButton.isDisable = false
                    rejectButton.style = ""
                }
            }
        }

        val actionButtonBox = HBox().apply {
            alignment = Pos.CENTER
            spacing = 80.0
            children.addAll(approveButton, rejectButton)
        }

        // The empty label here functions beautifully as a blank space separator :)
        children.addAll(infoView, Separator(), Label(), actionButtonBox)
    }
}
