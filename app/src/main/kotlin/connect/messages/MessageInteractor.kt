package connect.messages

import connect.*
import connect.contacts.ContactDAO
import connect.profiles.getActiveProfile
import keznacl.BadValueException
import keznacl.Encryptor
import libkeycard.MAddress
import libkeycard.RandomID
import libmensago.MServerPath
import libmensago.MessageAction
import utilities.Trace
import utilities.toObservable
import utilities.traceEvent

class MessageInteractor(private val model: MessageModel) {
    private val broker = MessageBroker()

    init {
        addDemoData()
    }

    /** Adds a new new note from existing content */
    fun addMessage(msg: MsgBaseItem): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: add message ${msg.subject}")
        broker.save(msg)?.let { return it }
        if (msg is ConMsgItem) {
            val conHeader: ConMsgHeaderItem = msg.toHeader()
            model.msgList.add(conHeader)
        } else
            model.msgList.add(msg.toHeader())
        return null
    }

    /** Changes an item's server-side path */
    fun moveMessage(oldPath: MServerPath, newPath: MServerPath): Throwable? {
        traceEvent(
            Trace.MessageInteractor,
            "MessageInteractor: move message from $oldPath to $newPath"
        )
        if (model.selectedMsg?.serverPath.toString() == oldPath.toString())
            model.selectedMsg?.serverPath =
                newPath.clone().push(oldPath.basename()).getOrThrow().toString()
        return broker.moveMessage(oldPath, newPath)
    }

    /** Replaces the item at the specified path with the new one */
    fun replaceMessage(oldItem: MServerPath, newItem: FilterableItem): Throwable? {
        broker.deleteFromPath(oldItem)?.let { return it }
        return if (newItem is MessageItem)
            addMessage(newItem)
        else null
    }

    /** Creates a new user message */
    fun createDraft(): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: create new draft")
        val item = MessageItem.new(
            model.selectedMsg?.fromName ?: "",
            model.selectedMsg?.fromAddr ?: "", "",
            "",
            ""
        ).apply { setLabels("Drafts") }
        return addMessage(item)
    }

    /** Selects the requested message */
    fun selectMessage(newHeaderItem: MsgBaseHeaderItem?): Throwable? {
        if (newHeaderItem == null) {
            traceEvent(Trace.MessageInteractor, "MessageInteractor: select msg: none")
            model.selectedMsg = null
            model.selectedItemClass = ""
            model.selectedMsgID = ""
            model.selectedMsgContents = ""
            model.selectedMsgType = ""
            return null
        }

        if (model.selectedMsgID == newHeaderItem.id) return null

        traceEvent(Trace.MessageInteractor, "MessageInteractor: select msg $newHeaderItem")
        broker.load(newHeaderItem.id).getOrElse { e -> return e }?.let { loaded ->
            model.selectedMsg = loaded
            model.selectedItemClass = loaded.javaClass.simpleName
            model.selectedMsgID = loaded.id
            model.selectedMsgContents = loaded.contents
            model.selectedMsgType = loaded.subType

            val conItem = loaded as? ConMsgItem
            model.selectedConItem = if (conItem != null)
                PayloadConverter().conToContactItem(loaded.contact).getOrNull()
            else null
        }
        return null
    }

    /** Selects the requested item filter */
    fun selectFilter(newFilter: Filter?): Throwable? {
        if (newFilter == model.selectedFilterProperty.get()) return null

        traceEvent(Trace.MessageInteractor, "MessageInteractor: select filter $newFilter")
        broker.saveSelectedContents(model.selectedMsgID, model.selectedMsgContents)
            ?.let { return it }
        model.msgListProperty.set(broker.loadHeaders(newFilter).getOrThrow().toObservable())
        model.selectedFilterProperty.set(newFilter)
        return null
    }

    /** Saves the contents of the selected message to the database */
    fun saveSelectedContents(): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: save selected contents")
        return broker.saveSelectedContents(model.selectedMsgID, model.selectedMsgContents)
    }

    /** Moves selected messages to the trash */
    fun trashSelectedMessages(): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: trash selected message")

        val oldSelected = model.msgList.find { it.id == model.selectedMsgID } ?: return null
        model.msgList.remove(oldSelected)

        return if (oldSelected.getLabels().contains("Trash"))
            broker.delete(oldSelected)
        else
            broker.addLabel(oldSelected, "Trash")
    }

    /** Restores selected messages from the trash */
    fun restoreSelectedMessages(): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: restore selected messages")
        val selected = model.msgList.find { it.id == model.selectedMsgID } ?: return null
        model.msgList.remove(selected)
        return broker.removeLabel(selected, "Trash")
    }

    /** Empties the trash */
    fun emptyTrash(): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: empty trash")
        broker.emptyTrash()?.let { return it }
        if (model.selectedFilterProperty.get()?.name == "Trash")
            model.msgList.clear()
        return null
    }

    /** Adds demonstration message data */
    fun addDemoData() {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: add demo data")
        broker.addDemoDataToDB()
        model.msgListProperty.set(broker.loadHeaders(null).getOrThrow().toObservable())
    }

    /** Loads all available message filters from the database and places them in the model */
    fun loadFilters() {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: load filters")
        model.filterListProperty.set(broker.loadFilters().getOrThrow().toObservable())
    }

    /** Approves a contact request and deletes the selected message */
    fun approveCR(): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: approve CR")

        val info = UserInfoDAO().loadPublicContact().getOrElse { return it }
        val cr = model.selectedMsg as ConMsgItem
        Client().approveContactRequest(cr.contact.getWAddress()!!, info, cr.contact, true)
            ?.let { return it }

        val id = RandomID.fromString(cr.id)
            ?: return BadValueException("Bad selected message id ${cr.id}")
        MessageDAO().applyAction(id, MessageAction.Approve)?.let { e -> return e }

        return trashSelectedMessages()
    }

    /**
     * Rejects a contact request, deletes the selected message, and optionally adds the sender to
     * a block list to automatically delete future requests.
     */
    fun rejectCR(blockSender: Boolean): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: reject CR($blockSender)")
        val cr = model.selectedMsg as ConMsgItem
        if (blockSender)
            ContactDAO().blockContact(cr.contact.getWAddress()!!)?.let { return it }

        model.selectedMsg?.let {
            val id = RandomID.fromString(cr.id)
                ?: return BadValueException("Bad selected message id ${cr.id}")
            val action = if (blockSender) MessageAction.Block else MessageAction.Reject
            MessageDAO().applyAction(id, action)?.let { e -> return e }
        }

        return trashSelectedMessages()
    }

    /** Approves login for a new device and uploads the key package */
    fun approveDevice(devid: RandomID, enc: Encryptor): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: approve device")

        val profile = getActiveProfile().getOrElse { return it }
        with(Client()) {
            val addr = MAddress.fromWAddress(
                profile.getWAddress().getOrElse { disconnect(); return it }
            )
            login(addr).getOrElse { disconnect(); return it }
            handleNewDevice(devid, MessageAction.Approve, enc)
                ?.let { disconnect(); return it }
            disconnect()
        }

        model.selectedMsg?.let {
            val id = RandomID.fromString(model.selectedMsgID)
                ?: return BadValueException("Bad selected message id ${model.selectedMsgID}")
            MessageDAO().applyAction(id, MessageAction.Approve)?.let { e -> return e }
        }
        return trashSelectedMessages()
    }

    /** Rejects login for a new device and prevents future login attempts */
    fun rejectDevice(devid: RandomID): Throwable? {
        traceEvent(Trace.MessageInteractor, "MessageInteractor: reject device")

        val profile = getActiveProfile().getOrElse { return it }
        with(Client()) {
            val addr = MAddress.fromWAddress(
                profile.getWAddress().getOrElse { disconnect(); return it }
            )
            login(addr).getOrElse { disconnect(); return it }
            handleNewDevice(devid, MessageAction.Reject, null)
                ?.let { disconnect(); return it }
            disconnect()
        }

        model.selectedMsg?.let {
            val id = RandomID.fromString(model.selectedMsgID)
                ?: return BadValueException("Bad selected message id ${model.selectedMsgID}")
            MessageDAO().applyAction(id, MessageAction.Reject)?.let { e -> return e }
        }

        return trashSelectedMessages()
    }
}
