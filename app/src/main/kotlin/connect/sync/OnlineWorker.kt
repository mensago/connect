package connect.sync

import connect.Client
import connect.main.FilterItemProcessor
import connect.profiles.ProfileManager
import connect.profiles.ProfilesModel
import libmensago.WorkspaceStatus
import java.time.Duration
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference

/**
 * The OnlineWorker class represents a background thread which runs during regular execution to
 * download and process updates from the server and to monitor online status.
 */
class OnlineWorker(itemproc: FilterItemProcessor, model: ProfilesModel) {

    private var workerThread = AtomicReference<Thread?>(null)
    private val quitThread = AtomicBoolean(false)

    private val client = Client()
    val handler = IncomingHandler(client, itemproc, model)

    // Flag used just for unit testing
    var testMode = false

    /**
     * Launches the instance's internal update thread. Updates will not be processed without
     * calling this. This call should be invoked ONLY ONCE in any application.
     */
    fun launch() {
        if (workerThread.get() == null)
            workerThread.set(Thread.ofVirtual().start { incomingWorker() })
    }

    /** Shuts down the update thread. */
    fun shutdown() {
        quitThread.set(true)
        if (workerThread.get() != null) {
            workerThread.get()!!.join()
            workerThread.set(null)
        }
    }

    /** Method used by IncomingHandler to periodically check for and process updates. */
    fun incomingWorker() {

        // TODO: Update ProfilesModel to set online status for application.

        if (ProfileManager.model.activeProfile == null) {
            println("No active profile. Update worker aborting.")
            workerThread.set(null)
            return
        }

        // ensureLoggedIn() returns null if we need to quit
        var workspaceStatus = ensureLoggedIn(client) ?: return

        // Updating this ensures the rest of the application knows we're online or not
        if (workspaceStatus != ProfileManager.model.activeIdentityStatus)
            ProfileManager.model.activeIdentityStatus = workspaceStatus

        if (workspaceStatus == WorkspaceStatus.Active) {
            handler.downloadUpdates()?.let {
                if (testMode) throw it
                println("Error downloading updates:")
                println(it.stackTraceToString())
            }

            handler.executeTasks()?.let {
                if (testMode) throw it
                println("Error executing update tasks:")
                println(it.stackTraceToString())
            }
        }

        do {
            // ensureLoggedIn() returns null if we need to quit
            workspaceStatus = ensureLoggedIn(client) ?: break

            // Updating this ensures the rest of the application knows we're online or not
            if (workspaceStatus != ProfileManager.model.activeIdentityStatus)
                ProfileManager.model.activeIdentityStatus = workspaceStatus

            if (workspaceStatus != WorkspaceStatus.Active) continue

            // checkForUpdates returns true if there are updates on the server that need to be
            // processed.
            if (!handler.checkForUpdates(client)) continue

            val err = handler.downloadUpdates()
            if (err != null) {
                if (testMode) throw err
                println("Error downloading updates:")
                println(err.stackTraceToString())
                continue
            }

            handler.executeTasks()?.let {
                if (testMode) throw it
                println("Error executing update tasks:")
                println(it.stackTraceToString())
            }

        } while (!testMode && !quitThread.get())

        client.logout()
        client.disconnect()

        workerThread.set(null)
    }

    /**
     * Private method called by incomingWorker that tries to connect to the server. It returns false
     * only to signal to the worker thread that it needs to exit.
     */
    private fun ensureLoggedIn(client: Client): WorkspaceStatus? {
        // Try to connect every 5 minutes, but check the quit flag every 5 seconds. The last
        // thing we need is having to force Connect to quit because the wifi dropped.

        val profile = ProfileManager.model.activeProfile ?: return null

        var out = WorkspaceStatus.Unknown
        do {
            val result = client.login(profile.getMAddress().getOrElse { return null })
            if (result.isFailure) {
                Thread.sleep(Duration.ofSeconds(5))
                if (quitThread.get()) return null
                continue
            }
            out = result.getOrNull()!!.first
        } while (out == WorkspaceStatus.Unknown && !testMode)

        return out
    }

}