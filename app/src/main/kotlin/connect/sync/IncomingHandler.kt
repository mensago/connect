package connect.sync

import connect.*
import connect.contacts.ContactDAO
import connect.main.FilterItemProcessor
import connect.messages.ConMsgItem
import connect.messages.MessageDAO
import connect.notes.NoteDAO
import connect.profiles.*
import keznacl.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import libkeycard.BadFieldValueException
import libkeycard.Domain
import libkeycard.RandomID
import libmensago.*
import libmensago.commands.download
import libmensago.commands.getUpdates
import utilities.AppSettings
import utilities.DBDataException
import utilities.MissingProfileException
import java.nio.file.Path
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.*
import kotlin.io.path.exists

/**
 * The IncomingHandler class is used to check for updates on workspaces. It is intended to download
 * update events, process them to generate update tasks, and then execute the update tasks.
 *
 * Although IncomingHandler methods can be called on their own, it is primarily intended to be
 * used with a separate thread that it maintains internally. Calling launch() and shutdown() are
 * all that are needed to utilize this usage scenario. It comes with the drawback that the
 * active profile MUST not change while the updater thread is running in the background.
 */
class IncomingHandler(
    private val client: Client, private val itemproc: FilterItemProcessor,
    private val model: ProfilesModel
) {
    var updatePeriodInSeconds = 300

    private val dao = SyncDAO()
    private val converter = PayloadConverter()

    private var connectedErrorCount = 0
    private var secondsSinceLastIdle = 0

    /**
     * Checks for updates on the server, downloads them, processes them into a list of tasks to
     * perform, and saves them to a queue in the database.
     */
    fun downloadUpdates(): Throwable? {

        // First, check the update table for existing ones
        val settings = AppSettings.newFromDB().getOrElse { return it }
        val lastCheckStr = settings.getField("last_update")?.first ?: "0"
        var lastCheck = runCatching { lastCheckStr.toLong() }
            .getOrElse { return DBDataException("Bad last_update time in settings") }

        val updates = getUpdates(client.getConnection(), lastCheck).getOrElse { return it }
        lastCheck = (System.currentTimeMillis() / 1000)
        settings.setField("last_update", lastCheck.toString(), false)?.let { return it }

        if (updates.isEmpty()) return null

        /*
         * The second update processing stage represented by processUpdates is necessary because
         * updates are a series of events -- a message can be delivered, replaced, and deleted in
         * the span of time represented by several update items. In cases such as this, the
         * IncomingHandler doesn't need to do anything at all. It reduces work needing to be done
         * and prevents errors.
         */
        processUpdates(updates).getOrElse { return it }.forEach { entry ->
            if (!dao.hasIncomingRecords(entry.id).getOrElse { return it })
                dao.queueIncomingItem(entry)?.let { err -> return err }
        }

        return null
    }

    /**
     * Currently this function merely loads and executes sync items directly from the database.
     * Once processUpdates is completed, it will instead load sync actions from the database and
     * execute them.
     */
    fun executeTasks(): Throwable? {
        val profile = getActiveProfile().getOrElse { return it }
        val thisDevice = profile.devid

        val updates = dao.loadIncomingItems().getOrElse { return it }
        for (update in updates) {

            // We skip any updates which were originally sourced by this device because the
            // associated actions don't need to be done.
            if (update.source == thisDevice) {
                dao.deleteIncomingItem(update.id)
                continue
            }

            when (update.op) {
                SyncOp.Create -> processCreateRecord(update, profile)?.let { return it }
                SyncOp.Delete -> processDeleteRecord(update)?.let { return it }
                SyncOp.Move -> processMoveRecord(update)?.let { return it }
                SyncOp.Receive -> processReceiveRecord(update, profile)?.let { return it }
                SyncOp.Replace -> processReplaceRecord(update, profile)?.let { return it }
                SyncOp.Rotate -> processRotateRecord(update)?.let { return it }
                else -> {
                    // We don't do anything for Mkdir or Rmdir
                }
            }
            dao.deleteIncomingItem(update.id)
        }

        return null
    }

    /** Ensures that the incoming update handler is connected to the requested domain */
    private fun ensureConnected(dom: Domain): Throwable? {
        if (client.isConnected() && client.currentDomain == dom) return null

        client.disconnect()?.let { return it }

        if (model.activeProfile == null)
            return MissingProfileException()

        val address = model.activeProfile!!.getMAddress().getOrElse { return it }
        return client.login(address).exceptionOrNull()
    }

    /**
     * Corresponds to ensureConnected(), ensuring that the server connection is closed. This call
     * is intended to primarily be called from within lambdas and in order to keep error handling
     * concise, it takes an optional Throwable that it returns unchanged. Note that if there is
     * a problem disconnecting from the server -- which should be never -- the error is shadowed
     * by the error passed to the call.
     */
    private fun ensureDisconnected(outError: Throwable? = null): Throwable? {
        client.disconnect()?.let { return outError ?: it }
        return null
    }

    private fun processCreateRecord(update: SyncItem, profile: Profile): Throwable? {
        // Create operations always contain an MServerPath referencing a file

        // Steps:
        // Download file
        // Read and decrypt file
        // Pass to processPayload for conversion to domain object and routing
        val tempPath = Paths.get(
            ProfileManager.profilesPath, profile.getProfileName(), "temp", update.id.toString()
        )

        ensureConnected(update.waddress.domain)?.let { return it }
        update.value as SyncValue.Create
        val payload = getEnvelope(update.value.serverpath, tempPath).getOrElse { return it }
        processPayload(update, payload)?.let { return ensureDisconnected(it) }
        tempPath.toFile().delete()

        return null
    }

    private fun processDeleteRecord(update: SyncItem): Throwable? {
        // Delete operations always contain an MServerPath referencing a file

        // Steps:
        // Look up file based on server path
        // Delete file from database
        update.value as SyncValue.Delete
        val pdao = ProfileDAO()
        val info = pdao.getItemInfo(update.value.serverpath).getOrElse { return it }
        deleteItem(info)?.let { return it }
        return pdao.removeItemInfo(update.value.serverpath)
    }

    private fun processMoveRecord(update: SyncItem): Throwable? {
        // Move operations contain the path to a file and the path to its destination
        // directory. Moves can never rename an item.

        // Steps:
        // Look up file based on server path
        // Look up destination directory
        // Check to see if file exists in database with old path
        //      If yes, update file entry with new path
        //      If no, attempt to download the file like a CREATE record
        update.value as SyncValue.Move
        itemproc.processMoveItem(update.value.src, update.value.dest)

        return null
    }

    private fun processReceiveRecord(update: SyncItem, profile: Profile): Throwable? {
        // Receive operations are like Create in that they always contain an MServerPath
        // referencing a file

        // Steps:
        // Download file
        // Read and decrypt file
        // Pass to processPayload for conversion to domain object and routing
        val tempPath = Paths.get(
            ProfileManager.profilesPath, profile.getProfileName(), "temp", update.id.toString()
        )

        ensureConnected(update.waddress.domain)?.let { return it }
        update.value as SyncValue.Receive
        val payload = getEnvelope(update.value.serverpath, tempPath)
            .getOrElse { return ensureDisconnected(it) }
        processPayload(update, payload)?.let { return ensureDisconnected(it) }
        tempPath.toFile().delete()

        return null
    }

    private fun processReplaceRecord(update: SyncItem, profile: Profile): Throwable? {
        // Replace operations contain two file paths.

        // Steps:
        // Download file
        // Read and decrypt file
        // Pass to processPayload for conversion to domain object and routing
        update.value as SyncValue.Replace
        ensureConnected(update.waddress.domain)?.let { return ensureDisconnected(it) }
        val tempPath = Paths.get(
            ProfileManager.profilesPath, profile.getProfileName(), "temp", update.id.toString()
        )

        val payload = getEnvelope(update.value.newfile, tempPath).getOrElse { return it }
        processPayload(update, payload)?.let { return ensureDisconnected(it) }

        return null
    }

    private fun processRotateRecord(update: SyncItem): Throwable? {
        // Steps:
        // Download file
        // Read and decrypt file
        // Pass to something else to update keys

        // POSTDEMO("Implement processRotateRecord($update)")
        println("processRotateRecord($update)")
        throw NotImplementedError()
    }

    /**
     * Downloads from a server an encrypted envelope file and returns the decrypted JSON payload.
     */
    private fun getEnvelope(item: MServerPath, tempFilePath: Path): Result<Payload> {
        val tempDir = tempFilePath.parent.toFile()
        if (!tempDir.exists())
            runCatching { tempDir.mkdirs() }.getOrElse { return it.toFailure() }

        if (!tempFilePath.exists())
            download(client.getConnection(), item, tempFilePath.toString())
                .getOrElse { return it.toFailure() }
        val env = Envelope.fromFile(tempFilePath.toFile())
            .getOrElse { return it.toFailure() }

        val result = KeyDAO().loadKeypair(env.tag.keyHash)
        val decryptor: Decryptor = if (result.isFailure) {
            if (result.exceptionOrNull() !is ResourceNotFoundException)
                return result.exceptionOrNull()!!.toFailure()

            KeyDAO().loadRelEncryptionPair(env.tag.keyHash)
                .getOrElse { return it.toFailure() }

        } else {
            val cspair = result.getOrNull()!!
            cspair.toSecretKey()
                ?: cspair.toEncryptionPair()
                ?: return DBDataException("Bad decryption key in processUpdates()").toFailure()
        }

        val payloadDecryptor: Decryptor = if (env.tag.payloadKey != null) {
            // If the payload key in the header is non-null, it means that the key
            // specified by the hash decrypts the ephemeral payload key. If it's null,
            // it means that the specified key decrypts the payload directly
            val raw = decryptor.decrypt(env.tag.payloadKey!!)
                .getOrElse { return it.toFailure() }
                .decodeToString()
            val keyCS = CryptoString.fromString(raw)
                ?: return BadFieldValueException("Bad payload key data in envelope").toFailure()
            keyCS.toSecretKey().getOrElse { "Bad payload key in envelope" } as Decryptor
        } else decryptor

        return Payload.decrypt(payloadDecryptor, env.payload)
    }

    /**
     * Converts a decrypted JSON payload into a domain item and passes it onto the main controller
     * with the appropriate action as specified by the update record.
     */
    private fun processPayload(update: SyncItem, payload: Payload): Throwable? {
        val infoType: String
        var serverPath: MServerPath? = null
        val dbid: RandomID?
        val newItem = when (payload.type) {
            PayloadType.Message -> {
                infoType = "message"
                val msg = Message.fromPayload(payload).getOrElse { return it }
                dbid = msg.id
                val newPath = replaceIncomingMessage(update, msg).getOrElse { return it }
                serverPath = newPath
                MessageDAO().import(msg, newPath, "Inbox,Unread")?.let { return it }
                converter.messageToMessageItem(msg).getOrElse { return it }.apply {
                    setLabels("Inbox,Unread")
                }
            }

            PayloadType.DeviceRequest -> {
                infoType = "devreq"
                val devmsg = DeviceRequest.fromPayload(payload).getOrElse { return it }
                dbid = devmsg.id
                val newPath = replaceDeviceMessage(update, devmsg).getOrElse { return it }
                serverPath = newPath

                val labels = "Inbox,Unread"
                val item = converter.devReqToDevReqMsgItem(devmsg).getOrElse { return it }.apply {
                    setLabels(labels)
                }
                MessageDAO().importDeviceRequest(item, newPath, labels)?.let { return it }
                item
            }

            PayloadType.ContactMessage -> {
                infoType = "conmsg"
                val cmsg = ContactMessage.fromPayload(payload).getOrElse { return it }
                dbid = cmsg.id
                val newPath = replaceContactMessage(update, cmsg).getOrElse { return it }
                serverPath = newPath

                val labels = if (cmsg.subType != "conup")
                    "Inbox,Unread,Contact Requests"
                else ""
                MessageDAO().importContactMessage(cmsg, newPath, labels)?.let { return it }
                converter.conMsgToConMsgItem(cmsg).getOrElse { return it }.apply {
                    setLabels(labels)
                }
            }

            PayloadType.Contact -> {
                infoType = "contact"
                val con = Contact.fromPayload(payload).getOrElse { return it }
                dbid = con.id
                ContactDAO().import(con)?.let { return it }
                converter.conToContactItem(con).getOrElse { return it }
            }

            PayloadType.Note -> {
                infoType = "note"
                val receiveRecord = update.value as SyncValue.Receive
                val note = Note.fromPayload(payload).getOrElse { return it }
                dbid = note.id
                NoteDAO().import(note, receiveRecord.serverpath)
                converter.noteToNoteItem(note).getOrElse { return it }
            }

            else -> return Exception(
                "BUG: Unsupported payload type ${payload.type} in processPayload"
            )
        }

        when (update.op) {
            SyncOp.Create -> {
                update.value as SyncValue.Create
                if (serverPath == null) serverPath = update.value.serverpath
            }

            SyncOp.Receive -> {
                if (newItem is ConMsgItem) {
                    if (newItem.subType == "conup")
                        itemproc.updateContactInfo(newItem)
                    else
                        itemproc.processNewItem(newItem)?.let { return it }
                } else
                    itemproc.processNewItem(newItem)
                update.value as SyncValue.Receive
                if (serverPath == null) serverPath = update.value.serverpath
            }

            SyncOp.Replace -> {
                update.value as SyncValue.Replace
                itemproc.processReplaceItem(update.value.oldfile, newItem)
                if (serverPath == null) serverPath = update.value.newfile
            }

            else -> return Exception("BUG: Unsupported op ${update.op} in processPayload")
        }

        val profile = getActiveProfile().getOrElse { return it }
        val waddress = profile.getWAddress().getOrElse { return it }
        val itemInfo = ServerItemInfo(waddress, infoType, serverPath, dbid)
        val pdao = ProfileDAO()
        return pdao.addItemInfo(itemInfo)
    }

    /**
     * Takes an incoming contact message which came in externally, creates a new file for internal
     * sync, and uploads the new the message to replace the incoming one. This is similar to
     * replaceNewMessage() except that it is specific to ContactMessages and handles internal
     * processing differently
     *
     * @return A server path to the new file
     */
    private fun replaceContactMessage(
        update: SyncItem,
        msg: ContactMessage
    ): Result<MServerPath> {
        val storageKey = KeyDAO().loadKeyByPurpose(KeyPurpose.Storage)
            .getOrElse { return it.toFailure() }
            .first.toSecretKey()
            .getOrElse { return it.toFailure() }

        val env = Envelope.seal(
            storageKey,
            AddressPair(msg.from, msg.to),
            Payload(PayloadType.ContactMessage, Json.encodeToString(msg))
        ).getOrThrow()

        return replaceMessage(update, env)
    }

    /**
     * Takes an incoming device request message which came in externally, creates a new file for
     * internal sync, and uploads the new the message to replace the incoming one. This is similar
     * to replaceNewMessage() except that it is specific to DeviceRequests and handles internal
     * processing differently
     *
     * @return A server path to the new file
     */
    private fun replaceDeviceMessage(update: SyncItem, msg: DeviceRequest): Result<MServerPath> {
        val storageKey = KeyDAO().loadKeyByPurpose(KeyPurpose.Storage)
            .getOrElse { return it.toFailure() }
            .first.toSecretKey()
            .getOrElse { return it.toFailure() }

        val env = Envelope.seal(
            storageKey,
            AddressPair(msg.from, msg.to),
            Payload(PayloadType.DeviceRequest, Json.encodeToString(msg))
        ).getOrThrow()

        return replaceMessage(update, env)
    }

    /**
     * Takes an existing message which came in externally, creates a new file for internal sync,
     * and uploads the new the message to replace the incoming one.
     *
     * @return A server path to the new file
     */
    private fun replaceIncomingMessage(update: SyncItem, msg: Message): Result<MServerPath> {
        val storageKey = KeyDAO().loadKeyByPurpose(KeyPurpose.Storage)
            .getOrElse { return it.toFailure() }
            .first.toSecretKey()
            .getOrElse { return it.toFailure() }

        val env = Envelope.seal(
            storageKey,
            AddressPair(msg.from, msg.to),
            Payload(PayloadType.Message, Json.encodeToString(msg))
        ).getOrThrow()

        return replaceMessage(update, env)
    }

    /**
     * Convenience function which handles all the common replacement code used by
     * replaceIncomingMessage and replaceContactMessage
     */
    private fun replaceMessage(update: SyncItem, env: Envelope): Result<MServerPath> {
        val profile = getActiveProfile().getOrElse { return it.toFailure() }

        update.value as SyncValue.Receive
        val incomingName = update.value.serverpath.basename()

        val tempFilePath = Paths.get(
            ProfileManager.profilesPath, profile.getProfileName(), "temp", incomingName
        )
        env.saveWithName(tempFilePath.toString())?.let { return it.toFailure() }

        val waddr = profile.getWAddress().getOrElse { return it.toFailure() }

        val nameParts = incomingName.split(".")
        val unixtime = nameParts[0].toLong()
        val date = Date(unixtime * 1000)
        val sdf = SimpleDateFormat("yyyyMM").format(date)
        val year = sdf.substring(0, 4).toInt()
        val month = sdf.substring(4, 6).toInt()

        val uploadPath = MServerPath("/ ${waddr.id} messages $year $month")
        ensureServerDir(client, uploadPath)?.let { return it.toFailure() }

        val replacementPath = update.value.serverpath.parent()!!.push(incomingName)
            .getOrElse { return it.toFailure() }

        client.upload(tempFilePath.toString(), uploadPath, replacementPath)
            .getOrElse { return it.toFailure() }

        return replacementPath.toSuccess()
    }

    private fun deleteItem(info: ServerItemInfo): Throwable? {
        return when (info.itemtype) {
            "message" -> MessageDAO().delete(info.dbid)
            "contact" -> ContactDAO().delete(info.dbid)
            "note" -> NoteDAO().delete(info.dbid)

            else -> BadValueException(
                "Bad item type '${info.itemtype}' in processPayload.deleteItem"
            )
        }
    }

    /** Ensures that a directory path exists, creating any parent directories as needed */
    private fun ensureServerDir(client: Client, path: MServerPath): Throwable? {

        // In the interest of efficiency, this method starts with the full path and works its way
        // up the chain until it finds a directory that exists such that if the full path exists --
        // which will be the common case -- only one EXISTS call is made.

        val parts = path.toString().split(" ")

        // Believe it or not, we can't forEachIndexed in combination with reversed() or asReversed()
        // because the indices are not returned in reverse order even though the items themselves
        // are. *sigh*
        var startIndex = 0
        for (i in parts.indices.reversed()) {
            val stringPath = parts.subList(0, i + 1).joinToString(" ")
            val currentPath = MServerPath(stringPath)
            if (client.exists(currentPath).getOrElse { return it }) {
                startIndex = i
                break
            }
        }

        for (i in startIndex until parts.size) {
            val currentPath = MServerPath(parts.subList(0, i + 1).joinToString(" "))
            client.mkdir(currentPath)?.let { return it }
        }
        return null
    }

    /**
     * Takes in a list of update events and returns an optimized list of actual tasks to be
     * performed later.
     */
    private fun processUpdates(updates: List<SyncItem>): Result<List<SyncItem>> {
        val out = mutableMapOf<String, SyncItem>()
        updates.forEach { item ->
            when (item.op) {
                SyncOp.Create -> {
                    // Create and Receive ops are more or less the same on the client side in that
                    // the client needs to download and process a file. As a result, we're going to
                    // convert Create items to Receive tasks because Create tasks mostly because
                    // the difference between the two on the server side is the source of the new
                    // file.
                    with(item) {
                        value as SyncValue.Create
                        SyncItem(
                            id, SyncOp.Receive, waddress, SyncValue.Receive(value.serverpath),
                            unixtime, source
                        ).also {
                            out[(item.value as SyncValue.Create).serverpath.toString()] = it
                        }
                    }
                }

                SyncOp.Delete -> {
                    val serverPath = (item.value as SyncValue.Delete).serverpath.toString()
                    if (out.containsKey(serverPath))
                        out.remove(serverPath)
                    else
                        out[serverPath] = item
                }

                SyncOp.Mkdir ->
                    out[(item.value as SyncValue.Mkdir).serverpath.toString()] = item

                SyncOp.Move -> {
                    item.value as SyncValue.Move
                    val srcStr = item.value.src.toString()
                    val destStr = item.value.dest.toString()
                    if (out.containsKey(srcStr)) {
                        val existing = out.remove(srcStr)!!
                        val moved = moveSyncTask(existing, item.value.dest)
                        out[moved.value.toString()] = moved
                    } else
                        out[destStr] = item
                }

                SyncOp.Receive ->
                    out[(item.value as SyncValue.Receive).serverpath.toString()] = item

                SyncOp.Replace -> {
                    item.value as SyncValue.Replace
                    val oldPath = item.value.oldfile.toString()
                    val newPath = item.value.newfile.toString()
                    if (out.containsKey(oldPath)) {
                        out.remove(oldPath)

                        val newItem = SyncItem(
                            item.id, SyncOp.Receive, item.waddress,
                            SyncValue.Receive(item.value.newfile), item.unixtime, item.source
                        )
                        out[newPath] = newItem

                    } else out[newPath] = item
                }

                SyncOp.Rmdir -> {
                    val serverPath = (item.value as SyncValue.Rmdir).serverpath.toString()
                    if (out.containsKey(serverPath))
                        out.remove(serverPath)
                    else
                        out[serverPath] = item
                }

                SyncOp.Rotate -> TODO("Implement SyncOp.Rotate processing")
            }
        }
        return out.map { it.value }.toList().toSuccess()
    }

    private fun moveSyncTask(item: SyncItem, newDir: MServerPath): SyncItem {

        return when (item.op) {
            SyncOp.Receive -> {
                item.value as SyncValue.Receive
                val newPath = newDir.clone().push(item.value.serverpath.basename()).getOrThrow()
                SyncItem(
                    item.id, item.op, item.waddress, SyncValue.Receive(newPath), item.unixtime,
                    item.source
                )
            }

            SyncOp.Replace -> {
                item.value as SyncValue.Replace
                SyncItem(
                    item.id, item.op, item.waddress, SyncValue.Replace(item.value.oldfile, newDir),
                    item.unixtime, item.source
                )
            }

            else -> throw UnsupportedOperationException()
        }
    }

    /**
     * Handles the periodic update checks. The IDLE command is sent only once every 5 minutes.
     * However, if
     *
     * @return true if there are updates to download and process.
     */
    fun checkForUpdates(client: Client): Boolean {
        // Only send an IDLE command if the polling wait time has expired
        Thread.sleep(1000)
        secondsSinceLastIdle++
        if (secondsSinceLastIdle < updatePeriodInSeconds)
            return false
        else
            secondsSinceLastIdle = 0

        val updateCount = client.idle().getOrElse {
            // If we got errors from the IDLE command over the course of 5 consecutive checks,
            // we will try refreshing the client connection in the hopes that it fixes the problem.
            connectedErrorCount++
            if (connectedErrorCount >= 5) {
                client.logout()
                client.disconnect()
            }
            return false
        }

        connectedErrorCount = 0

        return updateCount > 0
    }
}
