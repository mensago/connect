package connect

import javafx.beans.property.SimpleMapProperty
import javafx.beans.property.SimpleStringProperty
import libmensago.Attachment
import utilities.toObservable

/**
 * The DataHeaderItem class is a generic container for the different kinds of data used in Connect.
 * It is used to represent items stored in the database without having loaded the items' content.
 */
open class FilterableHeaderItem(idStr: String = "", formatStr: String = "") {
    private val metadata = HashMap<String, SimpleStringProperty>()
    protected val labels = mutableSetOf<String>().toObservable()
    protected val flags = mutableSetOf<String>().toObservable()

    /**
     * The filter() method returns true if the given Filter instance matches the object and false
     * if it doesn't.
     */
    open fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    protected open fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "label" -> term.compareSet(labels)
            "flag" -> term.compareSet(flags)

            "format" -> term.compare(format)

            else -> false
        }
    }

    fun addLabel(label: String) {
        if (label.isNotEmpty()) labels.add(label)
    }

    fun removeLabel(label: String) = labels.remove(label)

    fun setLabels(labelStr: String) = setLabels(labelStr.split(", ").toSet())
    fun setLabels(newLabels: Set<String>) {
        labels.clear()
        newLabels.forEach { labels.add(it) }
    }

    fun clearLabels() = labels.clear()

    fun getLabels(): MutableSet<String> = labels
    fun getLabelString(): String = labels.toList().sorted().joinToString(",")
    fun hasLabel(label: String): Boolean = labels.contains(label)

    // Flags are indicators used internally for managing state, such as marking a message as
    // important
    fun addFlag(flag: String) {
        if (flags.isNotEmpty()) flags.add(flag)
    }

    fun removeFlag(flag: String) = flags.remove(flag)

    fun setFlags(flagStr: String) = setFlags(flagStr.split(", ").toSet())
    fun setFlags(newFlags: Set<String>) {
        flags.clear()
        newFlags.forEach { flags.add(it) }
    }

    fun clearFlags() = flags.clear()

    fun getFlags(): MutableSet<String> = flags
    fun getFlagString(): String = flags.toList().sorted().joinToString(",")
    fun hasFlag(flag: String): Boolean = flags.contains(flag)

    private val idProperty = SimpleStringProperty(idStr)
    var id: String
        get() = idProperty.value
        set(value) = idProperty.set(value)

    private val formatProperty = SimpleStringProperty(formatStr)
    var format: String
        get() = formatProperty.value
        set(value) = formatProperty.set(value)
}

/**
 * The DataItem class is a generic container for the different kinds of data used in Connect. It
 * contains the full data representation of an item belonging to the user.
 */
open class FilterableItem(contents: String = "", id: String = "", format: String = "") :
    FilterableHeaderItem(id, format) {

    override fun filter(f: Filter): Boolean {
        f.terms.find { term -> match(term) }?.let { return true }
        return false
    }

    override fun match(term: FilterTerm): Boolean {
        return when (term.property.lowercase()) {
            "contents" -> term.compare(term.value)

            else -> super.match(term)
        }
    }

    private val contentsProperty = SimpleStringProperty(contents)
    var contents: String
        get() = contentsProperty.value
        set(value) = contentsProperty.set(value)

    private val attachmentProperty = SimpleMapProperty(
        mutableMapOf<String, Attachment>().toObservable()
    )
    var attachments
        get() = attachmentProperty.get()
        set(value) = attachmentProperty.set(value?.toObservable())
}
