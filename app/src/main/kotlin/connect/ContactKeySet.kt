package connect

import keznacl.EncryptionPair
import keznacl.SigningPair
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.BadFieldException

class ContactKeySet(var encryptionPair: EncryptionPair, var signingPair: SigningPair) {

    /**
     * Returns a map suitable for attaching to a Contact structure's key field that only contains
     * the public keys
     */
    fun toPublicMap(): MutableMap<String, String> {
        return mutableMapOf(
            "Encryption" to encryptionPair.pubKey.toString(),
            "Verification" to signingPair.pubKey.toString(),
        )
    }

    override fun toString(): String {
        return "ContactKeySet($encryptionPair, $signingPair)"
    }

    companion object {
        private val validUsage = setOf(
            "Encryption", "Verification", "MyEncryption", "MyDecryption", "MySigning",
            "MyVerification"
        )

        fun fromMap(keys: Map<String, String>): Result<ContactKeySet> {
            for (key in keys.keys)
                if (!validUsage.contains(key))
                    return BadFieldException("Bad key type $key").toFailure()

            return ContactKeySet(
                EncryptionPair.fromStrings(keys["MyEncryption"]!!, keys["MyDecryption"]!!)
                    .getOrElse { return it.toFailure() },
                SigningPair.fromStrings(keys["MyVerification"]!!, keys["MySigning"]!!)
                    .getOrElse { return it.toFailure() },
            ).toSuccess()
        }

        /* Generates a new ContactKeySet */
        fun generate(): Result<ContactKeySet> {
            return ContactKeySet(
                EncryptionPair.generate().getOrElse { return it.toFailure() },
                SigningPair.generate().getOrElse { return it.toFailure() },
            ).toSuccess()
        }
    }
}