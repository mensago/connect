package connect.notes

import connect.Filter
import connect.buildQueryString
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.RandomID
import libmensago.MServerPath
import libmensago.Note
import libmensago.db.DatabaseException
import utilities.DBProxy
import utilities.Trace
import utilities.traceEvent

/** DAO class for notes */
class NoteDAO {

    fun initNoteTable(): Throwable? {
        return DBProxy.get().execute(
            """CREATE TABLE IF NOT EXISTS notes (
            id	        UUID PRIMARY KEY NOT NULL UNIQUE,
            format      TEXT NOT NULL,
            labels      TEXT,
            title       TEXT NOT NULL,
            contents    TEXT,
            server_path TEXT
        );"""
        )
    }

    fun import(note: Note, serverPath: MServerPath): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: Saving $note")
        with(DBProxy.get()) {
            execute(
                "MERGE INTO notes(id,title,format,contents,server_path) KEY(id) " +
                        "VALUES(?,?,?,?,?)",
                note.id, note.title, note.format, note.contents, serverPath
            )?.let { return it }
        }
        return null
    }

    /**
     * Retrieves from the database a list of all notes with information suitable for display.
     */
    fun loadHeaders(filter: Filter? = null): Result<List<NoteHeaderItem>> {
        traceEvent(Trace.NoteDAO, "NoteDAO: loadHeaders(${filter?.name})")
        return queryHeaders(filter)
    }

    fun loadLabelFilters(): Result<List<Filter>> {
        traceEvent(Trace.NoteDAO, "NoteDAO: loading label filters")
        val out = mutableListOf(
            Filter("All Notes").apply {
                readOnly = true
                add("-labels:Trash")
            },
            Filter("Trash").apply {
                readOnly = true
                add("labels:Trash")
            }
        )

        val db = DBProxy.get()

        val rs = db.query("SELECT labels FROM notes").getOrElse { return Result.failure(it) }

        val labelSet = mutableSetOf<String>()
        while (rs.next()) {
            val labels = rs.getString("labels")?.split(",")?.toSet() ?: continue
            labelSet.addAll(labels)
        }
        labelSet.remove("")
        labelSet.remove("Trash")

        val filterList = labelSet.sorted().map { l ->
            Filter(l).apply {
                add("labels:$l")
                add("-labels:Trash")
            }
        }
        out.addAll(filterList)
        return out.toSuccess()
    }

    fun load(id: String): Result<NoteItem?> {
        traceEvent(Trace.NoteDAO, "NoteDAO: loading contents for $id")
        val out: NoteItem?
        with(DBProxy.get()) {
            val rs = query("SELECT * FROM notes WHERE id=?", id)
                .getOrElse { return it.toFailure() }

            if (!rs.next())
                return Result.success(null)

            out = NoteItem(
                rs.getString("title") ?: "",
                rs.getString("contents") ?: "",
                id,
                rs.getString("format") ?: "plain",
            ).apply {
                serverPath = rs.getString("server_path") ?: ""
            }
            rs.getString("labels")?.let { out.setLabels(it.split(", ").toSet()) }
        }
        return out.toSuccess()
    }

    fun saveTitle(id: String, title: String): Throwable? {
        id.ifEmpty { return null }

        traceEvent(Trace.NoteDAO, "NoteDAO: Saving title of $id")
        with(DBProxy.get()) {
            execute(
                "UPDATE notes SET title=? WHERE id=?", title, id
            )?.let { return it }
        }
        return null
    }

    fun saveContents(id: String, contents: String): Throwable? {
        id.ifEmpty { return null }

        traceEvent(Trace.NoteDAO, "NoteDAO: Saving contents of $id")
        with(DBProxy.get()) {
            execute(
                "UPDATE notes SET contents=? WHERE id=?", contents, id
            )?.let { return it }
        }
        return null
    }

    fun save(note: NoteItem): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: Saving $note")
        with(DBProxy.get()) {
            execute(
                "MERGE INTO notes(id,title,format,contents,labels,server_path) KEY(id) " +
                        "VALUES(?,?,?,?,?,?)",
                note.id, note.title, note.format, note.contents, note.getLabelString(),
                note.serverPath
            )?.let { return it }
        }
        return null
    }

    fun delete(note: NoteHeaderItem): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: deleting $note")
        val id = RandomID.fromString(note.id)
            ?: return DatabaseException("Bad note id '${note.id}in NoteDAO.delete()")
        return delete(id)
    }

    fun delete(id: RandomID): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: deleting note with id $id")
        with(DBProxy.get()) {
            execute("DELETE FROM notes WHERE id=?", id)?.let { return it }
        }
        return null
    }

    fun deleteFromPath(path: MServerPath): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: delete from path $path")
        with(DBProxy.get()) {
            execute("DELETE FROM notes WHERE server_path=?", path)?.let { return it }
        }
        return null
    }

    fun moveNote(oldPath: MServerPath, newPath: MServerPath): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: moving note from $oldPath to $newPath")
        with(DBProxy.get()) {
            execute(
                "UPDATE notes SET server_path=? WHERE server_path=?",
                newPath,
                oldPath
            )?.let { return it }
        }
        return null
    }

    fun rename(note: NoteHeaderItem, newName: String): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: renaming $note to $newName")
        with(DBProxy.get()) {
            execute("UPDATE notes SET title=? WHERE id=?", newName, note.id)?.let { return it }
        }
        return null
    }

    fun addLabel(note: NoteHeaderItem, label: String): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: adding label $label to $note")
        with(DBProxy.get()) {
            note.addLabel(label)
            execute("UPDATE notes SET labels=? WHERE id=?", note.getLabelString(), note.id)
                ?.let { return it }
        }
        return null
    }

    fun removeLabel(note: NoteHeaderItem, label: String): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: removing label $label from $note")
        with(DBProxy.get()) {
            note.removeLabel(label)
            execute("UPDATE notes SET labels=? WHERE id=?", note.getLabelString(), note.id)
                ?.let { return it }
        }
        return null
    }

    fun emptyTrash(): Throwable? {
        traceEvent(Trace.NoteDAO, "NoteDAO: emptying trash")
        with(DBProxy.get()) {
            execute("DELETE FROM notes WHERE labels ilike '%trash%'")?.let { return it }
        }
        return null
    }


    /** Generates and runs the necessary query to load note headers given a specified filter */
    private fun queryHeaders(filter: Filter?): Result<List<NoteHeaderItem>> {
        val out = mutableListOf<NoteHeaderItem>()
        val db = DBProxy.get()

        val rs = if (filter != null) {
            db.query(buildQueryString(filter, "notes")).getOrElse { return Result.failure(it) }
        } else {
            db.query("SELECT title,format,labels,id FROM notes ORDER BY title")
                .getOrElse { return Result.failure(it) }
        }

        while (rs.next()) {
            val item = NoteHeaderItem(
                rs.getString("title"),
                rs.getString("id"),
                rs.getString("format"),
            )
            rs.getString("labels")?.let { item.setLabels(it.split(",").toSet()) }
            out.add(item)
        }
        return out.toSuccess()
    }
}
