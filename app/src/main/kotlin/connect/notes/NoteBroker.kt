package connect.notes

import connect.Filter
import libmensago.MServerPath

class NoteBroker {
    private val dao = NoteDAO()

    init {
        dao.initNoteTable()?.let { throw it }
    }

    fun emptyTrash() = dao.emptyTrash()

    fun renameSelectedNote(item: NoteHeaderItem, name: String) = dao.rename(item, name)

    fun addLabel(item: NoteHeaderItem, label: String) = dao.addLabel(item, label)
    fun removeLabel(item: NoteHeaderItem, label: String) = dao.removeLabel(item, label)
    fun loadLabels(): Result<List<Filter?>> = dao.loadLabelFilters()

    fun loadHeaders(filter: Filter?): Result<List<NoteHeaderItem>> = dao.loadHeaders(filter)

    fun load(id: String): Result<NoteItem?> = dao.load(id)

    fun save(note: NoteItem): Throwable? = dao.save(note)

    fun saveSelectedTitle(id: String, title: String): Throwable? = dao.saveTitle(id, title)
    fun saveSelectedContents(id: String, contents: String): Throwable? =
        dao.saveContents(id, contents)

    fun delete(note: NoteHeaderItem): Throwable? = dao.delete(note)
    fun deleteFromPath(path: MServerPath): Throwable? = dao.deleteFromPath(path)

    fun moveNote(oldPath: MServerPath, newPath: MServerPath) = dao.moveNote(oldPath, newPath)

    fun addDemoDataToDB() {
        generateDemoNotes().forEach { dao.save(it)?.let { e -> throw e } }
    }
}
