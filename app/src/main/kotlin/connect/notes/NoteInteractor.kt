package connect.notes

import connect.Filter
import connect.FilterableItem
import libkeycard.RandomID
import libmensago.MServerPath
import utilities.Trace
import utilities.toObservable
import utilities.traceEvent
import java.util.*


/**
 * The Interactor portion of the MVCI framework for Connect's notetaking section
 */
class NoteInteractor(private val model: NoteModel) {
    private val broker = NoteBroker()

    /** Adds a new new note from existing content */
    fun addNote(note: NoteItem): Throwable? {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: add note ${note.title}")
        if (model.selectedNote != null)
            model.selectedNote!!.getLabels().forEach { note.addLabel(it) }
        broker.save(note)?.let { return it }
        insertItemSorted(note.toHeader())
        return null
    }

    /** Changes an item's server-side path */
    fun moveNote(oldPath: MServerPath, newPath: MServerPath) {
        traceEvent(
            Trace.NoteInteractor,
            "NoteInteractor: move note from $oldPath to $newPath"
        )
        if (model.selectedNote?.serverPath.toString() == oldPath.toString())
            model.selectedNote?.serverPath =
                newPath.clone().push(oldPath.basename()).getOrThrow().toString()
        broker.moveNote(oldPath, newPath)?.let { throw it }
    }

    /** Replaces the item at the specified path with the new one */
    fun replaceNote(oldItem: MServerPath, newItem: FilterableItem) {
        broker.deleteFromPath(oldItem)?.let { throw it }
        if (newItem is NoteItem)
            addNote(newItem)
    }

    /** Creates a new blank note */
    fun createNote(title: String) {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: create note $title")
        val item = NoteItem(title, "", RandomID.generate().toString())
        addNote(item)
    }

    fun selectNote(newHeaderItem: NoteHeaderItem?): Throwable? {
        if (newHeaderItem != null) {
            if (model.selectedNoteID == newHeaderItem.id) return null

            traceEvent(Trace.NoteInteractor, "NoteInteractor: select note $newHeaderItem")
            broker.load(newHeaderItem.id).getOrElse { e -> return e }?.let { loaded ->
                model.selectedNoteID = loaded.id
                model.selectedNoteTitle = newHeaderItem.title
                model.selectedNoteContents = loaded.contents
                model.selectedNoteProperty.set(loaded)
            }
        } else {
            traceEvent(Trace.NoteInteractor, "NoteInteractor: select note: none")
            model.selectedNoteID = ""
            model.selectedNoteTitle = ""
            model.selectedNoteContents = ""
            model.selectedNoteProperty.set(null)
        }
        return null
    }

    fun selectLabel(newFilter: Filter?) {
        if (newFilter == model.selectedFilterProperty.get()) return

        traceEvent(Trace.NoteInteractor, "NoteInteractor: select label $newFilter")
        broker.saveSelectedContents(model.selectedNoteID, model.selectedNoteContents)
        model.noteListProperty.set(broker.loadHeaders(newFilter).getOrThrow().toObservable())
        model.selectedFilterProperty.set(newFilter)
    }

    fun saveSelectedContents() {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: save selected contents")
        broker.saveSelectedContents(model.selectedNoteID, model.selectedNoteContents)
    }

    fun renameSelectedNote(name: String) {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: rename selected note to $name")
        val selected = model.noteList.find { it.id == model.selectedNoteID } ?: return
        selected.title = name
        broker.saveSelectedTitle(selected.id, selected.title)
        model.noteList.sortBy { it.title }
    }

    fun trashSelectedNote() {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: trash selected note")
        val oldSelected = model.noteList.find { it.id == model.selectedNoteID } ?: return
        model.noteList.remove(oldSelected)

        if (oldSelected.getLabels().contains("Trash"))
            broker.delete(oldSelected)
        else
            broker.addLabel(oldSelected, "Trash")
    }

    fun restoreSelectedNote() {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: restore selected note")
        val selected = model.noteList.find { it.id == model.selectedNoteID } ?: return
        model.noteList.remove(selected)
        broker.removeLabel(selected, "Trash")
    }

    fun emptyTrash() {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: empty trash")
        broker.emptyTrash()
        if (model.selectedFilterProperty.get()?.name == "Trash")
            model.noteList.clear()
    }

    fun addDemoData() {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: add demo data")
        broker.addDemoDataToDB()
        model.noteListProperty.set(broker.loadHeaders(null).getOrThrow().toObservable())
    }

    fun loadLabels() {
        traceEvent(Trace.NoteInteractor, "NoteInteractor: load labels")
        model.labelListProperty.set(broker.loadLabels().getOrThrow().toObservable())
    }

    private fun insertItemSorted(headerItem: NoteHeaderItem) {
        if (model.noteList.isEmpty()) {
            model.noteList.add(headerItem)
            return
        }

        var index = Collections.binarySearch(
            model.noteList, headerItem,
            Comparator.comparing(NoteHeaderItem::title)
        )
        if (index < 0)
            index = -index - 1

        model.noteList.add(index, headerItem)
    }
}
