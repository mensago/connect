package connect.notes

import connect.Filter
import connect.FilterableItem
import connect.common.getStringDialog
import connect.main.MainHandlers
import connect.main.MainModel
import libmensago.MServerPath
import utilities.Trace
import utilities.traceEvent

/**
 * The Controller portion of the MVCI framework for Connect's notetaking section
 */
class NoteController(private val mainModel: MainModel, private val mainHandlers: MainHandlers) {
    private val model = NoteModel()
    private val interactor = NoteInteractor(model)
    private val viewBuilder = NoteViewBuilder(model, mainModel, getHandlers())

    init {
        interactor.loadLabels()
        interactor.selectLabel(model.labelListProperty[0])
    }

    fun createNewNote() {
        getStringDialog("New Note", "Enter the name for the new note")
            ?.let { handleNewNote(it) }
    }

    /** Instructs the controller to register its view with the main model's mode info structure */
    fun registerView() {
        viewBuilder.build()
    }

    /** Load a new data item */
    fun loadNewItem(item: NoteItem): Throwable? = interactor.addNote(item)

    /** Changes an item's server-side path */
    fun moveItem(oldPath: MServerPath, newPath: MServerPath) = interactor.moveNote(oldPath, newPath)

    /** Replaces the item at the specified path with the new one */
    fun replaceItem(oldItem: MServerPath, newItem: FilterableItem) =
        interactor.replaceNote(oldItem, newItem)

    private fun handleNoteSelection(oldItem: NoteHeaderItem?, newItem: NoteHeaderItem?) {
        traceEvent(Trace.NoteController, "NoteController: handleNoteSelection($oldItem, $newItem)")
        if (oldItem == newItem)
            return

        interactor.selectNote(newItem)
        model.selectedNoteContentsProperty.subscribe { _, _ ->
            interactor.saveSelectedContents()
        }
    }

    private fun handleNewNote(name: String) {
        traceEvent(Trace.NoteController, "NoteController: handleNewNote($name)")
        interactor.createNote(name)
    }

    private fun handleLabelSelection(oldItem: Filter?, newItem: Filter?) {
        traceEvent(Trace.NoteController, "NoteController: handleLabelSelection($oldItem, $newItem)")
        interactor.selectLabel(newItem)

    }

    private fun handleRenameSelected(name: String) {
        traceEvent(Trace.NoteController, "NoteController: handleRenameSelected()")
        interactor.renameSelectedNote(name)
    }

    private fun handleTrashSelection() {
        traceEvent(Trace.NoteController, "NoteController: handleTrashSelection()")
        interactor.trashSelectedNote()
    }

    private fun handleRestoreSelection() {
        traceEvent(Trace.NoteController, "NoteController: handleRestoreSelection()")
        interactor.restoreSelectedNote()
    }

    private fun handleEmptyTrash() {
        traceEvent(Trace.NoteController, "NoteController: handleEmptyTrash()")
        interactor.emptyTrash()
    }

    private fun getHandlers(): NoteViewHandlers {
        return NoteViewHandlers(
            this::handleNoteSelection,
            this::handleLabelSelection,
            this::handleRenameSelected,
            this::handleTrashSelection,
            this::handleRestoreSelection,
            this::handleEmptyTrash,
            mainHandlers.newItem,
        )
    }
}
