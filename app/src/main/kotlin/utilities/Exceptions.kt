package utilities

class DBDataException(message: String = "") : Exception(message)
class DataMismatchException(message: String = "") : Exception(message)
class KeyNotFoundException(message: String = "") : Exception(message)
class LoginRequiredException : Exception()
class MessageTypeMismatchException(message: String = "") : Exception(message)
class MissingProfileException(message: String = "") : Exception(message)
class NotInitializedException : Exception()
class NotRegisteredException() : Exception()
class PermissionsException(message: String = "") : Exception(message)
class ReservedValueException : Exception()
class UnsupportedTypeException(message: String = "") : Exception(message)
class UnsupportedVersionException : Exception()
