package utilities

import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.Image
import keznacl.Base85
import keznacl.toFailure
import keznacl.toSuccess
import libmensago.MimeType
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO

/**
 * Ensures that the image data is valid and returns a decoded version ready for use in a ContactItem.
 */
fun getImageData(mime: MimeType, encodedData: String): Result<Pair<MimeType, ByteArray>> {
    val supported = ImageIO.getReaderMIMETypes().toSet()
    if (mime.value.lowercase() !in supported)
        return UnsupportedTypeException(mime.value).toFailure()

    val decoded = Base85.decode(encodedData).getOrElse { return it.toFailure() }
    runCatching { Image(ByteArrayInputStream(decoded)) }.exceptionOrNull()
        ?.let { return it.toFailure() }

    return Pair(mime, decoded).toSuccess()
}

/**
 * Loads a JavaFX Image from a binary file encoded in Base85. If given an empty string, it will
 * return null
 */
fun loadImage(s: String): Result<Image?> {
    if (s.isEmpty()) return Result.success(null)

    val decoded = Base85.decode(s).getOrElse { return it.toFailure() }
    return runCatching { Image(ByteArrayInputStream(decoded)) }
}

/**
 * Converts a JavaFX image to a PNG file encoded in Base85. If given a null Image, it will return
 * an empty string
 */
fun imageToBytes(image: Image?): Result<Pair<MimeType, ByteArray>?> {
    if (image == null) return Result.success(null)

    val bufferedImage = SwingFXUtils.fromFXImage(image, null)
    val outputStream = ByteArrayOutputStream()
    ImageIO.write(bufferedImage, "png", outputStream)
    return Pair(MimeType.fromString("image/png")!!, outputStream.toByteArray()).toSuccess()
}
