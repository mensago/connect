package utilities

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.collections.ObservableMap
import javafx.collections.ObservableSet
import javafx.scene.image.Image
import javafx.scene.image.WritableImage
import javafx.scene.paint.Color
import keznacl.CryptoString
import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.Timestamp
import libkeycard.UserID
import libmensago.Attachment
import libmensago.MimeTypes
import java.nio.file.Path
import java.nio.file.Paths
import java.sql.ResultSet
import java.time.LocalDateTime
import java.time.ZoneOffset


fun List<String>.toPath(): Path {
    return if (this.size == 1)
        Paths.get(this[0])
    else
        Paths.get(this[0], *this.subList(1, this.size).toTypedArray())
}

// Easily convert Kotlin containers to observable ones
fun <T> List<T>.toObservable(): ObservableList<T> = FXCollections.observableList(toMutableList())
fun <T> Set<T>.toObservable(): ObservableSet<T> = FXCollections.observableSet(toMutableSet())
fun <K, V> Map<K, V>.toObservable(): ObservableMap<K, V> =
    FXCollections.observableMap(toMutableMap())

// For making it easier to get common data types out of a ResultSet
fun ResultSet.getRandomID(name: String): RandomID? = RandomID.fromString(getString(name))
fun ResultSet.getUserID(name: String): UserID? = UserID.fromString(getString(name))
fun ResultSet.getDomain(name: String): Domain? = Domain.fromString(getString(name))
fun ResultSet.getCryptoString(name: String): CryptoString? =
    CryptoString.fromString(getString(name))

// Helpful Attachment conversions
fun Attachment.toRandomID(): RandomID? {
    if (mime != MimeTypes.randomID) return null
    return RandomID.fromString(data.decodeToString())
}

// Helpful Attachment conversions
fun Attachment.toCryptoString(): CryptoString? {
    if (mime != MimeTypes.cryptoString) return null
    return CryptoString.fromString(data.decodeToString())
}

// Common pattern with ResultSet
fun ResultSet.getStringOrNull(s: String): String? = this.getString(s)?.ifBlank { null }

// Because Image doesn't have a public copy constructor :/
fun Image.clone(): WritableImage {
    val width = width.toInt()
    val height = height.toInt()
    val out = WritableImage(width, height)

    for (y in 0 until height) {
        for (x in 0 until width) {
            val color: Color = pixelReader.getColor(x, y)
            out.pixelWriter.setColor(x, y, color)
        }
    }
    return out
}

fun Timestamp.toLocalDateTime(): LocalDateTime = LocalDateTime.ofInstant(this.value, ZoneOffset.UTC)

