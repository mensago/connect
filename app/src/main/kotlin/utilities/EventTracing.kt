package utilities

enum class Trace {
    All,
    ContactController,
    ContactDAO,
    ContactInteractor,
    MainController,
    MessageController,
    MessageDAO,
    MessageInteractor,
    NoteController,
    NoteDAO,
    NoteInteractor
}

/**
 * A singleton controlling MVCI event tracing.
 */
object EventTracing {
    var events = mutableSetOf<Trace>()
}

fun traceEvent(traceType: Trace, vararg args: Any?) {
    if (EventTracing.events.contains(traceType) || EventTracing.events.contains(Trace.All)) {
        args.forEach { print(it) }
        print("\n")
    }
}
