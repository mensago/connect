package utilities

import keznacl.BadValueException
import keznacl.EmptyDataException
import keznacl.toFailure
import keznacl.toSuccess
import libmensago.ResourceNotFoundException

/**
 * The AppSettings class handles storage and retrieval of various program preferences and user
 * settings. It is designed for local settings storage with the ability to designate settings which
 * are synced to the cloud, as well. Currently all settings are strings, but support for other
 * types may be added later.
 *
 * Settings themselves are stored in a dedicated database table instead of some configuration file.
 */
class AppSettings(var signature: String) {
    private var data = HashMap<String, Pair<String, Boolean>>()
    var modified = mutableSetOf<String>()
    var deleted = mutableSetOf<String>()

    companion object {
        /** Creates a new AppSettings instance populated with values in the database */
        fun newFromDB(): Result<AppSettings> {
            val out = AppSettings("")
            out.loadFromDB()?.let { return it.toFailure() }

            return out.toSuccess()
        }
    }

    /** Deletes the specified field */
    fun deleteField(name: String): Boolean {
        return if (data.remove(name) != null) {
            deleted.add(name)
            true
        } else false
    }

    /** Returns the specified field */
    fun getField(name: String): Pair<String, Boolean>? {
        return data[name]
    }

    /** Returns the value of the specified field or NULL on error */
    fun getValue(name: String): String? {
        return data[name]?.first
    }

    /** Returns true if the instance has the specified field */
    fun hasField(name: String): Boolean {
        return data[name] != null
    }

    /** Returns true if the instance has changes not in the database */
    fun isModified(): Boolean {
        return modified.isNotEmpty() || deleted.isNotEmpty()
    }

    /** Loads the instance state from the database, overwriting any changes */
    fun loadFromDB(): Throwable? {
        val db = DBProxy.get()
        data.clear()
        modified.clear()
        ensureTable()

        val rs = db.query("SELECT name,itemvalue,sync FROM appconfig").getOrElse { return it }

        while (rs.next()) {
            val name = rs.getString("name")
            if (name.isEmpty())
                return BadValueException("AppSettings::loadFromDB: empty field name in database")

            data[name] = Pair(rs.getString("itemvalue"), rs.getInt("sync") == 1)
        }
        signature = data["application_signature"]?.first ?: ""
        return null
    }

    /** Sets the value of a field, creating a new field if necessary */
    fun setField(name: String, value: String, sync: Boolean): Throwable? {
        if (name.isEmpty())
            return EmptyDataException()

        data[name] = Pair(value, sync)
        modified.add(name)
        return null
    }

    /**
     * Sets the value of a field only if it already exists. If the field doesn't exist, this call
     * does nothing and no error is returned.
     */
    fun setExistingField(name: String, value: String, sync: Boolean): Throwable? {
        if (name.isEmpty())
            return EmptyDataException()

        if (data[name] != null) {
            data[name] = Pair(value, sync)
            modified.add(name)
        }
        return null
    }

    /**
     * Sets the value of a field. This call will return an error if the field doesn't already exist.
     */
    fun setValue(name: String, value: String): Throwable? {
        if (name.isEmpty())
            return EmptyDataException()

        return if (data[name] != null) {
            data[name] = Pair(value, data[name]!!.second)
            modified.add(name)
            null
        } else {
            ResourceNotFoundException()
        }
    }

    private fun ensureTable(): Throwable? {
        return DBProxy.get().execute(
            "CREATE TABLE IF NOT EXISTS appconfig(name VARCHAR(128) NOT NULL UNIQUE, " +
                    "itemvalue VARCHAR(256) NOT  NULL, sync BOOL NOT NULL);"
        )
    }
}

