package utilities

import keznacl.Base85
import java.security.SecureRandom

// This is the default SDF/SFTM stylesheet. It's been minified because it's part of every document and it reduces
// visual clutter when browsing the raw data in the database. It also saves a bit of storage space, too.
const val defaultStyleSheet = """:root{color:#000;background-color:#fff;
    font-family:"Noto Sans", Arial, Helvetica, sans-serif;font-size:12pt}
    h1{font-size:18pt;margin-top:12pt;margin-bottom:6pt}
    h2{font-size:16pt;margin-top:10pt;margin-bottom:6pt}
    h3{font-size:14pt;margin-top:7pt;margin-bottom:6pt}
    h4{font-size:13pt;margin-top:6pt;margin-bottom:6pt;font-style:italic}
    h5{font-size:12pt;margin-top:6pt;margin-bottom:3pt}
    h6{font-size:12pt;margin-top:3pt;margin-bottom:3pt;font-style:italic}
    p{margin-top:10pt;margin-bottom:10pt;line-height:1.5em}
    a{color:#275a90;font-weight:700;text-decoration:underline 1px solid}
    q{font-style:italic}ol,ul{text-indent:1em}
    table{border-collapse:collapse;border-radius:1pt;border-style:solid;border-color:#bbb}
    thead{background-color:#5959a6;color:white;font-weight:700}
    tr:nth-child(even){background-color:#ddd}td{padding:10px}"""

const val defaultContent = "<html><head><style>$defaultStyleSheet</style></head>" +
        "<body contenteditable='true'/></body>" +
        "</html>"

/**
 * This function exists to convert plain text into the HTML content that the current implementation
 * of RichEditor requires. It's yet another hack, but we're stuck for now.
 */
fun convertPlainText(s: String): String {
    val convertedText = s.split("\n").joinToString("") {
        if (it.endsWith('\r'))
            "<p>${it.dropLast(1)}</p>"
        else
            "<p>$it</p>"
    }

    return "<html><head><style>$defaultStyleSheet</style></head>" +
            "<body contenteditable='true'/>$convertedText</body></html>"

}

/**
 *
 */
fun randomString(range: IntRange? = null): String {
    val actualRange = range ?: 1..24

    val rng = SecureRandom()
    val size = rng.nextInt(actualRange.first, actualRange.last + 1)
    return Base85.encode(
        ByteArray(size) { rng.nextInt(0, 256).toByte() }
    )
}

