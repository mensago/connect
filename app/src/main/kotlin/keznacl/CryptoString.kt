package keznacl

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.util.regex.Pattern

/**
 * A class which represents cryptographic hashes, keys, and other binary data as text with an
 * associated algorithm. The format consists of a prefix, a colon, and Base85-encoded binary data.
 *
 * The RFC 1924 variant of Base85 encoding was chosen because of its higher efficiency and source
 * code compatibility. The prefix consists of up to 24 characters, which may be capital ASCII
 * letters, numbers, or dashes.
 *
 * The official prefixes as of this writing are:
 *
 * - ED25519 / CURVE25519 -- Twisted Edwards curve 25519 as implemented by the NaCl library
 * - AES-128 / AES-256 / AES-384 / AES-512 -- AES GCM in its various sizes
 * - SALSA20 / XSALSA20 -- (X)Salsa20 with Poly1305
 * - SHA-256 / SHA-384 / SHA-512 -- The ubiquitous regular SHA hashing algorithms
 * - SHA3-256 / SHA3-384 / SHA3-512 -- The SHA3 hash variants
 * - BLAKE2B-256 / BLAKE2B-512 -- BLAKE2b, a runner-up to the Keccak algorithm that became SHA3
 * - BLAKE3-256 -- A super-fast variable-length hashing algorithm
 * - K12-256 -- KangarooTwelve, another variable-length runner-up to Keccak
 *
 * Regular usage of a CryptoString mostly involves creating an instance from other data. The
 * factory functions can take a CryptoString-formatted string or a string prefix and some raw bytes.
 * Once data has been put into the instance, getting it back out is just a matter of calling
 * [toString], [toByteArray], or [toRaw]. The last of these three methods only returns the raw data
 * stored in the object.
 */
@Serializable(with = CryptoStringAsStringSerializer::class)
open class CryptoString protected constructor(val prefix: String, val encodedData: String) {
    var value: String = ""
        private set
        get() {
            return "$prefix:$encodedData"
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CryptoString
        if (prefix != other.prefix) return false
        return encodedData == other.encodedData
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String {
        return value
    }

    /** Returns the instance's value as a ByteArray */
    fun toByteArray(): ByteArray {
        return value.encodeToByteArray()
    }

    /**
     * Returns the object's raw, unencoded data
     *
     * @exception IllegalArgumentException Returned if there was a Base85 decoding error
     */
    fun toRaw(): Result<ByteArray> {
        val out = Base85.decode(encodedData).getOrElse { return it.toFailure() }
        return out.toSuccess()
    }

    /** Calculates and returns the hash of the string value of the instance */
    fun hash(algorithm: CryptoType = getPreferredHashAlgorithm()): Result<Hash> {
        return hash(value.encodeToByteArray(), algorithm)
    }

    companion object {
        private val csPattern = Pattern.compile(
            "^([A-Z0-9-]{1,24}):([0-9A-Za-z!#\$%&()*+-;<=>?@^_`{|}~]+)\$"
        )!!

        /** Returns true if the supplied data matches the expected data format */
        fun checkFormat(value: String): Boolean {
            return csPattern.matcher(value).matches()
        }

        /** Creates a new instance from a string in CryptoString format or null if invalid.
         */
        fun fromString(value: String): CryptoString? {
            isValid(value).onFalse { return null }

            val parts = value.split(":")
            return if (parts.size != 2) null else CryptoString(parts[0], parts[1])
        }

        /** Creates a new CryptoString from a string containing the algorithm name and raw data */
        fun fromBytes(type: CryptoType, buffer: ByteArray): CryptoString? {
            return if (buffer.isEmpty())
                null
            else
                CryptoString(type.toString(), Base85.encode(buffer))
        }

        /** Returns true if the string passed conforms to CryptoString format */
        fun isValid(s: String): Boolean {
            return csPattern.matcher(s).matches()
        }
    }
}

/** @suppress */
object CryptoStringAsStringSerializer : KSerializer<CryptoString> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("CryptoString", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: CryptoString) {
        encoder.encodeString(value.toString())
    }

    override fun deserialize(decoder: Decoder): CryptoString {
        val out = CryptoString.fromString(decoder.decodeString())
            ?: throw SerializationException("Invalid value for CryptoString")
        return out
    }
}
