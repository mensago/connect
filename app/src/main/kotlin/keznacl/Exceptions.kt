package keznacl

/** @suppress */
class AlgorithmMismatchException(message: String = "") : Exception(message)

/** @suppress */
class BadValueException(message: String = "") : Exception(message)

/** @suppress */
class DecryptionFailureException(message: String = "") : Exception(message)

/** @suppress */
class EmptyDataException(message: String = "") : Exception(message)

/** @suppress */
class EncryptionFailureException(message: String = "") : Exception(message)

/** @suppress */
class KeyErrorException(message: String = "") : Exception(message)

/** @suppress */
class MissingDataException(message: String = "") : Exception(message)

/** @suppress */
class ProgramException(message: String = "") : Exception(message)

/** @suppress */
class SigningFailureException(message: String = "") : Exception(message)

/** @suppress */
class UnsupportedAlgorithmException(message: String = "") : Exception(message)
