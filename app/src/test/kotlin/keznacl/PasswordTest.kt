package keznacl

import kotlin.test.Test
import kotlin.test.assertEquals

class PasswordTest {

    @Test
    fun basicTest() {
        assert(getSupportedPasswordAlgorithms().contains("ARGON2ID"))
        assertEquals("ARGON2ID", getPreferredPasswordAlgorithm())
    }

    @Test
    fun getHasherForHashTest() {
        val argonHasher = getHasherForHash(
            "\$argon2id\$v=19\$m=1048576,t=10,p=4\$0QufQhLAVhgDqbr//8/hTA" +
                    "\$ocFjWRDrqEhLcedJG95CAt2CKQgkyDak7VMpfjwvveY"
        ).getOrThrow()
        assert(argonHasher is Argon2idPassword)
        val shaHasher = getHasherForHash(
            "\$sha3-256\$t=2\$A80fwv/3mo4cX4DMViiK2yT8r8Hbeac1xCnMqAtjA+0=" +
                    "\$JuOcXGZk2B/vK1akaibAHcuHchRfhtI0e9TaJ/4CzGw="
        ).getOrThrow()
        assert(shaHasher is SHAPassword)

        assert(getHasherForHash("\$badalgo-256\$t=2\$abcdefg").isFailure)
    }
}
