package keznacl

import org.junit.jupiter.api.Test

class Examples {
    @Test
    fun secretKeyEncryptionExample() {
        // This example just shows how simple it is to encrypt arbitrary data with the library.

        val mySecretKey = SecretKey.generate().getOrThrow()
        val keyHash = mySecretKey.getHash().getOrThrow()
        val msg = "One if by land, two if by sea"

        val encryptedData = mySecretKey.encrypt(msg).getOrThrow()
        println("My secret message:\n$msg\n")
        println("My encrypted secret message:\n$encryptedData\n")
        println("The hash of my secret key:\n$keyHash\n")
    }

    @Test
    fun asymmetricEncryptionExample() {
        // Here we do the same kind of thing, but with an asymmetric encryption keypair. Note that
        // this kind of encryption is slower, so it shouldn't be used for large chunks of data.

        // In this case, we're using the keypair as a Key Encryption Mechanism, where a SecretKey
        // is used to encrypt the data itself because it's faster, and the keypair is used to
        // encrypt the SecretKey itself.

        val myKeyPair = EncryptionPair.generate().getOrThrow()
        val messageKey = SecretKey.generate().getOrThrow()
        val msg = "When the whole world is running towards a cliff, he who is running in the " +
                "opposite direction appears to have lost his mind. -- C.S. Lewis"

        val encryptedData = messageKey.encrypt(msg).getOrThrow()

        // EncryptionPair's wrap() and unwrap() methods were designed explicitly for encrypting
        // and decrypting SecretKey instances.
        val encryptedKey = myKeyPair.wrap(messageKey).getOrThrow()
        println("My secret message:\n$msg\n")
        println("My encrypted secret message:\n$encryptedData\n")
        println("My message's encrypted encryption key: \n$encryptedKey")

        // The advantage to the library's API design is that decrypting the message is pretty
        // elegant using functional programming style.
        val myDecryptedMessage = myKeyPair.unwrap(encryptedKey).getOrThrow()
            .decryptString(encryptedData).getOrThrow()
        println("My decrypted message: \n$myDecryptedMessage")
    }

    @Test
    fun hashingExamples() {
        // If you want to hash some data, it's literally just a matter of calling one function. The
        // result is a Hash object that can easily be used to check other data. We can also
        // demonstrate some of the library's utility functions this way, as well.
        //
        // Although it's been said before, PLEASE don't use these functions to hash passwords. We
        // have *much* better tools than these for that purpose.
        //
        val testData = "This is some data".encodeToByteArray()
        val b2Hash = hash(testData).getOrThrow()

        val differentData = "Some other data".encodeToByteArray()
        b2Hash.check(differentData).getOrThrow().onFalse { println("The hashes don't match") }

        // If you want to specify the algorithm for a hash, you can do that. There are also calls
        // like blake2Hash() and sha256Hash() if you'd just rather call them that way.
        val shaHash = hash(testData, CryptoType.SHA_256).getOrThrow()
        println("My BLAKE-2B hash is: $b2Hash")
        println("My SHA256 hash is: $shaHash")
    }

    @Test
    fun passwordHashingExample() {
        // If you want to hash passwords, in the opinion of the authors is that the best algorithm
        // out there for this purpose is Argon2id, and keznacl provides classes for this.

        val password = "MyS3cretPassw*rd"

        // Create the instance of our password hasher
        val hasher = Argon2idPassword()

        // Depending on your needs, you might at this point adjust the hashing parameters, like
        // memory cost or time cost. The defaults provide a pretty good tradeoff of security vs
        // resource usage. While the defaults will cover many circumstances, consider testing for
        // your individual situation.

        val passHash = hasher.updateHash(password).getOrThrow()
        println("The hashed version of '$password' is: $passHash")

        // Checking a password against an existing hash is merely a matter of passing the hash to
        // a hasher instance and passing the cleartext password attempt to verify(). Just as it's
        // computationally expensive to create the hash, verifying it is equally expensive. Keep
        // this in mind so you don't accidentally DoS or DDosS yourself.
        val hashChecker = Argon2idPassword()
        hashChecker.setFromHash(passHash)?.let { throw it }
        hashChecker.verify(password).onTrue { println("The passwords match! YAY") }
    }
}
