package utilities

import javafx.scene.image.Image
import keznacl.Base85
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@ExtendWith(ApplicationExtension::class)
class ImageUtilsTest {

    @Test
    fun getImageDataTest() {
        val blankPhoto = Image("blankprofile.png")
        val info = imageToBytes(blankPhoto).getOrThrow()
        val encoded = Base85.encode(info!!.second)

        assertEquals("image/png", info.first.value)
        assertNotNull(info.second)

        val rawInfo = getImageData(info.first, encoded).getOrThrow()
        assertEquals(info.second.size, rawInfo.second.size)
        assert(info.second.contentEquals(rawInfo.second))
    }
}
