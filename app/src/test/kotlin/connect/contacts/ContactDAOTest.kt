package connect.contacts

import connect.Filter
import connect.initProfile
import connect.setupProfileBase
import libkeycard.Domain
import libkeycard.WAddress
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import utilities.DBProxy
import kotlin.test.assertNull

class ContactDAOTest {

    private fun setupDAOTest(testName: String, addDemoData: Boolean = true): ContactDAO {
        val testPath = setupProfileBase(testName)
        initProfile(testPath)

        val dao = ContactDAO()
        dao.initContactTables()
        if (addDemoData)
            generateDemoContacts(false)
                .forEach { dao.saveConItem(it)?.let { e -> throw e } }
        return dao
    }

    @Test
    fun basicTest() {
        val dao = setupDAOTest("contactdao_basic", false)
        assertEquals(0, dao.loadHeaders().getOrThrow().size)

        val id = "fcd6fbdd-a632-49e8-9b2c-f5be9986c115"
        val testItem = ContactItem("Corbin Simons", id).apply {
            givenName = "Corbin"
            familyName = "Simons"
            contents = "Some notes"
        }
        dao.saveConItem(testItem)?.let { throw it }

        with(DBProxy.get()) {
            val rs = query("SELECT * FROM contacts WHERE id=?", id).getOrThrow()
            assert(rs.next())
            assertEquals("Corbin Simons", rs.getString("formatted_name"))
            assertEquals(id, rs.getString("id"))
        }

        val headers = dao.loadHeaders().getOrThrow()
        assertEquals(1, headers.size)
        assertEquals("Corbin Simons", headers[0].address)
        assertEquals(id, headers[0].id)

        val loaded = dao.loadConItem(id).getOrThrow()!!
        assertEquals("Corbin Simons", loaded.formattedName)
        assertEquals("Corbin", loaded.givenName)
        assertEquals("Simons", loaded.familyName)
        assertEquals(false, loaded.isManaged)
        assertEquals("individual", loaded.entityType)
        assertEquals(id, loaded.id)
        assertEquals("plain", loaded.format)
        assertEquals("Some notes", loaded.contents)
    }

    @Test
    fun conItemForWAddressTest() {
        val dao = setupDAOTest("contactdao_contact4waddr", true)

        assertNull(
            dao.conItemForWAddress(
                WAddress.fromString("11111111-1111-1111-1111-111111111111/example.com")!!
            ).getOrThrow()
        )

        val contact = dao.conItemForWAddress(
            WAddress.fromString("714d66b1-8c0f-4763-a772-ef9acd1f1dee/example.com")!!
        ).getOrThrow()

        assertEquals("rbrannan", contact?.uid)
    }

    @Test
    fun loadConItemLabelsTest() {
        val dao = setupDAOTest("contactdao_loadlabels")
        val lf = dao.loadLabelFilters().getOrThrow()
        assertEquals(11, lf.size)
    }

    @Test
    fun loadConItemTest() {
        val dao = setupDAOTest("contactdao_load")
        val headers = dao.loadHeaders().getOrThrow()
        assertEquals(22, headers.size)

        val loaded = dao.loadConItem("b80de689-febf-4c43-9ff6-3f7d3251638a").getOrThrow()!!
        assertEquals("Dr. Richard Brannan, MD", loaded.formattedName)
        assertEquals("Dr.", loaded.prefix)
        assertEquals("Richard", loaded.givenName)
        assertEquals("M", loaded.middleName)
        assertEquals("Brannan", loaded.familyName)
        assertEquals("MD", loaded.suffix)
        assert(loaded.hasLabel("sample"))
        assertEquals("Male", loaded.gender)
        assertEquals("rbrannan", loaded.uid)
        assertEquals("example.com", loaded.domain)
        assertEquals("714d66b1-8c0f-4763-a772-ef9acd1f1dee", loaded.wid)
        assertEquals(
            "Underwater basket weaving and medieval plumbing are interests of mine",
            loaded.bio
        )
        assertEquals("14-07", loaded.anniversary)
        assertEquals("1975-04-15", loaded.birthday)
        assertEquals("Mifflin Medical Center", loaded.organization)
        assertEquals("Intensive Care", loaded.orgUnits)
        assertEquals("Hospitalist", loaded.title)
        assertEquals("eng,spa", loaded.languages)
        assertEquals("I like cheese", loaded.contents)
        assertEquals("plain", loaded.format)

        assertEquals(2, loaded.addressListProperty.size)
        val addr1 = loaded.addressListProperty[0]!!
        assertEquals("Home", addr1.label)
        assertEquals("California", addr1.region)
        assertEquals("United States", addr1.country)

        assertEquals(2, loaded.keyListProperty.size)
        val key1 = loaded.keyListProperty[1]!!
        assertEquals(false, key1.isPublic)
        assertEquals("crencryption", key1.purpose.toString())
        assertEquals("CURVE25519:2bLf2vMA?GA2?L~tv<PA9XOw6e}V~ObNi7C&qek>", key1.key)
        assertEquals("2024-06-01T13:25Z", key1.timestamp)

        assertEquals(2, loaded.phoneListProperty.size)
        val phone1 = loaded.phoneListProperty[0]!!
        assertEquals("Mobile", phone1.label)
        assertEquals("555-555-1234", phone1.value)
        assertEquals(true, phone1.isPreferred)
    }

    @Test
    fun emptyTrashTest() {
        val dao = setupDAOTest("contactdao_emptytrash")
        val trashFilter = Filter("Trash").apply {
            sortBy("formatted_name")
            add("labels:Trash")
        }
        assertEquals(1, dao.loadHeaders(trashFilter).getOrThrow().size)
        dao.emptyTrash()
        assertEquals(0, dao.loadHeaders(trashFilter).getOrThrow().size)
    }

    @Test
    fun blockListTest() {
        val dao = setupDAOTest("contactdao_blocklist")

        val con1 = WAddress.fromString("2e0c3db5-fcc8-40fb-bf47-bcd3a8d82c8e/example.com")!!
        val con2 = WAddress.fromString("e33da27a-1084-49bd-a5b0-af5e9c76d011/contoso.com")!!
        dao.blockContact(con1)?.let { throw it }
        dao.blockContact(con2)?.let { throw it }

        val dom1 = Domain.fromString("blocked.com")!!
        val dom2 = Domain.fromString("blocked2.com")!!
        dao.blockDomain(dom1)?.let { throw it }
        dao.blockDomain(dom2)?.let { throw it }

        val blockList = dao.loadBlockList().getOrThrow()
        assertEquals(blockList.size, 4)
        assertEquals(dom1.toString(), blockList[0].domain.toString())
        assertEquals(dom2.toString(), blockList[1].domain.toString())
        assertEquals(con2.toString(), blockList[2].toString())
        assertEquals(con1.toString(), blockList[3].toString())
    }
}
