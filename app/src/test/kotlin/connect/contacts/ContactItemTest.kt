package connect.contacts

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ContactItemTest {

    @Test
    fun toDisplayStringTest() {

        val c = ContactItem()
        assertEquals("Unidentified Contact", c.toDisplayString())

        c.formattedName = "Corbin Simons"
        assertEquals("Corbin Simons", c.toDisplayString())

        c.domain = "example.com"
        c.wid = "c8a6cc67-c1c7-4b23-9563-3cbf42a144d0"
        assertEquals(
            "Corbin Simons <c8a6cc67-c1c7-4b23-9563-3cbf42a144d0/example.com>",
            c.toDisplayString()
        )

        c.formattedName = ""
        assertEquals(
            "c8a6cc67-c1c7-4b23-9563-3cbf42a144d0/example.com",
            c.toDisplayString()
        )

        c.uid = "csimons"
        assertEquals("csimons/example.com", c.toDisplayString())

        c.formattedName = "Corbin Simons"
        assertEquals("Corbin Simons <csimons/example.com>", c.toDisplayString())
    }
}