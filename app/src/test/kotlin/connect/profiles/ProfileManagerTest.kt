package connect.profiles

import connect.setupProfileBase
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import utilities.DBProxy
import java.nio.file.Paths

class ProfileManagerTest {

    @Test
    fun activateTest() {
        val testPath = setupProfileBase("profman_activate")
        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            activateProfile("primary")?.let { throw it }
            scanProfiles()?.let { throw it }
            createProfile("secondary")?.let { throw it }

            deactivateProfile("primary")
            assertNull(model.activeProfile)
            assertFalse(DBProxy.get().isConnected())

            activateProfile("secondary")?.let { throw it }
            assertEquals("secondary", model.activeProfile?.name)
        }
    }

    @Test
    fun createDeleteTest() {
        val testPath = setupProfileBase("profman_createdelete")
        ProfileManager.setLocation(Paths.get(testPath))?.let { throw it }
        ProfileManager.ensureDefaultProfile()
        ProfileManager.activateProfile("primary")
        val model = ProfileManager.model

        ProfileManager.scanProfiles()?.let { throw it }
        assertEquals(1, model.profileNames.size)
        assert(model.profileNames.contains("primary"))
        val dbPath = Paths.get(
            ProfileManager.pathForProfile("primary"),
            "storage.mv.db"
        )
        assert(dbPath.toFile().exists())

        assertNotNull(ProfileManager.createProfile(""))
        ProfileManager.createProfile("secondary")?.let { throw it }

        ProfileManager.scanProfiles()?.let { throw it }
        assertEquals(2, model.profileNames.size)
        assert(model.profileNames.contains("secondary"))

        assertNotNull(ProfileManager.deleteProfile(""))
        assertNull(ProfileManager.deleteProfile("secondary"))
    }

    @Test
    fun ensureDefaultTest() {
        val testPath = setupProfileBase("profman_ensuredefault")
        ProfileManager.setLocation(Paths.get(testPath))?.let { throw it }
        val model = ProfileManager.model

        ProfileManager.scanProfiles()?.let { throw it }
        assertEquals(0, model.profileNames.size)

        ProfileManager.ensureDefaultProfile()
        ProfileManager.scanProfiles()?.let { throw it }
        assertEquals(1, model.profileNames.size)
        assert(model.profileNames.contains("primary"))
    }

    @Test
    fun renameProfileTest() {
        val testPath = setupProfileBase("profman_rename")

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile()
            activateProfile("primary")

            assert(renameProfile("primary", "foo") is ResourceInUseException)
            createProfile("secondary")?.let { throw it }
            renameProfile("secondary", "foo")?.let { throw it }
            assert(Paths.get(testPath, "foo").toFile().exists())
            assert(model.profileNames.contains("foo"))
        }
    }

    @Test
    fun scanProfileTest() {
        val testPath = setupProfileBase("profman_scanprofiles")
        ProfileManager.setLocation(Paths.get(testPath))?.let { throw it }
        val model = ProfileManager.model

        ProfileManager.scanProfiles()?.let { throw it }
        assertEquals(0, model.profileNames.size)

        Paths.get(testPath, "primary").toFile().mkdirs()
        ProfileManager.scanProfiles()?.let { throw it }
        assertEquals(1, model.profileNames.size)
    }

    @Test
    fun setDefaultProfileTest() {
        val testPath = setupProfileBase("profman_setdefault")

        with(ProfileManager) {
            setLocation(Paths.get(testPath))?.let { throw it }
            ensureDefaultProfile().getOrThrow()
            assertEquals("primary", model.defaultProfile)
            createProfile("secondary")?.let { throw it }
            val defaultPath = Paths.get(testPath, "primary", "default.txt")
            assert(defaultPath.toFile().exists())

            assertEquals("primary", model.defaultProfile)

            setDefaultProfile("secondary")?.let { throw it }
            assert(Paths.get(testPath, "secondary", "default.txt").toFile().exists())
        }
    }
}
