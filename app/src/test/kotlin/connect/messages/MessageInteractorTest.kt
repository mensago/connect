package connect.messages

import connect.initProfile
import connect.setupProfileBase
import libmensago.MServerPath
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

class MessageInteractorTest {

    @Test
    fun moveMessageTest() {
        val testPath = setupProfileBase("msgint_move")
        initProfile(testPath)
        val model = MessageModel()
        val msgint = MessageInteractor(model).apply { selectFilter(null) }
        msgint.addDemoData()
        assert(model.msgListProperty.size > 0)
        msgint.selectMessage(model.msgList[0])

        val dao = MessageDAO()
        val item = dao.load(model.msgList[0].id).getOrThrow()!!
        val oldPath = MServerPath(item.serverPath)
        val newPath = MServerPath("/ 042152cc-0fba-4ed7-a2d7-0ba58127e62a")
        msgint.moveMessage(oldPath, newPath)
        val newMessagePath = newPath.clone().push(oldPath.basename()).getOrThrow()
        assertEquals(newMessagePath.toString(), model.selectedMsg?.serverPath.toString())
    }

    @Test
    fun createMessageTest() {
        // This also exercises the addMessage() function

        val testPath = setupProfileBase("msgint_create")
        initProfile(testPath)
        val model = MessageModel()
        val msgint = MessageInteractor(model).apply { selectFilter(null) }
        msgint.addDemoData()
        val demoSize = model.msgListProperty.size
        assert(demoSize > 0)

        msgint.createDraft()
        assertEquals(demoSize + 1, model.msgListProperty.size)
    }

    @Test
    fun selectMessageTest() {
        val testPath = setupProfileBase("msgint_selectnote")
        initProfile(testPath)
        val model = MessageModel()
        val msgint = MessageInteractor(model).apply {
            selectFilter(null)
            addDemoData()
        }
        assert(model.msgListProperty.isNotEmpty())

        val first = model.msgListProperty.first()
        msgint.selectMessage(first)?.let { throw it }
        assertEquals(first.id, model.selectedMsgID)
        assert(model.selectedMsgContents.isNotEmpty())
        assertNotNull(model.selectedMsgProperty.get())
        assertEquals(first.subject, model.selectedMsgProperty.get()!!.subject)
    }

    @Test
    fun loadFiltersTest() {
        val testPath = setupProfileBase("msgint_loadlabels")
        initProfile(testPath)
        val model = MessageModel()
        val msgint = MessageInteractor(model).apply { selectFilter(null) }

        // At this point, we should have no data and no labels
        assert(model.msgListProperty.isEmpty())
        msgint.loadFilters()
        assertEquals(5, model.filterListProperty.size)
        assertEquals("Inbox", model.filterListProperty[0]!!.name)
        assertEquals("Sent", model.filterListProperty[1]!!.name)
        assertEquals("Drafts", model.filterListProperty[2]!!.name)
        assertEquals("All Mail", model.filterListProperty[3]!!.name)
        assertEquals("Trash", model.filterListProperty[4]!!.name)

        msgint.addDemoData()
        assert(model.msgListProperty.isNotEmpty())
        msgint.loadFilters()
        assertEquals(8, model.filterListProperty.size)
        assertEquals("bar", model.filterListProperty[5]!!.name)
        assertEquals("baz", model.filterListProperty[6]!!.name)
        assertEquals("foo", model.filterListProperty[7]!!.name)
    }

    @Test
    fun selectFilterTest() {
        val testPath = setupProfileBase("msgint_selectfilter")
        initProfile(testPath)
        val model = MessageModel()
        val msgint = MessageInteractor(model).apply {
            addDemoData()
            loadFilters()
            selectFilter(null)
        }

        val demoData = generateDemoMessages()
        assertEquals(demoData.size - 1, model.msgListProperty.size)
        listOf("bar", "baz", "foo").forEach { label ->
            val expectedCount = demoData.filter {
                it.getLabelString().contains(label) && !it.getLabels().contains("Trash")
            }.size
            val labelFilter = model.filterListProperty.find { it.name == label }!!
            msgint.selectFilter(labelFilter)
            val actualCount = model.msgList.size
            if (actualCount != expectedCount) fail(
                "Label count mismatch for label $label. Expected $expectedCount, got $actualCount"
            )
        }
    }

    @Test
    fun emptyTrashTest() {
        val testPath = setupProfileBase("msgint_emptytrash")
        initProfile(testPath)
        val model = MessageModel()
        val msgint = MessageInteractor(model).apply {
            addDemoData()
            loadFilters()
            // Select the Trash label
            selectFilter(model.filterListProperty[4]!!)
        }
        assert(model.msgListProperty.isNotEmpty())
        msgint.emptyTrash()
        assert(model.msgListProperty.isEmpty())
    }

    @Test
    fun trashMessageTest() {
        val testPath = setupProfileBase("msgint_trashnote")
        initProfile(testPath)
        val model = MessageModel()
        val msgint = MessageInteractor(model).apply {
            addDemoData()
            emptyTrash()
            loadFilters()
            // Select the Trash label
            selectFilter(model.filterListProperty[4]!!)
        }
        assert(model.msgListProperty.isEmpty())

        // Select All Items
        msgint.selectFilter(model.filterListProperty[3]!!)

        val count = model.msgList.size
        msgint.selectMessage(model.msgList[1]!!)
        msgint.trashSelectedMessages()
        assertEquals(count - 1, model.msgList.size)
    }
}
