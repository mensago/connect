package connect.messages

import connect.Filter
import connect.initProfile
import connect.setupProfileBase
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import utilities.DBProxy
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset

class MessageDAOTest {

    private fun setupDAOTest(testName: String, addDemoData: Boolean = true): MessageDAO {
        val testPath = setupProfileBase(testName)
        initProfile(testPath)

        val dao = MessageDAO()
        dao.initMessageTable()?.let { throw it }
        if (addDemoData)
            generateDemoMessages().forEach { dao.save(it)?.let { e -> throw e } }
        return dao
    }

    @Test
    fun basicTest() {
        val dao = setupDAOTest("msgdao_basic", false)
        assertEquals(0, dao.loadHeaders().getOrThrow().size)

        val id = "fcd6fbdd-a632-49e8-9b2c-f5be9986c115"
        val testItem = MessageItem("EmailSubject", "EmailContents", id).apply {
            date = LocalDateTime.ofInstant(Instant.parse("2024-06-10T12:00:00Z"), ZoneOffset.UTC)
            fromName = "Corbin Simons"
            fromAddr = "7faaac89-921b-41cc-af60-ffd4204b015f/example.com"
            toAddr = "4dbeaef5-5a64-43b4-bf22-8996b30b790a/example.com"
            threadID = "3bf6c06c-713a-4243-9fa6-b57b93c40ef4"
        }
        dao.save(testItem)?.let { throw it }

        with(DBProxy.get()) {
            val rs = query("SELECT * FROM messages WHERE id=?", id).getOrThrow()
            assert(rs.next())
            assertEquals("EmailSubject", rs.getString("subject"))
            assertEquals("EmailContents", rs.getString("contents"))
            assertEquals(id, rs.getString("id"))
            assertEquals("plain", rs.getString("format"))
        }

        val headers = dao.loadHeaders().getOrThrow()
        assertEquals(1, headers.size)
        assertEquals("EmailSubject", headers[0].subject)
        assertEquals(id, headers[0].id)
        assertEquals("plain", headers[0].format)

        val loaded = dao.load(id).getOrThrow()!!
        assertEquals("EmailSubject", loaded.subject)
        assertEquals("EmailContents", loaded.contents)
        assertEquals(id, loaded.id)
        assertEquals("plain", loaded.format)
    }

    @Test
    fun filterTest() {
        val dao = setupDAOTest("msgdao_filter")

        val filterB = Filter("All Contents").apply {
            sortBy("date")
            add("Lorem")
        }
        var headers = dao.loadHeaders(filterB).getOrThrow()
        assertEquals(5, headers.size)

        val filterTitle = Filter("Email C").apply {
            sortBy("date")
            add("subject:\"Email C\"")
        }
        headers = dao.loadHeaders(filterTitle).getOrThrow()
        assertEquals(1, headers.size)

        val filterLabels1 = Filter("baz").apply {
            sortBy("date")
            add("labels:baz")
        }
        headers = dao.loadHeaders(filterLabels1).getOrThrow()
        assertEquals(1, headers.size)
        assertEquals("Email E", headers[0].subject)

        val filterLabels2 = Filter("bar").apply {
            sortBy("date")
            add("labels:bar")
        }
        headers = dao.loadHeaders(filterLabels2).getOrThrow()
        assertEquals(2, headers.size)
        assertEquals("Email D", headers[0].subject)
        assertEquals("Email E", headers[1].subject)
    }

    @Test
    fun loadLabelsTest() {
        val dao = setupDAOTest("msgdao_loadlabels")

        val lf = dao.loadFilters().getOrThrow()
        assertEquals(8, lf.size)
    }

    @Test
    fun emptyTrashTest() {
        val dao = setupDAOTest("msgdao_emptytrash")

        val trashFilter = Filter("Trash").sortBy("date").apply { add("labels:Trash") }
        assertEquals(2, dao.loadHeaders(trashFilter).getOrThrow().size)
        dao.emptyTrash()
        assertEquals(0, dao.loadHeaders(trashFilter).getOrThrow().size)
    }

    @Test
    fun labelChangeTest() {
        val dao = setupDAOTest("msgdao_labelchange")
        val headers = dao.loadHeaders(
            Filter.fromStrings("Baz", "labels:baz")!!.sortBy("date")
        ).getOrThrow()
        assertEquals("Email E", headers[0].subject)
        assert(headers[0].getLabels().contains("baz"))
        assertFalse(headers[0].getLabels().contains("spamEggs"))
    }
}
