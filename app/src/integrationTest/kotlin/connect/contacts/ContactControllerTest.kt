package connect.contacts

import connect.main.MainModel
import javafx.scene.image.Image
import libmensago.MimeType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_BOTH_KEYCARDS
import testsupport.gUserProfileData

@ExtendWith(ApplicationExtension::class)
class ContactControllerTest {

    @Test
    fun loadNewItemTest() {
        ITestEnvironment("concontroller_loadnewitem").provision(SETUP_TEST_BOTH_KEYCARDS)
        val conItem = gUserProfileData.let {
            ContactItem(it.formattedName, it.wid.toString()).apply {
                givenName = it.givenName
                familyName = it.familyName
                uid = it.uid.toString()
                wid = it.wid.toString()
                domain = it.domain.toString()
                photoType = MimeType.fromString("image/jpeg")!!
                photo = Image("nope.jpg")
            }
        }

        val mainModel = MainModel()
        val conController = ContactController(mainModel)
        conController.loadNewItem(conItem)

        val condao = ContactDAO()
        val userItem = condao.loadHeaders().getOrThrow().first { it.name == "Corbin Simons" }

        val loaded = condao.loadConItem(userItem.id).getOrThrow()!!
        with(loaded) {
            assertEquals(conItem.givenName, givenName)
            assertEquals(conItem.familyName, familyName)
            assertEquals(conItem.uid, uid)
            assertEquals(conItem.wid, wid)
            assertEquals(conItem.domain, domain)
            assertEquals("image/png", photoType.toString())
            assertNotNull(photo)
        }
    }
}