package connect.contacts.newconreq

import connect.ContactName
import connect.UserInfoDAO
import connect.sync.OutgoingHandler
import javafx.scene.image.Image
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_BOTH_KEYCARDS
import testsupport.TestProcessor
import testsupport.gAdminProfileData

@ExtendWith(ApplicationExtension::class)
class NewCRInteractorTest {

    @Test
    fun queueCRTest() {
        val env = ITestEnvironment("newcrint_queueCR").provision(SETUP_TEST_BOTH_KEYCARDS)

        val model = NewCRModel().apply {
            recipient = "admin/example.com"
            msg = "This is a contact request message for the test 'newcrint_queueCR'"
        }
        val uidao = UserInfoDAO()
        with(gAdminProfileData) {
            uidao.saveName(ContactName(formattedName, givenName, familyName))?.let { throw it }
            uidao.saveIdentity(uid.toString(), wid.toString(), domain.toString())?.let { throw it }
            uidao.savePhoto(Image("nope.jpg"))?.let { throw it }
        }
        val interactor = NewCRInteractor(model)

        assertNull(OutgoingHandler.getOutgoingTask())
        interactor.queueCR().getOrThrow()

        val tproc = TestProcessor()
        env.getUpdates(gAdminProfileData, tproc)
        assertEquals(1, tproc.msgItems.size)
    }
}
