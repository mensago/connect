package connect.sync

import connect.Client
import connect.ContactKeySet
import connect.contacts.ContactDAO
import connect.profiles.ProfileManager
import connect.sync.OutgoingHandler.sendUnsafe
import libkeycard.MAddress
import libmensago.ContentFormat
import libmensago.Message
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_BOTH_KEYCARDS
import testsupport.gUserProfileData

@ExtendWith(ApplicationExtension::class)
class OutgoingTest {

    @Test
    fun sendUnsafeTest() {
        ITestEnvironment("outgoing_sendunsafe").provision(SETUP_TEST_BOTH_KEYCARDS)

        // sendUnsafe() requires the recipient's contact information, including keys, to be in the
        // database, so we'll just kind of hack things into place just to make the test work. Just
        // generating a set of keys in a real-world situation wouldn't work because the keys have
        // to be given by the recipient, not just generated out of thin air.
        gUserProfileData.contactInfo.keys = ContactKeySet.generate().getOrThrow().toPublicMap()
        ContactDAO().import(gUserProfileData.contactInfo)?.let { throw it }

        // This code is a good example of how to send a message on the Mensago platform. In this
        // particular case, a contact request cycle has not been completed, so the message would
        // get silently deleted by the recipient's client once processed, but the message itself is
        // delivered, and for the purposes of this test that is all that matters.

        val client = Client()
        val senderAddress = ProfileManager.model.activeProfile!!.getWAddress().getOrThrow()
        val recipientAddress = client.resolveAddress(
            MAddress.fromString("csimons/example.com")!!
        ).getOrThrow()

        val testMsg = Message(senderAddress, recipientAddress, ContentFormat.Text)
            .withSubject("Test Message")
            .withBody("This is a test message.")

        client.login(MAddress.fromWAddress(senderAddress)).getOrThrow()
        sendUnsafe(client, testMsg)?.let { throw it }
        client.disconnect()
    }

}
