package connect.sync

import connect.Client
import connect.profiles.ProfileManager
import libmensago.SyncOp
import libmensago.SyncValue
import libmensago.WorkspaceStatus
import libmensago.commands.idle
import libmensago.commands.setStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.*

@ExtendWith(ApplicationExtension::class)
class IncomingHandlerTest {

    @Test
    fun downloadUpdatesTest() {
        val env = ITestEnvironment("incoming_downloadupdates")
            .provision(SETUP_TEST_ADMIN_KEYCARD)
        env.login(gAdminProfileData)
        val dirs = generateDemoUpdates(env, gAdminProfileData)
        assertEquals(14, idle(env.conn, 0).getOrThrow())

        val tproc = TestProcessor()
        val client = Client()
        client.login(gAdminProfileData.address).getOrThrow()
        val handler = IncomingHandler(client, tproc, ProfileManager.model)
        handler.downloadUpdates()?.let { throw it }

        val updates = SyncDAO().loadIncomingItems().getOrThrow()

        // Due to how update records are optimized before being added to the database, there should
        // be only 8 records loaded because there is an RMDIR call that handles the very same
        // directory previously created in a MKDIR call and while 6 files were created, one was
        // moved and then deleted.
        assertEquals(8, updates.size)

        for (update in updates) {
            // This magic ULID is one we have manually set in generateDemoUpdates() to ensure
            // that the updates appear to come from another device and are, thus, processed instead
            // of skipped.
            assertEquals("19d720b6-9f67-4a32-8ab6-313045489348", update.source.toString())
        }

        assertEquals(SyncOp.Mkdir, updates[0].op)
        assertEquals(SyncOp.Mkdir, updates[1].op)
        assertEquals(SyncOp.Mkdir, updates[2].op)
        assertEquals(SyncOp.Receive, updates[3].op)
        assertEquals(SyncOp.Receive, updates[4].op)
        assertEquals(SyncOp.Receive, updates[5].op)
        assertEquals(SyncOp.Receive, updates[6].op)
        assertEquals(SyncOp.Receive, updates[7].op)

        for (i in 0..2) {
            assertEquals(dirs[i], (updates[i].value as SyncValue.Mkdir).serverpath)
        }
    }

    @Test
    fun processTasksTest() {
        val env = ITestEnvironment("incoming_processupdates")
            .provision(SETUP_TEST_ADMIN_KEYCARD)

        env.login(gAdminProfileData)
        generateDemoUpdates(env, gAdminProfileData)
        assertEquals(14, idle(env.conn, 0).getOrThrow())

        val client = Client()
        client.login(gAdminProfileData.address).getOrThrow()
        val tproc = TestProcessor()
        val handler = IncomingHandler(client, tproc, ProfileManager.model)
        handler.downloadUpdates()?.let { throw it }

        val updates = SyncDAO().loadIncomingItems().getOrThrow()
        assertEquals(8, updates.size)

        // When this call completes, we should have a bunch of stuff, like contacts, messages, and
        // notes
        handler.executeTasks()?.let { throw it }

        assertEquals(5, tproc.noteItems.size)
        assertEquals("Integration Test Note 3", tproc.noteItems[3].title)
        assertEquals("Integration Test Note 5", tproc.noteItems[4].title)
    }

    @Test
    fun statusHandlingTest() {
        ITestEnvironment("incoming_statushandling").provision(SETUP_TEST_BOTH_KEYCARDS)

        val client = Client()

        ProfileManager.activateProfile("user")?.let { throw it }
        client.login(gUserProfileData.address).getOrThrow()
        client.logout()?.let { throw it }

        ProfileManager.activateProfile("primary")?.let { throw it }
        client.login(gAdminProfileData.address).getOrThrow()
        setStatus(client.getConnection(), gUserProfileData.wid, "suspended")
        client.logout()?.let { throw it }

        ProfileManager.activateProfile("user")?.let { throw it }
        val info = client.login(gUserProfileData.address).getOrThrow()
        assertEquals(WorkspaceStatus.Suspended, info.first)

        val tproc = TestProcessor()
        val handler = IncomingHandler(client, tproc, ProfileManager.model)
        assertNotNull(handler.downloadUpdates())
    }
}

