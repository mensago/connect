package connect

import connect.contacts.ContactDAO
import connect.contacts.generateDemoContacts
import libkeycard.WAddress
import libmensago.ContactMessage.Companion.requestContact
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.*
import utilities.toLocalDateTime

@ExtendWith(ApplicationExtension::class)
class PayloadConverterTest {

    @Test
    fun messageToMessageItemTest() {
        ITestEnvironment("plconverter_msg2msgitem").provision(SETUP_TEST_BOTH_KEYCARDS)

        val dao = ContactDAO()
        dao.initContactTables()
        generateDemoContacts(false).forEach { dao.saveConItem(it)?.let { e -> throw e } }

        val paycon = PayloadConverter()
        val testMsg = makeTestMsg()
        val item = paycon.messageToMessageItem(testMsg).getOrThrow()

        assertEquals("", item.subType)
        assertEquals(testMsg.date.toLocalDateTime(), item.date)
        assertEquals(testMsg.subject, item.subject)
        assertEquals("Corbin Simons", item.fromName)
        assertEquals("csimons/example.com", item.fromAddr)
        assertEquals("Me", item.toName)
        assertEquals("admin/example.com", item.toAddr)
        assertEquals(item.cc, "Dr. Richard Brannan, MD")
        assertEquals(item.bcc, "Allie Bennett")
    }

    @Test
    fun conMsgToConMsgItemTest() {
        ITestEnvironment("plconverter_conmsg2conmsgitem").provision(SETUP_TEST_BOTH_KEYCARDS)

        gAdminProfileData.contactInfo.keys = mutableMapOf(
            "Encryption" to gAdminProfileData.encryption.pubKey.toString(),
            "Verification" to gAdminProfileData.signing.pubKey.toString(),
        )

        val paycon = PayloadConverter()
        val testConMsg = requestContact(
            gUserProfileData.waddress, gAdminProfileData.contactInfo, "CR test message"
        ).getOrThrow()

        assertEquals(testConMsg.subType, "conreq.1")
        assertEquals(gAdminProfileData.waddress, testConMsg.from)
        assertEquals(gUserProfileData.waddress, testConMsg.to)
        assertEquals("CR test message", testConMsg.body)
        assertNotNull(testConMsg.contactInfo.photo)

        val item = paycon.conMsgToConMsgItem(testConMsg).getOrThrow()

        assertEquals(item.subType, "conreq.1")
        assertEquals(item.fromName, "Me")
        assertEquals(item.subject, "Contact Request from Me")
        assert(item.threadID.isNotEmpty())
        assertNotNull(item.contact.photo)
    }

    @Test
    fun waddrToUserStringTest() {
        ITestEnvironment("plconverter_waddrtostr").provision(SETUP_TEST_BOTH_KEYCARDS)


        val dao = ContactDAO()
        generateDemoContacts(false).forEach { dao.saveConItem(it)?.let { e -> throw e } }

        // If not found anywhere, we should get our input back to us.
        var wid = "455f8c8f-4cad-4142-bede-7dcc708b2f38"
        PayloadConverter.infoForWAddress(WAddress.fromString("$wid/example.com")!!)
            .getOrThrow().let {
                assert(it.first.isEmpty())
                assertEquals("$wid/example.com", it.second)
            }


        // Success case where we get user's name and Mensago address
        wid = "714d66b1-8c0f-4763-a772-ef9acd1f1dee"
        PayloadConverter.infoForWAddress(WAddress.fromString("$wid/example.com")!!).getOrThrow()
            .let {
                assertEquals("Dr. Richard Brannan, MD", it.first)
                assertEquals("rbrannan/example.com", it.second)
            }

        // Contact with only a workspace address
        wid = "131a3147-e966-47ac-9690-bb49b61003a6"
        PayloadConverter.infoForWAddress(WAddress.fromString("$wid/example.com")!!).getOrThrow()
            .let {
                assertEquals("Charlie Cuevas", it.first)
                assertEquals("131a3147-e966-47ac-9690-bb49b61003a6/example.com", it.second)
            }

        // Technically there's also the case of a workspace address which refers to a non-contact
        // who has a Mensago address but no name listed. Too much work to bother with, so I'm
        // skipping it.
    }
}
