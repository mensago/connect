package connect

import connect.contacts.ContactDAO
import connect.main.MainController
import connect.messages.ConMsgHeaderItem
import connect.messages.MessageDAO
import connect.profiles.KeyDAO
import connect.profiles.ProfileManager
import connect.profiles.WorkspaceDAO
import connect.sync.IncomingHandler
import keznacl.SHAPassword
import keznacl.getHasherForHash
import libkeycard.RandomID
import libkeycard.RangeException
import libkeycard.Timestamp
import libkeycard.UserID
import libmensago.MServerPath
import libmensago.ProtocolException
import libmensago.ResourceNotFoundException
import libmensago.WorkspaceStatus
import libmensago.resolver.KCResolver
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.*
import utilities.ServerConfig
import java.io.File
import java.lang.Thread.sleep
import java.nio.file.Paths

@ExtendWith(ApplicationExtension::class)
class ClientTest {

    @Test
    fun changePassword() {
        val client = clientBasedTestSetup("client.changePassword")

        // This command requires being logged in
        val oldpw = getHasherForHash(gAdminProfileData.clientPasshash).getOrThrow()
        val newpw = getHasherForHash(gUserProfileData.clientPasshash).getOrThrow()

        assertNotNull(client.changePassword(oldpw, newpw))
        client.login(gAdminProfileData.address).getOrThrow()

        assertNotNull(client.changePassword(oldpw, SHAPassword()))
        assertNotNull("", gAdminProfileData.passhash)
        assertNull(client.changePassword(oldpw, newpw))

        client.disconnect()
    }

    @Test
    fun download() {
        ITestEnvironment("client_download").provision(SETUP_TEST_ADMIN_PROFILE)
        val client = Client().apply { login(gAdminProfileData.address).getOrThrow() }

        val config = ServerConfig()
        val fsAdminTop = Paths.get(
            config.getString("global.workspace_dir")!!,
            gAdminProfileData.wid.toString(),
        )

        val testFileInfo = makeTestFile(fsAdminTop.toString(), null, 8000)
        val serverFilePath = MServerPath.fromString(
            "/ ${gAdminProfileData.wid} ${testFileInfo.first}"
        )!!
        val clientPath = Paths.get(
            ProfileManager.model.activeProfilePath.toString(),
            testFileInfo.first
        )
        client.download(serverFilePath, clientPath.toString())?.let { throw it }
        val file = File(clientPath.toString())
        assert(file.exists())
        assertEquals(8000, file.length())
    }

    @Test
    fun login() {
        ITestEnvironment("client_login").provision(SETUP_TEST_ADMIN_PROFILE)
        with(Client()) {
            connect(gAdminProfileData.domain)?.let { throw it }
            login(gAdminProfileData.address).getOrThrow()
            disconnect()?.let { throw it }
        }
    }

    @Test
    fun makeCRTest() {
        ITestEnvironment("client_makecr").provision(SETUP_TEST_CONTACT_INFO)
        val client = Client()
        val info = UserInfoDAO().loadPublicContact().getOrThrow()
        val keySet = ContactKeySet.generate().getOrThrow()
        info.keys = keySet.toPublicMap()
        client.makeContactRequest(gUserProfileData.address, info, "client_makecr message")
            .getOrThrow()
    }

    @Test
    fun mkdir() {
        ITestEnvironment("client_mkdir").provision(SETUP_TEST_ADMIN_PROFILE)
        with(Client()) {
            assertNotNull(mkdir(MServerPath("/ test")))

            login(gAdminProfileData.address).getOrThrow()
            assertNull(mkdir(MServerPath("/ test")))
        }
    }

    @Test
    fun newDeviceLogin() {
        ITestEnvironment("client_login").provision(SETUP_TEST_ADMIN_KEYCARD)

        ProfileManager.createProfile("admin2")
        ProfileManager.activateProfile("admin2")?.let { throw it }
        Client().login(gAdminProfileData.address, gAdminProfileData.password).getOrThrow()
    }

    @Test
    fun preregister() {
        val env = ITestEnvironment("client_preregister").provision(SETUP_TEST_ADMIN)

        // This code is necessary because the test environment registers the user -- which we don't
        // want -- if we specify SETUP_TEST_ADMIN_PROFILE. This is a special case, so we're just
        // going to do the profile setup ourselves.
        ProfileManager.ensureDefaultProfile()
        ProfileManager.scanProfiles()?.let { throw it }
        env.setupProfile(gAdminProfileData)

        with(Client()) {
            connect(gAdminProfileData.domain)?.let { throw it }
            login(gAdminProfileData.address).getOrThrow()

            // Test case #1: No data supplied
            preregister(null, null).getOrThrow()

            // Test case #2: Workspace ID supplied
            preregister(
                UserID.fromWID(
                    RandomID.fromString(
                        "33333333-3333-3333-3333-333333333333"
                    )!!
                ), null
            ).getOrThrow()

            // Test case #3: User ID supplied
            preregister(gUserProfileData.uid, null).getOrThrow()

            disconnect()?.let { throw it }
        }
    }

    @Test
    fun regcode() {
        val env = ITestEnvironment("client_regcode").provision(SETUP_TEST_ADMIN)
        // Like with preregister, we manually handle creating the admin profile
        ProfileManager.ensureDefaultProfile()
        ProfileManager.scanProfiles()?.let { throw it }
        env.setupProfile(gAdminProfileData)

        // 1. Log in as admin, prereg user, and log out.
        // 2. Create a second profile for the user
        // 3. Use the regcode from earlier to register the user's first device
        // 4. Log in as the user and call update_keycard() twice, once to upload the root and a
        //    second time to chain and upload a second one

        with(Client()) {
            connect(gAdminProfileData.domain)?.let { throw it }
            login(gAdminProfileData.address).getOrThrow()

            val preregInfo = preregister(gUserProfileData.uid, null).getOrThrow()
            logout()?.let { throw it }

            ProfileManager.createProfile("user")?.let { throw it }
            ProfileManager.activateProfile("user")?.let { throw it }
            val pwHash = getHasherForHash(gUserProfileData.clientPasshash).getOrElse { throw it }
            redeemRegcode(gUserProfileData.address, preregInfo.regcode, pwHash)?.let { throw it }
            login(gUserProfileData.address).getOrThrow()
            updateKeycard()?.let { throw it }
            updateKeycard()?.let { throw it }

            disconnect()?.let { throw it }
        }
    }

    @Test
    fun register() {
        val env = ITestEnvironment("client_register").provision(SETUP_TEST_ADMIN)
        // Like with preregister and regcode, we manually handle creating the admin profile
        ProfileManager.ensureDefaultProfile()
        ProfileManager.scanProfiles()?.let { throw it }
        env.setupProfile(gAdminProfileData)

        ProfileManager.createProfile("user")?.let { throw it }
        ProfileManager.activateProfile("user")?.let { throw it }

        with(Client()) {
            connect(gUserProfileData.domain)?.let { throw it }
            val pwHash = getHasherForHash(gUserProfileData.clientPasshash).getOrElse { throw it }

            register(gUserProfileData.domain, pwHash, gUserProfileData.uid)?.let { throw it }
            assertEquals(
                WorkspaceStatus.Active, login(gUserProfileData.address).getOrThrow().first
            )
            updateKeycard()?.let { throw it }
            updateKeycard()?.let { throw it }

            disconnect()?.let { throw it }
        }
    }

    @Test
    fun resetPassword() {
        ITestEnvironment("client_resetPassword").provision(SETUP_TEST_BOTH_PROFILES)

        // Perform the password reset

        val client = Client()
        ProfileManager.activateProfile("primary")?.let { throw it }
        client.login(gAdminProfileData.address).getOrThrow()

        assert(client.resetPassword(gUserProfileData.wid, "abcd😉").isFailure)
        assert(client.resetPassword(gUserProfileData.wid, "a🙃".repeat(65)).isFailure)
        assert(
            client.resetPassword(
                gUserProfileData.wid, "abcdefgh",
                Timestamp().plusMinutes(5)
            ).isFailure
        )
        assert(
            client.resetPassword(
                gUserProfileData.wid, "abcdefgh",
                Timestamp().plusHours(60)
            ).isFailure
        )

        val resetData = client.resetPassword(
            gUserProfileData.wid, "abcdefghijklmnopq",
            Timestamp().plusHours(8)
        ).getOrThrow()
        client.logout()?.let { throw it }

        ProfileManager.activateProfile("user")?.let { throw it }
        client.login(gUserProfileData.address).getOrThrow()

        val userPass = getHasherForHash(gUserProfileData.clientPasshash).getOrThrow()
        var result = client.useRecoveryCode(gUserProfileData.wid, "aaaaaa", userPass)
        assertNotNull(result)
        assert(result is RangeException)

        val badWID = RandomID.fromString("11111111-1111-1111-1111-111111111111")!!
        result = client.useRecoveryCode(badWID, resetData.first, userPass)
        assertNotNull(result)
        assert(result is ProtocolException)
        assertEquals(401, (result as ProtocolException).code)

        // Remove unused code warnings for ProtocolException
        "${result.description} ${result.info}"

        assertNull(client.useRecoveryCode(gUserProfileData.wid, resetData.first, userPass))

        client.logout()?.let { throw it }

        client.disconnect()
    }

    @Test
    fun sendCRTest() {
        // This test case performs a full client-level Contact Request process with data verification
        // and is the reason for its unusual length. Tests are preferred to be short, but this
        // particular one cannot easily be short and achieve any practical usefulness.

        // The test begins with the admin profile active, and the admin sends a CR to the test
        // user's account. The test then switches to the user's profile, downloads updates, and
        // approves the admin's CR. From there, the test switches back to the admin's profile to
        // receive and validate the user's approval message.

        // Key test points:
        // - Are relationship keys created and saved and then migrated correctly once the process
        //   is complete?
        // - Does the admin's contact info appear in the user's address book after approval?
        // - Does the user's contact info show up in the admin's address book after receiving the
        //   approval?
        // - Is contact information received on each side correct?

        ITestEnvironment("client_sendcr").provision(SETUP_TEST_CONTACT_INFO)

        assertFalse(ContactDAO().hasPendingCR(gUserProfileData.waddress).getOrThrow())

        val client = Client()
        val adminPublicInfo = UserInfoDAO().loadPublicContact().getOrThrow()
        client.sendContactRequest(
            gUserProfileData.address, adminPublicInfo, "client_sendcr message", false
        ).getOrThrow()
        client.disconnect()

        // Verify that the relationship keys were saved to the database correctly

        with(ContactDAO()) {
            assert(hasPendingCR(gUserProfileData.waddress).getOrThrow())
            loadPendingCRKeys(gUserProfileData.waddress).getOrThrow()
        }

        ProfileManager.activateProfile("user")?.let { throw it }
        val userMain = MainController()

        // get updates
        client.login(gUserProfileData.address).getOrThrow()
        val handler = IncomingHandler(client, userMain, ProfileManager.model)
        handler.downloadUpdates()?.let { throw it }
        sleep(100)
        handler.executeTasks()?.let { throw it }

        val adminMsgCon = userMain.messaging()
        val adminMsgList = adminMsgCon.getMessages()
        assert(adminMsgList.last() is ConMsgHeaderItem)
        val conMsgItem = MessageDAO().loadConMsg(adminMsgList.last().id).getOrThrow()!!

        val adminCRInfo = conMsgItem.contact

        // Make sure that ALL of the contact info for the admin that we received is correct

        assertEquals(gAdminProfileData.contactInfo.formattedName, adminCRInfo.formattedName)
        assertEquals(gAdminProfileData.contactInfo.givenName, adminCRInfo.givenName)
        assertEquals(gAdminProfileData.contactInfo.familyName, adminCRInfo.familyName)
        assertEquals(gAdminProfileData.contactInfo.gender, adminCRInfo.gender)

        assertEquals(gAdminProfileData.contactInfo.websites!!.size, adminCRInfo.websites!!.size)
        assertEquals(gAdminProfileData.contactInfo.websites!![0], adminCRInfo.websites!![0])

        assertEquals(gAdminProfileData.contactInfo.phone!!.size, adminCRInfo.phone!!.size)
        assertEquals(gAdminProfileData.contactInfo.phone!![0], adminCRInfo.phone!![0])

        assertEquals(gAdminProfileData.contactInfo.birthday, adminCRInfo.birthday)
        assertEquals(gAdminProfileData.contactInfo.anniversary, adminCRInfo.anniversary)

        assertEquals(gAdminProfileData.contactInfo.email!!.size, adminCRInfo.email!!.size)
        assertEquals(gAdminProfileData.contactInfo.email!![0], adminCRInfo.email!![0])

        // Approve the contact request

        val userPublicInfo = UserInfoDAO().loadPublicContact().getOrThrow()
        client.approveContactRequest(
            gAdminProfileData.waddress, userPublicInfo, conMsgItem.contact, false
        )?.let { throw it }
        client.disconnect()

        // Ensure that contact keys are in the database and correct after approving the CR and also
        // ensure that the pending CR is removed from contact_requests

        KeyDAO().let {
            val conid = conMsgItem.contact.id
            assertEquals(
                conMsgItem.contact.keys?.get("Verification"),
                it.loadContactVerificationKey(conid).getOrThrow().toString()
            )
            assertEquals(
                conMsgItem.contact.keys?.get("Encryption"),
                it.loadContactEncryptionKey(conid).getOrThrow().toString()
            )
            it.loadRelSigningPair(conid).getOrThrow()
            it.loadRelEncryptionPair(conid).getOrThrow()
        }

        assert(
            ContactDAO().loadPendingCRKeys(gAdminProfileData.waddress)
                .exceptionOrNull() is ResourceNotFoundException
        )

        // Now we make sure that the information loaded from the imported contact is also correct

        val item = ContactDAO().loadConItem(conMsgItem.contact.id.toString()).getOrThrow()!!

        assertEquals(gAdminProfileData.contactInfo.formattedName, item.formattedName)
        assertEquals(gAdminProfileData.contactInfo.givenName, item.givenName)
        assertEquals(gAdminProfileData.contactInfo.familyName, item.familyName)
        assertEquals(gAdminProfileData.contactInfo.gender, item.gender)

        assertEquals(gAdminProfileData.contactInfo.websites!!.size, item.websiteListProperty.size)
        assertEquals(
            gAdminProfileData.contactInfo.websites!![0].value, item.websiteListProperty[0].value
        )

        assertEquals(gAdminProfileData.contactInfo.phone!!.size, item.phoneListProperty.size)
        assertEquals(
            gAdminProfileData.contactInfo.phone!![0].value, item.phoneListProperty[0].value
        )

        assertEquals(gAdminProfileData.contactInfo.birthday, item.birthday)
        assertEquals(gAdminProfileData.contactInfo.anniversary, item.anniversary)

        assertEquals(gAdminProfileData.contactInfo.email!!.size, item.emailListProperty.size)
        assertEquals(
            gAdminProfileData.contactInfo.email!![0].value, item.emailListProperty[0].value
        )

        // Switch back to admin and confirm approval

        ProfileManager.activateProfile("primary")?.let { throw it }

        // get admin's updates and the approval message

        client.login(gAdminProfileData.address).getOrThrow()
        val adminHandler = IncomingHandler(client, userMain, ProfileManager.model)
        adminHandler.downloadUpdates()?.let { throw it }
        sleep(100)
        adminHandler.executeTasks()?.let { throw it }
        client.disconnect()

        assert(adminMsgList.last() is ConMsgHeaderItem)
        val importedUserInfo = with(ContactDAO()) {
            val userConItem = conItemForWAddress(gUserProfileData.waddress).getOrThrow()!!
            loadContact(userConItem.id).getOrThrow()!!
        }

        // Make sure that all of the contact info for the *user* that we received is correct

        assertEquals(gUserProfileData.contactInfo.formattedName, importedUserInfo.formattedName)
        assertEquals(gUserProfileData.contactInfo.givenName, importedUserInfo.givenName)
        assertEquals(gUserProfileData.contactInfo.familyName, importedUserInfo.familyName)
        assertEquals(gUserProfileData.contactInfo.gender, importedUserInfo.gender)

        assertEquals(gUserProfileData.contactInfo.websites!!.size, importedUserInfo.websites!!.size)
        assertEquals(gUserProfileData.contactInfo.websites!![0], importedUserInfo.websites!![0])

        assertEquals(gUserProfileData.contactInfo.phone!!.size, importedUserInfo.phone!!.size)
        assertEquals(gUserProfileData.contactInfo.phone!![0], importedUserInfo.phone!![0])

        assertEquals(gUserProfileData.contactInfo.birthday, importedUserInfo.birthday)
        assertEquals(gUserProfileData.contactInfo.anniversary, importedUserInfo.anniversary)

        assertEquals(gUserProfileData.contactInfo.email!!.size, importedUserInfo.email!!.size)
        assertEquals(gUserProfileData.contactInfo.email!![0], importedUserInfo.email!![0])
    }

    // unregister() is tested in ISCommandsITest

    @Test
    fun updateIdentityDeviceKey() {
        ITestEnvironment("client_updateIdentityDeviceKey")
            .provision(SETUP_TEST_BOTH_PROFILES)

        ProfileManager.activateProfile("user")?.let { throw it }

        val client = Client()
        client.login(gUserProfileData.address).getOrThrow()

        val wdao = WorkspaceDAO()
        val waddr = ProfileManager.model.activeProfile!!.getWAddress().getOrThrow()
        val oldPair = wdao.getDeviceKeypair(waddr).getOrThrow()
        client.updateIdentityDeviceKey()?.let { throw it }
        val newPair = wdao.getDeviceKeypair(waddr).getOrThrow()

        assert(oldPair.pubKey.toString() != newPair.pubKey.toString())
        client.disconnect()
    }

    @Test
    fun upload() {
        ITestEnvironment("client_upload").provision(SETUP_TEST_ADMIN_PROFILE)

        val client = Client().apply { login(gAdminProfileData.address).getOrThrow() }

        val adminTop = MServerPath.fromString("/ ${gAdminProfileData.wid}")!!
        val testFileInfo = makeTestFile(ProfileManager.profilesPath, null, 8000)
        val testFilePath = Paths.get(ProfileManager.profilesPath, testFileInfo.first).toString()
        client.upload(testFilePath, adminTop).getOrThrow()

        // Try to upload something larger than the server allows
        val config = ServerConfig()
        val maxItemSize = config.getInteger("performance.max_file_size")!! * 1048576
        val bigFileInfo = makeTestFile(
            ProfileManager.profilesPath, null,
            maxItemSize + 1048576
        )
        val bigFilePath = Paths.get(ProfileManager.profilesPath, bigFileInfo.first).toString()
        val bigResult = client.upload(bigFilePath, adminTop)
        assert(bigResult.isFailure)
        assert(bigResult.exceptionOrNull() is ProtocolException)
        assertEquals(414, (bigResult.exceptionOrNull() as ProtocolException).code)
    }

    @Test
    fun lintRemoval() {
        // This test exists strictly to remove warnings from IDEA about parts of the API being
        // unused or able to be private merely because they're not referenced by something.

        ITestEnvironment("client_lintremoval").provision(SETUP_TEST_ADMIN_PROFILE)
        with(Client()) {
            KCResolver.dns.lookupA("example.com").getOrThrow()
            assert(!isAdmin)
            assertEquals(90, expiration)
            assert(!isConnected())
            assert(!isLoggedIn())
        }
    }
}