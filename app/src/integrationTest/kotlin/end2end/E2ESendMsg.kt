package end2end

import connect.Client
import connect.Filter
import connect.main.MainController
import connect.messages.MessageDAO
import connect.profiles.ProfileManager
import connect.sync.IncomingHandler
import libkeycard.RandomID
import libmensago.ContentFormat
import libmensago.Message
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_RELATIONSHIP
import testsupport.gAdminProfileData
import testsupport.gUserProfileData

@ExtendWith(ApplicationExtension::class)
class E2ESendMsg {

    @Test
    fun runTest() {
        // This test is similar to the CR integration test for the client, but this case uses the
        // test environment to establish the relationship and then tests sending a regular message.

        ITestEnvironment("e2e_sendmsg").provision(SETUP_TEST_RELATIONSHIP)

        val sentFilter = Filter("Sent").apply {
            sortBy("date")
            add("labels:Sent")
            add("-labels:Trash")
        }

        assert(MessageDAO().loadHeaders(sentFilter).getOrThrow().isEmpty())

        // Admin sends a regular message
        val testMsg = Message(
            gAdminProfileData.waddress, gUserProfileData.waddress, ContentFormat.Text
        ).withSubject("Test").withBody("This is a test message")

        val client = Client()
        client.send(testMsg, true)?.let { throw it }

        // Check that a copy of the message was saved and tagged as Sent

        val sentMessages = MessageDAO().loadHeaders(sentFilter).getOrThrow()
        assertEquals(1, sentMessages.size)
        assertEquals("Test", sentMessages[0].subject)
        assertEquals("Me", sentMessages[0].fromName)
        assertEquals(gAdminProfileData.address.toString(), sentMessages[0].fromAddr)
        assertEquals("Corbin Simons", sentMessages[0].toName)
        assertEquals("csimons/example.com", sentMessages[0].toAddr)

        // User receives and processes regular message

        ProfileManager.activateProfile("user")?.let { throw it }
        val userMain = MainController()

        // get updates
        client.login(gUserProfileData.address).getOrThrow()

        val handler = IncomingHandler(client, userMain, ProfileManager.model)
        handler.downloadUpdates()?.let { throw it }
        handler.executeTasks()?.let { throw it }

        val userMsgCon = userMain.messaging()
        val userMsgList = userMsgCon.getMessages()
        val msgItem = MessageDAO().load(userMsgList.last().id).getOrThrow()!!

        assertEquals("Test", msgItem.subject)
        assertEquals("This is a test message", msgItem.contents)
        assertEquals("", msgItem.subType)
        assertNotNull(msgItem.date)
        assertNotNull(RandomID.fromString(msgItem.threadID))
        assertNotNull(RandomID.fromString(msgItem.id))
        assertEquals("admin/example.com", msgItem.fromAddr)
        assertEquals("Mensago Administrator", msgItem.fromName)

        msgItem.getLabels().let {
            assert(it.contains("Inbox"))
            assert(it.contains("Unread"))
        }
    }
}
