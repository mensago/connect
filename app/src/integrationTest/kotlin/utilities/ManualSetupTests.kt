package utilities

import connect.Client
import connect.UserInfoDAO
import connect.contacts.ContactDAO
import connect.profiles.ProfileDAO
import connect.profiles.ProfileManager
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_ADMIN
import testsupport.SETUP_TEST_DATABASE
import testsupport.gAdminProfileData

// This class contains tests that perform setup for manual, interactive testing of the GUI
@ExtendWith(ApplicationExtension::class)
class ManualSetupTests {

    // Clears everything and preps for administrator registration using the prereg code
    // "Undamaged Shining Amaretto Improve Scuttle Uptake"
    @Test
    fun emptyLocalSetupTest() {
        ITestEnvironment("manual_emptylocalsetup").provision(SETUP_TEST_DATABASE)
    }

    // For testing "returning" logins of the administrator account. IOW, the administrator is
    // already registered and the client-side profile "primary" is associated with it. The keycard
    // for the administrator has also been uploaded, as well.
    @Test
    fun adminLoginSetupTest() {
        val env = ITestEnvironment("manual_adminloginsetup").provision(SETUP_TEST_ADMIN)
        ProfileManager.ensureDefaultProfile()
        ProfileManager.scanProfiles()?.let { throw it }
        env.setupProfile(env.adminProfile)
        env.setupKeycard(env.adminProfile)

        with(gAdminProfileData) {
            ProfileDAO().clearFirstStartup()?.let { throw it }
            ContactDAO().import(contactInfo)?.let { throw it }
            UserInfoDAO().saveIdentity(uid.toString(), wid.toString(), domain.toString())
                ?.let { throw it }
        }

        linkAdminProfile(env)
    }

    /**
     * Performs the extra work needed to ensure that the client-side profile 'primary' will
     * successfully log into the server with the admin's login info.
     */
    private fun linkAdminProfile(env: ITestEnvironment) {
        env.db!!.execute(
            "UPDATE iwkspc_devices SET devid=?,devkey=? WHERE wid=?",
            gAdminProfileData.devid,
            gAdminProfileData.devpair.pubKey,
            gAdminProfileData.wid,
        )

        val client = Client()
        client.login(gAdminProfileData.address).getOrThrow()
        client.updateKeycard()
        client.logout()?.let { throw it }
        client.disconnect()?.let { throw it }
    }
}
