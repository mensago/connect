@file:Suppress("unused")

package testsupport

import libkeycard.Domain
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.CmdStatus
import libmensago.ResourceNotFoundException
import libmensago.ServerResponse
import java.net.ProtocolException

// This file just contains some glue code so that certain classes pulled from mensagod, such as
// DBConn, can be imported unaltered from the server sources.

class DatabaseException(message: String = "") : Exception(message)
class DatabaseCorruptionException(message: String = "") : Exception(message)
class FSFailureException(message: String = "") : Exception(message)
class TestFailureException(message: String = "") : Exception(message)

/** The server's default domain. */
var gServerDomain = Domain.fromString("localdomain.priv")!!

/** The server's workspace ID / device ID */
val gServerDevID = RandomID.fromString("00000000-0000-0000-0000-000000000000")!!

/**  A workspace address to represent the server */
var gServerAddress = WAddress.fromParts(gServerDevID, gServerDomain)

fun ServerResponse.assertReturnCode(c: Int) {
    if (code != c) throw ProtocolException("Expected: $c, received $this")
}

fun ServerResponse.assertField(field: String, validator: (v: String) -> Boolean) {
    if (!data.containsKey(field))
        throw ResourceNotFoundException("Missing field $field")
    assert(validator(data[field]!!))
}

fun CmdStatus.assertReturnCode(c: Int) {
    if (code != c) throw ProtocolException("Expected: $c, received $this")
}

