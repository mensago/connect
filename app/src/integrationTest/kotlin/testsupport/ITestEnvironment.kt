package testsupport

import connect.Client
import connect.KeyPurpose
import connect.LogDAO
import connect.UserInfoDAO
import connect.contacts.ContactDAO
import connect.main.FilterItemProcessor
import connect.main.MainController
import connect.messages.ConMsgHeaderItem
import connect.profiles.*
import connect.sync.IncomingHandler
import connect.sync.OutgoingHandler
import keznacl.CryptoString
import keznacl.EncryptionPair
import keznacl.SigningPair
import keznacl.getHasherForHash
import libkeycard.*
import libmensago.*
import libmensago.commands.device
import libmensago.commands.login
import libmensago.commands.logout
import libmensago.commands.password
import libmensago.db.PGConfig
import libmensago.resolver.KCResolver
import org.apache.commons.io.FileUtils
import utilities.ServerConfig
import java.lang.Thread.sleep
import java.net.InetAddress
import java.nio.file.Path
import java.nio.file.Paths


const val SETUP_TEST_FILESYSTEM: Int = 1
const val SETUP_TEST_DATABASE: Int = 2
const val SETUP_TEST_ADMIN: Int = 3
const val SETUP_TEST_USER: Int = 4
const val SETUP_TEST_ADMIN_PROFILE: Int = 6
const val SETUP_TEST_BOTH_PROFILES: Int = 7
const val SETUP_TEST_ADMIN_KEYCARD: Int = 8
const val SETUP_TEST_BOTH_KEYCARDS: Int = 9
const val SETUP_TEST_CONTACT_INFO: Int = 10
const val SETUP_TEST_RELATIONSHIP: Int = 11

/**
 * This support class is responsible for setting up the environment for integration tests. It is
 * extremely similar to mensagod's ServerTestEnvironment class with the difference that this class
 * has the capability to set up both the client and server side. It provides a similar API so that
 * libmensago sources shared between the two environments can be maintained more easily, although
 * at some future time the library will be removed from both projects and moved into its own
 * with this code remaining.
 *
 * NOTE: If a test requires database setup, the user account which connects to the database
 * must have CREATEDB privileges to create a database for each test. This is to permit
 * parallelization and prevent tests from stepping on one another's toes.
 */
class ITestEnvironment(val testName: String) {
    val serverconfig: ServerConfig = ServerConfig.load().getOrThrow()
    private val dbconfig: PGConfig = PGConfig(
        serverconfig.getString("database.user")!!,
        serverconfig.getString("database.password")!!,
        serverconfig.getString("database.host")!!,
        serverconfig.getInteger("database.port")!!,
        serverconfig.getString("database.name")!!,
    )
    var db: PGConn? = null
    private var serverPrimary: SigningPair? = null
    private var serverEncryption: EncryptionPair? = null
    private var serverCard: Keycard? = null

    val testPath: String = Paths.get("build", "testfiles", testName).toAbsolutePath()
        .toString()
    val topdir: Path = Paths.get(serverconfig.getString("global.top_dir")!!)

    @Suppress("MemberVisibilityCanBePrivate")
    val profdir: Path = Paths.get(testPath, "profdir")

    val adminProfile = createAdminProfileData()
    val userProfile = createUserProfileData()

    var conn = ServerConnection()

    /**
     * Provisions the test environment based on the configuration set. Each specified provisioning
     * level builds upon the ones which precede.
     *
     * - SETUP_TEST_FILESYSTEM: Initializes the test's folder hierarchy.
     * - SETUP_TEST_DATABASE: Start the test with an empty server database, nothing more.
     * - SETUP_TEST_ADMIN: Register the administrator account and the administrator's first device
     * on the server side via direct access.
     * - SETUP_TEST_ADMIN: Also register a test user.
     * - SETUP_TEST_ADMIN_PROFILE: Register the admin and a test user, but also initialize the
     * admin's client-side profile.
     * - SETUP_TEST_BOTH_PROFILES: Also set up the test user's client-side profile.
     * - SETUP_TEST_ADMIN_KEYCARD: As above, but also upload the admin's initial keycard entry and
     * chain a second one to it.
     * - SETUP_TEST_BOTH_KEYCARDS: Also upload the test user's root keycard entry and chain a
     * second one to it.
     * - SETUP_TEST_CONTACT_INFO: Add each profile's contact information to its database
     * - SETUP_TEST_RELATIONSHIP: Perform a Contact Request exchange, including adding address book
     * info
     */
    fun provision(provisionLevel: Int): ITestEnvironment {
        setupTestBase()
        if (provisionLevel >= SETUP_TEST_DATABASE)
            initDatabase()
        if (provisionLevel >= SETUP_TEST_ADMIN)
            regAdmin()
        if (provisionLevel >= SETUP_TEST_USER)
            regUser()
        if (provisionLevel >= SETUP_TEST_ADMIN_PROFILE) {
            ProfileManager.ensureDefaultProfile()
            ProfileManager.scanProfiles()?.let { throw it }
            setupProfile(adminProfile)
        }
        if (provisionLevel >= SETUP_TEST_BOTH_PROFILES)
            setupProfile(userProfile)
        if (provisionLevel >= SETUP_TEST_ADMIN_KEYCARD) {
            ProfileManager.activateProfile("primary")
            setupKeycard(adminProfile)
        }
        if (provisionLevel >= SETUP_TEST_BOTH_KEYCARDS) {
            ProfileManager.activateProfile("user")
            setupKeycard(userProfile)
        }
        if (provisionLevel >= SETUP_TEST_CONTACT_INFO) {
            ProfileManager.activateProfile("primary")
            setupContactInfo(adminProfile)
            ProfileManager.activateProfile("user")
            setupContactInfo(userProfile)
        }
        if (provisionLevel >= SETUP_TEST_RELATIONSHIP)
            doConReqExchange()
        ProfileManager.activateProfile("primary")

        return this
    }

    /**
     * Shuts down the test environment. This is primarily to close any database connections.
     */
    fun done() {
        db?.disconnect()
    }

    /**
     * Returns a connection to the database. This will throw an exception if the database connection
     * has not been initialized.
     */
    fun getDB(): PGConn {
        return db ?: throw TestFailureException("Database not initialized")
    }

    /**
     * Returns the server's current primary signing keypair or throws an exception. This call
     * requires database initialization.
     */
    fun serverPrimaryPair(): SigningPair {
        return serverPrimary ?: throw TestFailureException("Database not initialized")
    }

    /**
     * Returns the server's current encryption keypair or throws an exception. This call requires
     * database initialization.
     */
    fun serverEncryptionPair(): EncryptionPair {
        return serverEncryption ?: throw TestFailureException("Database not initialized")
    }

    /** Connect to a Mensago server running on the same host to service the example.com domain */
    fun connectToServer() {
        if (conn.isConnected()) conn.disconnect()?.let { throw it }

        val port = serverconfig.getInteger("network.port")!!
        val ip = InetAddress.getByName(serverconfig.getString("network.listen_ip"))
        conn.connect(ip, port)?.let { throw it }
    }

    /** Logs in as the user associated with the specified test profile data */
    fun login(tpdata: TestProfileData): Boolean {
        if (!conn.isConnected()) connectToServer()

        val pwInfo = login(conn, tpdata.wid, serverEncryptionPair()).getOrThrow()
        val pwHash = getHasherForHash(tpdata.clientPasshash).getOrThrow()
        password(conn, pwHash, pwInfo)?.let { throw it }

        val devInfo = DeviceInfo(
            tpdata.devid, tpdata.encryption.pubKey, tpdata.encryption.privKey, mutableMapOf(
                "Device Name" to "laptop",
                "User Name" to "owner",
                "OS" to "Ubuntu 22.04",
                "Device ID" to tpdata.devid.toString(),
                "Device Key" to tpdata.devpair.pubKey.toString(),
                "Timestamp" to Timestamp().toString(),
            )
        )
        devInfo.encryptAttributes(tpdata.encryption).getOrThrow()
        val keyhash = tpdata.encryption.getPublicHash().getOrThrow()
        return device(conn, devInfo, keyhash).getOrThrow().first
    }

    /** Logs out of the current session, but remains connected to the Mensago server instance */
    fun logout() {
        if (conn.isConnected()) logout(conn)?.let { throw it }
    }

    /** Disconnects from the current server session */
    fun disconnect() = conn.disconnect()?.let { throw it }

    /**
     * Provisions the integration test folder hierarchy and environment baseline. The test root
     * contains one subfolder, topdir, which is the server's workspace data folder. This also
     * provides other initialization/setup, such as initializing the mock DNS handler.
     */
    private fun setupTestBase() {
        val topdirFile = topdir.toFile()
        if (topdirFile.exists())
        // The only reason we'll get an exception here is if we can't delete the folder under
        // Windows, and that's not a problem.... at least for now anyway.
            runCatching { FileUtils.cleanDirectory(topdirFile) }
        else
            topdirFile.mkdirs()

        val profdirFile = profdir.toFile()
        if (profdirFile.exists())
            FileUtils.cleanDirectory(profdirFile)
        else
            profdirFile.mkdirs()
        ProfileManager.setLocation(profdir)?.let { throw it }

        val dnsConn = serverconfig.connectToDB().getOrThrow()
        KCResolver.dns = FakeDNSHandler(dnsConn)
        OutgoingHandler.useThreads = false
        LocalFS.initialize(topdir.toString())?.let { throw it }
        gServerDomain = Domain.fromString(serverconfig.getString("global.domain"))!!
        gServerAddress = WAddress.fromParts(gServerDevID, gServerDomain)
    }

    /**
     * Provisions a database for the integration test. The database is populated with all tables
     * needed and adds basic data to the database as if setup had been run. It also rotates the org
     * keycard so that there are two entries.
     */
    private fun initDatabase() {
        resetDB(serverconfig).getOrThrow().close()
        db = PGConn(dbconfig)
        DBConn.initialize(serverconfig)?.let { throw it }

        val initialOSPair = SigningPair.fromStrings(
            "ED25519:r#r*RiXIN-0n)BzP3bv`LA&t4LFEQNF0Q@\$N~RF*",
            "ED25519:{UNQmjYhz<(-ikOBYoEQpXPt<irxUF*nq25PoW=_"
        ).getOrThrow()
        val initialOEPair = EncryptionPair.fromStrings(
            "CURVE25519:SNhj2K`hgBd8>G>lW\$!pXiM7S-B!Fbd9jT2&{{Az",
            "CURVE25519:WSHgOhi+bg=<bO^4UoJGF-z9`+TBN{ds?7RZ;w3o"
        ).getOrThrow()

        val rootEntry = OrgEntry().run {
            setFieldInteger("Index", 1)
            setField("Name", "Example, Inc.")
            setField(
                "Contact-Admin",
                "ae406c5e-2673-4d3e-af20-91325d9623ca/example.com"
            )
            setField("Language", "eng")
            setField("Domain", "example.com")
            setField("Primary-Verification-Key", initialOSPair.pubKey.toString())
            setField("Encryption-Key", initialOEPair.pubKey.toString())

            isDataCompliant()?.let { throw it }
            hash()?.let { throw it }
            sign("Organization-Signature", initialOSPair)?.let { throw it }
            verifySignature("Organization-Signature", initialOSPair).getOrThrow()
            isCompliant()?.let { throw it }
            this
        }

        serverCard = Keycard.new("Organization")!!
        serverCard!!.entries.add(rootEntry)

        db!!.execute(
            "INSERT INTO keycards(owner,creationtime,index,entry,fingerprint) " +
                    "VALUES('organization',?,?,?,?);",
            rootEntry.getFieldString("Timestamp")!!,
            rootEntry.getFieldInteger("Index")!!,
            rootEntry.getFullText(null).getOrThrow(),
            rootEntry.getAuthString("Hash")!!
        )

        val keys = serverCard!!.chain(initialOSPair, 365).getOrThrow()
        keys as OrgKeySet
        val newEntry = serverCard!!.entries[1]

        serverPrimary = keys.getSPair()
        serverEncryption = keys.getEPair()

        db!!.execute(
            "INSERT INTO keycards(owner,creationtime,index,entry,fingerprint) " +
                    "VALUES('organization',?,?,?,?);",
            newEntry.getFieldString("Timestamp")!!,
            newEntry.getFieldInteger("Index")!!,
            newEntry.getFullText(null).getOrThrow(),
            newEntry.getAuthString("Hash")!!
        )

        db!!.execute(
            "INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint) " +
                    "VALUES(?,?,?,'encrypt',?);",
            newEntry.getFieldString("Timestamp")!!,
            keys.getEPair().pubKey,
            keys.getEPair().privKey,
            keys.getEPair().pubKey.hash().getOrThrow()
        )

        db!!.execute(
            "INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint) " +
                    "VALUES(?,?,?,'sign',?);",
            newEntry.getFieldString("Timestamp")!!,
            keys.getSPair().pubKey,
            keys.getSPair().privKey,
            keys.getSPair().pubKey.hash().getOrThrow()
        )

        db!!.execute(
            "INSERT INTO orgkeys(creationtime,pubkey,privkey,purpose,fingerprint) " +
                    "VALUES(?,?,?,'altsign',?);",
            newEntry.getFieldString("Timestamp")!!,
            initialOSPair.pubKey,
            initialOSPair.privKey,
            initialOSPair.pubKey.hash().getOrThrow()
        )
        // Preregister the admin account
        db!!.execute(
            "INSERT INTO prereg(wid,uid,domain,regcode) VALUES(?,?,?,?)",
            adminProfile.wid, "admin", "example.com", adminProfile.reghash
        )

        // Forward abuse and support to admin
        db!!.execute(
            "INSERT INTO workspaces(wid,uid,domain,password,passtype,status,wtype) " +
                    "VALUES(?,'abuse','example.com','-','','active','alias')",
            adminProfile.wid
        )
        db!!.execute(
            "INSERT INTO workspaces(wid,uid,domain,password,passtype,status,wtype) " +
                    "VALUES(?,'support','example.com','-','','active','alias')",
            adminProfile.wid
        )
    }

    /**
     * Registers the admin user and the admin's first device
     */
    private fun regAdmin() {
        val db = getDB()
        finishProfileReg(db, adminProfile)
    }

    /**
     * Registers a user and their first device
     */
    private fun regUser() {
        val db = getDB()
        preregWorkspace(
            db, userProfile.wid, userProfile.uid, gServerDomain,
            userProfile.passhash
        )
        LocalFS.get().entry(MServerPath("/ ${userProfile.wid}")).makeDirectory()

        finishProfileReg(db, userProfile)
    }

    /**
     * Creates a profile using one of the prepopulated TestProfileData instances (adminProfile,
     * userProfile).
     */
    fun setupProfile(tpdata: TestProfileData) {
        with(ProfileManager) {
            scanProfiles()
            activateProfile(tpdata.profileName)?.let {
                if (it is ResourceNotFoundException) {
                    createProfile(tpdata.profileName)?.let { e -> throw e }
                    activateProfile(tpdata.profileName)?.let { e -> throw e }
                } else throw it
            }
        }

        val infoPair = Workspace.generate(
            tpdata.uid, tpdata.domain, tpdata.wid, tpdata.clientPasshash,
        ).getOrThrow().apply {
            first.status = WorkspaceStatus.Active
            first.devid = tpdata.devid
        }

        val wdao = WorkspaceDAO()

        val pwHash = getHasherForHash(tpdata.clientPasshash).getOrThrow()
        wdao.save(infoPair.first, pwHash)?.let { throw it }

        with(ProfileManager.model.activeProfile!!) {
            wid = infoPair.first.wid
            domain = infoPair.first.domain
            uid = infoPair.first.uid
        }

        UserInfoDAO().saveIdentity(
            infoPair.first.uid?.toString(), infoPair.first.wid.toString(),
            infoPair.first.domain.toString()
        )?.let { throw it }

        wdao.saveKeyData(infoPair.first.waddress, infoPair.second)?.let { throw it }
        ProfileManager.model.activeProfile!!.status = WorkspaceStatus.Active

        with(KeyDAO()) {
            saveKeypair(tpdata.waddress, tpdata.encryption, KeyPurpose.Encryption).getOrThrow()
            saveKeypair(tpdata.waddress, tpdata.crencryption, KeyPurpose.ConReqEncryption)
                .getOrThrow()
            saveKeypair(tpdata.waddress, tpdata.signing, KeyPurpose.Signing).getOrThrow()
            saveKeypair(tpdata.waddress, tpdata.crsigning, KeyPurpose.ConReqSigning).getOrThrow()
        }

        ContactDAO().initContactTables()
        LogDAO().initLogTable()
    }

    private fun finishProfileReg(db: PGConn, profData: TestProfileData) {
        val fakeInfo = CryptoString.fromString("XSALSA20:ABCDEFG1234567890")!!
        addWorkspace(
            db, profData.wid, profData.uid, gServerDomain, profData.passhash,
            "SHA3-256", "wE313+fzp8wMSWd0QYcFjP4DFyaXni9oE/CwW6oJ82E=",
            "t=2", WorkspaceStatus.Active, WorkspaceType.Individual
        )
        addDevice(
            db, profData.wid, profData.devid, profData.devpair.pubKey, fakeInfo,
            DeviceStatus.Registered
        )
        deletePrereg(db, WAddress.fromParts(profData.wid, gServerDomain))

        val localWsRoot = Paths.get(topdir.toString(), profData.wid.toString())
        localWsRoot.toFile().mkdirs()
    }

    /**
     * Sets up a keycard for the admin or the test user. Under most circumstances, calling this
     * directly is neither necessary nor desirable. It is usually called from provision(), but it
     * is available for direct calling in case the standard profile setup order is not desirable,
     * such as setting up the admin account without setting up any part of a user account.
     */
    fun setupKeycard(profile: TestProfileData) {
        val db = getDB()
        val card = profile.keycard
        with(card.current!!) {
            isDataCompliant()?.let { throw it }
            sign("Organization-Signature", serverPrimary!!)?.let { throw it }
            addAuthString(
                "Previous-Hash",
                serverCard!!.current!!.getAuthString("Hash")!!
            )?.let { throw it }
            hash()?.let { throw it }
            sign("User-Signature", profile.crsigning)?.let { throw it }
            isCompliant()?.let { throw it }
            addEntry(db, this)
        }


        val newKeys = card.chain(profile.crsigning).getOrThrow()
        newKeys as UserKeySet
        with(card.current!!) {
            sign("Organization-Signature", serverPrimary!!)?.let { throw it }
            hash()?.let { throw it }
            sign("User-Signature", profile.crsigning)?.let { throw it }
            isCompliant()?.let { throw it }
            addEntry(db, this)
        }
        assert(card.verify().getOrThrow())

        profile.data["crsigning"] = newKeys.getCRSPair()
        profile.data["crencryption"] = newKeys.getCREPair()
        profile.data["signing"] = newKeys.getSPair()
        profile.data["encryption"] = newKeys.getEPair()

        KeyDAO().saveEntryKeys(profile.waddress, newKeys)?.let { throw it }
    }

    private fun setupContactInfo(profile: TestProfileData) {
        ContactDAO().import(profile.contactInfo)?.let { throw it }
        UserInfoDAO().saveIdentity(
            profile.uid.toString(), profile.wid.toString(), profile.domain.toString()
        )?.let { throw it }
    }

    fun doConReqExchange() {
        ProfileManager.activateProfile("primary")?.let { throw it }

        // Admin sends a CR to the user

        val adminContact = UserInfoDAO().loadPublicContact().getOrThrow()
        with(Client()) {
            sendContactRequest(gUserProfileData.address, adminContact, "", false).getOrThrow()
            disconnect()?.let { throw it }
        }
        ProfileDAO().clearFirstStartup()?.let { throw it }

        // User receives the CR and sends an approval

        ProfileManager.activateProfile("user")?.let { throw it }

        MainController().let {
            getUpdates(gUserProfileData, it)

            val adminMsgCon = it.messaging()
            val adminMsgList = adminMsgCon.getMessages()
            assert(adminMsgList.last() is ConMsgHeaderItem)

            it.messaging().handleMsgSelection(null, adminMsgList.last())
            it.messaging().handleConReqAction(MessageAction.Approve)
        }
        ProfileDAO().clearFirstStartup()?.let { throw it }

        ProfileManager.activateProfile("primary")?.let { throw it }
        MainController().let {
            with(Client()) {
                getUpdates(gAdminProfileData, it)
                disconnect()?.let { e -> throw e }
            }
        }
    }

    fun getUpdates(profile: TestProfileData, proc: FilterItemProcessor) {
        val client = Client()
        client.login(profile.address).getOrThrow()
        with(IncomingHandler(client, proc, ProfileManager.model)) {
            downloadUpdates()?.let { throw it }
            sleep(100)
            executeTasks()?.let { throw it }
            sleep(900)
        }
        client.disconnect()
    }
}
