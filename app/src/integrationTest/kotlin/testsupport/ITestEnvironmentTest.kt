package testsupport

import connect.UserInfoDAO
import libmensago.MServerPath
import libmensago.resolver.KCResolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension

@ExtendWith(ApplicationExtension::class)
class ITestEnvironmentTest {
    @Test
    fun testBaseSetup() {
        ITestEnvironment("servertestenv_fs").provision(SETUP_TEST_FILESYSTEM)

        val rootPath = MServerPath()
        assert(rootPath.toHandle().exists().getOrThrow())

        val exampleIPs = KCResolver.dns
            .lookupA("example.com")
            .getOrThrow()
        assertEquals(1, exampleIPs.size)
        assertEquals("/127.0.0.1", exampleIPs[0].toString())
    }

    @Test
    fun testBaseDBSetup() {
        val env = ITestEnvironment("servertestenv_basedb")
            .provision(SETUP_TEST_DATABASE)

        // Lint removal
        // These are needed just to remove IntelliJ warnings about being private or unused because
        // most of the integration tests haven't been converted to using the new test environment
        // code yet.
        env.serverPrimaryPair()
        env.testName
        env.topdir
        env.userProfile
        env.getDB()
    }

    @Test
    fun testKeycardSetup() {
        ITestEnvironment("servertestenv_keycard")
            .provision(SETUP_TEST_BOTH_KEYCARDS)
    }

    @Test
    fun testLogin() {
        with(ITestEnvironment("servertestenv_login")) {
            provision(SETUP_TEST_ADMIN_KEYCARD)
            login(this.adminProfile)
        }
    }

    @Test
    fun testConInfo() {
        with(ITestEnvironment("servertestenv_coninfo")) {
            provision(SETUP_TEST_CONTACT_INFO)
            assertEquals(
                "Mensago Administrator", UserInfoDAO().loadFormattedName().getOrThrow()
            )
        }
    }

    @Test
    fun testConReq() {
        with(ITestEnvironment("servertestenv_conreq")) {
            provision(SETUP_TEST_CONTACT_INFO)
            doConReqExchange()
        }
    }
}
