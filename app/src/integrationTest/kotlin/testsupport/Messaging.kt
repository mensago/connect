package testsupport

import libkeycard.WAddress
import libmensago.ContentFormat
import libmensago.Message

fun makeTestMsg(): Message {
    return Message(gUserProfileData.waddress, gAdminProfileData.waddress, ContentFormat.Text)
        .withSubject("Test Message Subject")
        .withBody("Test Message Body")
        .addCC(WAddress.fromString("714d66b1-8c0f-4763-a772-ef9acd1f1dee/example.com")!!)
        .addBCC(WAddress.fromString("22280e4b-e297-4848-b274-5257664e2c67/example.com")!!)
}
