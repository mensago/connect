package libmensago

import kotlinx.serialization.json.Json
import libkeycard.WAddress
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import testsupport.ITestEnvironment
import testsupport.SETUP_TEST_DATABASE
import utilities.toPath
import java.io.File
import java.nio.file.Paths
import java.time.Instant
import java.util.*

class EnvelopeTest {
    private val oneAddr = WAddress.fromString(
        "11111111-1111-1111-1111-111111111111/example.com"
    )!!

    @Test
    fun sealOpenTest() {
        val env = ITestEnvironment("envelope_sealopen").provision(SETUP_TEST_DATABASE)

        val msg = Message(env.adminProfile.waddress, oneAddr, ContentFormat.Text)
            .withSubject("Test Message")
            .withBody("This message is just a test")

        val sealed = Envelope.seal(
            env.adminProfile.encryption, msg.getAddressPair(), msg.toPayload().getOrThrow()
        ).getOrThrow()

        val filelength = sealed.toStringResult().getOrThrow().length
        val filePath = listOf(
            env.testPath,
            "${Instant.now().epochSecond}.$filelength.${UUID.randomUUID().toString().lowercase()}"
        ).toPath().toString()
        sealed.saveWithName(filePath)?.let { throw it }

        val loaded = Envelope.fromFile(File(filePath)).getOrThrow()

        loaded.tag.decryptSender(env.serverEncryptionPair()).getOrThrow()
        loaded.tag.decryptReceiver(env.serverEncryptionPair()).getOrThrow()

        val payload = loaded.open(env.adminProfile.encryption).getOrThrow()
        val opened = Json.decodeFromString<Message>(payload.jsondata)
        assertEquals(opened.subject, "Test Message")
        assertEquals(opened.body, "This message is just a test")
        assertEquals(opened.from, env.adminProfile.waddress)
        assertEquals(opened.to, oneAddr)

        env.done()
    }

    @Test
    fun sealOpenNoteTest() {
        val env = ITestEnvironment("envelope_notesealopen").provision(SETUP_TEST_DATABASE)

        val note = Note("Test Note A", "This is some test content", ContentFormat.Text)
        val sealed = Envelope.seal(
            env.adminProfile.encryption, null, note.toPayload().getOrThrow()
        ).getOrThrow()

        val filePath = Paths.get(
            env.testPath,
            makeEnvelopeFilename(sealed.toStringResult().getOrThrow().length)
        )
        sealed.saveWithName(filePath.toString())?.let { throw it }

        val loaded = Envelope.fromFile(filePath.toFile()).getOrThrow()

        assertNull(loaded.tag.receiver)
        assertNull(loaded.tag.sender)

        val payload = loaded.open(env.adminProfile.encryption).getOrThrow()
        val opened = Json.decodeFromString<Note>(payload.jsondata)

        assertEquals(note.id, opened.id)
        assertEquals(note.version, opened.version)
        assertEquals(note.contents, opened.contents)
        assertEquals(note.attachments, opened.attachments)
        assertEquals(note.title, opened.title)
        assertEquals(note.type, opened.type)

        env.done()
    }

}
