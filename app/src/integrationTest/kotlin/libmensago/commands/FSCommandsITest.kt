package libmensago.commands

import connect.profiles.ProfileManager
import keznacl.hashFile
import libkeycard.RandomID
import libmensago.MServerPath
import libmensago.ProtocolException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.testfx.framework.junit5.ApplicationExtension
import testsupport.*
import java.io.File
import java.lang.Thread.sleep
import java.nio.file.Paths
import java.time.Instant
import java.util.*

@ExtendWith(ApplicationExtension::class)
class FSCommandsITest {

    @Test
    fun copyTest() {
        val env = ITestEnvironment("copy").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        val subDirName = "11111111-1111-1111-1111-111111111111"
        val adminTop = ensureWorkspacePaths(
            env.serverconfig, gAdminProfileData.wid.toString(),
            listOf(subDirName)
        )
        val testFileInfo = makeTestFile(adminTop)

        copy(
            env.conn,
            "/ ${gAdminProfileData.wid} ${testFileInfo.first}",
            "/ ${gAdminProfileData.wid} $subDirName"
        ).getOrThrow()
    }

    @Test
    fun deleteTest() {
        val env = ITestEnvironment("delete").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val adminTop = ensureWorkspacePaths(env.serverconfig, gAdminProfileData.wid.toString())

        val fileList = mutableListOf<String>()
        repeat(30) {
            fileList.add(makeTestFile(adminTop).first)
        }

        delete(env.conn, fileList)?.let { throw it }
    }

    @Test
    fun downloadTest() {
        val env = ITestEnvironment("download").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val adminTop = ensureWorkspacePaths(env.serverconfig, gAdminProfileData.wid.toString())
        val serverAdminTop = "/ ${gAdminProfileData.wid}"
        val fileInfo = makeTestFile(adminTop)
        val fileServerPath = "$serverAdminTop ${fileInfo.first}"
        val fileLocalPath = Paths.get(env.profdir.toString(), "test.txt").toString()
        val bytesDown = download(
            env.conn,
            MServerPath.fromString(fileServerPath)!!,
            fileLocalPath
        ).getOrThrow()
        assertEquals(fileInfo.second.toLong(), bytesDown)
        assert(File(fileLocalPath).exists())
    }

    @Test
    fun existsTest() {
        val env = ITestEnvironment("exists").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val adminTop = ensureWorkspacePaths(env.serverconfig, gAdminProfileData.wid.toString())
        val testFileInfo = makeTestFile(adminTop)

        assert(exists(env.conn, "").isFailure)
        assert(!exists(env.conn, "/ asdf").getOrThrow())
        assert(
            exists(
                env.conn,
                "/ ${gAdminProfileData.wid} ${testFileInfo.first}"
            ).getOrThrow()
        )
    }

    @Test
    fun idleTest() {
        val env = ITestEnvironment("exists").provision(SETUP_TEST_BOTH_PROFILES)
        env.connectToServer()
        assertEquals(0, idle(env.conn, null).getOrThrow())

        env.login(gAdminProfileData)
        val newDir = MServerPath.fromString(
            "/ ${gAdminProfileData.wid} 2608ae36-2ea4-4e6c-be22-54266074649b"
        )!!
        mkdir(env.conn, newDir)?.let { throw it }
        assertEquals(1, idle(env.conn, 0).getOrThrow())
        env.conn.disconnect()
    }

    @Test
    fun listFilesTest() {
        val env = ITestEnvironment("fscmds_listfiles").provision(SETUP_TEST_ADMIN_PROFILE)
        val subDirName = "11111111-1111-1111-1111-111111111111"
        val adminTop = Paths.get(env.topdir.toString(), env.adminProfile.wid.toString()).toString()

        for (i in 10..20) {
            val filesize = 1000 + i
            val fileID = RandomID.generate()
            val testfileName = "${Instant.now().epochSecond}.$filesize.$fileID"
            makeTestFile(adminTop, testfileName, filesize)
            sleep(200)
        }

        val subDir = "$adminTop/$subDirName"
        File(subDir).mkdirs()
        for (i in 10..30) {
            val filesize = 1000 + i
            val fileID = RandomID.generate()
            val testfileName = "${Instant.now().epochSecond}.$filesize.$fileID"
            makeTestFile(subDir, testfileName, filesize)
            sleep(200)
        }

        env.connectToServer()
        val err = listfiles(env.conn).exceptionOrNull() as? ProtocolException
            ?: throw TestFailureException("listfiles failure case didn't return an error")
        assertEquals(401, err.code)

        env.login(env.adminProfile)
        val testFiles = listfiles(env.conn).getOrThrow()
        assertEquals(11, testFiles.size)
        val subFiles = listfiles(env.conn, "/ ${env.adminProfile.wid} $subDirName")
            .getOrThrow()
        assertEquals(21, subFiles.size)

        env.done()
    }

    @Test
    fun listDirsTest() {
        val env = ITestEnvironment("listdirs").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val topSubDir = "11111111-1111-1111-1111-111111111111"
        val subDirPaths = listOf(
            "$topSubDir/5a6b5183-3344-4dad-912b-6fd71c525bae",
            "$topSubDir/3a7ac305-467a-4a6c-acf2-538a2e383d89",
            "$topSubDir/5bfd9898-c0ed-47d8-9582-8cc98dc07d2a",
            "$topSubDir/a05a0ade-0eb5-424e-911f-6844a48d56d5",
            "$topSubDir/820a5e3d-127a-49bb-ada1-af19596c2a14"
        )
        ensureWorkspacePaths(
            env.serverconfig, gAdminProfileData.wid.toString(),
            subDirPaths
        )

        val mainSubDir = listdirs(env.conn).getOrThrow()
        assertEquals(9, mainSubDir.size)
        val subDirs = listdirs(
            env.conn,
            "/ ${gAdminProfileData.wid} $topSubDir"
        ).getOrThrow()
        assertEquals(5, subDirs.size)
    }

    @Test
    fun mkdirTest() {
        val env = ITestEnvironment("mkdir").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val adminTop = ensureWorkspacePaths(env.serverconfig, gAdminProfileData.wid.toString())

        val topSubDir = "11111111-1111-1111-1111-111111111111"
        val serverPath = MServerPath.fromString(
            "/ ${gAdminProfileData.wid} $topSubDir"
        )!!

        // Test to make sure that trying to create a directory with a nonexistent parent fails
        // with an error
        val badPath = MServerPath.fromString(serverPath.toString())!!
        badPath.push("22222222-2222-2222-2222-222222222222").getOrThrow()
        assertNotNull(mkdir(env.conn, badPath))

        mkdir(env.conn, serverPath)?.let { throw it }
        assert(File(Paths.get(adminTop, topSubDir).toString()).exists())

        // Now that the parent directory has been created, this previously-bad call should succeed
        assertNull(mkdir(env.conn, badPath))
    }

    @Test
    fun moveTest() {
        val env = ITestEnvironment("move").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val subDirName = "11111111-1111-1111-1111-111111111111"
        val adminTop = ensureWorkspacePaths(
            env.serverconfig, gAdminProfileData.wid.toString(),
            listOf(subDirName)
        )
        val testFileInfo = makeTestFile(adminTop)

        move(
            env.conn,
            "/ ${gAdminProfileData.wid} ${testFileInfo.first}",
            "/ ${gAdminProfileData.wid} $subDirName"
        )?.let { throw it }

        val realDest = Paths.get(adminTop, subDirName, testFileInfo.first)
        assert(File(realDest.toString()).exists())
    }

    @Test
    fun quotaInfoTest() {
        val env = ITestEnvironment("cmd_quotainfo").provision(SETUP_TEST_BOTH_PROFILES)

        env.login(gAdminProfileData)
        setQuota(env.conn, gUserProfileData.wid, 12000)?.let { throw it }

        // Make sure admin can't have a quota
        var setQuotaError = setQuota(env.conn, gAdminProfileData.wid, 100000)
        assertNotNull(setQuotaError)
        assertEquals(403, (setQuotaError as ProtocolException).code)

        // Test bad value handling
        setQuotaError = setQuota(env.conn, gUserProfileData.wid, -1)
        assertNotNull(setQuotaError)
        assertEquals(400, (setQuotaError as ProtocolException).code)

        env.logout()

        ProfileManager.activateProfile("user")?.let { throw it }
        env.login(gUserProfileData)

        // Make sure users can't get quota info for other users
        val getQuotaError = getQuotaInfo(env.conn, gAdminProfileData.wid)
        assert(getQuotaError.isFailure)
        assertEquals(403, (getQuotaError.exceptionOrNull() as ProtocolException).code)

        val userTop = MServerPath.fromString("/ ${gUserProfileData.wid}")!!
        val testFileInfo = makeTestFile(ProfileManager.profilesPath, null, 8000)

        // We don't care about the file name or info in the server side... we only care about
        // errors for this call
        val testFilePath = Paths.get(ProfileManager.profilesPath, testFileInfo.first).toString()
        upload(env.conn, testFilePath, userTop).getOrThrow()

        // This call, on the other hand, should put the account over its quota and return an error.
        val uploadResult = upload(env.conn, testFilePath, userTop)
        assert(uploadResult.isFailure)

        // We can't just call makeTestFile() because disk usage tracking on the server side depends
        // on using official channels to create files in a workspace. makeTestFile() goes completely
        // around this and so disk usage reported by the server doesn't match what's on the disk.
        // Although this might seem like a problem, it ensures that all files in the user's
        // workspace were somehow authorized by the user.
        val qInfo = getQuotaInfo(env.conn).getOrThrow()
        assertEquals(8000, qInfo.first)
        assertEquals(12000, qInfo.second)

        env.logout()
    }

    @Test
    fun replaceTest() {
        // This test case is a little different from the others in that the test precisely
        // duplicates a real-world test case. During setup we create a file in the user's new
        // directory, simulating a new message that has come in from outside. We assume that the
        // client has already downloaded the item, has processed it, and is uploading the
        // processed version to replace the unprocessed one.
        val env = ITestEnvironment("replace").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)

        val mailDirID = UUID.randomUUID().toString()
        val adminTopServer = "/ ${gAdminProfileData.wid}"
        val adminTopActual = ensureWorkspacePaths(
            env.serverconfig, gAdminProfileData.wid.toString(),
            listOf("new", mailDirID)
        )
        val newDirActual = Paths.get(adminTopActual, "new").toString()
        val mailDirServer = MServerPath.fromString("$adminTopServer $mailDirID")!!
        val mailDirActual = Paths.get(adminTopActual, mailDirID).toString()

        val unprocessedFileInfo = makeTestFile(newDirActual)
        val unprocessedFileServer = MServerPath.fromString(
            "$adminTopServer new ${unprocessedFileInfo.first}"
        )!!
        val unprocessedFileActual = Paths.get(newDirActual, unprocessedFileInfo.first).toString()
        val processedFileInfo = makeTestFile(env.profdir.toString())
        val processedFileActual =
            Paths.get(env.profdir.toString(), processedFileInfo.first).toString()

        val replaceInfo = upload(
            env.conn, processedFileActual, mailDirServer,
            unprocessedFileServer
        ).getOrThrow()

        val newFileActual = Paths.get(mailDirActual, replaceInfo.first).toString()
        assert(File(newFileActual).exists())
        assert(!File(unprocessedFileActual).exists())
        assertEquals(processedFileInfo.second.toLong(), replaceInfo.second)
    }

    @Test
    fun rmdirTest() {
        val env = ITestEnvironment("rmdir").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val adminTop = ensureWorkspacePaths(env.serverconfig, gAdminProfileData.wid.toString())

        val topSubDir = "11111111-1111-1111-1111-111111111111"
        val serverPath = MServerPath.fromString(
            "/ ${gAdminProfileData.wid} $topSubDir"
        )!!
        mkdir(env.conn, serverPath)?.let { throw it }
        val testDir = File(Paths.get(adminTop, topSubDir).toString())
        assert(testDir.exists())
        rmdir(env.conn, serverPath)?.let { throw it }
        assert(!testDir.exists())
    }

    @Test
    fun selectTest() {
        val env = ITestEnvironment("select").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        ensureWorkspacePaths(env.serverconfig, gAdminProfileData.wid.toString())
        val topSubDir = "11111111-1111-1111-1111-111111111111"
        val subDirPath = MServerPath.fromString(
            "/ ${gAdminProfileData.wid} $topSubDir"
        )!!

        assertNotNull(select(env.conn, subDirPath))

        mkdir(env.conn, subDirPath)?.let { throw it }

        assertNull(select(env.conn, subDirPath))
    }

    @Test
    fun uploadTest() {
        val env = ITestEnvironment("upload").provision(SETUP_TEST_ADMIN_PROFILE)
        env.login(gAdminProfileData)
        resetWorkspaceDir(env.serverconfig)
        val adminTop = ensureWorkspacePaths(env.serverconfig, gAdminProfileData.wid.toString())
        val serverAdminTop = "/ ${gAdminProfileData.wid}"
        val fileInfo = makeTestFile(env.profdir.toString(), fileSize = 150000)

        val clientSidePath = Paths.get(env.profdir.toString(), fileInfo.first).toString()
        val uploadInfo = upload(
            env.conn, clientSidePath, MServerPath.fromString(serverAdminTop)!!
        ).getOrThrow()
        assertEquals(fileInfo.second.toLong(), uploadInfo.second)

        val localPathForUploaded = Paths.get(adminTop, uploadInfo.first).toString()
        assert(File(localPathForUploaded).exists())

        val serverHash = hashFile(localPathForUploaded).getOrThrow()
        val clientHash = hashFile(clientSidePath).getOrThrow()
        assertEquals(serverHash, clientHash)
    }
}

