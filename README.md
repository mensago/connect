# Mensago Connect

Connect is an open source organizer for your daily life for Windows, Mac, and Linux. It leverages the Mensago communications platform, making it end-to-end encrypted, cloud synchronized, and cross-platform, and it is licensed under the Mozilla Public License 2.0. What's the big deal? Read on.

## Description

Imagine for yourself if Apple had made Outlook today instead of Microsoft back in the late 1990s. Now imagine that it was also made as a cross-platform, open source program and released for free -- an utterly not-Apple move. Finally, imagine that it gave you access to a new world where e-mail is actually pretty safe, there is no spam, and booking appointments can be super easy. This is the very-real world of Mensago Connect.

Why bother? Millions of people deserve better than the dumpster fire that is e-mail. Business users deserve something better than Microsoft Outlook. Thousands of sysadmins deserve way better than the pile of manure that Microsoft Exchange is. [Mensago](https://mensago.org) changes this, and Connect is the way to it.

## Project Status

Connect is in early development, but if you're interested in building a much safer future, this is the place to be.

## Getting Involved

We need help in all sorts of places and have with a variety of skillsets. Connect is itself a desktop app written in Kotlin and built with [JavaFX](https://openjfx.io/). Please contact [Jon Yoder](mailto:jon@yoder.cloud) if you'd like to pitch in.

## Project Roadmap

Connect will consist of a number of different modes (for lack of a better term):

- Dashboard -- your day at a glance
- Messages -- Mensago's E2EE e-mail workalike
- Address Book
- Appointments
- Tasks
- News feeds
- Notes

The notetaking component will come first, followed by the address book, and then messaging. Priorities after messaging have yet to be determined.
